﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MDOclass.AccountScenarios
{
   public class Pages
    {
        public const string bronzePackage   = "http://myvirtualinventory.com";   //Ссылки на продакшен
        public const string silverPackage   = "http://combatsilver.myvirtualinventory.com";
        public const string goldPackage     = "http://combatgold.myvirtualinventory.com";
        
        public const string Portal          = "http://portal.mydealeronline.com/";
        public const string Backoffice      = "http://backoffice.mydealeronline.com/";

        public const string silverStPackage = "http://combatsilver.st.myvirtualinventory.com"; //Ссылки на стенд
        public const string goldStPackage   = "http://combatgold.st.myvirtualinventory.com";
       
        public const string PortalSt        = "http://portal.st.mydealeronline.com/";
        public const string BackofficeSt    = "http://st-backoffice.mydealeronline.com/";

        //-----------------Небоевые сайты для тестирования кнопочек, доп.страниц и прочих простых вещей
        //-----------------http:// не дописано намеренно, т.к. строки чаще используются в парсинге, чем в переходе на сайт
        public const string AdditionalPagesSite   = "othergold.myvirtualinventory.com";
        public const string AdditionalPagesSiteSt = "othergold.st.myvirtualinventory.com";
    }
}
