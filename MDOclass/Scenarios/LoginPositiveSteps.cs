﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OpenQA.Selenium;
using TechTalk.SpecFlow;
using Common;
using NUnit.Framework;

namespace MDOclass
{
    [Binding]
    public class LoginPositiveSteps : BaseSteps
    {
        [Given(@"I open homepage(.*)")]
        public void GivenIOpenHomepageHttpDiamond_Myvirtualinventory_Com(string website)
        {
            WebDriver.Manage().Cookies.DeleteAllCookies();
            WebDriver.Go(website);
            WebDriver.WaitFullLoad();
        }


        [Given(@"I click on SignIn button")]
        public void GivenIClickOnSignInButton()
        {
            WebDriver.GetById("ucToolbar_lblLogInOut").Click(); // нажатие на кнопку Sign In
        }

        [When(@"I type my login and password and sumbit them")]
        public void WhenITypeMyLoginAndPasswordAndSumbitThem()
        {
            WebDriver.GetByName("username").SendKeys("Ginger");                         // заполнение поля Login
            WebDriver.GetByName("password").SendKeys("1");                              // заполнение поля Password
            WebDriver.GetByPath("//i[@class='m-icon-swapright m-icon-white']").Click();// нажатие на кнопку Login
        }

        [Then(@"I redirected on main page")]
        public void ThenIRedirectedOnMainPage()
        {
            string expectedDestination = ("http://diamond.myvirtualinventory.com/default.aspx"); //адрес, на который идет редирект после логинации

            WebDriver.WaitFullLoad();
            Assert.True(expectedDestination.Equals(WebDriver.GetCurrentUrl()));                //проверка, что редирект прошел верно
            Assert.True(WebDriver.FindElement(By.Id("ucToolbar_imgUser")).Displayed);          // проверка отображения иконки юзера
        }
    }
}
