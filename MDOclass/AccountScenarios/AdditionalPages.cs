﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TechTalk.SpecFlow;
using Common;
using System.Threading;
using System.Windows.Forms;
using OpenQA.Selenium;
using NUnit.Framework;

namespace MDOclass.AccountScenarios
    {
    [Binding]
    public class AdditionalPages : BaseSteps
        {
        string AdditionalPageName = "QA-Automation - дополнительная тестовая страница";
        string AdditionalPageContent = "Some additional page content. Проверка кириллицы... Проверка текста... Проверка текста... Проверка текста... Проверка текста...";
        int i = 1;

        [Then(@"I enter my portal email and password for AdditionalPages account and submit them")]
        public void ThenIEnterMyPortalEmailAndPasswordForAdditionalPagesAccountAndSubmitThem()
            {
            WebDriver.GetByPath("//*[@id='CPH_txtDE']").SendKeys(MDOvariables.PortalLoginAdditionalPages);
            WebDriver.GetByPath("//*[@id='CPH_txtPW']").SendKeys(MDOvariables.PortalPasswordAdditionalPages);
            WebDriver.GetByPath("//*[@id='CPH_btnSignIn']").Click();
            }

        [Then(@"I open AdditionalPagesSite settings")]
        public void ThenIOpenAdditionalPagesSiteSettings()
            {
            if(MDOvariables.IsProductionTest==false)
            WebDriver.GetByPath("//*[contains(text(),'"+Pages.AdditionalPagesSiteSt+"')]/../../..//*[contains(text(),'Settings')]")
                .Click();
            else
            WebDriver.GetByPath("//*[contains(text(),'" + Pages.AdditionalPagesSite + "')]/../../..//*[contains(text(),'Settings')]")
            .Click();
            }

        [Then(@"I click the Customization Icon")]
        public void ThenIClickTheCustomizationIcon()
            {
            WebDriver.GetByPath("//a[contains(text(),'Customization')]").Click();
            }

        [Then(@"I switch to Customization>CustomPages")]
        public void ThenISwitchToCustomizationCustomPages()
            {
            Thread.Sleep(2000);
            WebDriver.SwitchTo().Window(WebDriver.WindowHandles.Last());
            WebDriver.GetByPath(".//*[@id='asideMenu']/ul/li[3]/a/span").Click();
            Thread.Sleep(500);
            WebDriver.GetByPath("//*[@id='asideMenu']/ul/li[3]/ul/li[4]/a").Click();
            }

        [Then(@"I add a new test custom page")]
        public void ThenIAddANewTestCustomPage()
            {
            WebDriver.GetByPath("//a[contains(text(),'+ Add New Page')]").Click();
            Thread.Sleep(500);
            }

        [Then(@"I enter information about this new page")]
        public void ThenIEnterInformationAboutThisNewPage()
            {
            WebDriver.GetByPath("//*[@id='CPH_DPCustomPages_txtPageName']").SendKeys(AdditionalPageName+" "+i.ToString());
            i++;
            WebDriver.SwitchTo().Frame(WebDriver.GetById("txtPageContentl1_ifr"));
            WebDriver.GetByPath(".//*[@id='tinymce']").SendKeys(AdditionalPageContent);
            WebDriver.SwitchTo().Window(WebDriver.WindowHandles.Last());
            }

        [Then(@"I upload a picture for this new page using the path '(.*)'")]
        public void ThenIUploadAPictureForThisNewPageUsingThePath(string p0)
            {
            WebDriver.GetByPath("//form[@id='form-txtPageContentl1']/input").Click();
            Thread.Sleep(500);
            //-------------------Загружаем картинку, путь которой передали
            Thread.Sleep(1000);
            SendKeys.SendWait(p0);
            Thread.Sleep(1000);
            SendKeys.SendWait(@"{Enter}");
            Thread.Sleep(500);
            WebDriver.GetByPath("//form[@id='form-txtPageContentl1']/../../div[2]/a").Click();
            Thread.Sleep(5000);
            }

        [Then(@"I save this new page")]
        public void ThenISaveThisNewPage()
            {
            WebDriver.GetByPath("//a[@class='custom-page-save btn btn-primary btn-sm pull-right btn-margin']").Click();
            Thread.Sleep(3000);
            }

        [Then(@"I open a new tab and switch there")]
        public void ThenIOpenANewTabAndSwitchThere()
            {
            //WebDriver.FindElement(By.XPath("//*")).SendKeys(OpenQA.Selenium.Keys.Control + "t");
            WebDriver.ExecuteJavaScript("window.open('')");
            WebDriver.SwitchTo().Window(WebDriver.WindowHandles.Last());
            }

        [Then(@"I open our AdditionalPagesWebsite")]
        public void ThenIOpenOurAdditionalPagesWebsite()
            {
            if(MDOvariables.IsProductionTest==true)
            WebDriver.Url = "http://" + Pages.AdditionalPagesSite;
            else
            WebDriver.Url = "http://" + Pages.AdditionalPagesSiteSt;
            }

        [Then(@"I open our test additional page on the website")]
        public void ThenIOpenOurTestAdditionalPageOnTheWebsite()
            {
            WebDriver.GetByPath("//*[contains(text(),'Additional Information')]").Click();
            Thread.Sleep(500);
            WebDriver.GetByPath("//*[contains(text(),'QA-Automation')]/../../li[1]/a").Click();
            }

        [Then(@"I check that all information is there")]
        public void ThenICheckThatAllInfromationIsThere()
            {
            WebDriver.Title.Contains(AdditionalPageName);
            WebDriver.GetByPath("//*[contains(text(),'" + AdditionalPageContent + "')]");
            }

        [Then(@"I switch to first tab")]
        public void ThenISwitchToTheFirstTab()
            {
            WebDriver.SwitchTo().Window(WebDriver.WindowHandles[0]);
            Thread.Sleep(1000);
            }

        [Then(@"I click the Edit button")]
        public void ThenIClickTheEditButton()
            {
            WebDriver.GetByPath("//*[@id='custom-pages-list']/div[1]/div[3]/a[1]").Click();
            Thread.Sleep(500);
            }

        [Then(@"I add some more text to the page content")]
        public void ThenIAddSomeMoreTextToThePageContent()
            {
            WebDriver.SwitchTo().Frame(WebDriver.GetById("txtPageContentl1_ifr"));
            WebDriver.GetByPath(".//*[@id='tinymce']").SendKeys("(Edit)...");
            WebDriver.SwitchTo().DefaultContent();
            }

        [Then(@"I switch to tab (.*)")]
        public void ThenISwitchToTheTab(int p0)
            {
            WebDriver.SwitchTo().Window(WebDriver.WindowHandles[p0-1]);
            }

        [Then(@"I refresh the page")]
        public void ThenIRefreshThePage()
            {
            WebDriver.Url = WebDriver.Url;
            }

        [Then(@"I check that all page content and new information is there")]
        public void ThenICheckThatAllPageContentAndNewInformationIsThere()
            {
            WebDriver.Title.Contains(AdditionalPageName);
            WebDriver.GetByPath("//*[contains(text(),'" + AdditionalPageContent +"')]");
            WebDriver.GetByPath("//*[contains(text(),'(Edit)...')]");
            }

        [Then(@"I click the Delete button and confirm page")]
        public void ThenIClickTheDeleteButtonAndConfirmPage()
            {
            WebDriver.GetByPath("//*[@id='custom-pages-list']/div[1]/div[3]/a[2]").Click();
            Thread.Sleep(500);
            WebDriver.SwitchTo().Alert().Accept();
            Thread.Sleep(500);
            }

        [Then(@"I check that I do not have any custom pages \(portal\)")]
        public void ThenICheckThatIDoNotHaveAnyCustomPagesPortal()
            {
            WebDriver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(3));
            try
                {
                WebDriver.GetByPath("//*[@id='custom-pages-list']/div[1]/div[3]/a[2]");
                Assert.Fail("There are still pages left!(((");
                }
            catch(WebDriverException)
            { }
            WebDriver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(BaseSteps.StandardTimeOut));
            }

        [Then(@"I check that the image is there")]
        public void ThenICheckThatTheImageIsThere()
            {
            WebDriver.GetByPath("//div[@class='masterContentArea']//img");
            }

        }
    }
