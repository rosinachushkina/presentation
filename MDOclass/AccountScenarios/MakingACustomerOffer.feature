﻿Feature: Placing an offer on some inventory
   As a legal customer, I would like to 
   place an offer on my liked inventory

@PlacingAnOffer
Scenario Outline: Placing an offer
	Given I open <main page> and type <phone>
	And I click on SignIn button
	When I type my login and password of a customer and submit them
	Then I should be redirected to main with my name on the top
	Then I click on search button
	Then I click on List Preview displaying
	Then I select a vehicle with the Current Offer option
	When I place a higher bid for this inventory
	Then My new bid should be placed successfully
	Then I go to the Dashboard
	Then I switch to CustomerOffers
	Then I check whether my customer_s offer is in my offers list
	Then I switch to Home
	Then I log out

	Then I wait 60 seconds

	Then I open gmail.com
	Then I enter our dealer's email and password and submit them (for GMAIL.COM)
	Then I open the last message at GMAIL.COM
	Then The new offer details should be in this email

	Then I open a new tab and switch there

	Given I open <main page> and type <phone>
	And I click on SignIn button
	When I type my login and password of a dealer and submit them
    Then I should be redirected to main with my name on the top
	Then I check whether I have a new offer from the main page
	Then I go to the Dashboard
	Then I switch to Offers
	Then I check whether a new offer is there in Offers
	Then I mark this new offer as read
	Then I switch to Reports
	Then I switch to Reports>Customer Offers
	Then I check whether this new offer is reflected there
	Then I switch to Home
	Then I log out

	Given I open main Backoffice page
	Then I type Kathryns Login and password and submit them
	Then I switch to Offers (BO)
	Then I check that my new offer is there in BO

	Examples: 
	| main page       | phone      |
	|      bronze	  | 7327572923 | 
	|      silver     |   0        |
	|       gold      |   0	       |
	