﻿Feature: Public Messages sending
 As a dealer I would like to be able to send public messages about items

@PublicMessageWithinTheItem 
Scenario Outline: Sending a public message within the item
Given I open <main page> and type <phone>
And I click on SignIn button
When I type my login and password of a dealer and submit them
Then Then I was logged in successfully to a redirected location

Then I click on search button
Then I choose <vehicleNumber>th vehicle
Then I save an item's title and ID into to variables VerificationString
When I click the Public Messages button
And I enter some text into the message window and confirm it using the Send button
Then My public message should be published and displayed below

When I click on My Dashboard
And I switch to Dashboard > Public Messages
Then I should see my new public message there

When I delete my message from Dashboard > Public Messages
Then the message should be deleted successfully

   Examples:
	| main page | phone      | vehicleNumber |
	| bronze    | 7327572923 |        1      |
	| silver    | 0          |        1      |
	| gold      | 0          |        1      |

@PublicMessageFromTheList
Scenario Outline: Sending a public message from the Item list
Given I open <main page> and type <phone>
And I click on SignIn button
When I type my login and password of a dealer and submit them
Then Then I was logged in successfully to a redirected location

Then I click on search button
Then I switch to the List view
Then I click the Add public message option on the first item
When I enter my text to the list public message window and click Save
Then My public message should be published and displayed below

Then I select a first vehicle from the list
Then I save an item's title and ID into to variables VerificationString

When I click on My Dashboard
And I switch to Dashboard > Public Messages
Then I should see my new public message there

When I delete my message from Dashboard > Public Messages
Then the message should be deleted successfully

   Examples:
	| main page | phone      | vehicleNumber |
	| bronze    | 7327572923 |        1      |
	| silver    | 0          |        1      |
	| gold      | 0          |        1      |