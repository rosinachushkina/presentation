﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TechTalk.SpecFlow;
using Common;
using NUnit.Framework;
using OpenQA.Selenium;

namespace MDOclass.AccountScenarios
    {
    [Binding]
    public class VinXXXX_MaskForNonDealers : BaseSteps
        {
        [Then(@"This vehicle VIN should have its last digits masked with '(.*)'")]
        public void ThenThisVehicleVINShouldHaveItsLastDigitsMaskedWith(string p0)
            {
            string text = WebDriver.GetByPath("//*[@id='CPH_lblVINData']").Text;
            Assert.IsTrue(text.Contains(p0) && text.IndexOf(p0) == text.Length - p0.Length);
            }

        [Then(@"This vehicles VIN should NOT have its last digits masked with '(.*)'")]
        public void ThenThisVehiclesVINShouldNOTHaveItsLastDigitsMaskedWith(string p0)
            {
            string text = WebDriver.GetByPath("//*[@id='CPH_lblVINData']").Text;
            Assert.IsFalse(text.Contains(p0));
            }

        [Then(@"This vehicle VIN should not be there")]
        public void ThenThisVehicleVINShouldNotBeThere()
            {
            try
                {
                string text = WebDriver.GetByPath("//*[@id='CPH_lblVINData']").Text;
                Assert.IsTrue(text.Length < 1);
                }
            catch(WebDriverException)
            { }
            }

        }
    }
