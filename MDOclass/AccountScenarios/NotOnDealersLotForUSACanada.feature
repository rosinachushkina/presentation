﻿Feature: NotOnDealersLotForUSACanada
	
@NotOnDealersLotForUSA
Scenario Outline: NotOnDealersLot_USA
    Given I open <webSite> and type <phone>
	Given I open the Portal main page
	Then I click the Login button
	Then I enter my portal email and password for AdditionalPages account and submit them
	Then I go to MyAccount (Portal)
	Then I switch my country to 'United States' (MyFrofile)
	
	Given I open <webSite> and type <phone>
	Then I click on a regular search button
	Then I choose 1th vehicle
	Then I check that the disclaimer "NOT ON DEALER'S LOT**" and description "NOTE: This vehicle is NOT on dealer's lot but available for purchase directly from the auction by this dealer. Contact DEALER for more information on this vehicle." are there

    Examples: 
	| webSite   | phone      |   
	| bronze    | 7327572923 |   
	| silver    | 0          |  
	| gold      | 0          |   

@NotOnDealersLotForCanada
Scenario Outline: NotOnDealersLot_Canada
    Given I open <webSite> and type <phone>
	Given I open the Portal main page
	Then I click the Login button
	Then I enter my portal email and password for AdditionalPages account and submit them
	Then I go to MyAccount (Portal)
	Then I switch my country to 'Canada' (MyFrofile)
	
	Given I open <webSite> and type <phone>
	Then I click on a regular search button
	Then I choose 1th vehicle
	Then I check that the disclaimer "NOT ON DEALER'S LOT**" and description "NOTE: This vehicle is NOT on dealer's lot but available for purchase directly from the auction by this dealer. Contact DEALER for more information on this vehicle." are there

	Given I open the Portal main page
	Then I go to MyAccount (Portal)
	Then I switch my country to 'United States' (MyFrofile)

    Examples: 
	| webSite   | phone      |   
	| bronze    | 7327572923 |   
	| silver    | 0          |  
	| gold      | 0          |   

@NotOnDealersLotFor_OtherCountries
Scenario Outline: NotOnDealersLotFor_OtherCountries
    Given I open <webSite> and type <phone>
	Given I open the Portal main page
	Then I click the Login button
	Then I enter my portal email and password for AdditionalPages account and submit them
	Then I go to MyAccount (Portal)
	Then I switch my country to NonUSCanada

	Given I open <webSite> and type <phone>
	Then I click on a regular search button
	Then I choose 1th vehicle
	Then I check that the disclaimer "NOT ON DEALER'S LOT**" and description "NOTE: This vehicle is NOT on dealer's lot but available for purchase directly from the auction by this dealer. Contact DEALER for more information on this vehicle." are NOT there

	Given I open the Portal main page
	Then I go to MyAccount (Portal)
	Then I switch my country to 'United States' (MyFrofile)

    Examples: 
	| webSite   | phone      |   
	| bronze    | 7327572923 |   
	| silver    | 0          |  
	| gold      | 0          |   