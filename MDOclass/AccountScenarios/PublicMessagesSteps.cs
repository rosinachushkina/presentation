﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TechTalk.SpecFlow;
using Common;
using NUnit.Core;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using NUnit.Framework;

namespace MDOclass.AccountScenarios
{
    [Binding]
    public class PublicMessagesSteps : BaseSteps
    {
        private string VerificationTitle, VerificationID; //Здесь будет храниться что-нибудь интересное для проверок

        private string MyMessage = 
            "0123456789gdfspogwhtphjnwgrfhojwnghojwngfhwfghjwnrgfh"; 
        //базовая строка мессаги. Будет изменена генератором по ходу пьесы



        [Then(@"I save an item's title and ID into to variables VerificationString")]
        public void ThenISaveAnItemSTitleAndIDIntoToVariablesVerificationString()
        {
            //-------------------Забираем нужные данные для сравнения (титл и айди)
            VerificationTitle = WebDriver.FindElement(By.XPath("//*[@id='CPH_lblVehicleHeader']")).Text;
            VerificationID = WebDriver.FindElement(By.XPath("//*[@id='CPH_lblInventoryNumberData2']")).Text;
        }

        [When(@"I click the Public Messages button")]
        public void WhenIClickThePublicMessagesButton()
        {
            WebDriver.FindElement(By.XPath("//*[@id='lnkGenMsgs']")).Click();
        }

        [When(@"I enter some text into the message window and confirm it using the Send button")]
        public void WhenIEnterSomeTextIntoTheMessageWindowAndConfirmItUsingTheSendButton()
        {

            //---------------Генерация случайного сообщения и отсылка
            var Handy = new HandyFunctions();
            MyMessage = Handy.RandomStringLatinAndNumbers(50);
            WebDriver.FindElement(By.XPath(" //*[@id='txtNewGenMsg']")).SendKeys(MyMessage);
           
            //---------------Подтверждаем
            WebDriver.FindElement(By.XPath("//*[@id='CPH_lblSaveGenMsgCaption']")).Click();
            
        }

        [Then(@"My public message should be published and displayed below")]
        public void ThenMyPublicMessageShouldBePublishedAndDisplayedBelow()
        {
            WebDriver.FindElement(By.XPath("//*[contains(text(),"+"'"+MyMessage+"')]"));
        }

        [When(@"I click on My Dashboard")]
        public void WhenIClickOnMyDashboard()
        {
            WebDriver.FindElement(By.XPath("//*[contains(text(),'My Dashboard')]")).Click();
            
        }

        [When(@"I switch to Dashboard > Public Messages")]
        public void WhenISwitchToDashboardPublicMessages()
        {
            WebDriver.FindElement(By.XPath(" //*[contains(text(),'Public Messages')]")).Click();
           
        }

        [Then(@"I should see my new public message there")]
        public void ThenIShouldSeeMyNewPublicMessageThere()
        {
            //--------------проверяем есть ли элемент и те ли у него параметры
            WebDriver.FindElement(By.XPath("//*[contains(text(),"+"'"+MyMessage+"')]"));
            //WebDriver.FindElement(By.XPath("//*[contains(text()," + "'" + VerificationTitle + "')]"));
            WebDriver.FindElement(By.XPath("//*[contains(text()," + "'" + VerificationID + "')]"));
            
        }

        [When(@"I delete my message from Dashboard > Public Messages")]
        public void WhenIDeleteMyMessageFromDashboardPublicMessages()
        {
            WebDriver.FindElement(By.XPath("//*[@class='item read']")).Click();         
            WebDriver.FindElement(By.XPath("//*[@class='remove-icon']")).Click();
            WebDriver.SwitchTo().Alert().Accept();
            
        }

        [Then(@"the message should be deleted successfully")]
        public void ThenTheMessageShouldBeDeletedSuccessfully()
        {
            WebDriver.FindElement(By.XPath("//*[contains(text(),'You don')]"));
            WebDriver.FindElement(By.XPath("//*[contains(text(),'t have any messages')]"));
        }

        //-------------------Отдельные методы для паблик мессаг из листа

        [Then(@"I switch to the List view")]
        public void ThenISwitchToTheListView()
        {
            WebDriver.FindElement(By.XPath("//*[@class='glyphicon glyphicon-align-justify']")).Click();
        }

        [Then(@"I click the Add public message option on the first item")]
        public void ThenIClickTheAddPublicMessageOptionOnTheFirstItem()
        {
            WebDriver.FindElement(By.XPath("//article[1]//a[contains(text(),'Add public message')]")).Click();
          
        }

        [Then(@"I select a first vehicle from the list")]
        public void ThenISelectAFirstVehicleFromTheList()
        {
            WebDriver.FindElement(By.XPath("//div[@id='searchResult']/article[1]//a[@class='btn btn-success']")).Click();
            
        }


        [When(@"I enter my text to the list public message window and click Save")]
        public void WhenIEnterMyTextToTheListPublicMessageWindowAndClickSave()
        {
         
            //---------------Генерация случайного сообщения и отсылка
            var Handy = new HandyFunctions();
            MyMessage = Handy.RandomStringLatinAndNumbers(50);

            WebDriver.FindElement(By.XPath("//div[@id='searchResult']//article[1]//input")).SendKeys(MyMessage);
            WebDriver.FindElement(By.XPath("//div[@id='searchResult']/article[1]//span[1]/a[1]")).Click();
            
        }


    }
}
