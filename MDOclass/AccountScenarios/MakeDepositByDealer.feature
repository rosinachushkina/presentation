﻿Feature: MakeDepositByDealer
	In order I as a Dealer create Deposit and check it's creation


@creatingDepositSmoke
Scenario Outline: Smoke creating Deposit 
	Given I open <main page> and type <phone>
	And I click on SignIn button
	When I type my login and password of a dealer and submit them
	Then I go to the Dashboard
	Then I switch to Customer List
	Then I click on Financial Block
	Then I click on Deposit Block
	
	Then I choose current date
	Then I send Amount in AmouuntInput
	Then I choose status Applied in StatusField
	Then I click on saving Deposit
	Then I check Deposit Tab after smoke Deposit
	
	Then I click Edit Deposit
	Then I type new amount for Deposit
	Then I choose Not Applied status for Deposit
	Then I choose next day date for Deposit
	Then I click save changes in Deposit
	Then I check Deposit Tab after smoke Deposit
	Then I delete Deposit
	
	
	Examples: 
	| main page       | phone      |
	|      bronze	  | 7327572923 | 
	|      silver     |   0        |
	|       gold      |   0	       |


@creatingDepositPositive
Scenario Outline: Create Deposit 
	Given I open <main page> and type <phone>
	And I click on SignIn button
	When I type my login and password of a dealer and submit them
	Then I go to the Dashboard

	Then I swith to Invoices
	Then I save first and second Invoice numbers

	Then I switch to Customer List
	Then I click on Financial Block
	Then I click on Deposit Block

	Then I choose current date
	Then I send Amount in AmouuntInput
	Then I choose status Applied in StatusField
	Then I click on Search Inventory Button
	Then I type Inventory number
	Then I click SearchButton in this dropdown
	Then I save inventory number of this vehicle in deposit
	Then I choose from results first Vehicle
	Then I send InvoiceNumber in Invoice input
	Then I click on saving Deposit
	Then I check current Date
	Then I check Invoice Number
	Then I check Deposit Tab
	
	Then I click Edit Deposit
	Then I type new amount for Deposit
	Then I choose Not Applied status for Deposit
	Then I choose next day date for Deposit
	Then I choose new InvoiceNumber
	Then I choose new Inventory
	Then I click save changes in Deposit
	Then I check new Invoice Number
	Then I check Deposit Tab
	Then I check tommorow Date


	Then I switch to Invoices
	Then I click Edit in second Invoice
	Then I check Deposit Tab in Invoice pop up
	Then I click close Invoice pop-up

	Then I switch to Customer List
	Then I click on Financial Block
	Then I click on Deposit Block
	Then I delete Deposit
	
	Examples: 
	| main page       | phone      |
	|      bronze	  | 7327572923 | 
	|      silver     |   0        |
	|       gold      |   0	       |
