﻿using System;
using System.Threading;
using System.Windows.Forms;
using Common;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using TechTalk.SpecFlow;

namespace MDOclass.AccountScenarios
{
    [Binding]
    public class MakeDepositByDealerSteps : BaseSteps
    {

        private string invoiceNumber;
        private string invoiceNumber2;
        private string amount = ("123");
        private string newAmount = ("321");
        private string status = ("Applied");
        private string invetory = ("acura");
        private string invetoryID;

        [Then(@"I switch to Customer List")]
        public void ThenISwitchToCustomerList()
        {
            WebDriver.GetByPath("//a[@href='/Dashboard/Customers/List']").Click();

        }

        [Then(@"I click on Financial Block")]
        public void ThenIClickOnFinancialBlock()
        {
            WebDriver.GetByPath("//a[@href='#financial']").Click();
        }

        [Then(@"I click on Deposit Block")]
        public void ThenIClickOnDepositBlock()
        {
            WebDriver.GetByPath("//a[@href='#deposit']").Click();
        }

        [Then(@"I choose current date")]
        public void ThenIChooseCurrentDate()
        {
            WebDriver.GetByPath("//input[@class='form-control form-filter']").Click();
            WebDriver.GetByPath("//td[@class='active day']").Click(); //----выбор текущей даты
        }


        [Then(@"I click on Search Inventory Button")]
        public void ThenIClickOnSearchInventoryButton()
        {
            WebDriver.GetByPath("//button[@class='btn btn-warning dropdown-toggle']/span").Click();
        }


        [Then(@"I type Inventory number")]
        public void ThenITypeInventoryNumber()
        {
            WebDriver.GetByPath("//div[@class='modal-body']//input[@class='form-control']").SendKeys(invetory);
        }

        [Then(@"I click SearchButton in this dropdown")]
        public void ThenIClickSearchButtonInThisDropdown()
        {
            WebDriver.GetByPath("//button[@id='searchBtn']").Click();
        }


        [Then(@"I choose from results first Vehicle")]
        public void ThenIChooseFromResultsFirstVehicle()
        {
            WebDriver.GetByPath("//section[@class='searchInventoryResult scroller']/div/div[1]//img").Click();
        }

        [Then(@"I send InvoiceNumber in Invoice input")]
        public void ThenISendInvoiceNumberInInvoiceInput()
        {
            WebDriver.GetByName("invoiceNumber").SendKeys(invoiceNumber);
        }

        [Then(@"I send Amount in AmouuntInput")]
        public void ThenISendAmountInAmouuntInput()
        {
            WebDriver.GetByName("amount").SendKeys(amount);
        }

        [Then(@"I choose status Applied in StatusField")]
        public void ThenISendStatusInStatusInput()
        {
            var typeselect =
                new SelectElement(WebDriver.GetByName("statusID"));
            typeselect.SelectByValue("18");

        }


        [Then(@"I click on saving Deposit")]
        public void ThenISaveMyDeposit()
        {
            Thread.Sleep(3000);
            WebDriver.GetById("btnSave").Click();
        }


        [Then(@"I delete Deposit")]
        public void ThenIDeleteDeposit()
        {
            WebDriver.GetByPath("//a[contains(text(), 'Remove')]").Click();
        }
        [Then(@"I see {{(.*)}} notification about requirement field")]
        public void ThenISeeAndNotificationAboutRequirementField(int numberRequirements)
        {
            var checkNumberRequirements =
                WebDriver.FindElements(By.XPath("//div[@class='col-lg-8 col-xs-7 form-validation-error']"));
            int checkNumberErrorNotificationSum = checkNumberRequirements.Count;

            if (numberRequirements != checkNumberErrorNotificationSum)

                Assert.Fail("Expected error notification is " + numberRequirements + "\n Actual is " + checkNumberErrorNotificationSum);
        }



        [Then(@"I check Deposit Tab after smoke Deposit")]
        public void ThenICheckDepositTabAfterSmokeDeposit()
        {
            /*---------------------------------------------------Date------------------------------------------------------------------------------
            var actualDateDepositText = WebDriver.GetByPath("//table/tbody/tr/td[1]/span[2]").Text;
            DateTime dateDeposit = DateTime.Today;
            
           DateTime actualDateDeposit = Convert.ToDateTime(actualDateDepositText);

            if (dateDeposit != actualDateDeposit)                                                                //-------сравнение
            {
                Assert.Fail("Date must be " + dateDeposit + "\n Actual is " + actualDateDeposit);
            }*/
            //--------------------------------------------------Amount-----------------------------------------------------------------------------
            Assert.True(WebDriver.GetByPath("//table/tbody/tr/td[2]/span[2][contains(text(), '" + amount + "')]").Enabled);
            //--------------------------------------------------Status-----------------------------------------------------------------------------
            Assert.True(WebDriver.GetByPath("//table/tbody/tr/td[5]/span[2][contains(text(), '" + status + "')]").Enabled);
        }

        [Then(@"I click Edit Deposit")]
        public void ThenIClickEditDeposit()
        {
            WebDriver.GetByPath("//table/tbody/tr/td[6]/a[1]").Click();
        }
        [Then(@"I type new amount for Deposit")]
        public void ThenITypeNewAmountForDeposit()
        {
            Thread.Sleep(3000);
            amount = ("321");


            WebDriver.GetByPath("//form/div/div[4]/div/input").Clear();
            WebDriver.GetByPath("//form/div/div[4]/div/input").SendKeys(amount);
        }
        [Then(@"I choose Not Applied status for Deposit")]
        public void ThenIChooseNotAppliedStatusForDeposit()
        {
            status = ("Not Applied");
            var typeselect =
               new SelectElement(WebDriver.GetByPath("//div/div[2]/form/div/div[5]/div/select"));
            typeselect.SelectByValue("19");
        }
        [Then(@"I choose next day date for Deposit")]
        public void ThenIChooseNextDayDateForDeposit()
        {
            WebDriver.GetByPath("//div[@class='modal-body invoice-modal']//input[@name='dateIssue']").Click();
            WebDriver.GetByPath("//td[@class='active day']/following-sibling::*[position()=1]").Click();
        }
        [Then(@"I click save changes in Deposit")]
        public void ThenIClickSaveChangesInDeposit()
        {
            WebDriver.GetByPath("//button[contains (text(), 'Save changes')]").Click();
        }

        [Then(@"I check current Date")]
        public void ThenICheckCurrentDate()
        {
            //---------------------------------------------------Date------------------------------------------------------------------------------
            var actualDateDepositText = WebDriver.GetByPath("//table/tbody/tr/td[1]/span[2]").Text;
            DateTime dateDeposit = DateTime.Today;

            DateTime actualDateDeposit = Convert.ToDateTime(actualDateDepositText);

            if (dateDeposit != actualDateDeposit) //-------сравнение
            {
                Assert.Fail("Date must be " + dateDeposit + "\n Actual is " + actualDateDeposit);
            }
        }


        [Then(@"I check Deposit Tab")]
        public void ThenICheckDepositTab()
        {

            //--------------------------------------------------Amount-----------------------------------------------------------------------------
                Assert.True(
                    WebDriver.GetByPath("//table/tbody/tr[1]/td[2]/span[2][contains(text(), '" + amount + "')]").Enabled);
                //--------------------------------------------------Status-----------------------------------------------------------------------------
                Assert.True(
                    WebDriver.GetByPath("//table/tbody/tr[1]/td[5]/span[2][contains(text(), '" + status + "')]").Enabled);
                //--------------------------------------------------InventoryID-----------------------------------------------------------------------
                Assert.True(
                    WebDriver.GetByPath("//table/tbody/tr[1]/td[3]/span[2][contains(text(), '" + invetoryID + "')]")
                        .Enabled);


            }
        [Then(@"I check Invoice Number")]
        public void ThenICheckInvoiceNumber()
        {
            Assert.True(
                WebDriver.GetByPath("//table/tbody/tr/td[4]/span[2][contains(text(), '" + invoiceNumber + "')]")
                    .Enabled);
        }
        [Then(@"I check new Invoice Number")]
        public void ThenICheckNewInvoiceNumber()
        {
            Assert.True(
                           WebDriver.GetByPath("//table/tbody/tr/td[4]/span[2][contains(text(), '" + invoiceNumber2 + "')]")
                               .Enabled);
        }


        [Then(@"I save inventory number of this vehicle in deposit")]
        public void ThenISaveInventoryNumberOfThisVehicleInDeposit()
        {
            invetoryID =
                WebDriver.GetByPath(
                    "//form/div[3]/div[1]/div/div/div/div/div[2]/div[2]/div/section/div/div[1]/div[2]/div[1]/span[2]")
                    .Text;
        }


        [Then(@"I choose new InvoiceNumber")]
        public void ThenIChooseNewInvoiceNumber()
        {
            WebDriver.GetByPath("//div[@class='modal-body invoice-modal']//input[@name='invoiceNumber']").Clear();
            WebDriver.GetByPath("//div[@class='modal-body invoice-modal']//input[@name='invoiceNumber']").SendKeys(invoiceNumber2);
        }


        [Then(@"I choose new Inventory")]
        public void ThenIChooseNewInventory()
        {
            invetory = "bmw";
            WebDriver.GetByPath("//div[@class='modal-body invoice-modal']//button/span[contains(text(), 'Search')]").Click();
            WebDriver.GetByPath("//div[@id='searchContainer2']//input").SendKeys(invetory);
            WebDriver.GetByPath("//button[@name='searchStr']").Click();
            invetoryID =
                WebDriver.GetByPath(
                    "//section[@class='searchInventoryResult scroller']/div/div[1]/div[2]/div[1]/span[2]").Text;
            WebDriver.GetByPath(
                    "//section[@class='searchInventoryResult scroller']/div/div[1]/div[2]/div[1]/span[2]").Click();
        }

        [Then(@"I save first and second Invoice numbers")]
        public void ThenISaveFirstInvoiceNumber()
        {
            invoiceNumber = WebDriver.GetByPath("//table//tr[1]/td[1]/span[3]").Text;
            invoiceNumber2 = WebDriver.GetByPath("//table//tr[2]/td[1]/span[3]").Text;
           
        }
        [Then(@"I check tommorow Date")]
        public void ThenICheckTommorowDate()
        {
            var actualDateDepositText = WebDriver.GetByPath("//table/tbody/tr/td[1]/span[2]").Text;
           DateTime dateDeposit = DateTime.Today.AddDays(1);
          //  string dateDeposit = DateTime.Today..ToString();

         DateTime actualDateDeposit = Convert.ToDateTime(actualDateDepositText);

            if (dateDeposit != actualDateDeposit) //-------сравнение
            {
                Assert.Fail("Date must be " + dateDeposit + "\n Actual is " + actualDateDeposit);
            }
        }
        [Then(@"I click Edit in second Invoice")]
        public void ThenIClickEditInSecondInvoice()
        {
            WebDriver.GetByPath("//table//tr[2]/td[10]/a[1]/span[2]").Click();
        }

        [Then(@"I check Deposit Tab in Invoice pop up")]
        public void ThenICheckDepositTabInInvoicePopUp()
        {
             //---------------------------------------------------Date------------------------------------------------------------------------------
            var actualDateDepositText = WebDriver.GetByPath("//h4[contains(text(), 'Deposit')]/following-sibling::*/tbody//td[1]").Text;
            DateTime dateDeposit = DateTime.Today.AddDays(1); 

            DateTime actualDateDeposit = Convert.ToDateTime(actualDateDepositText);

            if (dateDeposit != actualDateDeposit) //-------сравнение
            {
                Assert.Fail("Date must be " + dateDeposit + "\n Actual is " + actualDateDeposit);
            }
            //--------------------------------------------------Amount-----------------------------------------------------------------------------
                Assert.True(
                    WebDriver.GetByPath("//h4[contains(text(), 'Deposit')]/following-sibling::*/tbody//td[2][contains(text(), '" + amount + "')]").Enabled);
            
            //--------------------------------------------------Status-----------------------------------------------------------------------------
                Assert.True(
                    WebDriver.GetByPath("//h4[contains(text(), 'Deposit')]/following-sibling::*/tbody//td[3][contains(text(), '" + status + "')]").Enabled);
        }

        [Then(@"I click close Invoice pop-up")]
        public void ThenIClickCloseInvoicePop_Up()
        {
            WebDriver.GetByPath("//button[contains(text(), 'Close')]").Click(); 
        }

        }
    }


