﻿Feature: Registration
	I would like to be able to register on the website.
	Also I it would be great if the website could tell me when I do something wrong while performing registration.

@RegistrationPositive
Scenario Outline: Registration using correct credentials
	Given I open <main page> and type <phone>
	And I click on SignIn button
	And I click the Register button
	And I fill in some correct credentials and click Register
	Then I should be redirected to main with my name on the top
	Then I log out

	Given I open <main page> and type <phone>
	And I click on SignIn button
    When I type my login and password of a dealer and submit them
	Then I should be redirected to main with my name on the top
	Then I go to the Dashboard
	Then I switch to Reports
	Then I check whether all parameters of our new user are correct
	Then I mark this registration entry as read
	Then I switch to Home
	Then I log out


	Examples: 
	| main page       | phone      |
	|      bronze	  | 7327572923 | 
	|      silver     |   0        |
	|       gold      |   0	       |

@RegistrationNegative
Scenario Outline: Registration using incorrect credentials
	Given I open <main page> and type <phone>
	And I click on SignIn button
	And I click the Register button
	When I enter wrong <fname>,<lname>,<eml>,<phonenm>,<passwd>,<con> and click Register
	Then I should received the appropriate error message

	Examples: 
	| fname               | lname               | eml                 | phonenm             | passwd              | con                 | main page | phone      |
	| ""                  | ""                  | ""                  | lgjkl               | firstname           | firstname           | bronze    | 7327572923 |
	| ""                  | ""                  | ""                  | %$#^$!()            | firstname           | firstname           | bronze    | 7327572923 |
	| ""                  | ^&%#$               | ""                  | ""                  | firstname           | firstname           | bronze    | 7327572923 |
	| !@)(^&*$%#          |                     | ""                  | ""                  | firstname           | firstname           | bronze    | 7327572923 |
	| ""                  | ""                  | ""                  | ""                  | firstname           |                     | bronze    | 7327572923 |
	| ""                  | ""                  | ""                  | ""                  | ""                  | ""                  | bronze    | 7327572923 |
	| <script> 1 <script> | <script> 1 <script> | <script> 1 <script> | <script> 1 <script> | <script> 1 <script> | <script> 1 <script> | bronze    | 7327572923 |

    | ""                  | ""    | lgjkl | ""       | firstname | firstname | silver | 0 |
	| ""                  | ""    | ""    | %$#^$!() | firstname | firstname | silver | 0 |
	| ""                  | ^&%#$ | ""    | ""       | firstname | firstname | silver | 0 |
	| !@)(^&*$%#          | ""    | ""    | ""       | firstname | firstname | silver | 0 |
	| ""                  | ""    | ""    | ""       | firstname |           | silver | 0 |
	| ""                  | ""    | ""    | ""       | ""        | ""        | silver | 0 |
	| <script> 1 <script> | <script> 1 <script> | <script> 1 <script> | <script> 1 <script> | <script> 1 <script> | <script> 1 <script> | silver    | 0 |
	 
	| ""                  | ""    | lgjkl |          | firstname | firstname | gold | 0 |
	| ""                  | ""    | ""    | %$#^$!() | firstname | firstname | gold | 0 |
	| ""                  | ^&%#$ | ""    | ""       | firstname | firstname | gold | 0 |
	| !@)(^&*$%#          |       | ""    | ""       | firstname | firstname | gold | 0 |
	| ""                  | ""    | ""    | ""       | firstname |           | gold | 0 |
	| ""                  | ""    | ""    | ""       | ""        | ""        | gold | 0 |
	| <script> 1 <script> | <script> 1 <script> | <script> 1 <script> | <script> 1 <script> | <script> 1 <script> | <script> 1 <script> | silver    | 0 |