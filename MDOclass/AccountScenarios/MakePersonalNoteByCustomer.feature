﻿Feature: MakePersonalNoteByCustomer
	User as Customer create Personal Note from Vehicle preview page and List preview page

@personalNoteFromVehiclePage
Scenario Outline: Add Personal Note from Vehicle page
	Given I open <main page> and type <phone>
	And I click on SignIn button
	When I type my login and password of a customer and submit them
	Then I click on search button
	Then I choose 5th vehicle
	Then I type personal note and publish it
	Then I check that Personal Note was sent
	Then I save ID of this Vehicle
	Then I go to the Dashboard
	Then I switch to Personal Notes
	Then I check Personal Note and VehicleID
	Then I delete Personal Note from Dashboard

		Examples: 
	| main page       | phone      |
 	|      bronze	  | 7327572923 | 
 	|      silver     |   0        |
	|       gold      |   0	       |

	@pesonalNotefromListView
	Scenario Outline: Add Personal Note from List viewing
	Given I open <main page> and type <phone>
	And I click on SignIn button
	When I type my login and password of a customer and submit them
	Then I click on search button
	Then I click on List Preview displaying
	Then I click add Personal Note for first vehicle
	Then I type Personal Note from List preview for first vehicle
	Then I check that Personal Note was sent from Galery viewing
	Then I open this Vehicle
	Then I save ID of this Vehicle
	Then I go to the Dashboard
	Then I switch to Personal Notes
	Then I check Personal Note and VehicleID
	Then I delete Personal Note from Dashboard
	
	Examples: 
	| main page       | phone      |
 	|      bronze	  | 7327572923 | 
 	|      silver     |   0        |
	|       gold      |   0	       |