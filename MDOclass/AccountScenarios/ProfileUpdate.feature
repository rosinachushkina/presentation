﻿Feature: Profile Update
	I would like to be able to modify my profile
	and upload my photo after registration

@CustomerProfileUpdatePositive
Scenario Outline: Customer Profile Update with correct data
	Given I open <main page> and type <phone>
	And I click on SignIn button
	When I type my login and password of a customer and submit them
	Then I go to the Dashboard
	Then I go to My Profile
	Then I select my new country as a customer
	Then I select a state as a customer
	Then I enter my new correct customer information <first>,<last>,<e>,<p>,<st>,<ct>,<add1>,<add2>,<z>
	Then I upload my new photo from <photopath>
	Then I click the Save button
	Then My new customer profile information should be there

	Examples: 
	| main page | phone      | first | last | e              | p | st       | ct       | add1                | add2         | z   | photopath     |
	| bronze    | 7327572923 | test  | user | testuser@1.com | 1 | Virginia | Babruisk | Spacemarines str.17 | Chaos Square | 666 | "C:\111.jpg"  |
	| silver    | 0          | test  | user | testuser@1.com | 1 | Virginia | Babruisk | Spacemarines str.17 | Chaos Square | 666 | "C:\111.jpg"  |
	| gold      | 0          | test  | user | testuser@1.com | 1 | Virginia | Babruisk | Spacemarines str.17 | Chaos Square | 666 | "C:\111.jpg"  |

@DealerProfileUpdatePositive
Scenario Outline: Dealer Profile Update with correct data
	Given I open <main page> and type <phone>
	And I click on SignIn button
	When I type my login and password of a dealer and submit them
	Then I should be redirected to main with my name on the top
	Then I go to the Dashboard
	Then I go to My Profile
	Then I enter my new correct dealer information
	Then I click the Save button
	Then I upload my new dealer photo from <photopath>
	Then My new dealer profile information should be there

	Examples: 
	| main page | phone      |   photopath     |
	| bronze    | 7327572923 |   "C:\222.jpg"  |
	| silver    | 0          |   "C:\222.jpg"  |
	| gold      | 0          |   "C:\222.jpg"  |