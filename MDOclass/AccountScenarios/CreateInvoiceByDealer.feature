﻿Feature: CreateInvoiceByDealer
	I as a Dealer create Invoice and then as Customer check it

@createInvoiceFromDashboard
Scenario Outline: Creating Invoice from Dashboard
	Given I open <main page> and type <phone>
	And I click on SignIn button
	When I type my login and password of a dealer and submit them
	Then I go to the Dashboard
	Then I swith to Invoices

	Then I click Create Invoice button
	Then I fill Invoice Number field
	Then I choose Customer
	Then I choose status Unpaid in InvoiceStatusField
	Then I click on Shipment Date and choose current day 
	Then I choose Dirham in Currency
	Then I click Search Inventory ID
	Then I type {{Acura}} in Search input and click Search
	Then I choose from results first Vehicle
	Then I save current price of Vehicle

	Then I add Detail
	Then I type Description and Price for second detail
	Then I add Detail
	Then I type Description and Price for third detail
	Then I remove second detail
	Then I add Additional Instruction
	Then I click Save Invoice
	Then I check info in InvoiceTab by Dealer
	
	Then I click Edit Invoice
	Then I change description in Invoice pop up
	Then I add Detail
	Then I type Description and Price for third detail again
	Then I change Status of Description to Paid
	Then I change Currency to British Pound
	Then I click Save and Send to Customer
	Then I check info in InvoiceTab by Dealer again

	
	Then I click on logo
	Then I make logout

	Then I click on SignIn button
	When I type my login and password of a customer and submit them
	Then I see Notification about new Invoice
	Then I go to the Dashboard
	Then I see Notifications about new Invoices in Dashboard
	Then I switch to Invoices
	Then I see label new
	Then I click on Mark As Read
	Then I check InvoiceTab by Customer



	Examples: 
	| main page       | phone      |
	|      bronze	  | 7327572923 | 
	|      silver     |   0        |
	|       gold      |   0	       |

	@createInvoiceFromVehiclePage
	Scenario Outline: Creating Invoice from Vehicle Page
	Given I open <main page> and type <phone>
	And I click on SignIn button
	When I type my login and password of a dealer and submit them
	Then I click on search button
	Then I choose 1th vehicle
	Then I save current Description for Invoice from Vehicle page
	Then I click on Post Invoice button
	Then I choose Buyer for Invoice
	Then I fill Invoice Number field
	Then I click on Shipment Date and choose today day 
	Then I save current price of Vehicle in Invoice
	Then I fill Description and Amount and click Add in Invoice popup
	Then I fill Additional Option in Invoice popup
	Then I click Save and Send to Customer from Vehicle page
	
	Then I go to the Dashboard
	Then I switch to Invoices
	Then I click Edit Invoice
	Then I change description in Invoice pop up
	Then I add Detail
	Then I type Description and Price for third detail again
	Then I change Status of Description to Paid
	Then I change Currency to British Pound
	Then I click Save and Send to Customer
	Then I check info in InvoiceTab from Vehicle page by Dealer again
	
	Then I click on logo
	Then I make logout

	Then I click on SignIn button
	When I type my login and password of a customer and submit them
	Then I see Notification about new Invoice
	Then I go to the Dashboard
	Then I see Notifications about new Invoices in Dashboard
	Then I switch to Invoices
	Then I see label new
	Then I click on Mark As Read
	Then I check InvoiceTab from VehiclePage by Customer
	
	Examples: 
	| main page       | phone      |
	|      bronze	  | 7327572923 | 
	|      silver     |   0        |
	|       gold      |   0	       |