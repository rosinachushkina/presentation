﻿// ------------------------------------------------------------------------------
//  <auto-generated>
//      This code was generated by SpecFlow (http://www.specflow.org/).
//      SpecFlow Version:1.9.0.77
//      SpecFlow Generator Version:1.9.0.0
//      Runtime Version:4.0.30319.34014
// 
//      Changes to this file may cause incorrect behavior and will be lost if
//      the code is regenerated.
//  </auto-generated>
// ------------------------------------------------------------------------------
#region Designer generated code
#pragma warning disable
namespace MDOclass.AccountScenarios
{
    using TechTalk.SpecFlow;
    
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("TechTalk.SpecFlow", "1.9.0.77")]
    [System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    [NUnit.Framework.TestFixtureAttribute()]
    [NUnit.Framework.DescriptionAttribute("SendingMessageByCustomer")]
    public partial class SendingMessageByCustomerFeature
    {
        
        private static TechTalk.SpecFlow.ITestRunner testRunner;
        
#line 1 "SendingMessageByCustomer.feature"
#line hidden
        
        [NUnit.Framework.TestFixtureSetUpAttribute()]
        public virtual void FeatureSetup()
        {
            testRunner = TechTalk.SpecFlow.TestRunnerManager.GetTestRunner();
            TechTalk.SpecFlow.FeatureInfo featureInfo = new TechTalk.SpecFlow.FeatureInfo(new System.Globalization.CultureInfo("en-US"), "SendingMessageByCustomer", "As a customer I send message and then check it as a Dealer. Send answer and check" +
                    " it as Customer again", ProgrammingLanguage.CSharp, ((string[])(null)));
            testRunner.OnFeatureStart(featureInfo);
        }
        
        [NUnit.Framework.TestFixtureTearDownAttribute()]
        public virtual void FeatureTearDown()
        {
            testRunner.OnFeatureEnd();
            testRunner = null;
        }
        
        [NUnit.Framework.SetUpAttribute()]
        public virtual void TestInitialize()
        {
        }
        
        [NUnit.Framework.TearDownAttribute()]
        public virtual void ScenarioTearDown()
        {
            testRunner.OnScenarioEnd();
        }
        
        public virtual void ScenarioSetup(TechTalk.SpecFlow.ScenarioInfo scenarioInfo)
        {
            testRunner.OnScenarioStart(scenarioInfo);
        }
        
        public virtual void ScenarioCleanup()
        {
            testRunner.CollectScenarioErrors();
        }
        
        [NUnit.Framework.TestAttribute()]
        [NUnit.Framework.DescriptionAttribute("Sending messages from Vehicle page")]
        [NUnit.Framework.CategoryAttribute("SendingMessageFromVehiclePage")]
        [NUnit.Framework.TestCaseAttribute("bronze", "7327572923", null)]
        [NUnit.Framework.TestCaseAttribute("silver", "0", null)]
        [NUnit.Framework.TestCaseAttribute("gold", "0", null)]
        public virtual void SendingMessagesFromVehiclePage(string mainPage, string phone, string[] exampleTags)
        {
            string[] @__tags = new string[] {
                    "SendingMessageFromVehiclePage"};
            if ((exampleTags != null))
            {
                @__tags = System.Linq.Enumerable.ToArray(System.Linq.Enumerable.Concat(@__tags, exampleTags));
            }
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("Sending messages from Vehicle page", @__tags);
#line 5
this.ScenarioSetup(scenarioInfo);
#line 6
 testRunner.Given(string.Format("I open {0} and type {1}", mainPage, phone), ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line 7
 testRunner.And("I click on SignIn button", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 8
 testRunner.When("I type my login and password of a customer and submit them", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line 9
 testRunner.Then("I click on search button", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 10
 testRunner.Then("I choose 5th vehicle", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 11
 testRunner.Then("I type messages and send it", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 12
 testRunner.Then("I check that message was sent and get ID and Title", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 13
 testRunner.Then("I make logout", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 15
 testRunner.Then("I click on SignIn button", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 16
 testRunner.When("I type my login and password of a dealer and submit them", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line 17
 testRunner.Then("I check Notification in Header about message from Customer", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 18
 testRunner.Then("I go to the Dashboard", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 19
 testRunner.Then("I check Notification in Dashboard", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 20
 testRunner.Then("I open Messages", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 21
 testRunner.Then("I check message from Customer", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 22
 testRunner.Then("I type message from Dealer", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 23
 testRunner.Then("I delete message from Dashboard", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 25
 testRunner.Then("I click on logo", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 26
 testRunner.Then("I make logout", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 27
 testRunner.Then("I click on SignIn button", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 28
 testRunner.When("I type my login and password of a customer and submit them", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line 29
 testRunner.Then("I check Notification in Header about message from Dealer", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 30
 testRunner.Then("I go to the Dashboard", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 31
 testRunner.Then("I check Notification in Dashboard", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 32
 testRunner.Then("I check messsage from Dealer", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 33
 testRunner.Then("I check ID and Title", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 36
 testRunner.Then("I wait 60 seconds", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 38
 testRunner.Then("I open gmail.com", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 39
 testRunner.Then("I enter our dealer\'s email and password and submit them (for GMAIL.COM)", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 40
 testRunner.Then("I open the last message at GMAIL.COM", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 41
 testRunner.Then("I should see notification about message", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 43
 testRunner.Given(string.Format("I open {0} and type {1}", mainPage, phone), ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line 44
 testRunner.Then("I go to the Dashboard", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 45
 testRunner.Then("I type message from Customer again", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 46
 testRunner.Then("I delete message from Dashboard", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 47
 testRunner.Then("I click on logo", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 49
 testRunner.Then("I make logout", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 50
 testRunner.Then("I click on SignIn button", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 52
 testRunner.When("I type my login and password of a dealer and submit them", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line 53
 testRunner.Then("I check Notification in Header about message from Customer", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 54
 testRunner.Then("I go to the Dashboard", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 55
 testRunner.Then("I check Notification in Dashboard", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 56
 testRunner.Then("I open Messages", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 57
 testRunner.Then("I check message from Customer", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 58
 testRunner.Then("I delete message from Dashboard", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line hidden
            this.ScenarioCleanup();
        }
        
        [NUnit.Framework.TestAttribute()]
        [NUnit.Framework.DescriptionAttribute("Sending messages from Gallery List")]
        [NUnit.Framework.CategoryAttribute("SendingMessageFromGalleryListView")]
        [NUnit.Framework.TestCaseAttribute("bronze", "7327572923", null)]
        [NUnit.Framework.TestCaseAttribute("silver", "0", null)]
        [NUnit.Framework.TestCaseAttribute("gold", "0", null)]
        public virtual void SendingMessagesFromGalleryList(string mainPage, string phone, string[] exampleTags)
        {
            string[] @__tags = new string[] {
                    "SendingMessageFromGalleryListView"};
            if ((exampleTags != null))
            {
                @__tags = System.Linq.Enumerable.ToArray(System.Linq.Enumerable.Concat(@__tags, exampleTags));
            }
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("Sending messages from Gallery List", @__tags);
#line 68
 this.ScenarioSetup(scenarioInfo);
#line 69
 testRunner.Given(string.Format("I open {0} and type {1}", mainPage, phone), ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line 70
 testRunner.And("I click on SignIn button", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 71
 testRunner.When("I type my login and password of a GMAILcustomer and submit them", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line 72
 testRunner.Then("I click on search button", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 73
 testRunner.Then("I click on List Preview displaying", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 74
 testRunner.Then("I click on AskDealerAQuestion button", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 75
 testRunner.Then("I type message in this input and send it", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 76
 testRunner.Then("I check that message was sent from Gallery List and get ID and Title", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 80
 testRunner.Then("I make logout", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 81
 testRunner.Then("I click on SignIn button", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 82
 testRunner.When("I type my login and password of a dealer and submit them", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line 83
 testRunner.Then("I check Notification in Header about message from Customer", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 84
 testRunner.Then("I go to the Dashboard", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 85
 testRunner.Then("I check Notification in Dashboard", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 86
 testRunner.Then("I open Messages", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 87
 testRunner.Then("I check message from Customer", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 88
 testRunner.Then("I type message from Dealer", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 89
 testRunner.Then("I delete message from Dashboard", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 90
 testRunner.Then("I click on logo", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 91
 testRunner.Then("I make logout", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 92
 testRunner.Then("I click on SignIn button", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 93
 testRunner.When("I type my login and password of a GMAILcustomer and submit them", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line 94
 testRunner.Then("I check Notification in Header about message from Dealer", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 95
 testRunner.Then("I go to the Dashboard", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 96
 testRunner.Then("I check Notification in Dashboard", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 97
 testRunner.Then("I check messsage from Dealer", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 98
 testRunner.Then("I check ID and Title", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 99
 testRunner.Then("I delete message from Dashboard", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 101
 testRunner.Then("I wait 60 seconds", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 103
 testRunner.Then("I open gmail.com", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 104
 testRunner.Then("I enter our dealer\'s email and password and submit them (for GMAIL.COM)", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 105
 testRunner.Then("I open the last message at GMAIL.COM", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 106
 testRunner.Then("I should see notification about message", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 108
 testRunner.Then("I clean my cookies", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 109
 testRunner.Then("I open gmail.com", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 110
 testRunner.Then("I click sign in different account (gmail)", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 111
 testRunner.Then("I click add acount (gmail)", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 112
 testRunner.Then("I enter  our customer\'s email and password and submit them (for GMAIL.COM)", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 113
 testRunner.Then("I open the last message at GMAIL.COM", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 114
 testRunner.Then("I should see notification about message from dealer", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line hidden
            this.ScenarioCleanup();
        }
        
        [NUnit.Framework.TestAttribute()]
        [NUnit.Framework.DescriptionAttribute("Checking label about Invoice")]
        [NUnit.Framework.CategoryAttribute("chcekingCheckboxAboutInvoice")]
        [NUnit.Framework.TestCaseAttribute("bronze", "7327572923", null)]
        [NUnit.Framework.TestCaseAttribute("silver", "0", null)]
        [NUnit.Framework.TestCaseAttribute("gold", "0", null)]
        public virtual void CheckingLabelAboutInvoice(string mainPage, string phone, string[] exampleTags)
        {
            string[] @__tags = new string[] {
                    "chcekingCheckboxAboutInvoice"};
            if ((exampleTags != null))
            {
                @__tags = System.Linq.Enumerable.ToArray(System.Linq.Enumerable.Concat(@__tags, exampleTags));
            }
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("Checking label about Invoice", @__tags);
#line 123
 this.ScenarioSetup(scenarioInfo);
#line 124
 testRunner.Given(string.Format("I open {0} and type {1}", mainPage, phone), ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line 125
 testRunner.And("I click on SignIn button", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 126
 testRunner.When("I type my login and password of a customer and submit them", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line 127
 testRunner.Then("I click on search button", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 128
 testRunner.Then("I choose 5th vehicle", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 129
 testRunner.Then("I click on checkbox about Invoice", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 130
 testRunner.Then("I type messages and send it", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 131
 testRunner.Then("I check that message was sent and get ID and Title with invoice", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 132
 testRunner.Then("I go to the Dashboard", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 133
 testRunner.Then("I delete message from Dashboard", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 134
 testRunner.Then("I click on logo", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 135
 testRunner.Then("I make logout", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 136
 testRunner.Then("I click on SignIn button", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 138
 testRunner.When("I type my login and password of a dealer and submit them", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line 139
 testRunner.Then("I check Notification in Header about message from Customer", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 140
 testRunner.Then("I go to the Dashboard", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 141
 testRunner.Then("I check Notification in Dashboard", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 142
 testRunner.Then("I open Messages", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 143
 testRunner.Then("I check message from Customer", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 144
 testRunner.Then("I check label about Invoice", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 145
 testRunner.Then("I delete message from Dashboard", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 147
 testRunner.Then("I open Deleted Tab", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 148
 testRunner.Then("I type in search message input customer\'s message and try to find it", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 149
 testRunner.Then("I check that message was found", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line hidden
            this.ScenarioCleanup();
        }
        
        [NUnit.Framework.TestAttribute()]
        [NUnit.Framework.DescriptionAttribute("Sending message from Vehicle page by Anonymous")]
        [NUnit.Framework.CategoryAttribute("checkingSendingMessagesByAnonymous")]
        [NUnit.Framework.TestCaseAttribute("bronze", "7327572923", null)]
        [NUnit.Framework.TestCaseAttribute("silver", "0", null)]
        [NUnit.Framework.TestCaseAttribute("gold", "0", null)]
        public virtual void SendingMessageFromVehiclePageByAnonymous(string mainPage, string phone, string[] exampleTags)
        {
            string[] @__tags = new string[] {
                    "checkingSendingMessagesByAnonymous"};
            if ((exampleTags != null))
            {
                @__tags = System.Linq.Enumerable.ToArray(System.Linq.Enumerable.Concat(@__tags, exampleTags));
            }
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("Sending message from Vehicle page by Anonymous", @__tags);
#line 161
 this.ScenarioSetup(scenarioInfo);
#line 162
 testRunner.Given(string.Format("I open {0} and type {1}", mainPage, phone), ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line 164
 testRunner.Then("I click on search button", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 165
 testRunner.Then("I choose 7th vehicle", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 166
 testRunner.Then("I click on checkbox about Invoice", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 167
 testRunner.Then("I fill my Name and Email", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 168
 testRunner.Then("I type messages and send it", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 171
 testRunner.Then("I click on SignIn button", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 172
 testRunner.When("I type my login and password of a dealer and submit them", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line 173
 testRunner.Then("I check Notification in Header about message from Customer", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 174
 testRunner.Then("I go to the Dashboard", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 175
 testRunner.Then("I check Notification in Dashboard", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 176
 testRunner.Then("I open Messages", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 177
 testRunner.Then("I check message from Customer", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 178
 testRunner.Then("I check name of Anonymous", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 179
 testRunner.Then("I check email of Anonymous", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 180
 testRunner.Then("I check label about Invoice", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 181
 testRunner.Then("I delete message from Dashboard", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 183
 testRunner.Then("I wait 60 seconds", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 185
 testRunner.Then("I open gmail.com", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 186
 testRunner.Then("I enter our dealer\'s email and password and submit them (for GMAIL.COM)", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 187
 testRunner.Then("I open the last message at GMAIL.COM", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 188
 testRunner.Then("I should see notification about message from anon", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line hidden
            this.ScenarioCleanup();
        }
        
        [NUnit.Framework.TestAttribute()]
        [NUnit.Framework.DescriptionAttribute("Sending message from Search page by Anonymous")]
        [NUnit.Framework.CategoryAttribute("checkingSendingMessagesByAnonymousFromSearchPage")]
        [NUnit.Framework.TestCaseAttribute("bronze", "7327572923", null)]
        [NUnit.Framework.TestCaseAttribute("silver", "0", null)]
        [NUnit.Framework.TestCaseAttribute("gold", "0", null)]
        public virtual void SendingMessageFromSearchPageByAnonymous(string mainPage, string phone, string[] exampleTags)
        {
            string[] @__tags = new string[] {
                    "checkingSendingMessagesByAnonymousFromSearchPage"};
            if ((exampleTags != null))
            {
                @__tags = System.Linq.Enumerable.ToArray(System.Linq.Enumerable.Concat(@__tags, exampleTags));
            }
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("Sending message from Search page by Anonymous", @__tags);
#line 197
 this.ScenarioSetup(scenarioInfo);
#line 198
 testRunner.Given(string.Format("I open {0} and type {1}", mainPage, phone), ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line 200
 testRunner.Then("I click on search button", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 201
 testRunner.Then("I click on List Preview displaying", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 202
 testRunner.Then("I click on AskDealerAQuestion button", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 203
 testRunner.Then("I type message in this input and send it", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 204
 testRunner.Then("I type Name of anon and Email and agree with invoice", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 205
 testRunner.Then("I send message of anon", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 207
 testRunner.Then("I click on SignIn button", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 208
 testRunner.When("I type my login and password of a dealer and submit them", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line 209
 testRunner.Then("I check Notification in Header about message from Customer", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 210
 testRunner.Then("I go to the Dashboard", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 211
 testRunner.Then("I check Notification in Dashboard", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 212
 testRunner.Then("I open Messages", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 213
 testRunner.Then("I check message from Customer", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 214
 testRunner.Then("I check name of Anonymous", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 215
 testRunner.Then("I check email of Anonymous", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 216
 testRunner.Then("I check label about Invoice", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 217
 testRunner.Then("I delete message from Dashboard", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 219
 testRunner.Then("I wait 60 seconds", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 221
 testRunner.Then("I open gmail.com", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 222
 testRunner.Then("I enter our dealer\'s email and password and submit them (for GMAIL.COM)", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 223
 testRunner.Then("I open the last message at GMAIL.COM", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 224
 testRunner.Then("I should see notification about message from anon", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line hidden
            this.ScenarioCleanup();
        }
        
        [NUnit.Framework.TestAttribute()]
        [NUnit.Framework.DescriptionAttribute("Check cyrillic message and username search")]
        [NUnit.Framework.CategoryAttribute("CheckCyrillikMessageAndUserNameSearch")]
        [NUnit.Framework.TestCaseAttribute("bronze", "7327572923", null)]
        [NUnit.Framework.TestCaseAttribute("silver", "0", null)]
        [NUnit.Framework.TestCaseAttribute("gold", "0", null)]
        public virtual void CheckCyrillicMessageAndUsernameSearch(string mainPage, string phone, string[] exampleTags)
        {
            string[] @__tags = new string[] {
                    "CheckCyrillikMessageAndUserNameSearch"};
            if ((exampleTags != null))
            {
                @__tags = System.Linq.Enumerable.ToArray(System.Linq.Enumerable.Concat(@__tags, exampleTags));
            }
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("Check cyrillic message and username search", @__tags);
#line 234
 this.ScenarioSetup(scenarioInfo);
#line 235
 testRunner.Given(string.Format("I open {0} and type {1}", mainPage, phone), ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line 236
 testRunner.And("I click on SignIn button", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 237
 testRunner.When("I type my login and password of a customer and submit them", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line 238
 testRunner.Then("I click on search button", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 239
 testRunner.Then("I choose 5th vehicle", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 240
 testRunner.Then("I type cyryllic messages and send it", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 241
 testRunner.Then("I check that message was sent and get ID and Title", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 242
 testRunner.Then("I go to the Dashboard", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 243
 testRunner.Then("I delete message from Dashboard", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 244
 testRunner.Then("I click on logo", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 245
 testRunner.Then("I make logout", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 248
 testRunner.Then("I click on SignIn button", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 249
 testRunner.When("I type my login and password of a dealer and submit them", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line 250
 testRunner.Then("I check Notification in Header about message from Customer", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 251
 testRunner.Then("I go to the Dashboard", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 252
 testRunner.Then("I check Notification in Dashboard", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 253
 testRunner.Then("I open Messages", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 254
 testRunner.Then("I check message from Customer", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 255
 testRunner.Then("I delete message from Dashboard", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 257
 testRunner.Then("I open Deleted Tab", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 258
 testRunner.Then("I type in search message input customer\'s message and try to find it", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 259
 testRunner.Then("I check that message was found", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 261
 testRunner.Then("I type customer name in search input", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 262
 testRunner.Then("I check that customer name is displayed", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line hidden
            this.ScenarioCleanup();
        }
    }
}
#pragma warning restore
#endregion
