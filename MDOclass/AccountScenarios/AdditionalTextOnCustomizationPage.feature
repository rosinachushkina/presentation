﻿Feature: AdditionalTextOnCustpmizationPage
	In order to check that text, which filled on Portal > Websites settings > Customization > General, displayed on site

@ChangingGeneralTextOnCustomizationPage
Scenario: Checking additional text added on Customization > General Page
	Given I open the Portal main page
	Then I click the Login button
	Then I enter my portal email and password for AdditionalPages account and submit them

	Then I open AdditionalPagesSite settings
	Then I click the Customization Icon
	Then I clear About us section and fill it
	Then I clear Footer text and fill it
	Then I clear Inventory Page and fill it
	Then I save changes of general information on customization page
	Then I click on preview site button
	Then I switch to tab 2
	Then I check about us section
	Then I check Company Name section
	Then I click on search button
	Then I choose 3th vehicle
	Then I check Inventory Page section

	
