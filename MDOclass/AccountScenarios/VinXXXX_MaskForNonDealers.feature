﻿Feature: VinXXXX_MaskForNonDealers
	
@VinXXXX_Guest_Buyer
Scenario Outline: VinXXXX_Guest_Buyer
	Given I open <webSite> and type <phone>
	Then I click on a regular search button
	Then I select the 1 vehicle from the list (Tiles View)
	Then This vehicle VIN should not be there

	Examples: 
	|    webSite      | phone      |
	|      bronze	  | 7327572923 | 
	|      silver     |   0        |
	|       gold      |   0	       |

@VinXXXX_Dealer
Scenario Outline: VinXXXX_Dealer
	Given I open <webSite> and type <phone>
	And I click on SignIn button
	When I type my login and password of a dealer and submit them
	Then I click on a regular search button
	Then I select the 1 vehicle from the list (Tiles View)
	Then This vehicles VIN should NOT have its last digits masked with 'XXXX'

	Examples: 
	|    webSite      | phone      |
	|      bronze	  | 7327572923 | 
	|      silver     |   0        |
	|       gold      |   0	       |