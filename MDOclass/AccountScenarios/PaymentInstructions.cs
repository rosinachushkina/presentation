﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TechTalk.SpecFlow;
using Common;
using System.Threading;
using OpenQA.Selenium.Support.UI;
using NUnit.Framework;
using OpenQA.Selenium;

namespace MDOclass.AccountScenarios
    {
    [Binding]
    public class PaymentInstructions : BaseSteps
        {
        string AdditionalPageContent = "Some test payments instruction. И заодно на кириллице тоже...";
        string website = "http://soundcloud.com/laegolasse";
        string payinst = "Some test payment instruction for this method...";
        List<PaymentInstructionsClass> added = new List<PaymentInstructionsClass>();

        [Then(@"I fill some information for PaymentInstructions and Save it")]
        public void ThenIFillSomeInformationForPaymentInstructionsAndSaveIt()
            {
            WebDriver.SwitchTo().Frame(WebDriver.GetById("txtWebsiter5l1_ifr"));
            WebDriver.GetByPath(".//*[@id='tinymce']").Clear();
            Thread.Sleep(200);
            WebDriver.GetByPath(".//*[@id='tinymce']").SendKeys(AdditionalPageContent);
            WebDriver.SwitchTo().DefaultContent();
            Thread.Sleep(200);
            WebDriver.GetByPath("//*[@id='btn-paymentinstruction-save']").Click();
            Thread.Sleep(2000);
            }

        [Then(@"I select '(.*)' as a Payment Currency")]
        public void ThenISelectAsAPaymentCurrency(string p0)
            {
            var selector = new SelectElement(WebDriver.GetByPath("//*[@id='ddlCurrency']"));
            selector.SelectByText(p0);
            WebDriver.GetByPath("//*[@id='btn-paymentinstruction-save']").Click();
            Thread.Sleep(2000);
            }

        [Then(@"I add a payment type '(.*)' - '(.*)'")]
        public void ThenIAddAPaymentType_(string p0, string p1)
            {
            var selector = new SelectElement(WebDriver.GetByPath("//*[@id='ddlPaymentType']"));
            selector.SelectByText(p0);
            selector = new SelectElement(WebDriver.GetByPath("//*[@id='ddlPaymentReason']"));
            selector.SelectByText(p1);

            WebDriver.GetByPath("//*[@id='tbInstruction']").SendKeys(payinst);
            WebDriver.GetByPath("//*[@id='tbLink']").SendKeys(website);
            WebDriver.GetByPath("//*[@id='divAddPaymentInstruction']/div/a").Click();
            Thread.Sleep(2000);

            var temp=new PaymentInstructionsClass();
            temp.type=p0;
            temp.reason=p1;
            temp.link = website;
            temp.instruction = payinst;
            added.Add(temp);
            }

        [Then(@"I click the SaveAll button")]
        public void ThenIClickTheSaveAllButton()
            {
            WebDriver.GetByPath("//*[@id='btn-settings-save']").Click();
            Thread.Sleep(2000);
            }

        [Then(@"I check that all entered payment types are here in the inventory")]
        public void ThenICheckThatAllEnteredPaymentTypesAreHereInTheInventory()
            {
            foreach(PaymentInstructionsClass element in added)
                {
                WebDriver.GetByPath("//div[@id='paymentInstructions']//*[contains(text(),'" + element.instruction + "')]");
                WebDriver.GetByPath("//div[@id='paymentInstructions']//*[contains(text(),'" + element.link + "')]");
                WebDriver.GetByPath("//div[@id='paymentInstructions']//*[contains(text(),'" + element.type + "')]");
                string s=WebDriver.GetByPath("//div[@id='paymentInstructions']//*[contains(text(),'" + element.type + "')]/../..").Text;
                Assert.IsTrue(s.Contains(element.reason));
                }
            }

        [Then(@"I delete my (.*) entered payment types")]
        public void ThenIDeleteMyEnteredPaymentTypes(int p0)
            {
            for(int i=1; i<=3;i++)
                {
                WebDriver.GetByPath("//table[@id='paymentInstructionTable']/tbody/tr[1]//a[2]").Click();
                WebDriver.SwitchTo().Alert().Accept();
                Thread.Sleep(1000);
                }
            }

        [Then(@"I check that there are no entered payment types")]
        public void ThenICheckThatThereAreNoEnteredPaymentTypes()
            {
            WebDriver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(3));
            try
                {
                WebDriver.GetByPath("//table[@id='paymentInstructionTable']/tbody/tr[1]//a[2]");
                Assert.Fail("There are still payment types left!(((");
                }
            catch(WebDriverException)
            { }
            WebDriver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(BaseSteps.StandardTimeOut));
            }

        }

    public class PaymentInstructionsClass
        {
        public string type;
        public string reason;
        public string link;
        public string instruction;
        }
    }
