﻿// ------------------------------------------------------------------------------
//  <auto-generated>
//      This code was generated by SpecFlow (http://www.specflow.org/).
//      SpecFlow Version:1.9.0.77
//      SpecFlow Generator Version:1.9.0.0
//      Runtime Version:4.0.30319.34014
// 
//      Changes to this file may cause incorrect behavior and will be lost if
//      the code is regenerated.
//  </auto-generated>
// ------------------------------------------------------------------------------
#region Designer generated code
#pragma warning disable
namespace MDOclass.AccountScenarios
{
    using TechTalk.SpecFlow;
    
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("TechTalk.SpecFlow", "1.9.0.77")]
    [System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    [NUnit.Framework.TestFixtureAttribute()]
    [NUnit.Framework.DescriptionAttribute("NotOnDealersLotForUSACanada")]
    public partial class NotOnDealersLotForUSACanadaFeature
    {
        
        private static TechTalk.SpecFlow.ITestRunner testRunner;
        
#line 1 "NotOnDealersLotForUSACanada.feature"
#line hidden
        
        [NUnit.Framework.TestFixtureSetUpAttribute()]
        public virtual void FeatureSetup()
        {
            testRunner = TechTalk.SpecFlow.TestRunnerManager.GetTestRunner();
            TechTalk.SpecFlow.FeatureInfo featureInfo = new TechTalk.SpecFlow.FeatureInfo(new System.Globalization.CultureInfo("en-US"), "NotOnDealersLotForUSACanada", "", ProgrammingLanguage.CSharp, ((string[])(null)));
            testRunner.OnFeatureStart(featureInfo);
        }
        
        [NUnit.Framework.TestFixtureTearDownAttribute()]
        public virtual void FeatureTearDown()
        {
            testRunner.OnFeatureEnd();
            testRunner = null;
        }
        
        [NUnit.Framework.SetUpAttribute()]
        public virtual void TestInitialize()
        {
        }
        
        [NUnit.Framework.TearDownAttribute()]
        public virtual void ScenarioTearDown()
        {
            testRunner.OnScenarioEnd();
        }
        
        public virtual void ScenarioSetup(TechTalk.SpecFlow.ScenarioInfo scenarioInfo)
        {
            testRunner.OnScenarioStart(scenarioInfo);
        }
        
        public virtual void ScenarioCleanup()
        {
            testRunner.CollectScenarioErrors();
        }
        
        [NUnit.Framework.TestAttribute()]
        [NUnit.Framework.DescriptionAttribute("NotOnDealersLot_USA")]
        [NUnit.Framework.CategoryAttribute("NotOnDealersLotForUSA")]
        [NUnit.Framework.TestCaseAttribute("bronze", "7327572923", null)]
        [NUnit.Framework.TestCaseAttribute("silver", "0", null)]
        [NUnit.Framework.TestCaseAttribute("gold", "0", null)]
        public virtual void NotOnDealersLot_USA(string webSite, string phone, string[] exampleTags)
        {
            string[] @__tags = new string[] {
                    "NotOnDealersLotForUSA"};
            if ((exampleTags != null))
            {
                @__tags = System.Linq.Enumerable.ToArray(System.Linq.Enumerable.Concat(@__tags, exampleTags));
            }
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("NotOnDealersLot_USA", @__tags);
#line 4
this.ScenarioSetup(scenarioInfo);
#line 5
    testRunner.Given(string.Format("I open {0} and type {1}", webSite, phone), ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line 6
 testRunner.Given("I open the Portal main page", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line 7
 testRunner.Then("I click the Login button", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 8
 testRunner.Then("I enter my portal email and password for AdditionalPages account and submit them", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 9
 testRunner.Then("I go to MyAccount (Portal)", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 10
 testRunner.Then("I switch my country to \'United States\' (MyFrofile)", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 12
 testRunner.Given(string.Format("I open {0} and type {1}", webSite, phone), ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line 13
 testRunner.Then("I click on a regular search button", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 14
 testRunner.Then("I choose 1th vehicle", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 15
 testRunner.Then("I check that the disclaimer \"NOT ON DEALER\'S LOT**\" and description \"NOTE: This v" +
                    "ehicle is NOT on dealer\'s lot but available for purchase directly from the aucti" +
                    "on by this dealer. Contact DEALER for more information on this vehicle.\" are the" +
                    "re", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line hidden
            this.ScenarioCleanup();
        }
        
        [NUnit.Framework.TestAttribute()]
        [NUnit.Framework.DescriptionAttribute("NotOnDealersLot_Canada")]
        [NUnit.Framework.CategoryAttribute("NotOnDealersLotForCanada")]
        [NUnit.Framework.TestCaseAttribute("bronze", "7327572923", null)]
        [NUnit.Framework.TestCaseAttribute("silver", "0", null)]
        [NUnit.Framework.TestCaseAttribute("gold", "0", null)]
        public virtual void NotOnDealersLot_Canada(string webSite, string phone, string[] exampleTags)
        {
            string[] @__tags = new string[] {
                    "NotOnDealersLotForCanada"};
            if ((exampleTags != null))
            {
                @__tags = System.Linq.Enumerable.ToArray(System.Linq.Enumerable.Concat(@__tags, exampleTags));
            }
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("NotOnDealersLot_Canada", @__tags);
#line 24
this.ScenarioSetup(scenarioInfo);
#line 25
    testRunner.Given(string.Format("I open {0} and type {1}", webSite, phone), ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line 26
 testRunner.Given("I open the Portal main page", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line 27
 testRunner.Then("I click the Login button", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 28
 testRunner.Then("I enter my portal email and password for AdditionalPages account and submit them", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 29
 testRunner.Then("I go to MyAccount (Portal)", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 30
 testRunner.Then("I switch my country to \'Canada\' (MyFrofile)", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 32
 testRunner.Given(string.Format("I open {0} and type {1}", webSite, phone), ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line 33
 testRunner.Then("I click on a regular search button", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 34
 testRunner.Then("I choose 1th vehicle", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 35
 testRunner.Then("I check that the disclaimer \"NOT ON DEALER\'S LOT**\" and description \"NOTE: This v" +
                    "ehicle is NOT on dealer\'s lot but available for purchase directly from the aucti" +
                    "on by this dealer. Contact DEALER for more information on this vehicle.\" are the" +
                    "re", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 37
 testRunner.Given("I open the Portal main page", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line 38
 testRunner.Then("I go to MyAccount (Portal)", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 39
 testRunner.Then("I switch my country to \'United States\' (MyFrofile)", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line hidden
            this.ScenarioCleanup();
        }
        
        [NUnit.Framework.TestAttribute()]
        [NUnit.Framework.DescriptionAttribute("NotOnDealersLotFor_OtherCountries")]
        [NUnit.Framework.CategoryAttribute("NotOnDealersLotFor_OtherCountries")]
        [NUnit.Framework.TestCaseAttribute("bronze", "7327572923", null)]
        [NUnit.Framework.TestCaseAttribute("silver", "0", null)]
        [NUnit.Framework.TestCaseAttribute("gold", "0", null)]
        public virtual void NotOnDealersLotFor_OtherCountries(string webSite, string phone, string[] exampleTags)
        {
            string[] @__tags = new string[] {
                    "NotOnDealersLotFor_OtherCountries"};
            if ((exampleTags != null))
            {
                @__tags = System.Linq.Enumerable.ToArray(System.Linq.Enumerable.Concat(@__tags, exampleTags));
            }
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("NotOnDealersLotFor_OtherCountries", @__tags);
#line 48
this.ScenarioSetup(scenarioInfo);
#line 49
    testRunner.Given(string.Format("I open {0} and type {1}", webSite, phone), ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line 50
 testRunner.Given("I open the Portal main page", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line 51
 testRunner.Then("I click the Login button", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 52
 testRunner.Then("I enter my portal email and password for AdditionalPages account and submit them", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 53
 testRunner.Then("I go to MyAccount (Portal)", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 54
 testRunner.Then("I switch my country to NonUSCanada", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 56
 testRunner.Given(string.Format("I open {0} and type {1}", webSite, phone), ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line 57
 testRunner.Then("I click on a regular search button", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 58
 testRunner.Then("I choose 1th vehicle", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 59
 testRunner.Then("I check that the disclaimer \"NOT ON DEALER\'S LOT**\" and description \"NOTE: This v" +
                    "ehicle is NOT on dealer\'s lot but available for purchase directly from the aucti" +
                    "on by this dealer. Contact DEALER for more information on this vehicle.\" are NOT" +
                    " there", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 61
 testRunner.Given("I open the Portal main page", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line 62
 testRunner.Then("I go to MyAccount (Portal)", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 63
 testRunner.Then("I switch my country to \'United States\' (MyFrofile)", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line hidden
            this.ScenarioCleanup();
        }
    }
}
#pragma warning restore
#endregion
