﻿using System;
using System.Threading;
using Common;
using NUnit.Framework;
using OpenQA.Selenium;
using TechTalk.SpecFlow;

namespace MDOclass.AccountScenarios
{
    [Binding]
    public class OfferTextFromPortalSteps: BaseSteps
    {
        public string offerText = "";
        private string defautlOfferText = "";

        Double MyBid; //Переменные для проверок
        private string s;
        
        [Then(@"I click on Website Settings icon")]
        public void ThenIClickOnWebsiteSettingsIcon()
        {
            WebDriver.GetByPath("//article[2]/a/img").Click();
        }

        [Then(@"I click on tab Profit Center")]
        public void ThenIClickOnTabProfitCenter()
        {
            WebDriver.GetByPath("//section[2]/div[2]/div[3]/ul/li[1]/a[contains(text(), 'Profit Center')]").Click();
        }

        [Then(@"I click on checkbox displaying offers")]
        public void ThenIClickOnCheckboxDisplayingOffers()
        {
            if (
                !WebDriver.GetByPath("//span[contains(text(), 'Add Place an Offer Button')]/preceding-sibling::input")
                    .Selected)
            WebDriver.GetByPath("//span[contains(text(), 'Add Place an Offer Button')]/preceding-sibling::input").Click();
         }

        [Then(@"I type new phrase for displaying after placing an offer")]
        public void ThenITypeNewPhraseForDisplayingAfterPlacingAnOffer()
        {
            var Handy = new HandyFunctions();
            offerText = Handy.RandomStringLatinAndNumbers(10);

            WebDriver.GetByPath("//section[2]/div[2]/div[3]/div/div[1]/div[3]/div//textarea").Click();
            WebDriver.GetByPath("//section[2]/div[2]/div[3]/div/div[1]/div[3]/div//textarea").Clear();
            WebDriver.GetByPath("//section[2]/div[2]/div[3]/div/div[1]/div[3]/div//textarea").SendKeys(offerText);
        }

        [Then(@"I save changes of Profit Center page on website settings")]
        public void ThenISaveChangesOfProfitCenterPageOnWebsiteSettings()
        {
            WebDriver.GetById("btn-settings-save").Click();
            Thread.Sleep(3000);
        }

        [Then(@"I check that text of offer is displayed")]
        public void ThenICheckThatTextOfOfferIsDisplayed()
        {
            Assert.True(WebDriver.GetByPath("//*[@id='bodyInstruction2'][contains(text(), '" + offerText + "')]").Enabled);
        }
        [When(@"I place a higher bid for this inventory with out clicking ok")]
        public void WhenIPlaceAHigherBidForThisInventoryWithOutClickingOk()
        {
            //------------------Забираем текущую цену и вычленяем саму сумму из строки
            s = WebDriver.FindElement(By.XPath("//*[@id='CPH_ucOpenInvoice_divCurrentBid']")).Text;
            s = s.Substring(1, s.Length - 5);

            //------------------Запоминаем и придумываем свою ставку
            MyBid = Convert.ToDouble(s) + 200;

            //------------------Чистим инпут и записываемся в него
            WebDriver.FindElement(By.XPath("//input[@id='txtCustomerBid']")).Clear();
            WebDriver.FindElement(By.XPath("//input[@id='txtCustomerBid']")).SendKeys(MyBid.ToString());
            WebDriver.FindElement(By.XPath("//a[@class='btn btn-primary pull-left']")).Click();
            WebDriver.FindElement(By.XPath("//a[@href='javascript:BidConfirmation()']")).Click();
        }

        [Then(@"I click Restore Default of Offer text")]
        public void ThenIClickRestoreDefaultOfOfferText()
        {
            WebDriver.GetById("offerInstructionsDefault").Click();
            Thread.Sleep(1000);
            
        }

        [Then(@"I save default text of Offer")]
        public void ThenISaveDefaultTextOfOffer()
        {
            defautlOfferText = WebDriver.GetByPath("//section[2]/div[2]/div[3]/div/div[1]/div[3]/div//textarea").Text;
        }


        [Then(@"I check that default text of offer is displayed")]
        public void ThenICheckThatDefaultTextOfOfferIsDisplayed()
        {
            Thread.Sleep(500);
            string normalText;
            string text = WebDriver.GetById("bodyInstruction2").Text;
            normalText = text;
            normalText = HandyFunctions.SubstringRemove(normalText, "\n");
            normalText = HandyFunctions.SubstringRemove(normalText, "\r");
            normalText = HandyFunctions.SubstringRemove(normalText, " ");
            defautlOfferText = HandyFunctions.SubstringRemove(normalText, " ");
            Assert.IsTrue(normalText.Contains(defautlOfferText));
        }

        [Then(@"I disable offer button")]
        public void ThenIDisableOfferButton()
        {
            WebDriver.GetByPath("//span[contains(text(), 'Add Place an Offer Button')]/preceding-sibling::input").Click();

            if (WebDriver.GetByPath("//span[contains(text(), 'Add Place an Offer Button')]/preceding-sibling::input").Selected)
                Assert.Fail("Checkbox isn't unselected");
        }
        [Then(@"I check that button of offer isn't displayed")]
        public void ThenICheckThatButtonOfOfferIsnTDisplayed()
        {
            WebDriver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(3));
            try
            {
                WebDriver.GetByPath("//a[contains(text(), 'Place an offer')]");
                Assert.Fail("Button Place an offer is displayed");
            }
            catch (WebDriverException)
            {}
            WebDriver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(BaseSteps.StandardTimeOut));
        }

    }
}
