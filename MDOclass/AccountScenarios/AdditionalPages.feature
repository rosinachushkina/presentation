﻿Feature: AdditionalPages
	
@AdditionalPages
Scenario: AdditionalPages
	Given I open the Portal main page
	Then I click the Login button
	Then I enter my portal email and password for AdditionalPages account and submit them

	Then I open AdditionalPagesSite settings
	Then I click the Customization Icon
	Then I switch to Customization>CustomPages
	Then I add a new test custom page
    Then I enter information about this new page
	Then I upload a picture for this new page using the path 'c:\111.jpg'
	Then I save this new page

	Then I add a new test custom page
    Then I enter information about this new page
	Then I upload a picture for this new page using the path 'c:\111.jpg'
	Then I save this new page

	Then I open a new tab and switch there
	Then I open our AdditionalPagesWebsite
	Then I open our test additional page on the website
	Then I check that all information is there
	Then I check that the image is there
	
	Then I switch to first tab
	Then I click the Edit button
	Then I add some more text to the page content
	Then I save this new page
	
	Then I switch to tab 2
	Then I refresh the page
	Then I check that all page content and new information is there
	Then I check that the image is there

	Then I switch to first tab
	Then I click the Delete button and confirm page 

	Then I click the Edit button
	Then I add some more text to the page content
	Then I save this new page
	
	Then I switch to first tab
    Then I click the Delete button and confirm page

	Then I check that I do not have any custom pages (portal)