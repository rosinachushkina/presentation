﻿using System;
using System.Threading;
using Common;
using NUnit.Framework;
using TechTalk.SpecFlow;

namespace MDOclass
{
    [Binding]
    public class AdditionalTextOnCustpmizationPageSteps : BaseSteps
    {
        public string addedText = "";
        [Then(@"I clear About us section and fill it")]
        public void ThenIClearAboutUsSection()
        {
            var Handy = new HandyFunctions();
            addedText = Handy.RandomStringLatinAndNumbers(10);


            WebDriver.SwitchTo().Frame(WebDriver.GetById("txtWebsiter1l1_ifr"));
            WebDriver.GetById("tinymce").Clear();
            WebDriver.GetById("tinymce").SendKeys(addedText);
            WebDriver.SwitchTo().DefaultContent();

        }
        [Then(@"I clear Footer text and fill it")]
        public void ThenIClearFooterTextAndFillIt()
        {
            WebDriver.SwitchTo().Frame(WebDriver.GetById("txtWebsiter3l1_ifr"));
            WebDriver.GetById("tinymce").Clear();
            WebDriver.GetById("tinymce").SendKeys(addedText);
            WebDriver.SwitchTo().DefaultContent();

        }

        [Then(@"I clear Inventory Page and fill it")]
        public void ThenIClearInventoryPageAndFillIt()
        {
            WebDriver.SwitchTo().Frame(WebDriver.GetById("txtWebsiter7l1_ifr"));
            WebDriver.GetById("tinymce").Clear();
            WebDriver.GetById("tinymce").SendKeys(addedText);
            WebDriver.SwitchTo().DefaultContent();
            Thread.Sleep(3000);

        }
        [Then(@"I save changes of general information on customization page")]
        public void ThenISaveChangesOfGeneralInformationOnCustomizationPage()
        {
            WebDriver.GetById("btn-wst-save").Click();
        }

        [Then(@"I check about us section")]
        public void ThenICheckAboutUsSection()
        {
            WebDriver.ExecuteJavaScript("window.scrollTo(0, 200)");
            Assert.True(WebDriver.GetByPath("//div[@id='ucFooter_footerText']/p[contains(text(), '" + addedText + "')]").Enabled);
        }

        [Then(@"I check Company Name section")]
        public void ThenICheckCompanyNameSection()
        {
            Assert.True(WebDriver.GetByPath("//span[@id='CPH_lblDealerProfileDesc']/p[contains(text(), '" + addedText + "')]").Enabled);
        }

        [Then(@"I check Inventory Page section")]
        public void ThenICheckInventoryPageSection()
        {
            Assert.True(WebDriver.GetByPath("//span[@id='CPH_lblDealerProfileInventoryDesc']/p[contains(text(), '" + addedText + "')]").Enabled);
        }

    }
}
