﻿Feature: VehicleCreation
	I would like to be able to create a new
	vehicle using my dealer's account

@VehicleCreationPositive
Scenario Outline: VehicleCreationPositive

Given I open <main page> and type <phone>
And I click on SignIn button
When I type my login and password of a dealer and submit them
Then I go to the Dashboard
Then I go to Dashboard > MyListings
Then I click the AddNew button
Then I enter all information in Specifications
Then I switch to Information
Then I enter all information in Information
Then I switch to Dealer Info
Then I enter all information in Dealer Info
Then I switch to Condition
Then I enter all information in Condition
Then I switch to Paperwork
Then I enter all information in Paperwork
Then I switch to Images and Video
Then I enter all information in Images and Video <pic>
Then I check than my vehicle image was uploaded successfully
Then I click the SaveAndPublish button
Then My new listing should be added to MyListings list

	Examples: 
	| main page | phone      |    pic              |
	| bronze    | 7327572923 |     "c:\111.jpg"    |
	| silver    | 0          |    "c:\111.jpg"     |
	| gold      | 0          |    "c:\111.jpg"     |