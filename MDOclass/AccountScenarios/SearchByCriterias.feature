﻿Feature: SearchByCriterias
	This test performs search by criterias
	and verifies its results

@SearchByID
Scenario Outline: SearchByID
	Given I open <webSite> and type <phone>
	Then I click on search button
	Then I choose <vehicleNumber>th vehicle
	Then I will remember its ID and title
	Then I switch to Home
	Then I make a search by ID
	Then I verify this inventory's ID and title with my saved ones

	Examples: 
	| webSite | phone      |   vehicleNumber     |
	| bronze    | 7327572923 |   1  |
	| silver    | 0          |   1  |
	| gold      | 0          |   1  |

@SearchByCountry
Scenario Outline: SearchByCountry
	Given I open <webSite> and type <phone>
	Then I select a random country
	Then I click on a regular search button
	Then I choose <vehicleNumber>th vehicle
	Then The country of this vehicle should be the same as requested
	
	Examples: 
	| webSite | phone        |   vehicleNumber     |
	| bronze    | 7327572923 |   1                 |
	| silver    | 0          |   1                 |
	| gold      | 0          |   1                 |

@SearchByCountryAndState
Scenario Outline: SearchByCountryAndState
	Given I open <webSite> and type <phone>
	Then I select a random country
	Then I select a random state
	Then I click on a regular search button
	Then I choose <vehicleNumber>th vehicle
	Then The country of this vehicle should be the same as requested
	Then The state of this vehicle should be the same as requested
	
	Examples: 
	| webSite | phone        |   vehicleNumber     |
	| bronze    | 7327572923 |   1                 |
	| silver    | 0          |   1                 |
	| gold      | 0          |   1                 |

@SearchByMake
Scenario Outline: SearchByMake
	Given I open <webSite> and type <phone>
	Then I select a random Make for my search
	Then I click on a regular search button
	Then I choose <vehicleNumber>th vehicle
	Then This vehicle's Make should be the same as I wanted
	
	Examples: 
	| webSite | phone        |   vehicleNumber     |
	| bronze    | 7327572923 |   1                 |
	| silver    | 0          |   1                 |
	| gold      | 0          |   1                 |

@SearchByMakeAndModel
Scenario Outline: SearchByMakeAndModel
	Given I open <webSite> and type <phone>
	Then I select a random Make for my search
	Then I select a model
	Then I click on a regular search button
	Then I choose <vehicleNumber>th vehicle
	Then This vehicle's Make should be the same as I wanted
	Then This vehicle's Model should be the same as I wanted 
	
	Examples: 
	| webSite | phone        |   vehicleNumber     |
	| bronze    | 7327572923 |   1                 |
	| silver    | 0          |   1                 |
	| gold      | 0          |   1                 |

@SearchByCountryZIP&Range
Scenario Outline: SearchByCountryZIP&Range
	Given I open <webSite> and type <phone>
	Then I select USA as a country for the fiter
	Then I select a minimal range for the filter
	Then I enter a <ZIP> code for the filter
	Then I click on a regular search button
	Then I select the <vehicleNumber> vehicle from the list (Tiles View)
	Then My vehicle's <District> and Range (if possible) should be the same as I required
	
	Examples: 
	| webSite | phone      | vehicleNumber | ZIP   | District      |
	| bronze  | 7327572923 | 1             | 20707 |     "DC,MD"   |
	| silver  | 0          | 1             | 20707 |     "DC,MD"   |
	| gold    | 0          | 1             | 20707 |     "DC,MD"   |

@SearchByBuyNow
Scenario Outline: SearchByBuyNow
	Given I open <webSite> and type <phone>
	Then I click the BuyNow filter checkbox
	Then I click on a regular search button
	Then All vehicles on the page should have the BuyNow type.
	
	Examples: 
	| webSite | phone      | 
	| bronze  | 7327572923 | 
	| silver  | 0          |          
	| gold    | 0          |            

@SearchByBrand
Scenario Outline: SearchByBrand
	Given I open <webSite> and type <phone>
	Then I select a brand from the Search By Brand list
	Then All vehicles on the page should have the selected brand.

	Examples: 
	| webSite | phone      | 
	| bronze  | 7327572923 |
	| silver  | 0          |   
	| gold    | 0          | 