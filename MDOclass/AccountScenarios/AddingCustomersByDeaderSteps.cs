﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TechTalk.SpecFlow;
using Common;
using OpenQA.Selenium.Support.UI;
using System.Threading;
using NUnit.Framework;

namespace MDOclass.AccountScenarios 
    {
    [Binding]
    public class AddingCustomersByDeaderSteps : BaseSteps
        {
        string NewCustomerEmail,NewCustomerPass;
        int TotalCustomers;

        [Then(@"I save the TotalCustomers number")]
        public void ThenISaveTheTotalCustomersNumber()
            {
            TotalCustomers = Convert.ToInt32(HandyFunctions.TrimToDigits(
                WebDriver.GetByPath("//div[@class='dashboard-stat blue']/div[2]/div[1]").Text));
            }

        [Then(@"I switch to Customers List")]
        public void ThenISwitchToCustomersList()
            {
            WebDriver.GetByPath("/html/body/div[2]/aside/ul/li[9]/a/span").Click();
            }

        [Then(@"I click the AddNewCustomer button")]
        public void ThenIClickTheAddNewCustomerButton()
            {
            WebDriver.GetByPath("/html/body/div[2]/div/div/div[2]/div/div/div/div/div[2]/div[3]/a/span[2]").Click();
            }

        [Then(@"I enter all information about my new customer and submit it")]
        public void ThenIEnterAllInformationAboutMyNewCustomerAndSubmitIt()
            {
            var H = new HandyFunctions();
            NewCustomerEmail = H.RandomStringLatinAndNumbers(8) + "@test.tsbua.com";
            NewCustomerPass = H.RandomStringLatinAndNumbers(8);

            WebDriver.GetByPath("/html/body/div[2]/div/div/div[2]/div/div/div/div/div[3]/div/div/div[2]/form/div/div/input").
                SendKeys(MDOvariables.CustomerFirstname);
            WebDriver.GetByPath("/html/body/div[2]/div/div/div[2]/div/div/div/div/div[3]/div/div/div[2]/form/div[2]/div/input").
                SendKeys(MDOvariables.CustomerLastname);
            WebDriver.GetByPath("/html/body/div[2]/div/div/div[2]/div/div/div/div/div[3]/div/div/div[2]/form/div[3]/div/input").
                SendKeys(NewCustomerEmail);
            WebDriver.GetByPath("/html/body/div[2]/div/div/div[2]/div/div/div/div/div[3]/div/div/div[2]/form/div[4]/div/input").
                SendKeys(NewCustomerPass);
            WebDriver.GetByPath("/html/body/div[2]/div/div/div[2]/div/div/div/div/div[3]/div/div/div[2]/form/div[5]/div/input").
                SendKeys(H.RandomStringNumbers(10));

            //-----------Выбираем USA
            var Selector = new SelectElement(WebDriver.GetByPath("/html/body/div[2]/div/div/div[2]/div/div/div/div/div[3]/div/div/div[2]/form/div[6]/div/select"));
            Selector.SelectByText("United States");

            Selector = new SelectElement(WebDriver.GetByPath("/html/body/div[2]/div/div/div[2]/div/div/div/div/div[3]/div/div/div[2]/form/div[7]/div/select"));
            
            //------------Рандомизируем выбор штата
            var r = new Random(Environment.TickCount);
            int i = r.Next(2, 20);
            Selector.SelectByIndex(i);

            WebDriver.GetByPath("/html/body/div[2]/div/div/div[2]/div/div/div/div/div[3]/div/div/div[2]/form/div[8]/div/input").
                SendKeys("Test City");
            WebDriver.GetByPath("/html/body/div[2]/div/div/div[2]/div/div/div/div/div[3]/div/div/div[2]/form/div[9]/div/input").
                SendKeys("Test Address1");
            WebDriver.GetByPath("/html/body/div[2]/div/div/div[2]/div/div/div/div/div[3]/div/div/div[2]/form/div[10]/div/input").
                SendKeys("Test Address2");
            WebDriver.GetByPath("/html/body/div[2]/div/div/div[2]/div/div/div/div/div[3]/div/div/div[2]/form/div[11]/div/input").
               SendKeys(H.RandomStringNumbers(5));

            WebDriver.GetByPath("/html/body/div[2]/div/div/div[2]/div/div/div/div/div[3]/div/div/div[3]/button[2]").Click();
            }

        [Then(@"I make a search by new customers ID in CustomersList")]
        public void ThenIMakeASearchByNewCustomersIDInCustomersList()
            {
            WebDriver.GetByPath("/html/body/div[2]/div/div/div[2]/div/div/div/div/div[2]/div/input").
                SendKeys(NewCustomerEmail);
            WebDriver.GetByPath("/html/body/div[2]/div/div/div[2]/div/div/div/div/div[2]/div[2]/a").Click();
            }


        [Then(@"My new customer should be successfully added to the customers list")]
        public void ThenMyNewCustomerShouldBeSuccessfullyAddedToTheCustomersList()
            {
            Thread.Sleep(2000);
            WebDriver.GetByPath("//*[contains(text(),'"+MDOvariables.CustomerFirstname+"')]");
            WebDriver.GetByPath("//*[contains(text(),'" + MDOvariables.CustomerLastname + "')]");
            WebDriver.GetByPath("//*[contains(text(),'" + NewCustomerEmail + "')]");

            //------------------Найдем его и сверим дату с сегодняшней
            WebDriver.GetByPath("//*[contains(text(),'" + NewCustomerEmail + "')]").Click();
            Thread.Sleep(2000);
            string CreationDateStr = WebDriver.GetByPath("/html/body/div[2]/div/div/div[2]/div/div/div[2]/section/div/div/div/div[2]/div/div/form/div[6]/div/input").GetAttribute("value");
            //DateTime Date = Convert.ToDateTime(CreationDateStr);
            Assert.IsTrue(CreationDateStr.Contains(DateTime.Today.Day.ToString()));
            Assert.IsTrue(CreationDateStr.Contains(DateTime.Today.Year.ToString()));
            }

        [Then(@"I switch to Statistics")]
        public void ThenISwitchToStatistics()
            {
            WebDriver.GetByPath("/html/body/div[2]/aside/ul/li[2]/a/span").Click();
            }

        [Then(@"My TotalCustomers number should be higher by (.*)")]
        public void ThenMyTotalCustomersNumberShouldBeHigherBy(int p0)
            {
            if (TotalCustomers + 1 != Convert.ToInt32(HandyFunctions.TrimToDigits(
                WebDriver.GetByPath("//div[@class='dashboard-stat blue']/div[2]/div[1]").Text)))
                Assert.Fail("Total Customers after creating another one should be " + (TotalCustomers + 1).ToString());
            }

        [Then(@"My new customer should be displayed at Reports>CustomerMasterList")]
        public void ThenMyNewCustomerShouldBeDisplayedAtReportsCustomerMasterList()
            {
            Assert.IsTrue(WebDriver.GetByPath("/html/body/div[2]/div/div/div[2]/div/div/div/div/div/div/div/div/section/div/div/table/tbody/tr/td[2]/span[2]").
                Text == MDOvariables.CustomerFirstname + " " + MDOvariables.CustomerLastname);
            Assert.IsTrue(WebDriver.GetByPath("/html/body/div[2]/div/div/div[2]/div/div/div/div/div/div/div/div/section/div/div/table/tbody/tr/td[3]/span[2]").
                Text == NewCustomerEmail);
            Assert.IsTrue(WebDriver.GetByPath("/html/body/div[2]/div/div/div[2]/div/div/div/div/div/div/div/div/section/div/div/table/tbody/tr/td[5]/span[2]").
                Text == "Active");
            }

        [Then(@"I enter my new customer credentials and submit them")]
        public void ThenIEnterMyNewCustomerCredentialsAndSubmitThem()
            {
            WebDriver.GetByName("username").SendKeys(NewCustomerEmail);    
            WebDriver.GetByName("password").SendKeys(NewCustomerPass); 
            WebDriver.GetByPath("//i[@class='m-icon-swapright m-icon-white']").Click();
            }

        [Then(@"I wait (.*) seconds")]
        public void ThenIWaitSeconds(int p0)
            {
            Thread.Sleep(p0 * 1000);
            }

        }
    }
