﻿Feature: Logination
	In order to check logination on website
	I try to login 

@loginPositive
Scenario Outline: Logination by website
	Given I open <main page> and type <phone>
	And I click on SignIn button
	When I type my login and password of a dealer and submit them
	Then Then I was logged in successfully to a redirected location
	
	Examples: 
	| main page       | phone      |
	|      bronze	  | 7327572923 | 
	|      silver     |   0        |
	|       gold      |   0	       |

@loginNegaive
Scenario Outline: Authorization using wrong credentials
	Given I open <main page> and type <phone>
	And I click on SignIn button
	When I type an INCORRECT <login> and <password>
	Then I receive the error message
	
	Examples: 
	| login                                                                                              | password                                                                   | main page | phone       |
	| ginger                                                                                             | hfsghsfg                                                                   |  bronze   | 7327572923  |
	| hsfghfsg                                                                                           | 1                                                                          |  bronze   | 7327572923  |
	|                                                                                                    |                                                                            |  bronze   | 7327572923  |
	| hwormghowrfignhpowgfjhnfsgkjhnpwruhwifgjuhnriwuthjwprfghijwfpghijwfgohijwrfogihjwfghijwogfihjgfijf | hgfh                                                                       |  bronze   | 7327572923  |
	| <script> 1 <script>                                                                                | <script> 1 <script>    |  bronze   | 7327572923  |
	
	| ginger                                                                                             | hfsghsfg                                                                   |  silver   | 0  |
	| hsfghfsg                                                                                           | 1                                                                          |  silver   | 0  |
	|                                                                                                    |                                                                            |  silver   | 0  |
	| hwormghowrfignhpowgfjhnfsgkjhnpwruhwifgjuhnriwuthjwprfghijwfpghijwfgohijwrfogihjwfghijwogfihjgfijf | hgfh                                                                       |  silver   | 0  |
	| <script> 1 <script>                                                                                | <script> 1 <script>  |  silver   | 0  |

	| ginger                                                                                             | hfsghsfg                                                                   |  gold   | 0  |
	| hsfghfsg                                                                                           | 1                                                                          |  gold   | 0  |
	|                                                                                                    |                                                                            |  gold   | 0  |
	| hwormghowrfignhpowgfjhnfsgkjhnpwruhwifgjuhnriwuthjwprfghijwfpghijwfgohijwrfogihjwfghijwogfihjgfijf | hgfh                                                                       |  gold   | 0  |
	| <script> 1 <script>                                                                                |  <script> 1 <script>   |  gold   | 0  |
	