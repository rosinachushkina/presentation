﻿Feature: AddingToWatchListByCustomer
	In order I want to check adding inventory to Watchlist

@addingToWatchListFromVehiclePagePreview
Scenario Outline: Adding Inventory to Watchlist from Vehicle page with Preview
	Given I open <main page> and type <phone>
	And I click on SignIn button
	When I type my login and password of a customer and submit them
	Then I click on clean search button
	Then I search for Vehicle with preview mode
	Then I click Add to Watchlist
	Then I go to Watchlist
	Then I check name of added Vehicle
	Then I check that current bid equals 0
	Then I delete Vehicle from Watchlist
	Then I see phrase about empty Watchlist
		Examples: 
	| main page       | phone      |
	|      bronze	  | 7327572923 | 
	|      silver     |   0        |
	|       gold      |   0	       |


	@addingToWatchListFromVehiclePagePreviewBySeller
Scenario Outline: Adding Inventory to Watchlist from Vehicle page with Preview by Dealer
	Given I open <main page> and type <phone>
	And I click on SignIn button
	When I type my login and password of a dealer and submit them
	Then I click on clean search button
	Then I search for Vehicle with preview mode
	Then I click Add to Watchlist
	Then I go to Watchlist
	Then I check name of added Vehicle
	Then I check that current bid equals 0
	Then I delete Vehicle from Watchlist
	Then I see phrase about empty Watchlist
		Examples: 
	| main page       | phone      |
	|      bronze	  | 7327572923 | 
	|      silver     |   0        |
	|       gold      |   0	       |


	@addingToWatchlistFromGalleryBuyNow
	Scenario Outline: Adding Inventory to Watchlist from Gallery viewing
	Given I open <main page> and type <phone>
	And I click on SignIn button
	When I type my login and password of a customer and submit them
	Then I click on Buy Now search button
	Then I search for Vehicle with ByuNow price
	Then I compare prices on on List Page and preview Vehicle page
	Then I click Add to Watchlist
	Then I go to Watchlist
	Then I check name of added Vehicle
	Then I check price of added Vehicle
	Then I delete Vehicle from Watchlist
	Then I see phrase about empty Watchlist


		Examples: 
	| main page       | phone      |
	|      bronze	  | 7327572923 | 
	|      silver     |   0        |
	|       gold      |   0	       |

	@addingToWatchlistFromListGalleryCurrentOffer
	Scenario Outline: Adding Inventory to Watchlist from Gallery List viewing
	Given I open <main page> and type <phone>
	And I click on SignIn button
	When I type my login and password of a customer and submit them
	Then I click on search button
	Then I click on List Preview displaying
	Then I search for Vehicle with Current Offer and add it to Watchlist
	Then I save current offer
	Then I go to Watchlist from List
	Then I check name of added Vehicle
	Then I check price of added Vehicle
	Then I delete Vehicle from Watchlist
	Then I see phrase about empty Watchlist

			Examples: 
	| main page       | phone      |
	|      bronze	  | 7327572923 | 
	|      silver     |   0        |
	|       gold      |   0	       |