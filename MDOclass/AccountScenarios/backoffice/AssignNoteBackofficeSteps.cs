﻿using System;
using System.Threading;
using Common;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using TechTalk.SpecFlow;

namespace MDOclass.AccountScenarios.backoffice
{
    [Binding]
    public class AssignNoteBackofficeSteps : BaseSteps
    {
        public string note = "";
        public int numberOfNotes;

        [Then(@"I swith to My notes tab")]
        public void ThenISwithToMyNotesTab()
        {
            WebDriver.GetByPath("//span[1][contains(text(), 'My notes')]").Click();
        }
        [Then(@"I click on bell and then open first note")]
        public void ThenIClickOnBellAndThenOpenFirstNote()
        {
            WebDriver.GetByPath("//div[2]/ul/li[1]/a/i").Click();
            WebDriver.GetByPath("//div[2]/ul/li[1]/a/i").Click();
            WebDriver.GetByPath("//ul/li[2]/div/ul/li[1]/a/span[3]").Click();
        }

        [Then(@"I fill note in backoffice")]
        public void ThenIFillNoteInBackoffice()
        {
            var Handy = new HandyFunctions();
            note = Handy.RandomStringLatinAndNumbers(5);
            WebDriver.GetByPath("//div/div/div/div[1]/div[1]/div/textarea").SendKeys(note);
        }

        [Then(@"I add this note for another Dealer")]
        public void ThenIAddThisNoteForAnotherDealer()
        {

            WebDriver.GetByPath("//button[contains(text(), 'Add note')]").Click();
            Thread.Sleep(500);
        }

        [Then(@"I assign this note to Minh")]
        public void ThenIAssignThisNoteToMinh()
        {
            WebDriver.GetByPath("//a[contains(text(), 'Assign')]").Click();
            Thread.Sleep(500);
            var dealerSelect =
            new SelectElement(WebDriver.GetByPath("//form/div[1]/select"));
            dealerSelect.SelectByText("MInh Nguyen");

            var statusSelect =
            new SelectElement(WebDriver.GetByPath("//form/div[2]/select"));
            statusSelect.SelectByText("Urgent");



            WebDriver.GetByPath("//button[2][contains(text(), 'Submit')]").Click();

        }

        [Then(@"I assign this note to Minh from CC page")]
        public void ThenIAssignThisNoteToMinhFromCCPage()
        {
            WebDriver.GetByPath("//a[contains(text(), 'Assign')]").Click();
            Thread.Sleep(500);
            var dealerSelect =
            new SelectElement(WebDriver.GetByPath("//div/div[2]/div/div[1]/select"));
            dealerSelect.SelectByText("MInh Nguyen");

            var statusSelect =
            new SelectElement(WebDriver.GetByPath("//div/div[2]/select"));
            statusSelect.SelectByText("Urgent");



            WebDriver.GetByPath("//button[2][contains(text(), 'Submit')]").Click();
        }


        
        [Then(@"I make logout from backoffice")]
        public void ThenIMakeLogoutFromBackoffice()
        {
            WebDriver.GetByPath("//div/div[2]/ul/li[2]/a/span").Click();
            WebDriver.GetByPath("//div/div[2]/ul/li[2]/a/span").Click();
            WebDriver.GetByPath("//a[@href='/Accounts/LogOff']").Click();
            Thread.Sleep(2000);
        }

        [Then(@"I switch to Profiles tab in backoffice")]
        public void ThenISwitchTpProfilesTabInBackoffice()
        {
            WebDriver.GetByPath("//a/span[1][contains(text(), 'Profiles')]").Click();
        }

        [Then(@"I choose tab Notes in dealers tab")]
        public void ThenIChooseTabNotesInDealersTab()
        {
            WebDriver.GetByPath("//ul/li[3]/a[contains(text(), 'Notes')]").Click();
        }


        [Then(@"I check notification about (.*) new note")]
        public void ThenICheckNotificationAboutNewNote(int p0)
        {
            Assert.True(WebDriver.GetByPath("//div[2]/ul/li[1]/a/span[contains(text(), '" + p0 + "')]").Enabled);
        }

        [Then(@"I open dealer notes details")]
        public void ThenIOpenDealerNotesDetails()
        {
            WebDriver.GetByPath("//div[1]/table/tbody/tr[1]/td[9]/a").Click();
        }

        [Then(@"I check that note which was sent is the same")]
        public void ThenICheckThatNoteWhichWasSentIsTheSame()
        {
            Assert.True(WebDriver.GetByPath("//table/tbody/tr/td[2][contains(text(), '" + note + "')]").Enabled);
            Assert.True(WebDriver.GetByPath("//table/tbody/tr[1]/td[6][contains(text(), 'Urgent')]").Enabled);
        }

        [Then(@"I complete this note")]
        public void ThenICompleteThisNote()
        {
            WebDriver.GetByPath("//button[1][contains(text(), 'Complete')]").Click();
            Thread.Sleep(500);
            WebDriver.GetByPath("//button[2][contains(text(), 'OK')]").Click();
        }

        [Then(@"I check that note notification disabled")]
        public void ThenICheckThatNoteNotificationDisabled()
        {
            WebDriver.Navigate().Refresh();
            Assert.True(WebDriver.GetByPath("//div[2]/ul/li[1]/a/span[contains(text(), '0')]").Enabled);
        }

        [Then(@"I switch to CC Payment History page")]
        public void ThenISwitchToCCPaymentHistoryPage()
        {
            WebDriver.GetByPath("//span[1][contains(text(), 'CC Payment History')]").Click();
        }

        [Then(@"I click details near first dealer")]
        public void ThenIClickDetailsNearFirstDealer()
        {
           WebDriver.GetByPath("//table/tbody/tr[1]/td[10]/a").Click();
        }

    }
}
