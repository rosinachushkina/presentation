﻿Feature: CheckDealerSearch
	In order to check search input in backoffice
	I type parameters 
	and try to find Kathryn

@DealerSearchChecking
Scenario: Checking Dealer's search
	Given I open main Backoffice page
	Then I type Minhs Login and password and submit them
	Then I type email and try to find Dealer
	Then I save dealer variables
	Then I reset search field in dealer's search
	Then I type name of our Dealer and try to find him
	Then I reset search field in dealer's search
	Then I type ID of our Dealer and try to find him
	Then I reset search field in dealer's search
	Then I type Phone number of our Dealer and try to find him
	