﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TechTalk.SpecFlow;
using Common;
using System.Threading;
using NUnit.Framework;
using OpenQA.Selenium;
using System.Windows.Forms;
using System.IO;

namespace MDOclass.AccountScenarios.backoffice
    {
    [Binding]
    public class DealerProfiles : BaseSteps
        {
        string downloadpath = Environment.GetFolderPath(Environment.SpecialFolder.UserProfile) + "\\Downloads";

        [Then(@"I check the Date From Filter with the value '(.*)'")]
        public void ThenICheckTheDateFromFilterWithTheValue(string p0)
            {
            Thread.Sleep(2000);
            WebDriver.GetByPath("//*[@id='dateFrom']").SendKeys(p0);
            WebDriver.GetByPath("//a[@ng-click='Search()']").Click();
            Thread.Sleep(2000);

            int number = Convert.ToInt32(WebDriver.GetByPath("//*[@class='pull-right page-number-middle']/b[2]").Text);
            string s;

            for(int i=1;i<=number;i++)
                {
                s = WebDriver.GetByPath("//table[@class='table table-striped table-bordered table-hover dataTable']/tbody/tr["+i.ToString()+"]/td[3]").Text;
                Assert.IsTrue(Convert.ToDateTime(s) >= Convert.ToDateTime(p0));
                }
            }

        [Then(@"I check the Date To filter with the value '(.*)'")]
        public void ThenICheckTheDateToFilterWithTheValue(string p0)
            {
            Thread.Sleep(2000);
            WebDriver.GetByPath("//*[@id='dateTo']").SendKeys(p0);
            WebDriver.GetByPath("//a[@ng-click='Search()']").Click();
            Thread.Sleep(2000);

            int number = Convert.ToInt32(WebDriver.GetByPath("//*[@class='pull-right page-number-middle']/b[2]").Text);
            string s;

            for (int i = 1; i <= number; i++)
                {
                s = WebDriver.GetByPath("//table[@class='table table-striped table-bordered table-hover dataTable']/tbody/tr[" + i.ToString() + "]/td[3]").Text;
                Assert.IsTrue(Convert.ToDateTime(s) <= Convert.ToDateTime(p0));
                }
            }

        [Then(@"I check filtering with both dates using the following inverval: '(.*)' - '(.*)'")]
        public void ThenICheckFilteringWithBothDatesUsingTheFollowingInverval_(string p0, string p1)
            {
            Thread.Sleep(2000);
            WebDriver.GetByPath("//*[@id='dateFrom']").SendKeys(p0);
            WebDriver.GetByPath("//*[@id='dateTo']").SendKeys(p1);
            WebDriver.GetByPath("//a[@ng-click='Search()']").Click();
            Thread.Sleep(2000);

            int number = Convert.ToInt32(WebDriver.GetByPath("//*[@class='pull-right page-number-middle']/b[2]").Text);
            
            string s;

            for (int i = 1; i <= number; i++)
                {
                s = WebDriver.GetByPath("//table[@class='table table-striped table-bordered table-hover dataTable']/tbody/tr[" + i.ToString() + "]/td[3]").Text;
                Assert.IsTrue(Convert.ToDateTime(s) >= Convert.ToDateTime(p0));
                Assert.IsTrue(Convert.ToDateTime(s) <= Convert.ToDateTime(p1));
                }
            }

        [Then(@"I reset the search")]
        public void ThenIResetTheSearch()
            {
            WebDriver.GetByPath("//a[@class='btn btn-info date-reset']/i").Click();
            Thread.Sleep(500);
            }

        [Then(@"I check filtering by category using '(.*)'")]
        public void ThenICheckFilteringByCategoryUsing(string p0)
            {
            WebDriver.GetByPath("//div[@ng-class='{open: $select.open}']/button").Click();
            Thread.Sleep(500);
            WebDriver.GetByPath("//ul[@class='ui-select-choices ui-select-choices-content dropdown-menu ng-scope']//*[contains(text(),'"+p0+"')]").
                Click();
            WebDriver.GetByPath("//a[@ng-click='Search()']").Click();
            Thread.Sleep(2000);

            int number=0;
            string s;

            try
                {
                WebDriver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(2));
                WebDriver.GetByPath("//*[contains(text(),'No profiles found for this parameters')]");
                WebDriver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(BaseSteps.StandardTimeOut));
                goto end;
                }
            catch(WebDriverException)
                {
                number = Convert.ToInt32(WebDriver.GetByPath("//*[@class='pull-right page-number-middle']/b[2]").Text);
                for (int i = 1; i <= number; i++)
                    {
                    s = WebDriver.GetByPath("//table[@class='table table-striped table-bordered table-hover dataTable']/tbody/tr[" + i.ToString() + "]/td[7]").Text;
                    Assert.IsTrue(s.ToUpper() == p0.ToUpper());
                    }
                }
            end: ;
            }

        [Then(@"I check the search by a keyphrase '(.*)'")]
        public void ThenICheckTheSearchByAKeyphrase(string p0)
            {
            WebDriver.GetByPath("//*[@id='searchString']").SendKeys(p0);
            WebDriver.GetByPath("//a[@ng-click='Search()']").Click();
            Thread.Sleep(2000);

            int number = 0;
     
            try
                {
                WebDriver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(2));
                WebDriver.GetByPath("//*[contains(text(),'No profiles found for this parameters')]");
                WebDriver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(BaseSteps.StandardTimeOut));
                goto end;
                }
            catch (WebDriverException)
                {
                number = Convert.ToInt32(WebDriver.GetByPath("//*[@class='pull-right page-number-middle']/b[2]").Text);
                for (int i = 1; i <= number; i++)
                    {
                    WebDriver.GetByPath("//table[@class='table table-striped table-bordered table-hover dataTable']/tbody/tr[" + i.ToString() + "]//*[contains(text(),'"+p0+"')]");
                    }
                }
        end: ;
            }

        [Then(@"I download an Excel file")]
        public void ThenIDownloadAnExcelFile()
            {
            WebDriver.GetByPath("//a[@ng-click='downloadXLS()']").Click();
            Thread.Sleep(1500);
            SendKeys.SendWait(@"{Down}");
            Thread.Sleep(500);
            SendKeys.SendWait(@"{Enter}");
            Thread.Sleep(500);
            }

        [Then(@"I check that my '(.*)' file was downloaded")]
        public void ThenICheckThatMyFileWasDownloaded(string p0)
            {
            bool ok = false;

            string[] files = Directory.GetFiles(@downloadpath);

            if (files.Length == 0)
                Assert.Fail("Download was not successful!(((");

            foreach (string path in files)
                if (path.Contains(p0))
                    ok = true;

                    if(ok==false)
                        Assert.Fail("Download was not successful!(((");
            }

        }
    }
