﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TechTalk.SpecFlow;
using Common;
using System.Threading;
using OpenQA.Selenium.Support.UI;
using System.Windows.Forms;
using OpenQA.Selenium;
using System.IO;
using NUnit.Framework;

namespace MDOclass.AccountScenarios.backoffice
    {
    [Binding]
    public class ComissionReport : BaseSteps
        {
        string downloadpath = Environment.GetFolderPath(Environment.SpecialFolder.UserProfile)+"\\Downloads";

        [Then(@"I switch to Commision Reports")]
        public void ThenISwitchToCommisionReports()
            {
            WebDriver.GetByPath("//span[contains(text(),'Comission reports')]").Click();
            Thread.Sleep(500);
            }

        [Then(@"I select the Auction House '(.*)' and Year '(.*)' and month '(.*)'")]
        public void ThenISelectTheAuctionHouseAndYearAndMonth(string p0, string p1, string p2)
            {
            var selector = new SelectElement(WebDriver.GetByPath("//*[@id='auctionHouseSelect']"));
            selector.SelectByText(p0);
            selector = new SelectElement(WebDriver.GetByPath("//*[@id='yearSelect']"));
            selector.SelectByText(p1);
            selector = new SelectElement(WebDriver.GetByPath("//*[@id='monthSelect']"));
            selector.SelectByText(p2);
            }

        [Then(@"I download the Commision Report")]
        public void ThenIDownloadTheCommisionReport()
            {
            try
                {
                WebDriver.GetByPath("//button[@ng-click='downloadPdf()']").Click();
                Thread.Sleep(7000);
                WebDriver.SwitchTo().Alert().Accept();
                }
            catch(WebDriverException)
            { }
            }

        [Then(@"I press enter")]
        public void ThenIPressEnter()
            {
            SendKeys.SendWait(@"{Enter}");
            Thread.Sleep(500);
            }

        [Then(@"I delete all files from my Downloads folder")]
        public void ThenIDeleteAllFilesFromMyDownloadsFolder()
            {
            string[] files = Directory.GetFiles(@downloadpath);
            foreach (string path in files)
                File.Delete(path);
            }


        [Then(@"I check that my Commission Report was downloaded")]
        public void ThenICheckThatMyCommissionReportWasDownloaded()
            {
            bool ok = false;
            string[] files = Directory.GetFiles(@downloadpath);

            if(files.Length==0)
                Assert.Fail("Download was not successful!(((");

            foreach (string path in files)
                if (path == @downloadpath + "\\CommissionReport_IAA.pdf")
                    ok = true;
            
            if(ok==false)
                    Assert.Fail("Download was not successful!(((");
            }

        [Then(@"I click View Report")]
        public void ThenIClickViewReport()
            {
            WebDriver.GetByPath("//button[@ng-click='viewPdf()']").Click();
            }

        [Then(@"I check that my Commission Report was opened")]
        public void ThenICheckThatMyCommissionReportWasOpened()
            {
            WebDriver.GetByPath("//*[contains(text(),'COMMISSION REPORT')]");
            }

        }
    }
