﻿Feature: CCPaymentHistorySearch
	In order to check smth
	for smth
	I made smth

@CCPaymentHistorySearch
Scenario: CC Payment History search
	Given I open main Backoffice page
	Then I type Kathryns Login and password and submit them
	Then I switch to CC Payment History page
	Then I check the Date From Filter with the value '03.11.2015' in CC Payment History
	Then I reset the search
	Then I check the Date To filter with the value '01.01.2015' in CC Payment History
	Then I reset the search
	Then I check filtering with both dates using the following inverval: '01.01.2014' - '01.01.2015' in CC Payment History
	Then I reset the search
	Then I check the search by a keyphrase 'Rosina' in First Name CC Payment History
	Then I reset the search
	Then I check the search by a keyphrase 'Shcherbakov' in Last Name CC Payment History
	Then I reset the search
	Then I check the search by a keyphrase 'www.myvirtualinventory.com' in Web Site URL CC Payment History
	Then I reset the search
	Then I check the search by a keyphrase '99' in Price CC Payment History
	Then I reset the search
	Then I check the search by a keyphrase 'c34b355a2f7a3437845053a258add137' in TransactionId CC Payment History
	Then I download an Excel file
	Then I check that my 'CreditCardPaymentHistory' file was downloaded