﻿Feature: FeedbackSearch
Test for checking search in tab Feedback in Backoffice

@FeedbackSearch
Scenario: Feedback search checking
 Given I open main Backoffice page
 Then I type Kathryns Login and password and submit them
 Then I switch to Feedback tab
 Then I check the Date From Filter with the value '03.11.2015' in FeedbackTab
 Then I reset the search
 Then I check the Date To filter with the value '01.01.2015' in FeedbackTab
 Then I reset the search
 Then I check filtering with both dates using the following inverval: '01.01.2014' - '01.01.2015' in FeedbackTab
 Then I reset the search
 Then I check the search by a keyphrase 'Rosina' in First Name Feedback
 Then I reset the search
 Then I check the search by a keyphrase 'testuser@1.com' in Email Feedback
 Then I reset the search
 Then I check the search by a keyphrase 'utrpa1eooj' in Message Text Feedback