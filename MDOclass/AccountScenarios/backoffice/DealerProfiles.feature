﻿Feature: DealerProfiles
	
@DealerProfiles
Scenario: BO-DealerProfilesSearch
 Given I open main Backoffice page
 Then I type Kathryns Login and password and submit them
 Then I switch to Profiles tab in backoffice
 Then I check the Date From Filter with the value '03.11.2015'
 Then I reset the search
 Then I check the Date To filter with the value '01.01.2015'
 Then I reset the search
 Then I check filtering with both dates using the following inverval: '01.01.2014' - '01.01.2015'

 Then I reset the search

 Then I check filtering by category using 'Active'
 Then I reset the search
 Then I check filtering by category using 'Deleted'
 Then I reset the search
 Then I check filtering by category using 'Pending'
 Then I reset the search
 Then I check filtering by category using 'Cancelled'
 Then I reset the search
 Then I check filtering by category using 'Expired'

 Then I reset the search

 Then I check the search by a keyphrase 'katya'

 Then I delete all files from my Downloads folder
 Then I download an Excel file
 Then I check that my 'DealerProfiles' file was downloaded