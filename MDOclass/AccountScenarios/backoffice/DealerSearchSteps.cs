﻿using System;
using System.Threading;
using Common;
using NUnit.Framework;
using TechTalk.SpecFlow;

namespace MDOclass.AccountScenarios.backoffice
{
    [Binding]
    public class DealerSearchSteps : BaseSteps
    {
        public string dealerName = "";
        public string dealerID = "";
        public string phoneNumber = "";
        public string registerDate = "";

        [Then(@"I type email and try to find Dealer")]
        public void ThenITypeEmailAndTryToFindDealer()
        {
            WebDriver.GetById("searchString").SendKeys(MDOvariables.BackofficeLoginKathryn);
            WebDriver.GetByPath("//div/div[1]/div/div/span[1]/a").Click();
            Assert.True(WebDriver.GetByPath("//a[@href='mailto:" + MDOvariables.BackofficeLoginKathryn + "']").Enabled);
        }

        [Then(@"I save dealer variables")]
        public void ThenISaveDealerVariables()
        {
            dealerID = WebDriver.GetByPath("//a[@href='mailto:" + MDOvariables.BackofficeLoginKathryn + "']/parent::span/../../div[4]/span").Text; //сохранение ID дилера
            dealerID = dealerID.Substring(3);
            phoneNumber = WebDriver.GetByPath("//a[@href='mailto:" + MDOvariables.BackofficeLoginKathryn + "']/parent::span/../../div[3]/span").Text; //сохранение номера дилера
            dealerName = WebDriver.GetByPath("//a[@href='mailto:" + MDOvariables.BackofficeLoginKathryn + "']/parent::span/../../div[1]").Text; //сохранение имени дилера
            dealerName = HandyFunctions.SubstringRemove(dealerName, "\r\nOpen in new tab");
            registerDate = WebDriver.GetByPath("//a[@href='mailto:" + MDOvariables.BackofficeLoginKathryn + "']/parent::span/../../div[2]/span[3]").Text; //сохранение даты регистрации дилера

        }


        [Then(@"I open details of found Dealer and get First Name and Last Name")]
        public void ThenIOpenDetaislOfFoundDealerAndGetFirstNameAndLastName()
        {
            WebDriver.GetByPath("//a[@href='mailto:" + MDOvariables.BackofficeLoginKathryn + "']/..").Click();
        }

        [Then(@"I type ID of our Dealer and try to find him")]
        public void ThenIGetIDOfOurDealer()
        {
            WebDriver.GetById("searchString").Clear();
            WebDriver.GetById("searchString").SendKeys(dealerID);
            WebDriver.GetByPath("//div/div[1]/div/div/span[1]/a").Click();
            Assert.True(WebDriver.GetByPath("//a[@href='mailto:" + MDOvariables.BackofficeLoginKathryn + "']").Enabled);
        }

        [Then(@"I type name of our Dealer and try to find him")]
        public void ThenITypeNameOfOurDealerAndTryToFindHim()
        {
            WebDriver.GetById("searchString").Clear();
            WebDriver.GetById("searchString").SendKeys(dealerName);
            WebDriver.GetByPath("//div/div[1]/div/div/span[1]/a").Click();
            Assert.True(WebDriver.GetByPath("//a[@href='mailto:" + MDOvariables.BackofficeLoginKathryn + "']").Enabled);
        }


        [Then(@"I type Phone number of our Dealer and try to find him")]
        public void ThenIGetPhoneNumberOfOurDealerAndTryToFindHim()
        {
            WebDriver.GetById("searchString").Clear();
            WebDriver.GetById("searchString").SendKeys(phoneNumber);
            WebDriver.GetByPath("//div/div[1]/div/div/span[1]/a").Click();
            Assert.True(WebDriver.GetByPath("//a[@href='mailto:" + MDOvariables.BackofficeLoginKathryn + "']").Enabled);
        }
        [Then(@"I reset search field in dealer's search")]
        public void ThenIResetSearchFieldInDealerSSearch()
        {
            WebDriver.GetById("searchString").Clear();
            WebDriver.GetByPath("//div/div[1]/div/div/span[1]/a").Click();
        }

        [Then(@"I type date of registration of our Dealer and try to find him")]
        public void ThenITypeDateOfRegistrationOfOurDealerAndTryToFindHim()
        {
            WebDriver.GetByPath("//div/div[2]/div/div[2]/div[1]/input").SendKeys(registerDate);
            WebDriver.GetByPath("//div/div[1]/div/div/span[1]/a").Click();
            Assert.True(WebDriver.GetByPath("//a[@href='mailto:" + MDOvariables.BackofficeLoginKathryn + "']").Enabled);
        }



    }

}
