﻿using System;
using System.Threading;
using Common;
using NUnit.Framework;
using OpenQA.Selenium;
using TechTalk.SpecFlow;

namespace MDOclass.AccountScenarios.backoffice
{
    [Binding]
    public class FeedbackSearchSteps: BaseSteps
    {
        [Then(@"I check the Date From Filter with the value '(.*)' in FeedbackTab")]
        public void ThenICheckTheDateFromFilterWithTheValueInFeedbackTab(string p0)
        {
            Thread.Sleep(2000);
            WebDriver.GetByPath("//*[@id='dateFrom']").SendKeys(p0);
            WebDriver.GetByPath("//a[@ng-click='Search()']").Click();
            Thread.Sleep(2000);

            int number = Convert.ToInt32(WebDriver.GetByPath("//*[@class='pull-right page-number-middle']/b[2]").Text);
            string s;

            for (int i = 1; i <= number; i++)
            {
                s = WebDriver.GetByPath("//table/tbody/tr[" + i + "]/td[1]").Text;
                Assert.IsTrue(Convert.ToDateTime(s) >= Convert.ToDateTime(p0));
            }
        }

        [Then(@"I check the Date To filter with the value '(.*)' in FeedbackTab")]
        public void ThenICheckTheDateToFilterWithTheValueInFeedbackTab(string p0)
        {
            Thread.Sleep(2000);
            WebDriver.GetByPath("//*[@id='dateTo']").SendKeys(p0);
            WebDriver.GetByPath("//a[@ng-click='Search()']").Click();
            Thread.Sleep(2000);

            int number = Convert.ToInt32(WebDriver.GetByPath("//*[@class='pull-right page-number-middle']/b[2]").Text);
            string s;

            for (int i = 1; i <= number; i++)
            {
                s = WebDriver.GetByPath("//table/tbody/tr[" + i + "]/td[1]").Text;
                Assert.IsTrue(Convert.ToDateTime(s) <= Convert.ToDateTime(p0));
            }
        }

        [Then(@"I check filtering with both dates using the following inverval: '(.*)' - '(.*)' in FeedbackTab")]
        public void ThenICheckFilteringWithBothDatesUsingTheFollowingInverval_InFeedbackTab(string p0, string p1)
        {
            Thread.Sleep(2000);
            WebDriver.GetByPath("//*[@id='dateFrom']").SendKeys(p0);
            WebDriver.GetByPath("//*[@id='dateTo']").SendKeys(p1);
            WebDriver.GetByPath("//a[@ng-click='Search()']").Click();
            Thread.Sleep(2000);

            int number = Convert.ToInt32(WebDriver.GetByPath("//*[@class='pull-right page-number-middle']/b[2]").Text);

            string s;

            for (int i = 1; i <= number; i++)
            {
                s = WebDriver.GetByPath("//table/tbody/tr[" + i + "]/td[1]").Text;
                Assert.IsTrue(Convert.ToDateTime(s) >= Convert.ToDateTime(p0));
                Assert.IsTrue(Convert.ToDateTime(s) <= Convert.ToDateTime(p1));
            }
        }

        [Then(@"I check the search by a keyphrase '(.*)' in First Name Feedback")]
        public void ThenICheckTheSearchByAKeyphraseInFirstNameFeedback(string p0)
        {
            WebDriver.GetByPath("//*[@id='searchString']").SendKeys(p0);
            WebDriver.GetByPath("//a[@ng-click='Search()']").Click();
            Thread.Sleep(2000);

            int number = 0;

            try
            {
                WebDriver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(2));
                WebDriver.GetByPath("//*[contains(text(),'No profiles found for this parameters')]");
                WebDriver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(BaseSteps.StandardTimeOut));
                goto end;
            }
            catch (WebDriverException)
            {
                number = Convert.ToInt32(WebDriver.GetByPath("//*[@class='pull-right page-number-middle']/b[2]").Text);
                for (int i = 1; i <= number; i++)
                {
                    WebDriver.GetByPath("//table/tbody/tr[" + i+ "]/td[2][contains(text(),'" + p0 + "')]");
                }
            }
        end: ;
        }

        [Then(@"I check the search by a keyphrase '(.*)' in Email Feedback")]
        public void ThenICheckTheSearchByAKeyphraseInEmailFeedback(string p0)
        {
            WebDriver.GetByPath("//*[@id='searchString']").SendKeys(p0);
            WebDriver.GetByPath("//a[@ng-click='Search()']").Click();
            Thread.Sleep(2000);

            int number = 0;

            try
            {
                WebDriver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(2));
                WebDriver.GetByPath("//*[contains(text(),'No profiles found for this parameters')]");
                WebDriver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(BaseSteps.StandardTimeOut));
                goto end;
            }
            catch (WebDriverException)
            {
                number = Convert.ToInt32(WebDriver.GetByPath("//*[@class='pull-right page-number-middle']/b[2]").Text);
                for (int i = 1; i <= number; i++)
                {
                    WebDriver.GetByPath("//table/tbody/tr[" + i + "]/td[5][contains(text(),'" + p0 + "')]");
                }
            }
        end: ;
        }

        [Then(@"I check the search by a keyphrase '(.*)' in Message Text Feedback")]
        public void ThenICheckTheSearchByAKeyphraseInMessageTextFeedback(string p0)
        {
            WebDriver.GetByPath("//*[@id='searchString']").SendKeys(p0);
            WebDriver.GetByPath("//a[@ng-click='Search()']").Click();
            Thread.Sleep(2000);

            int number = 0;

            try
            {
                WebDriver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(2));
                WebDriver.GetByPath("//*[contains(text(),'No profiles found for this parameters')]");
                WebDriver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(BaseSteps.StandardTimeOut));
                goto end;
            }
            catch (WebDriverException)
            {
                number = Convert.ToInt32(WebDriver.GetByPath("//*[@class='pull-right page-number-middle']/b[2]").Text);
                for (int i = 1; i <= number; i++)
                {
                    WebDriver.GetByPath("//table/tbody/tr[" + i + "]/td[6]/span[contains(text(),'" + p0 + "')]");
                }
            }
        end: ;
        }

    }
}
