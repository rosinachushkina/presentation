﻿using System;
using System.Threading;
using Common;
using NUnit.Framework;
using OpenQA.Selenium;
using TechTalk.SpecFlow;

namespace MDOclass.AccountScenarios.backoffice
{
    [Binding]
    public class CCPaymentHistorySearchSteps : BaseSteps
    {
        [Then(@"I check the Date From Filter with the value '(.*)' in CC Payment History")]
        public void ThenICheckTheDateFromFilterWithTheValueInCCPaymentHistory(string p0)
        {
            Thread.Sleep(2000);
            WebDriver.GetByPath("//*[@id='dateFrom']").SendKeys(p0);
            WebDriver.GetByPath("//a[@ng-click='Search()']").Click();
            Thread.Sleep(2000);

            int number = Convert.ToInt32(WebDriver.GetByPath("//*[@class='pull-right page-number-middle']/b[2]").Text);
            string s;

            for (int i = 1; i <= number; i++)
            {
                s =
                    WebDriver.GetByPath(
                        "//table[@class='table table-striped table-bordered table-hover dataTable']/tbody/tr[" +
                        i.ToString() + "]/td[1]").Text;
                Assert.IsTrue(Convert.ToDateTime(s) >= Convert.ToDateTime(p0));
            }
        }

        [Then(@"I check the Date To filter with the value '(.*)' in CC Payment History")]
        public void ThenICheckTheDateToFilterWithTheValueInCCPaymentHistory(string p0)
        {
            Thread.Sleep(2000);
            WebDriver.GetByPath("//*[@id='dateTo']").SendKeys(p0);
            WebDriver.GetByPath("//a[@ng-click='Search()']").Click();
            Thread.Sleep(2000);

            int number = Convert.ToInt32(WebDriver.GetByPath("//*[@class='pull-right page-number-middle']/b[2]").Text);
            string s;

            for (int i = 1; i <= number; i++)
            {
                s =
                    WebDriver.GetByPath(
                        "//table[@class='table table-striped table-bordered table-hover dataTable']/tbody/tr[" +
                        i.ToString() + "]/td[1]").Text;
                Assert.IsTrue(Convert.ToDateTime(s) <= Convert.ToDateTime(p0));
            }
        }

        [Then(@"I check filtering with both dates using the following inverval: '(.*)' - '(.*)' in CC Payment History")]
        public void ThenICheckFilteringWithBothDatesUsingTheFollowingInverval_InCCPaymentHistory(string p0, string p1)
        {
            Thread.Sleep(2000);
            WebDriver.GetByPath("//*[@id='dateFrom']").SendKeys(p0);
            WebDriver.GetByPath("//*[@id='dateTo']").SendKeys(p1);
            WebDriver.GetByPath("//a[@ng-click='Search()']").Click();
            Thread.Sleep(2000);

            int number = Convert.ToInt32(WebDriver.GetByPath("//*[@class='pull-right page-number-middle']/b[2]").Text);

            string s;

            for (int i = 1; i <= number; i++)
            {
                s =
                    WebDriver.GetByPath(
                        "//table[@class='table table-striped table-bordered table-hover dataTable']/tbody/tr[" +
                        i.ToString() + "]/td[1]").Text;
                Assert.IsTrue(Convert.ToDateTime(s) >= Convert.ToDateTime(p0));
                Assert.IsTrue(Convert.ToDateTime(s) <= Convert.ToDateTime(p1));
            }
        }

        [Then(@"I check the search by a keyphrase '(.*)' in First Name CC Payment History")]
        public void ThenICheckTheSearchByAKeyphraseInCCPaymentHistory(string p0)
        {
            WebDriver.GetByPath("//*[@id='searchString']").SendKeys(p0);
            WebDriver.GetByPath("//a[@ng-click='Search()']").Click();
            Thread.Sleep(2000);

            int number = 0;

            try
            {
                WebDriver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(2));
                WebDriver.GetByPath("//*[contains(text(),'No profiles found for this parameters')]");
                WebDriver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(BaseSteps.StandardTimeOut));
                goto end;
            }
            catch (WebDriverException)
            {
                number = Convert.ToInt32(WebDriver.GetByPath("//*[@class='pull-right page-number-middle']/b[2]").Text);
                for (int i = 1; i <= number; i++)
                {
                    WebDriver.GetByPath("//table[@class='table table-striped table-bordered table-hover dataTable']/tbody/tr[" + i + "]/td[2][contains(text(),'" + p0 + "')]");
                }
            }
        end: ;
        }

        [Then(@"I check the search by a keyphrase '(.*)' in Last Name CC Payment History")]
        public void ThenICheckTheSearchByAKeyphraseInLastNameCCPaymentHistory(string p0)
        {
            WebDriver.GetByPath("//*[@id='searchString']").SendKeys(p0);
            WebDriver.GetByPath("//a[@ng-click='Search()']").Click();
            Thread.Sleep(2000);

            int number = 0;

            try
            {
                WebDriver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(2));
                WebDriver.GetByPath("//*[contains(text(),'No profiles found for this parameters')]");
                WebDriver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(BaseSteps.StandardTimeOut));
                goto end;
            }
            catch (WebDriverException)
            {
                number = Convert.ToInt32(WebDriver.GetByPath("//*[@class='pull-right page-number-middle']/b[2]").Text);
                for (int i = 1; i <= number; i++)
                {
                    WebDriver.GetByPath("//table[@class='table table-striped table-bordered table-hover dataTable']/tbody/tr[" + i.ToString() + "]/td[3][contains(text(),'" + p0 + "')]");
                }
            }
        end: ;
        }

        [Then(@"I check the search by a keyphrase '(.*)' in Web Site URL CC Payment History")]
        public void ThenICheckTheSearchByAKeyphraseInWebSiteURLCCPaymentHistory(string p0)
        {
            WebDriver.GetByPath("//*[@id='searchString']").SendKeys(p0);
            WebDriver.GetByPath("//a[@ng-click='Search()']").Click();
            Thread.Sleep(2000);

            int number = 0;

            try
            {
                WebDriver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(2));
                WebDriver.GetByPath("//*[contains(text(),'No profiles found for this parameters')]");
                WebDriver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(BaseSteps.StandardTimeOut));
                goto end;
            }
            catch (WebDriverException)
            {
                number = Convert.ToInt32(WebDriver.GetByPath("//*[@class='pull-right page-number-middle']/b[2]").Text);
                for (int i = 1; i <= number; i++)
                {
                    WebDriver.GetByPath("//table[@class='table table-striped table-bordered table-hover dataTable']/tbody/tr[" + i.ToString() + "]/td[4][contains(text(),'" + p0 + "')]");
                }
            }
        end: ;
        }

        [Then(@"I check the search by a keyphrase '(.*)' in Price CC Payment History")]
        public void ThenICheckTheSearchByAKeyphraseInPriceCCPaymentHistory(string p0)
        {
            WebDriver.GetByPath("//*[@id='searchString']").SendKeys(p0);
            WebDriver.GetByPath("//a[@ng-click='Search()']").Click();
            Thread.Sleep(2000);

            int number = 0;

            try
            {
                WebDriver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(2));
                WebDriver.GetByPath("//*[contains(text(),'No profiles found for this parameters')]");
                WebDriver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(BaseSteps.StandardTimeOut));
                goto end;
            }
            catch (WebDriverException)
            {
                number = Convert.ToInt32(WebDriver.GetByPath("//*[@class='pull-right page-number-middle']/b[2]").Text);
                for (int i = 1; i <= number; i++)
                {
                    WebDriver.GetByPath("//table[@class='table table-striped table-bordered table-hover dataTable']/tbody/tr[" + i.ToString() + "]/td[6][contains(text(),'" + p0 + "')]");
                }
            }
        end: ;
        }

        [Then(@"I check the search by a keyphrase '(.*)' in TransactionId CC Payment History")]
        public void ThenICheckTheSearchByAKeyphraseInTransactionIdCCPaymentHistory(string p0)
        {
            WebDriver.GetByPath("//*[@id='searchString']").SendKeys(p0);
            WebDriver.GetByPath("//a[@ng-click='Search()']").Click();
            Thread.Sleep(2000);

            int number = 0;

            try
            {
                WebDriver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(2));
                WebDriver.GetByPath("//*[contains(text(),'No profiles found for this parameters')]");
                WebDriver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(BaseSteps.StandardTimeOut));
                goto end;
            }
            catch (WebDriverException)
            {
                number = Convert.ToInt32(WebDriver.GetByPath("//*[@class='pull-right page-number-middle']/b[2]").Text);
                for (int i = 1; i <= number; i++)
                {
                    WebDriver.GetByPath("//table[@class='table table-striped table-bordered table-hover dataTable']/tbody/tr[" + i.ToString() + "]/td[9][contains(text(),'" + p0 + "')]");
                }
            }
        end: ;
        }

    }
}
