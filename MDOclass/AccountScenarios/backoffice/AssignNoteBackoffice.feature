﻿Feature: AsignNoteBackoffice
	In order to check note assigning in backoffice
	One Dealer assign to another
	Another Dealer check it.

@AssigningNoteBackofficeFromDealersPage
Scenario: Assigning Note in Backoffice Dealer Page
	Given I open main Backoffice page
	Then I type Kathryns Login and password and submit them
	Then I choose tab Notes in dealers tab
	Then I fill note in backoffice
	Then I add this note for another Dealer
	Then I assign this note to Minh
	Then I check that note which was sent is the same
	Then I make logout from backoffice


	Then I type Minhs Login and password and submit them
	Then I swith to My notes tab
	Then I check notification about 1 new note
	Then I open dealer notes details
	Then I check that note which was sent is the same
	Then I complete this note
	Then I check that note notification disabled	
	
@AssigningNoteBackofficeFromPaymentPage
Scenario: Assigning Note in Backoffice Payment Page
	Given I open main Backoffice page
	Then I type Kathryns Login and password and submit them
	Then I switch to CC Payment History page
	Then I click details near first dealer
	Then I fill note in backoffice
	Then I add this note for another Dealer
	Then I assign this note to Minh from CC page
	Then I check that note which was sent is the same
	Then I make logout from backoffice


	Then I type Minhs Login and password and submit them
	Then I swith to My notes tab
	Then I check notification about 1 new note
	Then I open dealer notes details
	Then I check that note which was sent is the same
	Then I complete this note
	Then I check that note notification disabled	

