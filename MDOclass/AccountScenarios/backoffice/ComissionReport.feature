﻿Feature: ComissionReport
	
@ComissionReport
Scenario: ComissionReportDownloading
    Given I open main Backoffice page
	Then I type Kathryns Login and password and submit them
	Then I switch to Commision Reports
	Then I select the Auction House 'IAA' and Year '2015' and month 'January'
	Then I delete all files from my Downloads folder
	Then I download the Commision Report
	Then I press enter
	Then I wait 10 seconds
	Then I check that my Commission Report was downloaded

	Scenario: ComissionReportOpening
    Given I open main Backoffice page
	Then I type Kathryns Login and password and submit them
	Then I switch to Commision Reports
	Then I select the Auction House 'IAA' and Year '2015' and month 'January'
	Then I click View Report
	Then I wait 10 seconds
	Then I switch to tab 2
	Then I check that my Commission Report was opened