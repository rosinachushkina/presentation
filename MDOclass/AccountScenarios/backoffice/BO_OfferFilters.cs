﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TechTalk.SpecFlow;
using Common;
using NUnit.Framework;
using System.Threading;

namespace MDOclass.AccountScenarios.backoffice
    {
    [Binding]
    public class BO_OfferFilters : BaseSteps
        {
        [Then(@"I check the Offers Date From Filter with the value '(.*)'")]
        public void ThenICheckTheOffersDateFromFilterWithTheValue(string p0)
            {
            Thread.Sleep(2000);
            WebDriver.GetByPath("//*[@id='dateFrom']").SendKeys(p0);
            WebDriver.GetByPath("//a[@ng-click='Search()']").Click();
            Thread.Sleep(2000);

            int number = Convert.ToInt32(WebDriver.GetByPath("//*[@class='pull-right page-number-middle']/b[2]").Text);
            string s;

            for (int i = 1; i <= number; i++)
                {
                s = WebDriver.GetByPath("//table[@class='table table-striped table-bordered table-hover dataTable']/tbody/tr[" + i.ToString() + "]/td[3]").Text;
                Assert.IsTrue(Convert.ToDateTime(s) >= Convert.ToDateTime(p0));
                }
            }

        [Then(@"I check the Offers Date To filter with the value '(.*)'")]
        public void ThenICheckTheOffersDateToFilterWithTheValue(string p0)
            {
            Thread.Sleep(2000);
            WebDriver.GetByPath(".//*[@id='dateTo']").SendKeys(p0);
            WebDriver.GetByPath("//a[@ng-click='Search()']").Click();
            Thread.Sleep(2000);

            int number = Convert.ToInt32(WebDriver.GetByPath("//*[@class='pull-right page-number-middle']/b[2]").Text);
            string s;

            for (int i = 1; i <= number; i++)
                {
                s = WebDriver.GetByPath("//table[@class='table table-striped table-bordered table-hover dataTable']/tbody/tr[" + i.ToString() + "]/td[3]").Text;
                Assert.IsTrue(Convert.ToDateTime(s) <= Convert.ToDateTime(p0));
                }
            }

        }
    }
