﻿Feature: BO_OfferFilters
	
@BO_OfferFilters
Scenario: BO_OfferFilters

Given I open main Backoffice page
Then I type Kathryns Login and password and submit them
Then I switch to Offers (BO)

 Then I check the Offers Date From Filter with the value '03.11.2015'
 Then I reset the search
 Then I check the Offers Date To filter with the value '01.01.2015'
 Then I reset the search