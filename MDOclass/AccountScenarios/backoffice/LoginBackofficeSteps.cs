﻿using System;
using System.Diagnostics.Eventing.Reader;
using System.Threading;
using System.Windows.Forms.VisualStyles;
using Common;
using NUnit.Framework;
using TechTalk.SpecFlow;

namespace MDOclass.AccountScenarios.backoffice
{
    [Binding]
    public class LoginBackofficeSteps : BaseSteps
    {
        [Given(@"I open main Backoffice page")]
        public void GivenIOpenMainBackofficePage()
        {
            if (MDOvariables.IsProductionTest == true)
                WebDriver.Url = Pages.Backoffice;
            else
                WebDriver.Url = Pages.BackofficeSt;
        }

        [Then(@"I type Kathryns Login and password and submit them")]
        public void ThenITypeKathrynsLoginAndPasswordAndSubmitThem()
        {
            WebDriver.GetByName("email").SendKeys(MDOvariables.BackofficeLoginKathryn);
            WebDriver.GetByName("password").SendKeys(MDOvariables.BackofficePasswordKathryn);
            WebDriver.GetByPath("//form[1]/div[3]/button").Click();
        }

        [Then(@"I check that I was authorizated in Backoffice")]
        public void ThenICheckThatIWasAuthorizatedInBackoffice()
        {
            Assert.True(WebDriver.GetByPath("//div[3]/div/div/h3[contains(text(), 'BackOffice - Dealers')]").Enabled);
        }

        [Then(@"I type Minhs Login and password and submit them")]
        public void ThenITypeMinhsLoginAndPasswordAndSubmitThem()
        {
            WebDriver.GetByName("email").Click();
            WebDriver.GetByName("email").SendKeys(MDOvariables.BackofficeLoginMinh);
             WebDriver.GetByName("password").Click();
            WebDriver.GetByName("password").SendKeys(MDOvariables.BackofficePasswordMinh);
            WebDriver.GetByPath("//form[1]/div[3]/button").Click();
        }

    }
}
