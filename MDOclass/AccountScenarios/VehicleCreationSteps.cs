﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using System.Windows.Forms.VisualStyles;
using Common;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using TechTalk.SpecFlow;

namespace MDOclass.AccountScenarios
{
    [Binding]
    public class VehicleCreationSteps : BaseSteps
    {
        HandyFunctions Handy = new HandyFunctions();
        private string VIN, VehicleType, Year, Make, Model, Trim, ExteriorColor, InteriorColor;
        private int i;
        private Random Random=new Random(Environment.TickCount);

        [Then(@"I go to Dashboard > MyListings")]
        public void ThenIGoToDashboardMyListings()
        {
            WebDriver.FindElement(By.XPath("/html/body/div[2]/aside/ul/li[8]/a/span")).Click();
        }

        [Then(@"I click the AddNew button")]
        public void ThenIClickTheAddNewButton()
        {
            WebDriver.FindElement(By.XPath("/html/body/div[2]/div/div/div[2]/div/div/ul/li[4]/button")).Click();
        }


        [Then(@"I enter all information in Specifications")]
        public void ThenIEnterAllInformationAboutMyNewListingAndPublishIt()
        {
            VIN = Handy.RandomStringNumbers(10);
            WebDriver.FindElement(By.XPath("//input[@name='vin']")).SendKeys(VIN);

            //--------Рандомизируем выбор типа транспортного средства - и заполняемся
          
            i = Random.Next(2,9);
         
            VehicleType = WebDriver.FindElement(By.XPath("//select[@name='vehicleType']/option[" + i + "]")).Text;
           
            var Selector =
               new SelectElement(WebDriver.GetByName("vehicleType"));
               Selector.SelectByIndex(i-1);

            Year = Random.Next(1980, 2015).ToString();
            WebDriver.FindElement(By.XPath("//input[@name='year']")).SendKeys(Year);

            i = Random.Next(2,20);
          
            Make = WebDriver.FindElement(By.XPath("//select[@name='makeId']/option[" + i + "]")).Text;
            Selector = new SelectElement(WebDriver.GetByName("makeId"));
            Selector.SelectByIndex(i-1);

            Model = Handy.RandomStringLatinAndNumbers(10);
            WebDriver.FindElement(By.XPath("//input[@name='model']")).SendKeys(Model);

            Trim = Handy.RandomStringLatinAndNumbers(10);
            WebDriver.FindElement(By.XPath("//input[@name='trim']")).SendKeys(Trim);
           
            i = Random.Next(2, 20);
           
            ExteriorColor = WebDriver.FindElement(By.XPath("//select[@name='exteriorColor']/option[" + i + "]")).Text;
            Selector = new SelectElement(WebDriver.GetByName("exteriorColor"));
            Selector.SelectByIndex(i-1);

            i = Random.Next(2, 20);
           
            InteriorColor = WebDriver.FindElement(By.XPath("//select[@name='interiorColor']/option[" + i + "]")).Text;
            Selector = new SelectElement(WebDriver.GetByName("interiorColor"));
            Selector.SelectByIndex(i-1);
           
        }
        
        [Then(@"I switch to Information")]
        public void ThenISwitchToInformation()
        {
            WebDriver.FindElement(By.XPath("//a[@href='#tabInformation']")).Click();
        }

        [Then(@"I switch to Dealer Info")]
        public void ThenISwitchToDealerInfo()
        {
            WebDriver.FindElement(By.XPath("//a[@href='#tabDealerInfo']")).Click();
        }

        [Then(@"I switch to Condition")]
        public void ThenISwitchToCondition()
        {
            WebDriver.FindElement(By.XPath("//a[@href='#tabCondition']")).Click();
        }

        [Then(@"I switch to Paperwork")]
        public void ThenISwitchToPaperwork()
        {
            WebDriver.FindElement(By.XPath("//a[@href='#tabPaperwork']")).Click();
        }

        [Then(@"I switch to Images and Video")]
        public void ThenISwitchToImagesAndVideo()
        {
            WebDriver.FindElement(By.XPath("//a[@href='#tabImagesVideos']")).Click();
        }


        [Then(@"I enter all information in Information")]
        public void ThenIEnterAllInformationInInformation()
        {
            var Selector =
              new SelectElement(WebDriver.GetByName("transmission"));
            i = Random.Next(2, 5);
            Selector.SelectByIndex(i - 1);

             Selector =
              new SelectElement(WebDriver.GetByName("fuelType"));
            i = Random.Next(2, 15);
            Selector.SelectByIndex(i - 1);

            WebDriver.FindElement(By.XPath("//input[@name='bodyType']")).SendKeys(Handy.RandomStringLatinAndNumbers(10));
            WebDriver.FindElement(By.XPath("//input[@name='drive']")).SendKeys(Handy.RandomStringLatinAndNumbers(10));
            WebDriver.FindElement(By.XPath("//input[@name='engineType']")).SendKeys(Handy.RandomStringLatinAndNumbers(10));
        }

        [Then(@"I enter all information in Dealer Info")]
        public void ThenIEnterAllInformationInDealerInfo()
        {
            var Selector =
              new SelectElement(WebDriver.GetByName("locationId"));
          
            Selector.SelectByIndex(1);

            WebDriver.FindElement(By.XPath("//input[@name='buyNowPrice']")).SendKeys(Handy.RandomStringNumberWithOutZero(4));

            WebDriver.GetByName("biddingStartDate").Click();
            WebDriver.GetByPath("/html/body/div[3]/div/table/tbody/tr[2]/td[2]").Click();
      
            WebDriver.GetByName("biddingEndDate").Click();
            WebDriver.GetByPath("/html/body/div[3]/div/table/tbody/tr[4]/td[5]").Click();

            WebDriver.FindElement(By.XPath("//textarea[@name='vehicleDescription']")).SendKeys(Handy.RandomStringLatinAndNumbers(150));
        }

        [Then(@"I enter all information in Condition")]
        public void ThenIEnterAllInformationInCondition()
        {
            var Odo = Random.Next(0, 500000);
            WebDriver.FindElement(By.XPath("//input[@name='odometer']")).SendKeys(Odo.ToString());

            var Selector =
              new SelectElement(WebDriver.GetByName("odometerStatus"));
            i = Random.Next(2, 5);
            Selector.SelectByIndex(i - 1);

            Selector =
              new SelectElement(WebDriver.GetByName("isKeyPresent"));
            i = Random.Next(2, 4);
            Selector.SelectByIndex(i - 1);

            Selector =
           new SelectElement(WebDriver.GetByName("runningCondition"));
            i = Random.Next(2, 6);
            Selector.SelectByIndex(i - 1);

            Selector =
         new SelectElement(WebDriver.GetByName("odometerUnit"));
            i = Random.Next(2, 4);
            Selector.SelectByIndex(i - 1);

            WebDriver.FindElement(By.XPath("//input[@name='keyLocation']")).SendKeys(Handy.RandomStringLatinAndNumbers(25));

            Selector=new SelectElement(WebDriver.GetByName("primaryDamage"));
            i = Random.Next(2, 6);
            Selector.SelectByIndex(i - 1);

            Selector=new SelectElement(WebDriver.GetByName("secondaryDamage"));
            i = Random.Next(2, 6);
            Selector.SelectByIndex(i - 1);

            Selector=new SelectElement(WebDriver.GetByName("lossType"));
            i = Random.Next(2, 6);
            Selector.SelectByIndex(i - 1);
        }

        [Then(@"I enter all information in Paperwork")]
        public void ThenIEnterAllInformationInPaperwork()
        {
            var Selector =
             new SelectElement(WebDriver.GetByName("titleDocument"));
            i = Random.Next(2, 30);
            Selector.SelectByIndex(i - 1);

            Selector = new SelectElement(WebDriver.GetByName("titleCountry"));
            i = Random.Next(2, 30);
            Selector.SelectByIndex(i - 1);

            Selector = new SelectElement(WebDriver.GetByName("isTitleClean"));
            i = Random.Next(2, 4);
            Selector.SelectByIndex(i - 1);
        }


        [Then(@"I enter all information in Images and Video ""(.*)""")]
        public void ThenIEnterAllInformationInImagesAndVideo(string p0)
        {
            WebDriver.FindElement(By.XPath("//*[@id='addPicture']")).Click();

            //-------------------Загружаем картинку, путь которой передали
            Thread.Sleep(1000);
            SendKeys.SendWait(p0);
            Thread.Sleep(1000);
            SendKeys.SendWait(@"{Enter}");
            Thread.Sleep(5000);
        }

        [Then(@"I click the SaveAndPublish button")]
        public void ThenIClickTheSaveAndPublishButton()
        {
            WebDriver.FindElement(By.XPath("//button[@class='btn btn-success']")).Click();
            Thread.Sleep(3000);
        }


        [Then(@"My new listing should be added to MyListings list")]
        public void ThenMyNewListingShouldBeAddedToMyListingsList()
        {
            string s,ver;
            bool AreActiveListings = true;

            //--------Проверяем на этой странице
            Thread.Sleep(3000);

            //------------------Сначала надо проверить есть ли вообще листинги (акт 1)
            try
                {
                WebDriver.GetByPath("//*[contains(text(),'t have any listings')]");
                AreActiveListings = false;
                }
            catch(WebDriverException)
            { }

            //------------------Сначала надо проверить есть ли вообще листинги (акт 2)
            if (AreActiveListings==true)
            try
            {
           
            s = "//div[@class='item read'][1]/h2/span[2]";
            Assert.IsTrue(WebDriver.FindElement(By.XPath(s)).
                    Text.Contains(Year + ' ' + Make + ' ' + Model.ToUpper()));

            }
            catch (AssertionException)
            {
              
            //--------Если нет, значит переходим на просроченные
            s = "//li[@id='expired']/a";
            WebDriver.FindElement(By.XPath(s)).Click();
            Thread.Sleep(3000);

            s = "//div[@class='item read'][1]/h2/span[2]";
            Assert.IsTrue(WebDriver.FindElement(By.XPath(s)).
                Text.Contains(Year + ' ' + Make + ' ' + Model.ToUpper()));
            }
            //---------------------Иначе сразу на просроченные
            else
                {
                //--------Если нет, значит переходим на просроченные
                s = "//li[@id='expired']/a";
                WebDriver.FindElement(By.XPath(s)).Click();
                Thread.Sleep(3000);

                s = "//div[@class='item read'][1]/h2/span[2]";
                Assert.IsTrue(WebDriver.FindElement(By.XPath(s)).
                    Text.Contains(Year + ' ' + Make + ' ' + Model.ToUpper()));
                }
           
        }

        [Then(@"I check than my vehicle image was uploaded successfully")]
        public void ThenICheckThanMyVehicleImageWasUploadedSuccessfully()
            {
            WebDriver.GetByPath("//div[@id='addedPicturesContainer']/div[1]/ul/li/img");
            }


    }
}
