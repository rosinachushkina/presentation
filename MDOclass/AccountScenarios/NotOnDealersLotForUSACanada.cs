﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TechTalk.SpecFlow;
using Common;
using NUnit.Framework;
using OpenQA.Selenium;
using System.Threading;
using OpenQA.Selenium.Support.UI;

namespace MDOclass.AccountScenarios
    {
    [Binding]
    public class NotOnDealersLotForUSACanada : BaseSteps
        {

        string text="";
        string text2 = "";

        [Then(@"I check that the disclaimer ""(.*)"" and description ""(.*)"" are there")]
        public void ThenICheckThatTheDisclaimerAndDescriptionAreThere(string p0, string p1)
            {
            text = WebDriver.GetByPath("//*[@id='divBidTop']/div[2]/div[2]").Text;
            Assert.IsTrue(text.Trim() == p0);

            //text2 = WebDriver.GetByPath("//div[@id='biddingDetails']/div[9]/div[3]").Text;
            //Assert.IsTrue(text2.Trim() == p1);

            WebDriver.GetByPath("//*[@id='CPH_ucOpenInvoice_panLocation']/following-sibling::div/span").Click();
            Thread.Sleep(500);
            text2=WebDriver.GetByPath("//*[@id='CPH_ucOpenInvoice_panLocation']/following-sibling::div/div").Text;
            Assert.IsTrue(text2.Trim() == p1);
            }

        [Then(@"I check that the disclaimer ""(.*)"" and description ""(.*)"" are NOT there")]
        public void ThenICheckThatTheDisclaimerAndDescriptionAreNOTThere(string p0, string p1)
            {
            WebDriver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(3));
            try
                {
                text = WebDriver.GetByPath("//*[@id='divBidTop']/div[2]/div[2]").Text;
                Assert.IsTrue(text.Trim() != p0);
                }
            catch (WebDriverException)
            {  }

            try
                {
                WebDriver.GetByPath("//*[@id='CPH_ucOpenInvoice_panLocation']/following-sibling::div/span").Click();
                Thread.Sleep(500);
                text2 = WebDriver.GetByPath("//*[@id='CPH_ucOpenInvoice_panLocation']/following-sibling::div/div").Text;
                Assert.IsTrue(text2.Trim() != p1);
                }
            catch(WebDriverException)
            {  }
            WebDriver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(BaseSteps.StandardTimeOut));
            }

        [Then(@"I go to MyAccount \(Portal\)")]
        public void ThenIGoToMyAccountPortal()
            {
            WebDriver.GetByPath("//li[@class='dropdown pull-right'][1]/a").Click();
            Thread.Sleep(500);
            WebDriver.GetByPath("//a[contains(text(),'My Account')]").Click();
            }

        [Then(@"I switch my country to NonUSCanada")]
        public void ThenISwitchMyCountryToNonUSCanada()
            {
            var selector = new SelectElement(WebDriver.GetById("ddCountry"));
            selector.SelectByText("Austria");
            WebDriver.GetById("CPH_btnUpdate").Click();
            Thread.Sleep(2000);
            }

        [Then(@"I switch my country to '(.*)' \(MyFrofile\)")]
        public void ThenISwitchMyCountryToMyFrofile(string p0)
            {
            var selector = new SelectElement(WebDriver.GetById("ddCountry"));
            selector.SelectByText(p0);
            WebDriver.GetById("CPH_btnUpdate").Click();
            Thread.Sleep(2000);
            }

        }
    }
