﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TechTalk.SpecFlow;
using Common;
using System.Threading;
using System.Windows.Forms;
using NUnit.Framework;
using OpenQA.Selenium;

namespace MDOclass.AccountScenarios.portal
    {
    [Binding]
    public class PortalAgreementPrintSteps : BaseSteps
        {
        string agreementinitialurl = "";

        [Then(@"I check the Print button for agreement - '(.*)'")]
        public void ThenICheckThePrintButtonForAgreement_(string p0)
            {
            //agreementinitialurl=WebDriver.GetCurrentUrl();
            WebDriver.GetByPath("//*[@id='btnPrint']").Click();
            Thread.Sleep(2000);
            SendKeys.SendWait(@"{Escape}");
            Thread.Sleep(500);

            WebDriver.SwitchTo().Window(WebDriver.WindowHandles.Last());
            string s = WebDriver.GetByPath("/html/body/p[2]").Text;
            Assert.IsTrue(s.Contains(p0));
            Assert.IsTrue(s.Contains("By executing this Subscription Agreement, Customer is ordering the Services set forth herein."));
           
            //Assert.IsTrue(agreementinitialurl != WebDriver.GetCurrentUrl());
            WebDriver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(3));
            try
                {
                WebDriver.GetByPath("//*[@id='btnPrint']");
                Assert.Fail("We are not in the Print mode!(((");
                }
            catch(WebDriverException)
            { }
            WebDriver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(BaseSteps.StandardTimeOut));
            WebDriver.Close();
            WebDriver.SwitchTo().Window(WebDriver.WindowHandles[0]);
            }

        }
    }
