﻿Feature: PortalDealerProfit

Background: I go to Portal
Given I open the Portal main page
Then I click the Login button
Then I enter my portal email and password and submit them
Then I click the Portal's Logo to get back to main

@PortalDealerProfitPecentsSilver
Scenario: PortalDealerProfitPecentsSilver
Then I click the StartFreeTrial button
Then I select the Silver option
Then I enter all Step One information for silver
Then I select a widget design on Step Two - design 9
Then I enter a domain on Step Three - 'automation.qa'
Then Then I select a 35 % for a dealer's fee for Step Four
Then I click Next on the Summary step
Then I confirm the Agreement of Step Five

Then I click the Customize button
Then I switch to Website Settings > Profit Center
Then I check that my dealer's fee percentage is set to 35 %

@PortalDealerProfitFlatFeeGold
Scenario: PortalDealerProfitFlatFeeGold
Then I click the StartFreeTrial button
Then I select the Gold option
Then I enter all Step One information for silver
Then I select a widget design on Step Two - design 9
Then I enter gold Step Three information
Then Then I select a 100 flat fee for a dealer's fee for Step Four
Then I click Next on the Summary step
Then I confirm the Agreement of Step Five

Then I click the Customize button (gold)
Then I switch to Website Settings > Profit Center
Then I check that my dealer's flat fee is set to 100