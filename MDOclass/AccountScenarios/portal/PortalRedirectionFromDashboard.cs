﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TechTalk.SpecFlow;
using Common;
using System.Threading;

namespace MDOclass.AccountScenarios.portal
    {
    [Binding]
    public class PortalRedirectionFromDashboard : BaseSteps
        {
      
        [Then(@"I open '(.*)'/'(.*)' site")]
        public void ThenIOpenSite(string p0, string p1)
            {
            if (MDOvariables.IsProductionTest == true)
                WebDriver.GetByPath("//*[contains(text(),'" + p0 + "')]/../../..//*[contains(text(),'Settings')]").Click();
            else
                WebDriver.GetByPath("//*[contains(text(),'" + p1 + "')]/../../..//*[contains(text(),'Settings')]").Click();
            }

        [Then(@"I go Back")]
        public void ThenIGoBack()
            {
            WebDriver.Navigate().Back();
            }

        [Then(@"I check the Company Information redirection from Dashboard")]
        public void ThenICheckTheCompanyInformationRedirectionFromDashboard()
            {
            WebDriver.GetByPath("//a[contains(text(),'Company Information')]").Click();
            Thread.Sleep(500);
            WebDriver.GetByPath("//*[contains(text(),'Let your customer know how they can contact you. All of the information you enter here, will be displayed on your website.')]");
            }

        [Then(@"I check the Website Settings redirection from Dashboard")]
        public void ThenICheckTheWebsiteSettingsRedirectionFromDashboard()
            {
            WebDriver.GetByPath("//a[contains(text(),'Website Settings')]").Click();
            Thread.Sleep(500);
            WebDriver.GetByPath("//*[contains(text(),'Visibility of Sections')]");
            }

        [Then(@"I check the Billing & Payments redirection from Dashboard")]
        public void ThenICheckTheBillingPaymentsRedirectionFromDashboard()
            {
            WebDriver.GetByPath("//a[contains(text(),'Billing & Payments')]").Click();
            Thread.Sleep(500);
            WebDriver.GetByPath("//*[contains(text(),'Issue / Expiration Date')]");
            }

        [Then(@"I check the Profit Center redirection from Dashboard")]
        public void ThenICheckTheProfitCenterRedirectionFromDashboard()
            {
            WebDriver.GetByPath("//a[contains(text(),'Profit Center')]").Click();
            Thread.Sleep(500);
            WebDriver.GetByPath("//*[contains(text(),'Dealer Fees')]");
            }

        [Then(@"I check the Templates & Design redirection from Dashboard")]
        public void ThenICheckTheTemplatesDesignRedirectionFromDashboard()
            {
            WebDriver.GetByPath("//a[contains(text(),'Templates & Design')]").Click();
            Thread.Sleep(500);
            WebDriver.GetByPath("//*[contains(text(),'Select a template:')]");
            }

        [Then(@"I check the Customization redirection from Dashboard")]
        public void ThenICheckTheCustomizationRedirectionFromDashboard()
            {
            WebDriver.GetByPath("//a[contains(text(),'Customization')]").Click();
            Thread.Sleep(500);
            WebDriver.GetByPath("//*[contains(text(),'About Us Section')]");
            }

        [Then(@"I check the Widget Settings from Dashboard")]
        public void ThenICheckTheWidgetSettingsFromDashboard()
            {
            WebDriver.GetByPath("//a[contains(text(),'Widget Settings')]").Click();
            Thread.Sleep(500);
            WebDriver.GetByPath("//*[contains(text(),'Client Widget Site')]");
            }

        }
    }
