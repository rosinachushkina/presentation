﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TechTalk.SpecFlow;
using Common;
using System.Threading;
using NUnit.Framework;
using OpenQA.Selenium;

namespace MDOclass.AccountScenarios.portal
    {
    [Binding]
    public class PortalDomainSettingsSteps : BaseSteps
        {
        [Then(@"I check that DomainSettings are NOT there")]
        public void ThenICheckThatDomainSettingsAreNOTThere()
            {
            Thread.Sleep(2000);
            WebDriver.SwitchTo().Window(WebDriver.WindowHandles.Last());
            WebDriver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(3));
            try
                {
                WebDriver.GetByPath("//*[contains(text(),'Domain Settings')]");
                Assert.Fail("Domain Settings should not be here!(((");
                }
            catch(WebDriverException)
            { }
            WebDriver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(BaseSteps.StandardTimeOut));
            }

        [Then(@"I switch to DomainSettings")]
        public void ThenISwitchToDomainSettings()
            {
            Thread.Sleep(2000);
            WebDriver.SwitchTo().Window(WebDriver.WindowHandles.Last());
            WebDriver.GetByPath(".//*[@id='asideMenu']/ul/li[5]/a/span").Click();
            Thread.Sleep(1000);
            }

        [Then(@"I check that I am at DomainSettings")]
        public void ThenICheckThatIAmAtDomainSettings()
            {
            WebDriver.GetByPath("//*[contains(text(),'Current Website Domains')]");
            }

        }
    }
