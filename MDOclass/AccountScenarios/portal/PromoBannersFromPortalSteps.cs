﻿using System;
using System.Threading;
using System.Windows.Forms;
using Common;
using NUnit.Framework;
using TechTalk.SpecFlow;

namespace MDOclass.AccountScenarios.portal
{
    [Binding]
    public class PromoBannersFromPortalSteps: BaseSteps
    {
        public string bannerName = "";
        public string bannerURL = "";
        public string bannerScr = "";

        [Then(@"I click on Website settings image")]
        public void ThenIClickOnWebsiteSettingsImage()
        {
            WebDriver.GetByPath("//article[@id='customization']//img").Click();
        }

        [Then(@"I open Marketing and Analysis tab")]
        public void ThenIOpenMarketingAndAnalysisTab()
        {
            WebDriver.GetByPath("//section[2]/div[2]/div[3]/ul/li[3]/a").Click();
        }

        [Then(@"I upload new Promo Banner '(.*)'")]
        public void ThenIUploadBewPromoBanner(string s)
        {
            WebDriver.GetById("i_promo_upload").Click();
            Thread.Sleep(500);
           
            Thread.Sleep(1000);
            SendKeys.SendWait(s);
            Thread.Sleep(1000);
            SendKeys.SendWait(@"{Enter}");
            Thread.Sleep(500);
        }

        [Then(@"I fill Name of banner")]
        public void ThenIFillNameOfBanner()
        {
            var Handy = new HandyFunctions();
            bannerName = Handy.RandomStringLatin(5);
            bannerURL = "www." + Handy.RandomStringLatin(5)+ ".com";

            WebDriver.GetByPath("//input[@class='item_promo_name form-control']").SendKeys(bannerName);
        }

        [Then(@"I fill URL of banner")]
        public void ThenIFillURLOfBanner()
        {
            WebDriver.GetByPath("//input[@class='item_promo_url form-control']").SendKeys(bannerURL);
        }

        [Then(@"I save scr of vanner")]
        public void ThenISaveScrOfVanner()
        {
            bannerScr = WebDriver.GetByPath("//div[@class='item_promo_content']/img").GetAttribute("src");
        }

        [Then(@"I click save banner")]
        public void ThenIClickSaveBanner()
        {
            WebDriver.GetByPath("//a[@class='btnUpdatePromo btn btn-primary btn-sm']").Click();
            Thread.Sleep(500);
        }

        [Then(@"I click save all changes of Marketing and Analysis")]
        public void ThenIClickSaveAllChangesOfMarketingAndAnalysis()
        {
            WebDriver.GetById("btn-settings-save").Click();
            Thread.Sleep(2000);
        }

        [Then(@"I check image of banner")]
        public void ThenICheckImageOfBanner()
        {
            string newBannerSrc = WebDriver.GetByPath("//div[@class='item item-banner active']/a/img").GetAttribute("src");
            newBannerSrc = newBannerSrc.Substring(newBannerSrc.IndexOf("="),
                newBannerSrc.Length - newBannerSrc.IndexOf("="));
            bannerScr = bannerScr.Substring(bannerScr.IndexOf("="),
                bannerScr.Length - bannerScr.IndexOf("="));

            
            Assert.True(bannerScr.Contains(newBannerSrc));
        }

        [Then(@"I click on banner")]
        public void ThenIClickOnBannerAndCheckCorrectRedirection()
        {
            WebDriver.GetByPath("//div[@class='item item-banner active']/a/img").Click();

        }

        [Then(@"I check correct redirection")]
        public void ThenICheckCorrectRedirection()
        {
          string s =  WebDriver.GetCurrentUrl();
          Assert.True(s.Contains(bannerURL));
        }

        [Then(@"I delete banner")]
        public void ThenIDeleteBanner()
        {
            WebDriver.GetByPath("//a[@class='btnRemovePromo btn btn-danger btn-sm']").Click();
            WebDriver.SwitchTo().Alert().Accept();
        }

    }
}
