﻿Feature: PortalDomainSettings
	
	@PortalDomainSettingsGold
	Scenario: PortalDomainSettingsGold
	Given I open the Portal main page
	Then I click the Login button
	Then I enter my portal email and password and submit them
    Then I open 'combatgold.myvirtualinventory.com'/'combatgold.st.myvirtualinventory.com' site

	Then I check the Company Information redirection from Dashboard
	Then I switch to DomainSettings
	Then I check that I am at DomainSettings

	@PortalDomainSettingsSilver
    Scenario: PortalDomainSettingsSilver
	Given I open the Portal main page
	Then I click the Login button
	Then I enter my portal email and password and submit them
    Then I open 'combatsilver.myvirtualinventory.com'/'combatsilver.st.myvirtualinventory.com' site

	Then I check the Company Information redirection from Dashboard
	Then I check that DomainSettings are NOT there