﻿Feature: PortalWebDesign
	
@PortalWebDesign
Scenario: PortalWebDesign
	Given I open the Portal main page
	Then I click the Login button
	Then I enter my portal email and password for AdditionalPages account and submit them
	Then I open AdditionalPagesSite settings
	Then I click the Customization Icon
	Then I switch to Customization>WebsiteDesign

	Then I switch the 'Site Color' using a random value and remember color_s value
	Then I refresh the page
	Then I switch the 'Site Color #2' using a random value and remember color_s value
	Then I refresh the page
	Then I switch the 'Menu Color' using a random value and remember color_s value

	Then I open a new tab and switch there
    Then I open our AdditionalPagesWebsite
	Then My selected design color should be refrected there