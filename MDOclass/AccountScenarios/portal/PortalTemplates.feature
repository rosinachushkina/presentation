﻿Feature: PortalTemplates

@PortalTemplates
Scenario: PortalTemplates
	Given I open the Portal main page
	Then I click the Login button
	Then I enter my portal email and password for AdditionalPages account and submit them

	Then I open AdditionalPagesSite settings
	Then I click the Customization Icon
	Then I switch to Website>Templates
	Then I switch the template to 'mint'

	Then I open a new tab and switch there
	Then I open our AdditionalPagesWebsite
	Then I check that the website template is set to the selected template

	Then I switch to tab 1
	Then I switch the template to 'mini'
	Then I switch to tab 2
	Then I refresh the page
	Then I check that the website template is set to the selected template

	Then I switch to tab 1
	Then I switch the template to 'Blue Vista'
	Then I switch to tab 2
	Then I refresh the page
	Then I check that the website template is set to the selected template