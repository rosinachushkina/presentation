﻿using System;
using System.Threading;
using Common;
using NUnit.Framework;
using OpenQA.Selenium.Support.UI;
using TechTalk.SpecFlow;

namespace MDOclass.AccountScenarios
{
    [Binding]
    public class ContactUsPortalSteps : BaseSteps
    {
        public string country = "";
        public string messageText = "";
        public string countryCode = "UA";

        [Then(@"I click on Contact Us page")]
        public void ThenIClickOnContactUsPage()
        {
            WebDriver.GetById("ucHeader_hplContact").Click();
        }

        [Then(@"I type Full Name \(ContactUs Page\)")]
        public void ThenITypeFullNameContactUsPage()
        {
            WebDriver.GetById("txtCustomerName").SendKeys(MDOvariables.CustomerFirstname);
        }

        [Then(@"I type Email Address \(ContactUs Page\)")]
        public void ThenITypeEmailAddressContactUsPage()
        {
            WebDriver.GetById("txtCustomerEmail").SendKeys(MDOvariables.CustomerLogin);
        }

        [Then(@"I type Phone Number \(ContactUs Page\)")]
        public void ThenITypePhoneNumberContactUsPage()
        {
            WebDriver.GetById("txtCustomerPhone").SendKeys(MDOvariables.CustomerPhone);
        }

        [Then(@"I choose (.*) country \(ContactUs Page\)")]
        public void ThenIChooseUkraineCountryContactUsPage(string country1)
        {
            country1 = country;

            var countrySelector =
            new SelectElement(WebDriver.GetById("ddlCustomerCountry"));
            countrySelector.SelectByValue(countryCode);
        }

        [Then(@"I type message to support \(ContactUs Page\)")]
        public void ThenITypeMessageToSupportContactUsPage()
        {
            var Handy = new HandyFunctions();
            messageText = Handy.RandomStringLatinAndNumbers(10);
            WebDriver.GetById("txtMessage").SendKeys(messageText);
        }

        [Then(@"I click Submit button \(ContactUs Page\)")]
        public void ThenIClickSubmotButtonContactUsPage()
        {
            WebDriver.GetByPath("//button[contains(text(), 'Submit')]").Click();
        }

        [Then(@"I see message about successful sending \(ContactUs Page\)")]
        public void ThenISeeMessageAboutSuccessfulSendingContactUsPage()
        {
            Thread.Sleep(500);
            string s = WebDriver.GetById("divFeedBackSuccess").Text;
            Assert.True(s.Contains("Thank you for your feedback!"));
        }

        [Then(@"I switch to Feedback tab")]
        public void ThenISwitchToFeedbackTab()
        {
            WebDriver.GetByPath("//ul/li[7]/a/span[contains(text(), 'Feedback')]").Click();
        }

        [Then(@"I check that feedback displayed correct")]
        public void ThenICheckThatFeedbackDisplayedCorrect()
        {
            Thread.Sleep(500);
            string dateInBackoffice = WebDriver.GetByPath("//table/tbody/tr[1]/td[1]").Text;
            DateTime dateInBackoffice1 = Convert.ToDateTime(dateInBackoffice);
            Assert.True(dateInBackoffice1 == DateTime.Today); //проверка что фидбэк отправлен сегодня
            Assert.True(WebDriver.GetByPath("//table/tbody/tr[1]/td[2][contains(text(), '" + MDOvariables.CustomerFirstname + "')]").Enabled); //проверка имени оправителя
            Assert.True(WebDriver.GetByPath("//table/tbody/tr[1]/td[3][contains(text(), '" + country + "')]").Enabled); //проверка страны
            Assert.True(WebDriver.GetByPath("//table/tbody/tr[1]/td[4][contains(text(), 'UA')]").Enabled); //проверка кода страны
            Assert.True(WebDriver.GetByPath("//table/tbody/tr[1]/td[5][contains(text(), '" + MDOvariables.CustomerLogin + "')]").Enabled); //проверка email
            Assert.True(WebDriver.GetByPath("//table/tbody/tr[1]/td[6]/span[contains(text(), '" + messageText + "')]").Enabled); //проверка сообщения
        }


    }
}
