﻿using System;
using Common;
using NUnit.Framework;
using OpenQA.Selenium;
using TechTalk.SpecFlow;

namespace MDOclass.AccountScenarios.portal
{
    [Binding]
    public class SocialIconsForLoginSteps: BaseSteps
    {
        [Then(@"I click on website settings icon")]
        public void ThenIClickOnWebsiteSettingsIcon()
        {
            WebDriver.GetByPath("//article[@id='customization']//img").Click();
        }

        [Then(@"I click on checkboxes of social authorization")]
        public void ThenIDisableSocialAuthorization()
        {
           WebDriver.GetByPath("//span[contains(text(), 'Facebook')]/preceding-sibling::*").Click();
           WebDriver.GetByPath("//span[contains(text(), 'Twitter')]/preceding-sibling::*").Click();
           WebDriver.GetByPath("//span[contains(text(), 'Google')]/preceding-sibling::*").Click();
        }

        [Then(@"I check that icons of social authorization aren't enable")]
        public void ThenICheckThatIconsOfSocialAuthorizationArenTEnable()
        {
            try
            {
                WebDriver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(2));
                WebDriver.GetByPath("//ul[@class='social-icons pull-right']/li/a");
                WebDriver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(BaseSteps.StandardTimeOut));
                Assert.Fail("Buttons for Social Authorization are displayed");
                
            }
            catch (WebDriverException)
            {
            }
        }

        [Then(@"I check that icons of social authorization are enabled")]
        public void ThenICheckThatIconsOfSocialAuthorizationAreEnabled()
        {
            int s = WebDriver.FindElements(By.XPath("//ul[@class='social-icons pull-right']/li/a")).Count;
            Assert.True(s==3);
        }

    }
}
