﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TechTalk.SpecFlow;
using Common;
using OpenQA.Selenium.Support.UI;
using System.Threading;
using NUnit.Framework;

namespace MDOclass.AccountScenarios.portal
    {
    [Binding]
    public class PortalDealerProfitSteps : BaseSteps
        {
        [Given(@"I open the Portal main page")]
        public void GivenIOpenThePortalMainPage()
            {
            if(MDOvariables.IsProductionTest==true)
            WebDriver.Url = Pages.Portal;
            else
            WebDriver.Url = Pages.PortalSt;
            }

        [Then(@"I click the Login button")]
        public void ThenIClickTheLoginButton()
            {
            WebDriver.GetByPath("//a[@href='/login']").Click();
            }

        [Then(@"I enter my portal email and password and submit them")]
        public void ThenIEnterMyPortalEmailAndPasswordAndSubmitThem()
            {
            WebDriver.GetByPath("//*[@id='CPH_txtDE']").SendKeys(MDOvariables.PortalLogin);
            WebDriver.GetByPath("//*[@id='CPH_txtPW']").SendKeys(MDOvariables.PortalPassword);
            WebDriver.GetByPath("//*[@id='CPH_btnSignIn']").Click();
            }

        [Then(@"I click the Portal's Logo to get back to main")]
        public void ThenIClickThePortalSLogoToGetBackToMain()
            {
            WebDriver.GetByPath("//a[@class='navbar-brand']").Click();
            }

        [Then(@"I click the StartFreeTrial button")]
        public void ThenIClickTheStartFreeTrialButton()
            {
            WebDriver.GetByPath("//a[@class='btn btn-warning'][contains(text(),'Start FREE Trial')]").Click();
            }

        [Then(@"I select the Silver option")]
        public void ThenISelectTheSilverOption()
            {
            WebDriver.GetByPath("//*[@id='packages']/article[1]//button").Click();
            }

        [Then(@"I enter all Step One information for silver")]
        public void ThenIEnterAllStepOneInformationForSilver()
            {
            WebDriver.GetByPath("//*[@id='txtPhoneNumber']").SendKeys(MDOvariables.PortalPhoneNumber);
            WebDriver.GetByPath("//*[@id='CPH_txtCompanyName']").SendKeys(MDOvariables.PortalCompanyName);
            WebDriver.GetByPath("//*[@id='CPH_txtFaxNumber']").SendKeys(MDOvariables.PortalFaxNumber);
            WebDriver.GetByPath("//*[@id='CPH_txtAddress1']").SendKeys(MDOvariables.PortalAddress1);
            WebDriver.GetByPath("//*[@id='txtCity']").SendKeys(MDOvariables.PortalCity);
            WebDriver.GetByPath("//*[@id='txtZipCode']").SendKeys(MDOvariables.PortalZIP);

            var selector = new SelectElement(WebDriver.GetByPath("//*[@id='ddCountry']"));
            selector.SelectByText(MDOvariables.PortalCountry);

            WebDriver.GetByPath("//*[@id='CPH_btnNext']").Click();
            }

        [Then(@"I select a widget design on Step Two - design (.*)")]
        public void ThenISelectAWidgetDesignOnStepTwo_Design(int p0)
            {
            WebDriver.GetByPath("//*[@id='CPH_rWebSiteTemplate_lbTemplate_"+(p0-1)+"']").Click();
            }


        [Then(@"I enter a domain on Step Three - '(.*)'")]
        public void ThenIEnterADomainOnStepThree_(string p0)
            {
            WebDriver.GetByPath("//*[@id='txtExistingtUrlWidget']").SendKeys(MDOvariables.PortalDomain);
            WebDriver.GetByPath("//*[@id='CPH_btnNextDealerProfileUrl']").Click();
            }

        [Then(@"Then I select a (.*) % for a dealer's fee for Step Four")]
        public void ThenThenISelectAForADealerSFeeForStepFour(int p0)
            {
            if (!WebDriver.GetByPath("//*[@id='dealerFeePercent']").Selected)
                WebDriver.GetByPath("//*[@id='dealerFeePercent']").Click();

            WebDriver.GetByPath("//*[@id='dealerFeeValue']").Clear();
            WebDriver.GetByPath("//*[@id='dealerFeeValue']").SendKeys(p0.ToString());
            WebDriver.GetByPath("//*[@id='CPH_btnDealerFeesNext']").Click();
            }

        [Then(@"Then I select a (.*) flat fee for a dealer's fee for Step Four")]
        public void ThenThenISelectAFlatFeeForADealerSFeeForStepFour(int p0)
            {
            if (!WebDriver.GetByPath("//*[@id='dealerFeeAmount']").Selected)
                WebDriver.GetByPath("//*[@id='dealerFeeAmount']").Click();

            WebDriver.GetByPath("//*[@id='dealerFeeValue']").Clear();
            WebDriver.GetByPath("//*[@id='dealerFeeValue']").SendKeys(p0.ToString());
            WebDriver.GetByPath("//*[@id='CPH_btnDealerFeesNext']").Click();
            }

        [Then(@"I click Next on the Summary step")]
        public void ThenIClickNextOnTheSummaryStep()
            {
            WebDriver.GetByPath("//*[@id='CPH_Button4']").Click();
            Thread.Sleep(500);
            }

        [Then(@"I confirm the Agreement of Step Five")]
        public void ThenIConfirmTheAgreementOfStepFive()
            {
            WebDriver.GetByPath("//*[@id='CPH_btnNextDealerProfileAgreement']").Click();
            }

        [Then(@"I click the Customize button")]
        public void ThenIClickTheCustomizeButton()
            {
            WebDriver.GetByPath("//*[@id='CPH_hlCustomizeWidget']").Click();
            }

        [Then(@"I switch to Website Settings > Profit Center")]
        public void ThenISwitchToWebsiteSettingsProfitCenter()
            {
            Thread.Sleep(2000);
            WebDriver.SwitchTo().Window(WebDriver.WindowHandles.Last());
            WebDriver.GetByPath(".//*[@id='asideMenu']/ul/li[4]/a/span").Click();
            Thread.Sleep(500);
            WebDriver.GetByPath("//*[@id='asideMenu']/ul/li[4]/ul/li[1]/a").Click();
            }

        [Then(@"I check that my dealer's fee percentage is set to (.*) %")]
        public void ThenICheckThatMyDealerSFeePercentageIsSetTo(int p0)
            {
            Assert.IsTrue(WebDriver.GetByPath("//*[@id='dealerFeePercent']").Selected);
            Assert.IsTrue(Convert.ToInt32(WebDriver.GetByPath("//*[@id='dealerFeeValue']").GetAttribute("value"))==p0);
            }

        [Then(@"I select the Gold option")]
        public void ThenISelectTheGoldOption()
            {
            WebDriver.GetByPath("//*[@id='packages']/article[2]//button").Click();
            }

        [Then(@"I enter gold Step Three information")]
        public void ThenIEnterGoldStepThreeInformation()
            {
            //WebDriver.GetByPath("//*[@id='CPH_txtSubDomain']").
            //    SendKeys(MDOvariables.PortalDomain.Substring(0,MDOvariables.PortalDomain.Length-3));
            var h = new HandyFunctions();
            string s = h.RandomStringLatin(20);
            WebDriver.GetByPath("//*[@id='CPH_txtSubDomain']").
                SendKeys(s);
            WebDriver.GetByPath("//*[@id='CPH_btnNextDealerProfileUrl']").Click();
            }

        [Then(@"I click the Customize button \(gold\)")]
        public void ThenIClickTheCustomizeButtonGold()
            {
            WebDriver.GetByPath("//*[@id='CPH_hlCustomizeMySite']").Click();
            }

        [Then(@"I check that my dealer's flat fee is set to (.*)")]
        public void ThenICheckThatMyDealerSFlatFeeIsSetTo(int p0)
            {
            Assert.IsTrue(WebDriver.GetByPath("//*[@id='dealerFeeAmount']").Selected);
            Assert.IsTrue(Convert.ToInt32(WebDriver.GetByPath("//*[@id='dealerFeeValue']").GetAttribute("value")) == p0);
            }


        }
    }
