﻿Feature: PortalRedirectionFromDashboard
	
@PortalRedirectionFromDashboard
Scenario: PortalRedirectionFromDashboard
	Given I open the Portal main page
	Then I click the Login button
	Then I enter my portal email and password for AdditionalPages account and submit them
    Then I open 'combatsilver.myvirtualinventory.com'/'combatsilver.st.myvirtualinventory.com' site

	Then I check the Company Information redirection from Dashboard
	Then I go Back
	Then I check the Website Settings redirection from Dashboard
	Then I go Back
	Then I check the Billing & Payments redirection from Dashboard
	Then I go Back
	Then I check the Profit Center redirection from Dashboard
	Then I go Back
	Then I check the Templates & Design redirection from Dashboard
	Then I go Back
	Then I check the Customization redirection from Dashboard
	Then I go Back
	Then I check the Widget Settings from Dashboard