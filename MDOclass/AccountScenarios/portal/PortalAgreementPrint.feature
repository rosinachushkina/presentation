﻿Feature: PortalAgreementPrint
	
Background: I go to Portal
Given I open the Portal main page
Then I click the Login button
Then I enter my portal email and password and submit them
Then I click the Portal's Logo to get back to main

@PortalAgreementPrintSilver
Scenario: PortalAgreementPrintSilver
	Then I click the StartFreeTrial button
    Then I select the Silver option
    Then I enter all Step One information for silver
    Then I select a widget design on Step Two - design 9
    Then I enter a domain on Step Three - ''
    Then Then I select a 35 % for a dealer's fee for Step Four
    Then I click Next on the Summary step
	Then I check the Print button for agreement - 'Silver'

@PortalAgreementPrintGold
Scenario: PortalAgreementPrintGold
Then I click the StartFreeTrial button
Then I select the Gold option
Then I enter all Step One information for silver
Then I select a widget design on Step Two - design 9
Then I enter gold Step Three information
Then Then I select a 100 flat fee for a dealer's fee for Step Four
Then I click Next on the Summary step
Then I check the Print button for agreement - 'Gold'