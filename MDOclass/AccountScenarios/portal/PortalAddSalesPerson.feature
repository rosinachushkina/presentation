﻿Feature: PortalAddSalesPerson
	
@PortalAddSalesPerson
Scenario: PortalAddSalesPerson
	Given I open the Portal main page
	Then I click the Login button
	Then I enter my portal email and password for AdditionalPages account and submit them

	Then I open AdditionalPagesSite settings
	Then I click on company information button
	Then I switch to CompanyInformation > EmployeersAndSalesTeam
	Then I add a new sales person (with all checkboxes enabled)
	Then I check that this sales person was added here in portal

	Then I click on preview site button
	Then I switch to last tab
	Then I check that my new sales person is shown on the website main

	Then I switch to tab 1
	Then I click the Edit button for sales person number 2
	Then I change all information for this sales person and Save it
	Then I switch to tab 2
	Then I refresh the page
	Then I check that my new sales person is shown on the website main

	Then I switch to tab 1
	Then I Delete sales person number 2
	Then I refresh the page
	Then I check that I have only 1 sales person left