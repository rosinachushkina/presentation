﻿Feature: GeneralSettingsChanging
	In order to avoid silly mistakes
	As a math idiot
	I want to be told the sum of two numbers

@GeneralInfoChanging
Scenario: Changing general information on portal site
Given I open the Portal main page
Then I click the Login button
Then I enter my portal email and password for AdditionalPages account and submit them
Then I open AdditionalPagesSite settings
Then I click on company information button

Then I type random Company name
Then I type random Address1
Then I type random Address2
Then I type random City
Then I type random Postal Code
Then I type random Phone Number
Then I type random Phone Number 2
Then I type random Fax Number
Then I type random Email address
Then I choose Austria country
Then I type random hours of operation
Then I save changes of general information

Then I click on preview site button
Then I switch on next window

Then I check random Company name
Then I check random Address1
Then I check random Address2
Then I check random City
Then I check random Postal Code
Then I check random Phone Number
Then I check random Phone Number2
Then I check random Fax Number
Then I check random Email address
Then I check Austria country
Then I check random hours of operation

Given I open the Portal main page
Then I open my websites
Then I open AdditionalPagesSite settings
Then I click on company information button

Then I type old Company name
Then I type old Address1
Then I type old Address2
Then I type old City
Then I type old Postal Code
Then I type old Phone Number
Then I type old Phone Number 2
Then I type old Fax Number
Then I type old Email address
Then I choose Ukraine country
Then I type old hours of operation
Then I save changes of general information