﻿Feature: PortalLinksFromMain
	
@PortalLinksFromMain
Scenario: PortalLinksFromMain
	Given I open the Portal main page
	Then I check the HowTo link
	Then I click the Portal's Logo to get back to main
	Then I check the FAQ link
	Then I click the Portal's Logo to get back to main
	Then I check the ContactUs link
	Then I click the Portal's Logo to get back to main
	Then I check the Facebook link
	Then I go Back
	Then I check the Twitter link
	Then I go Back
	Then I check the G+ link
	Then I go Back
	Then I check the ContactUs button
	Then I click the Portal's Logo to get back to main
	Then I check the PrivatePolicy link
	Then I check the TermsOfUse link
	Then I check the ContactUs footer link
	Then I click the Portal's Logo to get back to main
	Then I check 'SILVER' SeeAllFeatures link
	Then I click the Portal's Logo to get back to main
	Then I check 'GOLD' SeeAllFeatures link
	Then I click the Portal's Logo to get back to main
	Then I check the AppStore link
	Then I go Back
	Then I check the GooglePlay link