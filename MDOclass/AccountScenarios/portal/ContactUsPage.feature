﻿Feature: ContactUsPortal
 In order to check Contact Us page
 I send message to support

@ContactUsPortal
Scenario: Checking Contact Us on Portal
 Given I open the Portal main page
 Then I click on Contact Us page
 Then I type Full Name (ContactUs Page)
 Then I type Email Address (ContactUs Page)
 Then I type Phone Number (ContactUs Page)
 Then I choose Ukraine country (ContactUs Page)
 Then I type message to support (ContactUs Page)
 Then I click Submit button (ContactUs Page)
 Then I see message about successful sending (ContactUs Page)

 Given I open main Backoffice page
 Then I type Minhs Login and password and submit them
 Then I switch to Feedback tab
 Then I check that feedback displayed correct