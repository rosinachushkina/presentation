﻿using System;
using System.Linq;
using System.Threading;
using Common;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using TechTalk.SpecFlow;

namespace MDOclass.AccountScenarios.portal
{
    [Binding]
    public class GeneralSettingsChangingSteps : BaseSteps
    {
        public string randomCompanyName = "";
        public string randomAddress1 = "";
        public string randomAddress2 = "";
        public string randomCity = "";
        public string randomPostalCode = "";
        public string randomPhoneNumber = "";
        public string randomPhoneNumber2 = "";
        public string randomFaxNumber = "";
        public string randomEmail = "";
        public string randomTime1 = "";
        public string randomTime2 = "";

        public string oldCompanyName = "TSB";
        public string oldAddress1 = "Frunze 160";
        public string oldAddress2 = "Khreshatik 14";
        public string oldCity = "Kyiw";
        public string oldPostalCode = "03057";
        public string oldPhoneNumber = "66324";
        public string oldPhoneNumber2 = "00357";
        public string oldFaxNumber = "454648";
        public string oldEmailAddress = "tsbit@tsbua.com";
        public string oldTime = "10-14 a.m.";




        [Then(@"I click on settings second site on my websites page")]
        public void ThenIChooseSecondSiteOnMyWebsitesPage()
        {
            WebDriver.GetByPath("//div[2]/div/div[3]/span[2]/a").Click();
        }

        [Then(@"I click on company information button")]
        public void ThenIClickOnCompanyInformationButton()
        {
            WebDriver.GetByPath("//article[@id='companyInfo']//img").Click();
        }

        [Then(@"I type random Company name")]
        public void ThenITypeRandomCompanyName()
        {
            var Handy = new HandyFunctions();
            randomCompanyName = Handy.RandomStringLatinAndNumbers(10);
            WebDriver.GetById("CPH_ucCompanyInfoOpenSite_txtCompanyName").Clear();
            WebDriver.GetById("CPH_ucCompanyInfoOpenSite_txtCompanyName").SendKeys(randomCompanyName);
        }

        [Then(@"I type random Address1")]
        public void ThenITypeRandomAddress1()
        {
            var Handy = new HandyFunctions();
            randomAddress1 = Handy.RandomStringLatinAndNumbers(10);
            WebDriver.GetById("CPH_ucCompanyInfoOpenSite_txtAddress1").Clear();
            WebDriver.GetById("CPH_ucCompanyInfoOpenSite_txtAddress1").SendKeys(randomAddress1);
        }

        [Then(@"I type random Address2")]
        public void ThenITypeRandomAddress2()
        {
            var Handy = new HandyFunctions();
            randomAddress2 = Handy.RandomStringLatinAndNumbers(10);
            WebDriver.GetById("CPH_ucCompanyInfoOpenSite_txtAddress2").Clear();
            WebDriver.GetById("CPH_ucCompanyInfoOpenSite_txtAddress2").SendKeys(randomAddress2);
        }


        [Then(@"I type random City")]
        public void ThenITypeRandomCity()
        {
            var Handy = new HandyFunctions();
            randomCity = Handy.RandomStringLatinAndNumbers(10);
            WebDriver.GetById("txtCity").Clear();
            WebDriver.GetById("txtCity").SendKeys(randomCity);
        }

        [Then(@"I type random Postal Code")]
        public void ThenITypeRandomPostalCode()
        {
            var Handy = new HandyFunctions();
            randomCity = Handy.RandomStringNumbers(5);
            WebDriver.GetById("txtZipCode").Clear();
            WebDriver.GetById("txtZipCode").SendKeys(randomCity);
        }

        [Then(@"I type random Phone Number")]
        public void ThenITypeRandomPhoneNumber()
        {
            var Handy = new HandyFunctions();
            randomPhoneNumber = Handy.RandomStringNumbers(5);
            WebDriver.GetById("CPH_ucCompanyInfoOpenSite_txtPhoneNumber").Clear();
            WebDriver.GetById("CPH_ucCompanyInfoOpenSite_txtPhoneNumber").SendKeys(randomPhoneNumber);
        }

        [Then(@"I type random Phone Number 2")]
        public void ThenITypeRandomPhoneNumber2()
        {
            var Handy = new HandyFunctions();
            randomPhoneNumber2 = Handy.RandomStringNumbers(5);
            WebDriver.GetById("CPH_ucCompanyInfoOpenSite_txtPhoneNumber2").Clear();
            WebDriver.GetById("CPH_ucCompanyInfoOpenSite_txtPhoneNumber2").SendKeys(randomPhoneNumber2);
        }

        [Then(@"I type random Fax Number")]
        public void ThenITypeRandomFaxNumber()
        {
            var Handy = new HandyFunctions();
            randomFaxNumber = Handy.RandomStringNumbers(5);
            WebDriver.GetById("CPH_ucCompanyInfoOpenSite_txtFaxNumber").Clear();
            WebDriver.GetById("CPH_ucCompanyInfoOpenSite_txtFaxNumber").SendKeys(randomFaxNumber);
        }

        [Then(@"I type random Email address")]
        public void ThenITypeRandomEmailAddress()
        {
            var Handy = new HandyFunctions();
            randomEmail = Handy.RandomStringNumbers(5) + "@tsbua.com";
            WebDriver.GetById("CPH_ucCompanyInfoOpenSite_txtEmail").Clear();
            WebDriver.GetById("CPH_ucCompanyInfoOpenSite_txtEmail").SendKeys(randomEmail);
        }

        [Then(@"I save changes of general information")]
        public void ThenISaveChangesOfGeneralInformation()
        {
            WebDriver.GetById("btn-ci-save").Click();
            Thread.Sleep(2000);
        }

        [Then(@"I click on preview site button")]
        public void ThenIClickOnPreviewSiteButton()
        {
            WebDriver.GetByPath("//section[2]/div[1]/a").Click();
        }

        [Then(@"I choose Austria country")]
        public void ThenIChooseAustriaCountry()
        {
            var countrySelect = new SelectElement(WebDriver.GetById("ddCountry"));
            countrySelect.SelectByValue("AT");
        }

        [Then(@"I type random hours of operation")]
        public void ThenITypeRandomHoursOfOperation()
        {
            var Handy = new HandyFunctions();
            randomTime1 = Handy.RandomStringNumbers(2) + "-";
            
            WebDriver.GetById("CPH_ucCompanyInfoOpenSite_txtOpenHours").Clear();
            WebDriver.GetById("CPH_ucCompanyInfoOpenSite_txtOpenHours").SendKeys(randomTime1);
        }

        [Then(@"I check random Company name")]
        public void ThenICheckRandomCompanyName()
        {
            Assert.True(WebDriver.GetByPath("//div/div[2]//*[contains(text(), '"+randomCompanyName+"')]").Enabled);
        }

        [Then(@"I check random Address1")]
        public void ThenICheckRandomAddress()
        {
            Assert.True(WebDriver.GetByPath("//div/div/div[1]//*[contains(text(), '"+randomAddress1+"')]").Enabled);
        }

        [Then(@"I check random Address2")]
        public void ThenICheckRandomAddress2()
        {
            Assert.True(WebDriver.GetByPath("//div/div/div[1]//*[contains(text(), '" + randomAddress2 + "')]").Enabled);
        }

        [Then(@"I check random City")]
        public void ThenICheckRandomCity()
        {
            Assert.True(WebDriver.GetByPath("//div/div/div[1]//*[contains(text(), '" + randomCity + "')]").Enabled);
        }

        [Then(@"I check random Postal Code")]
        public void ThenICheckRandomPostalCode()
        {
            Assert.True(WebDriver.GetByPath("//div/div/div[1]//*[contains(text(), '" + randomPostalCode + "')]").Enabled);
        }

        [Then(@"I check random Phone Number")]
        public void ThenICheckRandomPhoneNumber()
        {
            Assert.True(WebDriver.GetByPath("//span[contains(text(), '"+randomPhoneNumber+"')]").Enabled);
        }

        [Then(@"I check random Phone Number2")]
        public void ThenICheckRandomPhoneNumber2()
        {
            Assert.True(WebDriver.GetByPath("//p[contains(text(), '" + randomPhoneNumber2 + "')]").Enabled);
        }

        [Then(@"I check random Fax Number")]
        public void ThenICheckRandomFaxNumber()
        {
            Assert.True(WebDriver.GetByPath("//p[4][contains(text(), '" + randomFaxNumber + "')]").Enabled);
        }

        [Then(@"I check random Email address")]
        public void ThenICheckRandomEmailAddress()
        {
            Assert.True(WebDriver.GetByPath("//p[5][contains(text(), '" + randomEmail+ "')]").Enabled);
        }

        [Then(@"I check Austria country")]
        public void ThenICheckAustriaCountry()
        {
       string text = WebDriver.GetByPath("html/body/div[1]/div/div/div[1]/p[7]").Text;
            Assert.IsTrue(text.Contains("Austria"));
        }

        [Then(@"I check random hours of operation")]
        public void ThenICheckRandomHoursOfOperation()
        {
            string text = WebDriver.GetByPath("html/body/div[1]/div/div/div[1]/p[7]").Text;
            Assert.IsTrue(text.Contains(randomTime1));
       }


        [Then(@"I open my websites")]
        public void ThenIOpenMyWebsites()
        {
            WebDriver.GetById("ucHeader_hpMyWebSites").Click();
        }
        [Then(@"I type old Company name")]
        public void ThenITypeOldCompanyName()
        {
            WebDriver.GetById("CPH_ucCompanyInfoOpenSite_txtCompanyName").Clear();
            WebDriver.GetById("CPH_ucCompanyInfoOpenSite_txtCompanyName").SendKeys(oldCompanyName);
        }

        [Then(@"I type old Address1")]
        public void ThenITypeOldAddress1()
        {
            WebDriver.GetById("CPH_ucCompanyInfoOpenSite_txtAddress1").Clear();
            WebDriver.GetById("CPH_ucCompanyInfoOpenSite_txtAddress1").SendKeys(oldAddress1);
        }

        [Then(@"I type old Address2")]
        public void ThenITypeOldAddress2()
        {
            WebDriver.GetById("CPH_ucCompanyInfoOpenSite_txtAddress2").Clear();
            WebDriver.GetById("CPH_ucCompanyInfoOpenSite_txtAddress2").SendKeys(oldAddress2);
        }

        [Then(@"I type old City")]
        public void ThenITypeOldCity()
        {
            WebDriver.GetById("txtCity").Clear();
            WebDriver.GetById("txtCity").SendKeys(oldCity);
        }

        [Then(@"I type old Postal Code")]
        public void ThenITypeOldPostalCode()
        {
            WebDriver.GetById("txtZipCode").Clear();
            WebDriver.GetById("txtZipCode").SendKeys(oldCity);
        }

        [Then(@"I type old Phone Number")]
        public void ThenITypeOldPhoneNumber()
        {
            WebDriver.GetById("CPH_ucCompanyInfoOpenSite_txtPhoneNumber").Clear();
            WebDriver.GetById("CPH_ucCompanyInfoOpenSite_txtPhoneNumber").SendKeys(oldPhoneNumber);
        }

        [Then(@"I type old Phone Number 2")]
        public void ThenITypeOldPhoneNumbe2()
        {
            WebDriver.GetById("CPH_ucCompanyInfoOpenSite_txtPhoneNumber2").Clear();
            WebDriver.GetById("CPH_ucCompanyInfoOpenSite_txtPhoneNumber2").SendKeys(oldPhoneNumber2);
        }

        [Then(@"I type old Fax Number")]
        public void ThenITypeOldFaxNumber()
        {
            WebDriver.GetById("CPH_ucCompanyInfoOpenSite_txtFaxNumber").Clear();
            WebDriver.GetById("CPH_ucCompanyInfoOpenSite_txtFaxNumber").SendKeys(oldFaxNumber);
        }

        [Then(@"I type old Email address")]
        public void ThenITypeOldEmailAddress()
        {
            WebDriver.GetById("CPH_ucCompanyInfoOpenSite_txtEmail").Clear();
            WebDriver.GetById("CPH_ucCompanyInfoOpenSite_txtEmail").SendKeys(oldEmailAddress);
        }

        [Then(@"I choose Ukraine country")]
        public void ThenIChooseUkraineCountry()
        {
            var countrySelect = new SelectElement(WebDriver.GetById("ddCountry"));
            countrySelect.SelectByValue("UA");
        }

        [Then(@"I type old hours of operation")]
        public void ThenITypeOldHoursOfOperation()
        {
            WebDriver.GetById("CPH_ucCompanyInfoOpenSite_txtOpenHours").Clear();
            WebDriver.GetById("CPH_ucCompanyInfoOpenSite_txtOpenHours").SendKeys(oldTime);
        }



        [Then(@"I switch on next window")]
        public void ThenISwithOnNextWindow()
        {
            WebDriver.SwitchTo().Window(WebDriver.WindowHandles.Last());
        }

    }
}
