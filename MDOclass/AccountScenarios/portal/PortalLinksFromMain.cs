﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TechTalk.SpecFlow;
using Common;
using System.Threading;
using NUnit.Framework;
using System.Windows.Forms;

namespace MDOclass.AccountScenarios.portal
    {
    [Binding]
    public class PortalLinksFromMain : BaseSteps
        {
        [Then(@"I check the HowTo link")]
        public void ThenICheckTheHowToLink()
            {
            WebDriver.GetByPath("//*[@id='ucHeader_hplHowTo2']").Click();
            Thread.Sleep(1000);
            WebDriver.GetByPath("//*[contains(text(),'Congratulations on your new website! Now what????')]");
            }

        [Then(@"I check the FAQ link")]
        public void ThenICheckTheFAQLink()
            {
            WebDriver.GetByPath("//*[@id='ucHeader_hplFAQ3']").Click();
            Thread.Sleep(1000);
            WebDriver.GetByPath("//*[contains(text(),'Frequently Asked Questions')]");
            }

        [Then(@"I check the ContactUs link")]
        public void ThenICheckTheContactUsLink()
            {
            WebDriver.GetByPath("//*[@id='ucHeader_hplContact']").Click();
            Thread.Sleep(1000);
            WebDriver.GetByPath("//*[contains(text(),'Customer Service')]");
            }

        [Then(@"I check the Facebook link")]
        public void ThenICheckTheFacebookLink()
            {
            WebDriver.GetByPath("//div[@id='socialNetworks']/a[1]").Click();
            Thread.Sleep(1000);
            string s = WebDriver.GetByPath("//div[@class='fsxl fwb']").Text;
            Assert.IsTrue(s.Contains("My Dealer Online"));
            Assert.IsTrue(s.Contains("is on Facebook."));
            }

        [Then(@"I check the Twitter link")]
        public void ThenICheckTheTwitterLink()
            {
            WebDriver.GetByPath("//div[@id='socialNetworks']/a[2]").Click();
            Thread.Sleep(2000);
            SendKeys.SendWait(@"{Escape}");
            Thread.Sleep(500);
            Assert.IsTrue(WebDriver.GetByPath("//*[@class='ProfileHeaderCard-name']/a").
                Text.Contains("My Dealer Online"));
            }

        [Then(@"I check the G\+ link")]
        public void ThenICheckTheGLink()
            {
            WebDriver.GetByPath("//div[@id='socialNetworks']/a[3]").Click();
            Thread.Sleep(1000);
            Assert.IsTrue(WebDriver.GetByPath("//*[@guidedhelpid='profile_name']").
                Text == "MyDealerOnline");
            }

        [Then(@"I check the ContactUs button")]
        public void ThenICheckTheContactUsButton()
            {
            WebDriver.GetByPath("//div[@class='carousel-inner']//a[@href='/ContactUs.aspx'][@class='btn']").Click();
            Thread.Sleep(1000);
            WebDriver.GetByPath("//*[contains(text(),'Customer Service')]");
            }

        [Then(@"I check the PrivatePolicy link")]
        public void ThenICheckThePrivatePolicyLink()
            {
            WebDriver.GetByPath("//*[@id='ucFooter_hplPrivacy']").Click();
            Thread.Sleep(1000);
            WebDriver.GetByPath("//*[contains(text(),'This Privacy Policy describes the information')]");
            }

        [Then(@"I check the TermsOfUse link")]
        public void ThenICheckTheTermsOfUseLink()
            {
            WebDriver.GetByPath("//*[@id='ucFooter_hplTerms']").Click();
            Thread.Sleep(1000);
            WebDriver.GetByPath("//*[contains(text(),'When using this Site, you have to be aware that links to third party sites and advertisements of products/services are only there for your convenience')]");
            }

        [Then(@"I check the ContactUs footer link")]
        public void ThenICheckTheContactUsFooterLink()
            {
            WebDriver.GetByPath("//*[@id='ucFooter_hplContact']").Click();
            Thread.Sleep(1000);
            WebDriver.SwitchTo().Window(WebDriver.WindowHandles.Last());
            WebDriver.GetByPath("//*[contains(text(),'Customer Service')]");
            WebDriver.Close();
            WebDriver.SwitchTo().Window(WebDriver.WindowHandles[0]);
            }

        [Then(@"I check '(.*)' SeeAllFeatures link")]
        public void ThenICheckSeeAllFeaturesLink(string p0)
            {
            WebDriver.GetByPath("//*[contains(text(),'"+p0+"')]/../../div[2]/a").Click();
            Thread.Sleep(1000);
            WebDriver.GetByPath("//*[contains(text(),'Features Comparison Chart')]");
            }

        [Then(@"I check the AppStore link")]
        public void ThenICheckTheAppStoreLink()
            {
            WebDriver.GetByPath("//a[@href='https://itunes.apple.com/us/app/mydealeronline-dashboard/id855346874?mt=8']").Click();
            Thread.Sleep(1000);
            WebDriver.GetByPath("//*[contains(text(),'The MyDealerOnline Dashboard app is a revolutionary technology')]");
            Assert.IsTrue(WebDriver.GetCurrentUrl().Contains("itunes.apple.com"));
            }

        [Then(@"I check the GooglePlay link")]
        public void ThenICheckTheGooglePlayLink()
            {
            WebDriver.GetByPath("//a[@href='https://play.google.com/store/apps/details?id=com.mdo360.android']").Click();
            Thread.Sleep(1000);
            WebDriver.GetByPath("//*[contains(text(),'The MyDealerOnline Dashboard app is a revolutionary technology')]");
            Assert.IsTrue(WebDriver.GetCurrentUrl().Contains("play.google.com"));
            }

        }
    }
