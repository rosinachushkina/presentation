﻿Feature: SocialIconsForLogin
	Checking that social icons are displayed on login page

@SocialIconsOnPortalLogin
Scenario: Social icons for Portal login
	Given I open the Portal main page
	Then I click the Login button
	Then I enter my portal email and password for AdditionalPages account and submit them

	Then I open AdditionalPagesSite settings
	Then I click on website settings icon
	Then I click on checkboxes of social authorization
	Then I click save all changes of Marketing and Analysis
	
	Then I click on preview site button
	Then I switch on next window
	Then I click on SignIn button

	Then I check that icons of social authorization aren't enable

	Then I switch to first tab
	Then I click on checkboxes of social authorization
	Then I click save all changes of Marketing and Analysis

	Then I switch on next window
	Then I refresh the page
	Then I check that icons of social authorization are enabled