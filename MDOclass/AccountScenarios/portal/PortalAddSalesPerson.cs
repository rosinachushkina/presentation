﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TechTalk.SpecFlow;
using Common;
using System.Threading;
using NUnit.Framework;
using OpenQA.Selenium;

namespace MDOclass.AccountScenarios.portal
    {
    [Binding]
    public class PortalAddSalesPerson : BaseSteps
        {
        string fname, lname, title, phonenumber, email, language, username, password;

        [Then(@"I switch to CompanyInformation > EmployeersAndSalesTeam")]
        public void ThenISwitchToCompanyInformationEmployeersAndSalesTeam()
            {
            Thread.Sleep(2000);
            WebDriver.SwitchTo().Window(WebDriver.WindowHandles.Last());
            WebDriver.GetByPath(".//*[@id='asideMenu']/ul/li[2]/a/span").Click();
            Thread.Sleep(500);
            WebDriver.GetByPath("//*[@id='asideMenu']/ul/li[2]/ul/li[2]/a").Click();
            }

        [Then(@"I add a new sales person \(with all checkboxes enabled\)")]
        public void ThenIAddANewSalesPersonWithAllCheckboxesEnabled()
            {
            var r = new HandyFunctions();

            fname = r.RandomStringLatin(10);
            WebDriver.GetByPath("//form[@id='sales_team_add_new_form']//input[@name='FirstName']").SendKeys(fname);
            lname = r.RandomStringCyrillic(10);
            WebDriver.GetByPath("//form[@id='sales_team_add_new_form']//input[@name='LastName']").SendKeys(lname);
            title = r.RandomStringCyrillic(10);
            WebDriver.GetByPath("//form[@id='sales_team_add_new_form']//input[@name='Title']").SendKeys(title);
            phonenumber = r.RandomStringNumbers(12);
            WebDriver.GetByPath("//form[@id='sales_team_add_new_form']//input[@name='PhoneNumber']").SendKeys(phonenumber);
            email = r.RandomStringLatin(10) + "@test.tsbua.com";
            WebDriver.GetByPath("//form[@id='sales_team_add_new_form']//input[@name='Email']").SendKeys(email);
            language = r.RandomStringLatin(25);
            WebDriver.GetByPath("//form[@id='sales_team_add_new_form']//input[@name='SalesPersonLanguages']").SendKeys(language);

            if (!WebDriver.GetByPath("//form[@id='sales_team_add_new_form']//input[@name='ShowOnWebsite']").Selected)
                WebDriver.GetByPath("//form[@id='sales_team_add_new_form']//input[@name='ShowOnWebsite']").Click();

            username = r.RandomStringLatin(10);
            WebDriver.GetByPath("//form[@id='sales_team_add_new_form']//input[@name='SiteUserName']").SendKeys(username);
            password = r.RandomStringLatin(10);
            WebDriver.GetByPath("//form[@id='sales_team_add_new_form']//input[@name='SiteUserNamePassword']").SendKeys(password);
            WebDriver.GetByPath("//form[@id='sales_team_add_new_form']//input[@name='SiteUserNamePasswordConfirm']").SendKeys(password);

            WebDriver.GetByPath("//form[@id='sales_team_add_new_form']//a[@class='sales-team-member-add-button btn btn-primary']").
                Click();
            Thread.Sleep(1000);
            }

        [Then(@"I check that this sales person was added here in portal")]
        public void ThenICheckThatThisSalesPersonWasAddedHereInPortal()
            {
            WebDriver.GetByPath("//*[contains(text(),'"+fname+"')]");
            WebDriver.GetByPath("//*[contains(text(),'"+lname+"')]");
            WebDriver.GetByPath("//*[contains(text(),'"+title+"')]");
            WebDriver.GetByPath("//*[contains(text(),'"+phonenumber+"')]");
            WebDriver.GetByPath("//*[contains(text(),'"+email+"')]");
            }

        [Then(@"I switch to last tab")]
        public void ThenISwitchToLastTab()
            {
            WebDriver.SwitchTo().Window(WebDriver.WindowHandles.Last());
            Thread.Sleep(1000);
            }

        [Then(@"I check that my new sales person is shown on the website main")]
        public void ThenICheckThatMyNewSalesPersonIsShownOnTheWebsiteMain()
            {
            WebDriver.GetByPath("//div[@id='salesTeam']//*[contains(text(),'" + fname +"')]");
            WebDriver.GetByPath("//div[@id='salesTeam']//*[contains(text(),'" + lname + "')]");
            WebDriver.GetByPath("//div[@id='salesTeam']//*[contains(text(),'" + email + "')]");
            WebDriver.GetByPath("//div[@id='salesTeam']//*[contains(text(),'" + phonenumber + "')]");
            WebDriver.GetByPath("//div[@id='salesTeam']//*[contains(text(),'" + language + "')]");
            }

        [Then(@"I click the Edit button for sales person number (.*)")]
        public void ThenIClickTheEditButtonForSalesPersonNumber(int p0)
            {
            WebDriver.GetByPath("//table[@id='sales_team_member_grid_area']/tbody/tr["+p0.ToString()+"]/td[6]/a[1]").Click();
            Thread.Sleep(2000);
            }

        [Then(@"I change all information for this sales person and Save it")]
        public void ThenIChangeAllInformationForThisSalesPersonAndSaveIt()
            {
            var r = new HandyFunctions();
            
            fname = r.RandomStringLatin(10);
            WebDriver.GetByPath("//div[@id='sales-team-edit-member-area']//input[@name='FirstName']").Clear();
            WebDriver.GetByPath("//div[@id='sales-team-edit-member-area']//input[@name='FirstName']").SendKeys(fname);
            lname = r.RandomStringCyrillic(10);
            WebDriver.GetByPath("//div[@id='sales-team-edit-member-area']//input[@name='LastName']").Clear();
            WebDriver.GetByPath("//div[@id='sales-team-edit-member-area']//input[@name='LastName']").SendKeys(lname);
            title = r.RandomStringCyrillic(10);
            WebDriver.GetByPath("//div[@id='sales-team-edit-member-area']//input[@data-field-name='Title']").Clear();
            WebDriver.GetByPath("//div[@id='sales-team-edit-member-area']//input[@data-field-name='Title']").SendKeys(title);
            phonenumber = r.RandomStringNumbers(12);
            WebDriver.GetByPath("//div[@id='sales-team-edit-member-area']//input[@data-field-name='PersonPhone']").Clear();
            WebDriver.GetByPath("//div[@id='sales-team-edit-member-area']//input[@data-field-name='PersonPhone']").SendKeys(phonenumber);
            email = r.RandomStringLatin(10) + "@test.tsbua.com";
            WebDriver.GetByPath("//div[@id='sales-team-edit-member-area']//input[@name='Email']").Clear();
            WebDriver.GetByPath("//div[@id='sales-team-edit-member-area']//input[@name='Email']").SendKeys(email);
            language = r.RandomStringLatin(25);
            WebDriver.GetByPath("//div[@id='sales-team-edit-member-area']//input[@name='SalesPersonLanguages']").Clear();
            WebDriver.GetByPath("//div[@id='sales-team-edit-member-area']//input[@name='SalesPersonLanguages']").SendKeys(language);

            if (!WebDriver.GetByPath("//div[@id='sales-team-edit-member-area']//input[@name='ShowOnWebsite']").Selected)
                WebDriver.GetByPath("//div[@id='sales-team-edit-member-area']//input[@name='ShowOnWebsite']").Click();

            username = r.RandomStringLatin(10);
            WebDriver.GetByPath("//div[@id='sales-team-edit-member-area']//input[@name='SiteUserName']").Clear();
            WebDriver.GetByPath("//div[@id='sales-team-edit-member-area']//input[@name='SiteUserName']").SendKeys(username);
            password = r.RandomStringLatin(10);
            WebDriver.GetByPath("//div[@id='sales-team-edit-member-area']//input[@name='SiteUserNamePassword']").Clear();
            WebDriver.GetByPath("//div[@id='sales-team-edit-member-area']//input[@name='SiteUserNamePassword']").SendKeys(password);
            WebDriver.GetByPath("//div[@id='sales-team-edit-member-area']//input[@name='SiteUserNamePasswordConfirm']").Clear();
            WebDriver.GetByPath("//div[@id='sales-team-edit-member-area']//input[@name='SiteUserNamePasswordConfirm']").SendKeys(password);

            WebDriver.GetByPath("//a[@class='sales-team-save-member-button btn btn-primary']").
                Click();
            }

        [Then(@"I Delete sales person number (.*)")]
        public void ThenIClickTheDeleteButtonForSalesPersonNumber(int p0)
            {
            WebDriver.GetByPath("//table[@id='sales_team_member_grid_area']/tbody/tr[" + p0.ToString() + "]/td[6]/a[2]").Click();
            WebDriver.SwitchTo().Alert().Accept();
            Thread.Sleep(2000);
            }

        [Then(@"I check that I have only (.*) sales person left")]
        public void ThenICheckThatIHaveOnlySalesPersonLeft(int p0)
            {
            WebDriver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(3));
            try
                {
                WebDriver.GetByPath("//table[@id='sales_team_member_grid_area']/tbody/tr[" + (p0 + 1).ToString() + "]/td[6]/a[2]");
                Assert.Fail("There is more than " + p0.ToString() + " sales persons left!(((");
                }
            catch(WebDriverException)
            { }
            WebDriver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(BaseSteps.StandardTimeOut));
            }

        }
    }
