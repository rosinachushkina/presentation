﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TechTalk.SpecFlow;
using Common;
using System.Threading;
using NUnit.Framework;

namespace MDOclass.AccountScenarios.portal
    {
    [Binding]
    public class PortalTemplates : BaseSteps
        {
        PortalTemplate selectedtemplate;

        [Then(@"I switch to Website>Templates")]
        public void ThenISwitchToWebsiteTemplates()
            {
            Thread.Sleep(2000);
            WebDriver.SwitchTo().Window(WebDriver.WindowHandles.Last());
            WebDriver.GetByPath(".//*[@id='asideMenu']/ul/li[3]/a/span").Click();
            Thread.Sleep(500);
            WebDriver.GetByPath("//*[@id='asideMenu']/ul/li[3]/ul/li[2]/a").Click();
            }

        [Then(@"I switch the template to '(.*)'")]
        public void ThenISwitchTheTemplateTo(string p0)
            {
            WebDriver.GetByPath("//*[contains(text(),'" + p0 + "')]/following-sibling::a[2]").Click();
            Thread.Sleep(1000);
            WebDriver.GetByPath("//*[@id='select_site_template_continue']").Click();
            Thread.Sleep(3000);
            
            selectedtemplate = new PortalTemplate();
            selectedtemplate.name = p0;
            
            if(selectedtemplate.name=="mint")
                selectedtemplate.headercssvalue=PortalTemplatesInfo.MintCSSHeaderBG;
            if (selectedtemplate.name == "mini")
                selectedtemplate.headercssvalue = PortalTemplatesInfo.MiniCSSHeaderBG;
            if (selectedtemplate.name == "Blue Vista")
                selectedtemplate.headercssvalue = PortalTemplatesInfo.BlueVistaSSHeaderBG;
            }

        [Then(@"I check that the website template is set to the selected template")]
        public void ThenICheckThatTheWebsiteTemplateIsSetToTheSelectedTemplate()
            {
            string currentcssvalue = WebDriver.GetByPath("//*[@id='topNavigation']/div/div/div/div[1]/div[2]").
                GetCssValue("background-color");
            Assert.IsTrue(currentcssvalue == selectedtemplate.headercssvalue);
            }

        }

    public class PortalTemplate
        {
        public string name;
        public string headercssvalue;
        }

    }
