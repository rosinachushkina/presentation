﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TechTalk.SpecFlow;
using Common;
using System.Threading;
using NUnit.Framework;

namespace MDOclass.AccountScenarios.portal
    {
    [Binding]
    public class PortalWebDesign : BaseSteps
        {
        List<WebDesignColor> selectedcolors = new List<WebDesignColor>();

        [Then(@"I switch to Customization>WebsiteDesign")]
        public void ThenISwitchToCustomizationWebsiteDesign()
            {
            Thread.Sleep(2000);
            WebDriver.SwitchTo().Window(WebDriver.WindowHandles.Last());
            WebDriver.GetByPath(".//*[@id='asideMenu']/ul/li[3]/a/span").Click();
            Thread.Sleep(500);
            WebDriver.GetByPath("//*[@id='asideMenu']/ul/li[3]/ul/li[3]/a").Click();
            }

        [Then(@"I switch the '(.*)' using a random value and remember color_s value")]
        public void ThenISwitchTheUsingARandomValueAndRememberColor_SValue(string p0)
            {
            var r = new HandyFunctions();
          

            Thread.Sleep(500);
            WebDriver.GetByPath("//div[@class='col-md-6 design-site-colors']//*[contains(text(),'"+p0+"')]/span/span/span[3]").Click();
            Thread.Sleep(1000);

            WebDriver.GetByPath("//div[@class='col-md-6 design-site-colors']//*[contains(text(),'" + p0 + "')]/div/table/tbody/tr[6]/td[2]/input").
                Clear();
            WebDriver.GetByPath("//div[@class='col-md-6 design-site-colors']//*[contains(text(),'" + p0 + "')]/div/table/tbody/tr[6]/td[2]/input").
                SendKeys(r.RandomStringNumbers(2));

            WebDriver.GetByPath("//div[@class='col-md-6 design-site-colors']//*[contains(text(),'" + p0 + "')]/div/table/tbody/tr[7]/td[2]/input").
                Clear();
            WebDriver.GetByPath("//div[@class='col-md-6 design-site-colors']//*[contains(text(),'" + p0 + "')]/div/table/tbody/tr[7]/td[2]/input").
                SendKeys(r.RandomStringNumbers(2));

            WebDriver.GetByPath("//div[@class='col-md-6 design-site-colors']//*[contains(text(),'" + p0 + "')]/div/table/tbody/tr[8]/td[2]/input").
                Clear();
            WebDriver.GetByPath("//div[@class='col-md-6 design-site-colors']//*[contains(text(),'" + p0 + "')]/div/table/tbody/tr[8]/td[2]/input").
                SendKeys(r.RandomStringNumbers(2));

            Thread.Sleep(500);

            var temp = new WebDesignColor();
            temp.name = p0;
            temp.color = WebDriver.GetByPath("//div[@class='col-md-6 design-site-colors']//*[contains(text(),'" + p0 + "')]/div/table/tbody/tr[2]/td[3]/div/span[1]").
                GetCssValue("background-color");
            selectedcolors.Add(temp);

            WebDriver.GetByPath("//div[@class='col-md-6 design-site-colors']//*[contains(text(),'" + p0 + "')]/div[1]/table/tbody//input[@class='Ok']").Click();
            Thread.Sleep(500);
            WebDriver.GetByPath("//*[@id='btn-wsc-save']").Click();
            Thread.Sleep(1000);
            }

        [Then(@"My selected design color should be refrected there")]
        public void ThenMySelectedDesignColorShouldBeRefrectedThere()
            {
            string color;

            foreach (WebDesignColor element in selectedcolors)
                {
                switch (element.name)
                    {
                    case "Site Color":
                        color = WebDriver.GetByPath("//*[@id='CPH_rptMainMakeColumns_rptMainMakes_0_lblMk_0']").GetCssValue("color");
                        Assert.IsTrue(color == element.color);
                        break;
                    case "Site Color #2":
                        color = WebDriver.GetByPath("//*[@id='ucToolbar_hplHome']").GetCssValue("background-color");
                        Assert.IsTrue(color == element.color);
                        break;
                    case "Menu Color":
                        color = WebDriver.GetByPath("/html/body/form/nav/div/div/div/div/div[2]").GetCssValue("background-color");
                        Assert.IsTrue(color == element.color);
                        break;
                    }
                }
            }      
        }

    public class WebDesignColor
        {
        public string name;
        public string color;
        }
    }
