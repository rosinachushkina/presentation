﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TechTalk.SpecFlow;
using Common;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium;
using NUnit.Framework;

namespace MDOclass.AccountScenarios
    {
    [Binding]
    public class SortBySteps : BaseSteps
        {

        int NumberOfVehicles = 30;

        [Then(@"I set a sorting mode to PRICE:HIGHEST FIRST")]
        public void ThenISetASortingModeToPRICEHIGHESTFIRST()
            {
            var selector = new SelectElement(WebDriver.FindElement(By.XPath("//*[@id='CPH_ddlSort']")));
            selector.SelectByText("PRICE: HIGHEST FIRST");
            }

        [Then(@"All vehicles should be sorted by PRICE:HIGHEST FIRST")]
        public void ThenAllShouldBeSortedByPRICEHIGHESTFIRST()
            {
            Double LastPrice;
            string s;
            bool noPriceMode = false;
            bool FirstPriceConflict = false;

            //------------Запоминаем цену первого айтема
            s = WebDriver.FindElement
                (By.XPath("//*[@id='searchResult']/div[1]/div/div[2]/div[2]/span")).Text;
            s=s.Remove(0, 1);
            s=s.Remove(s.Length - 4, 4);

            LastPrice=Convert.ToDouble(s);
            
            //------------Проверяем чтобы все было правильно отсортировано
            //------------С учетом того, что сначала идут залитые дилером машины, а потом
            //------------уже основные аукционные. Также учтены машины без цены в конце дилер.списка.

            if (MDOvariables.IsProductionTest == true)
                NumberOfVehicles = MDOvariables.VehiclesOnPageDefault;
            else NumberOfVehicles = MDOvariables.VehiclesOnPageDefaultSt;

            for (int i = 2; i <= NumberOfVehicles; i++)
                {
               
                WebDriver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(1));
                try
                    {//-------проверим есть ли вообще цена у текущего элемента, если нет - то заходим в режим без цены
                          s = WebDriver.FindElement
                          (By.XPath("//*[@id='searchResult']/div[" + i + "]/div/div[2]/div[2]/span")).Text;
                          noPriceMode = false;
                    }
                catch(WebDriverException)
                    {
                    noPriceMode = true;
                    }
                WebDriver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(BaseSteps.StandardTimeOut));
                             
                    //-------Если все нормально - сравниваем в штатном режиме
                    if (noPriceMode == false)
                        try
                            {
                            s = s.Remove(0, 1);
                            s = s.Remove(s.Length - 4, 4);
                            Assert.IsTrue(Convert.ToDouble(s) <= LastPrice);
                            LastPrice = Convert.ToDouble(s);
                            }
                        catch (AssertionException)
                            {
                            //---------Если уже был конфликт цены (переход на общие машины) - сваливаем тест.
                            if (FirstPriceConflict == true)
                                Assert.IsTrue(1 == 2);

                            FirstPriceConflict = true;
                            LastPrice = Convert.ToDouble(s);
                            }

                }
            }

        [Then(@"I set a sorting mode to PRICE:LOWEST FIRST")]
        public void ThenISetASortingModeToPRICELOWESTFIRST()
            {
            var selector = new SelectElement(WebDriver.FindElement(By.XPath("//*[@id='CPH_ddlSort']")));
            selector.SelectByText("PRICE: LOWEST FIRST");
            }

        [Then(@"All vehicles should be sorted by PRICE:LOWEST FIRST")]
        public void ThenAllShouldBeSortedByPRICELOWESTFIRST()
            {
            Double LastPrice;
            string s;
            bool noPriceMode = false;
            bool FirstPriceConflict = false;

            //------------Запоминаем цену первого айтема
            s = WebDriver.FindElement
                (By.XPath("//*[@id='searchResult']/div[1]/div/div[2]/div[2]/span")).Text;
            s = s.Remove(0, 1);
            s = s.Remove(s.Length - 4, 4);

            LastPrice = Convert.ToDouble(s);

            //------------Проверяем чтобы все было правильно отсортировано
            //------------С учетом того, что сначала идут залитые дилером машины, а потом
            //------------уже основные аукционные. Также учтены машины без цены в конце дилер.списка.

            if (MDOvariables.IsProductionTest == true)
                NumberOfVehicles = MDOvariables.VehiclesOnPageDefault;
            else NumberOfVehicles = MDOvariables.VehiclesOnPageDefaultSt;

            for (int i = 2; i <= NumberOfVehicles; i++)
                {

                WebDriver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(1));
                try
                    {//-------проверим есть ли вообще цена у текущего элемента, если нет - то заходим в режим без цены
                    s = WebDriver.FindElement
                    (By.XPath("//*[@id='searchResult']/div[" + i + "]/div/div[2]/div[2]/span")).Text;
                    noPriceMode = false;
                    }
                catch (WebDriverException)
                    {
                    noPriceMode = true;
                    }
                WebDriver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(BaseSteps.StandardTimeOut));

                //-------Если все нормально - сравниваем в штатном режиме
                if (noPriceMode == false)
                    try
                        {
                        s = s.Remove(0, 1);
                        s = s.Remove(s.Length - 4, 4);
                        Assert.IsTrue(Convert.ToDouble(s) >= LastPrice);
                        LastPrice = Convert.ToDouble(s);
                        }
                    catch (AssertionException)
                        {
                        //---------Если уже был конфликт цены (переход на общие машины) - сваливаем тест.
                        if (FirstPriceConflict == true)
                            Assert.IsTrue(1 == 2);

                        FirstPriceConflict = true;
                        LastPrice = Convert.ToDouble(s);
                        }

                }
            }

        [Then(@"I set a sorting mode to TIME: ENDING SOONEST")]
        public void ThenISetASortingModeToTIMEENDINGSOONEST()
            {
            var selector = new SelectElement(WebDriver.FindElement(By.XPath("//*[@id='CPH_ddlSort']")));
            selector.SelectByText("TIME: ENDING SOONEST");
            }

        [Then(@"All vehicles should be sorted by TIME: ENDING SOONEST")]
        public void ThenAllShouldBeSortedByTIMEENDINGSOONEST()
            {
            int LastDate;
            string s;
            bool noDateMode = false;
            bool FirstDateConflict = false;

            //-----------По-хорошему надо бы все переписать в секунды, но пока сравниваем на скорую руку
            //-----------т.к. метод по конвертации еще не готов

            //------------Запоминаем дату первого айтема
            string path = "//*[@id='searchResult']/div[1]/div/div[1]/div/div/div[2]";
            s = WebDriver.FindElement
                (By.XPath(path+"/span[1]")).Text
                + WebDriver.FindElement
                (By.XPath(path + "/span[2]")).Text
                + WebDriver.FindElement
                (By.XPath(path + "/span[3]")).Text
                + WebDriver.FindElement
                (By.XPath(path + "/span[4]")).Text;

            LastDate = Convert.ToInt32(HandyFunctions.TrimToDigitsAndSeparators(s));

            //------------Проверяем чтобы все было правильно отсортировано
            //------------С учетом того, что сначала идут залитые дилером машины, а потом
            //------------уже основные аукционные. Также учтены машины без даты в конце дилер.списка.

            if (MDOvariables.IsProductionTest == true)
                NumberOfVehicles = MDOvariables.VehiclesOnPageDefault;
            else NumberOfVehicles = MDOvariables.VehiclesOnPageDefaultSt;

            for (int i = 2; i <= NumberOfVehicles; i++)
                {

                WebDriver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(1));
                try
                    {//-------проверим есть ли вообще дата у текущего элемента, если нет - то заходим в режим без даты
                    s = WebDriver.FindElement
                    (By.XPath("//*[@id='searchResult']/div[" + i + "]/div/div[1]/div/div/div[2]/span[1]")).Text;

                    s = WebDriver.FindElement
                (By.XPath(path + "/span[1]")).Text
                + WebDriver.FindElement
                (By.XPath(path + "/span[2]")).Text
                + WebDriver.FindElement
                (By.XPath(path + "/span[3]")).Text
                + WebDriver.FindElement
                (By.XPath(path + "/span[4]")).Text;

                    noDateMode = false;
                    }
                catch (WebDriverException)
                    {
                    noDateMode = true;
                    }
                WebDriver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(BaseSteps.StandardTimeOut));

                //-------Если все нормально - сравниваем в штатном режиме
                if (noDateMode == false)
                    try
                        {
                        s = HandyFunctions.TrimToDigitsAndSeparators(s);
                        Assert.IsTrue(Convert.ToInt32(s) >= LastDate);
                        LastDate = Convert.ToInt32(s);
                        }
                    catch (AssertionException)
                        {
                        //---------Если уже был конфликт даты (переход на общие машины) - сваливаем тест.
                        if (FirstDateConflict == true)
                            Assert.IsTrue(1 == 2);

                        FirstDateConflict = true;
                        LastDate = Convert.ToInt32(s);
                        }

                }
            }

        [Then(@"All vehicles should be sorted by TIME: ENDING SOONEST \(with Conversion to Sec\)")]
        public void ThenAllShouldBeSortedByTIMEENDINGSOONESTWithConversionToSec()
            {
            int LastDate, Date;
            string s;
            bool noDateMode = false;
            bool FirstDateConflict = false;

            //------------Запоминаем дату первого айтема
            string path = "//*[@id='searchResult']/div[1]/div/div[1]/div/div/div[2]";
            s = WebDriver.FindElement
                (By.XPath(path + "/span[1]")).Text
                + WebDriver.FindElement
                (By.XPath(path + "/span[2]")).Text
                + WebDriver.FindElement
                (By.XPath(path + "/span[3]")).Text
                + WebDriver.FindElement
                (By.XPath(path + "/span[4]")).Text;

            LastDate = HandyFunctions.ConvertStringDHMSToSeconds(s);

            //------------Проверяем чтобы все было правильно отсортировано
            //------------С учетом того, что сначала идут залитые дилером машины, а потом
            //------------уже основные аукционные. Также учтены машины без даты в конце дилер.списка.

            if (MDOvariables.IsProductionTest == true)
                NumberOfVehicles = MDOvariables.VehiclesOnPageDefault;
            else NumberOfVehicles = MDOvariables.VehiclesOnPageDefaultSt;

            for (int i = 2; i <= NumberOfVehicles; i++)
                {

                WebDriver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(1));
                try
                    {//-------проверим есть ли вообще дата у текущего элемента, если нет - то заходим в режим без даты
                    s = WebDriver.FindElement
                    (By.XPath("//*[@id='searchResult']/div[" + i + "]/div/div[1]/div/div/div[2]/span[1]")).Text;

                    s = WebDriver.FindElement
                (By.XPath(path + "/span[1]")).Text
                + WebDriver.FindElement
                (By.XPath(path + "/span[2]")).Text
                + WebDriver.FindElement
                (By.XPath(path + "/span[3]")).Text
                + WebDriver.FindElement
                (By.XPath(path + "/span[4]")).Text;

                    noDateMode = false;
                    }
                catch (WebDriverException)
                    {
                    noDateMode = true;
                    }
                WebDriver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(BaseSteps.StandardTimeOut));

                //-------Если все нормально - сравниваем в штатном режиме
                if (noDateMode == false)
                    try
                        {
                        Date = HandyFunctions.ConvertStringDHMSToSeconds(s);
                        Assert.IsTrue(Date >= LastDate);
                        LastDate = Date;
                        }
                    catch (AssertionException)
                        {
                        //---------Если уже был конфликт даты (переход на общие машины) - сваливаем тест.
                        if (FirstDateConflict == true)
                            Assert.IsTrue(1 == 2);

                        FirstDateConflict = true;
                        LastDate = HandyFunctions.ConvertStringDHMSToSeconds(s);
                        }

                }
            }

        [Then(@"I set a sorting mode to YEAR, MAKE: NEWEST FIRST")]
        public void ThenISetASortingModeToYEARMAKENEWESTFIRST()
            {
            var selector = new SelectElement(WebDriver.FindElement(By.XPath("//*[@id='CPH_ddlSort']")));
            selector.SelectByText("YEAR, MAKE: NEWEST FIRST");
            }

        [Then(@"All vehicles should be sorted by YEAR, MAKE: NEWEST FIRST")]
        public void ThenAllShouldBeSortedByYEARMAKENEWESTFIRST()
            {
            int LastYear, Year;
            string s;
            int ConflictsCount = 0;

           //-------------Запоминаем год выпуска первой машины
            s = "//*[@id='searchResult']/div[1]/div/p/a/span";
            LastYear = Convert.ToInt32(WebDriver.FindElement(By.XPath(s)).Text.Substring(0, 4));

            //------------Проверяем чтобы все было правильно отсортировано
            //------------С учетом того, что сначала идут залитые дилером машины, а потом
            //------------уже основные аукционные. Также учтены машины с Preview в конце дилер.списка.

            if (MDOvariables.IsProductionTest == true)
                NumberOfVehicles = MDOvariables.VehiclesOnPageDefault;
            else NumberOfVehicles = MDOvariables.VehiclesOnPageDefaultSt;

            for (int i = 2; i <= NumberOfVehicles; i++)
                {
                    try
                        {
                        s = "//*[@id='searchResult']/div["+i+"]/div/p/a/span";
                        Year = Convert.ToInt32(WebDriver.FindElement(By.XPath(s)).Text.Substring(0, 4));
                        Assert.IsTrue(Year <= LastYear);
                        LastYear = Year;
                        }
                    catch (AssertionException)
                        {
                        //---------Если уже больше двух ошибок (переходов между группами) - сваливаем тест
                        if (ConflictsCount ==2)
                            Assert.IsTrue(1 == 2);

                        ConflictsCount++;
                        Year = Convert.ToInt32(WebDriver.FindElement(By.XPath(s)).Text.Substring(0, 4));
                        LastYear = Year;
                        }

                }
            }

        [Then(@"I set a sorting mode to YEAR, MAKE: OLDEST FIRST")]
        public void ThenISetASortingModeToYEARMAKEOLDESTFIRST()
            {
            var selector = new SelectElement(WebDriver.FindElement(By.XPath("//*[@id='CPH_ddlSort']")));
            selector.SelectByText("YEAR, MAKE: OLDEST FIRST");
            }

        [Then(@"All vehicles should be sorted by YEAR, MAKE: OLDEST FIRST")]
        public void ThenAllShouldBeSortedByYEARMAKEOLDESTFIRST()
            {
            int LastYear, Year;
            string s;
            int ConflictsCount = 0;

            //-------------Запоминаем год выпуска первой машины
            s = "//*[@id='searchResult']/div[1]/div/p/a/span";
            LastYear = Convert.ToInt32(WebDriver.FindElement(By.XPath(s)).Text.Substring(0, 4));

            //------------Проверяем чтобы все было правильно отсортировано
            //------------С учетом того, что сначала идут залитые дилером машины, а потом
            //------------уже основные аукционные. Также учтены машины с Preview в конце дилер.списка.

            if (MDOvariables.IsProductionTest == true)
                NumberOfVehicles = MDOvariables.VehiclesOnPageDefault;
            else NumberOfVehicles = MDOvariables.VehiclesOnPageDefaultSt;

            for (int i = 2; i <= NumberOfVehicles; i++)
                {
                try
                    {
                    s = "//*[@id='searchResult']/div[" + i + "]/div/p/a/span";
                    Year = Convert.ToInt32(WebDriver.FindElement(By.XPath(s)).Text.Substring(0, 4));
                    Assert.IsTrue(Year >= LastYear);
                    LastYear = Year;
                    }
                catch (AssertionException)
                    {
                    //---------Если уже больше двух ошибок (переходов между группами) - сваливаем тест
                    if (ConflictsCount == 2)
                        Assert.IsTrue(1 == 2);

                    ConflictsCount++;
                    Year = Convert.ToInt32(WebDriver.FindElement(By.XPath(s)).Text.Substring(0, 4));
                    LastYear = Year;
                    }
                }

            }

        [Then(@"I set a sorting mode to MAKE, MODEL: NEWEST FIRST")]
        public void ThenISetASortingModeToMAKEMODELNEWESTFIRST()
            {
            var selector = new SelectElement(WebDriver.FindElement(By.XPath("//*[@id='CPH_ddlSort']")));
            selector.SelectByText("MAKE, MODEL: NEWEST FIRST");
            }

        [Then(@"All vehicles should be sorted by MAKE, MODEL: NEWEST FIRST")]
        public void ThenAllShouldBeSortedByMAKEMODELNEWESTFIRST()
            {
            string LastMakeModel="";
            string MakeModel = "";
            string s;
            int ConflictsCount = 0;

            //-------------Запоминаем make model первой машины
            s = "//*[@id='searchResult']/div[1]/div/p/a/span";
            IWebElement element=WebDriver.FindElement(By.XPath(s));
            LastMakeModel = element.Text.Substring(4, element.Text.Length - 4);
            LastMakeModel = LastMakeModel.Trim();

            //------------Проверяем чтобы все было правильно отсортировано
            //------------С учетом того, что сначала идут залитые дилером машины, а потом
            //------------уже основные аукционные. Также учтены машины с Preview в конце дилер.списка.

            if (MDOvariables.IsProductionTest == true)
                NumberOfVehicles = MDOvariables.VehiclesOnPageDefault;
            else NumberOfVehicles = MDOvariables.VehiclesOnPageDefaultSt;

            for (int i = 2; i <= NumberOfVehicles; i++)
                {
                try
                    {
                    s = "//*[@id='searchResult']/div[" + i + "]/div/p/a/span";
                    element = WebDriver.FindElement(By.XPath(s));
                    MakeModel = element.Text.Substring(4, element.Text.Length - 4);
                    MakeModel = MakeModel.Trim();

                    Assert.IsTrue(MakeModel[0] >= LastMakeModel[0]);
                    LastMakeModel = MakeModel;
                    }
                catch (AssertionException)
                    {
                    //---------Если уже больше двух ошибок (переходов между группами) - сваливаем тест
                    if (ConflictsCount == 2)
                        Assert.Fail("Conflicts amount is "+ConflictsCount.ToString());

                    ConflictsCount++;
                    MakeModel = element.Text.Substring(4, element.Text.Length - 4);
                    LastMakeModel = MakeModel;
                    }
                }
            }


        }
    }
