﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using Common;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using TechTalk.SpecFlow;

namespace MDOclass.AccountScenarios
{
    [Binding]
    public class SearchByCriteriasSteps : BaseSteps
    {
        public static string ID="";
        public static string Title="";
        public static string Country="";
        public static string State="";
        public static string Sstate = "";
        public static string Make="";
        public static string Model="";
        public static string ZIP = "";
        private DateTime AuctionDate;
        private string Year,s;
        private Random random = new Random(Environment.TickCount);
        private int i,n;

        [Then(@"I will remember its ID and title")]
        public void ThenIWillRememberItsID()
        {
            ID = WebDriver.FindElement(By.XPath("//*[@id='CPH_lblInventoryNumberData2']")).Text;
            Title = WebDriver.FindElement(By.XPath("//*[@id='CPH_lblVehicleHeader']")).Text;
            
        }

        [Then(@"I make a search by ID")]
        public void ThenIMakeASearchByID()
        {
            WebDriver.FindElement(By.XPath("//*[@id='txtInventoryID']")).SendKeys(ID);
            WebDriver.FindElement(By.XPath("//*[@id='lbtSearchByInventoryID']")).Click();
        }

        [Then(@"I verify this inventory's ID and title with my saved ones")]
        public void ThenIVerifyThisInventorySIDWithMySavedOne()
        {
            Assert.IsTrue(WebDriver.FindElement(By.XPath("//*[@id='CPH_lblInventoryNumberData2']")).Text==ID);
            Assert.IsTrue(WebDriver.FindElement(By.XPath("//*[@id='CPH_lblVehicleHeader']")).Text == Title);
        }

        [Then(@"I select a random country")]
        public void ThenISelectARandomCountry()
        {
           var selector=new SelectElement(WebDriver.FindElement(By.XPath("//*[@id='CPH_ucSearchOptions_ddlNewCountry']")));
           Thread.Sleep(2000);

           //--------------Рандомизируем выбор и формируем строку для выбора
           i = random.Next(2, 4);
           s = "//*[@id='CPH_ucSearchOptions_ddlNewCountry']/option[" + (i).ToString() + "]";

           //--------------Запоминаем страну и тыкаем на нее
            Country = WebDriver.FindElement(By.XPath(s)).Text;
            selector.SelectByIndex(i - 1);
            Thread.Sleep(2000);
        }

        [Then(@"I click on a regular search button")]
        public void ThenIClickOnARegularSearchButton()
        {
            WebDriver.FindElement(By.XPath("//*[@id='CPH_ucSearchOptions_lblShowItems']")).Click();
        }

        [Then(@"The country of this vehicle should be the same as requested")]
        public void ThenTheCountryOfThisVehicleShouldBeTheSameAsRequested()
        {
            s=WebDriver.FindElement(By.XPath("//*[@id='CPH_ucOpenInvoice_lblItemLocationData']")).Text;
            Assert.IsTrue(s.Contains(Country));
        }

        [Then(@"I select a random state")]
        public void ThenISelectARandomState()
        {
        var selector = new SelectElement(WebDriver.FindElement(By.XPath("//*[@id='CPH_ucSearchOptions_ddlST']")));
            Thread.Sleep(2000);

            //--------------Рандомизируем выбор и формируем строку для выбора
            i = random.Next(2, 2);
            s = "//*[@id='CPH_ucSearchOptions_ddlST']/option[" + (i).ToString() + "]";

            //--------------Запоминаем штат и тыкаем на него
            State = WebDriver.FindElement(By.XPath(s)).GetAttribute("value");
            Sstate = WebDriver.FindElement(By.XPath(s)).Text;
            selector.SelectByIndex(i - 1);

            Thread.Sleep(200);
        }

        [Then(@"The state of this vehicle should be the same as requested")]
        public void ThenTheStateOfThisVehicleShouldBeTheSameAsRequested()
        {
            s = WebDriver.FindElement(By.XPath("//*[@id='CPH_ucOpenInvoice_lblItemLocationData']")).Text;
            Assert.IsTrue(s.Contains(State));
        }

        [Then(@"I select a random Make for my search")]
        public void ThenISelectARandomMakeForMySearch()
        {
            var selector=new SelectElement(WebDriver.FindElement(By.XPath("//*[@id='CPH_ucSearchOptions_ddlMA']")));
          
            //--------------Рандомизируем выбор и формируем строку для выбора
            i = random.Next(4, 50);
            s = "//*[@id='CPH_ucSearchOptions_ddlMA']/option[" + (i).ToString() + "]";

            //--------------Запоминаем Make и тыкаем на него
            Make = WebDriver.FindElement(By.XPath(s)).Text;
            selector.SelectByIndex(i-1);

            Thread.Sleep(2000);

        }

        [Then(@"This vehicle's Make should be the same as I wanted")]
        public void ThenThisVehicleSMakeShouldBeTheSameAsIWanted()
        {
            s = WebDriver.FindElement(By.XPath("//*[@id='CPH_lblVehicleData']")).Text;
            Assert.IsTrue(s.Contains(Make));
        }

        [Then(@"I select a model")]
        public void ThenISelectAModel()
        {
            var selector=new SelectElement(WebDriver.FindElement(By.XPath("//*[@id='CPH_ucSearchOptions_ddlMD']")));
            Thread.Sleep(2000);

            //-----------формируем строку для выбора
            i = 2;
            s = "//*[@id='CPH_ucSearchOptions_ddlMD']/option[" + (i).ToString() + "]";

            //--------------Запоминаем Make и тыкаем на него
            Model = WebDriver.FindElement(By.XPath(s)).Text;
            selector.SelectByIndex(i - 1);
        }

        [Then(@"This vehicle's Model should be the same as I wanted")]
        public void ThenThisVehicleSModelShouldBeTheSameAsIWanted()
        {
            s = WebDriver.FindElement(By.XPath("//*[@id='CPH_lblVehicleData']")).Text;
            Assert.IsTrue(s.Contains(Model));
        }

        [Then(@"I select USA as a country for the fiter")]
        public void ThenISelectUSAAsACountryForTheFiter()
            {
            WebDriver.FindElement(By.XPath("//*[@id='CPH_ucSearchOptions_ddlNewCountry']")).Click();
            Thread.Sleep(2000);

            s = "//*[@id='CPH_ucSearchOptions_ddlNewCountry']/option[3]";

            //--------------Запоминаем страну и тыкаем на нее
            Country = WebDriver.FindElement(By.XPath(s)).Text;
            WebDriver.FindElement(By.XPath(s)).Click();

            Thread.Sleep(2000);
            }

        [Then(@"I enter a (.*) code for the filter")]
        public void ThenIEnterACodeForTheFilter(int p0)
            {
            WebDriver.FindElement(By.XPath("//*[@id='CPH_ucSearchOptions_txtZipCode']")).SendKeys(p0.ToString());
            ZIP = p0.ToString();
            }

        [Then(@"I select the (.*) vehicle from the list \(Tiles View\)")]
        public void ThenISelectTheVehicleFromTheListTilesView(int p0)
            {
            WebDriver.FindElement(By.XPath("//div[@id='searchResult']/div[1]/div/div[1]")).Click();
            WebDriver.FindElement(By.XPath("//div[@id='searchResult']/div[1]/div/div/a[1]")).Click(); 
            }


        [Then(@"I select a minimal range for the filter")]
        public void ThenISelectARandomForTheFilter()
            {
            var Select = new SelectElement(WebDriver.FindElement(By.XPath("//*[@id='CPH_ucSearchOptions_ddlMilesFrom']")));
            Select.SelectByIndex(1);
            Thread.Sleep(1000);
            }

        [Then(@"My vehicle's ""(.*)"" and Range \(if possible\) should be the same as I required")]
        public void ThenMyVehicleSAndRangeIfPossibleShouldBeTheSameAsIRequired(string p0)
            {
            string str=WebDriver.FindElement(By.XPath("//*[@id='CPH_ucOpenInvoice_lblItemLocationData']")).Text;
            str = HandyFunctions.TrimToStateAcro(str);
            Assert.IsTrue(p0.Contains(str));
            }

        [Then(@"I click the BuyNow filter checkbox")]
        public void ThenIClickTheBuyNowFilterCheckbox()
            {
            WebDriver.FindElement(By.XPath("//*[@id='CPH_ucSearchOptions_chkBuyNow']")).Click();
            }

        [Then(@"All vehicles on the page should have the BuyNow type\.")]
        public void ThenAllOnThePageShouldHaveTheBuyNowType_()
            {
            if (MDOvariables.IsProductionTest==true)
                    n=MDOvariables.VehiclesOnPageDefault;
               else n=MDOvariables.VehiclesOnPageDefaultSt;

            for (int i = 1; i <= n; i++)
                Assert.IsTrue(WebDriver.FindElement(
                    By.XPath("//div[@id='searchResult']/div["+i+"]/div/div[2]/div[2]")).
                    Text.Contains("Buy Now:"));
            }

        [Then(@"I select a brand from the Search By Brand list")]
        public void ThenISelectAFromTheSearchByBrandList()
            {
            //----------------Выбираем для проверки Lincoln по прямому пасу и запоминаем его
            Make = WebDriver.FindElement(By.XPath("//span[1][contains(text(),'LINCOLN')]")).Text;
            WebDriver.FindElement(By.XPath("//span[1][contains(text(),'LINCOLN')]")).Click();
            }

        [Then(@"All vehicles on the page should have the selected brand\.")]
        public void ThenAllOnThePageShouldHaveTheSelectedBrand_()
            {
                if (MDOvariables.IsProductionTest==true)
                    n=MDOvariables.VehiclesOnPageDefault;
               else n=MDOvariables.VehiclesOnPageDefaultSt;

            for (int i = 1; i <= n; i++)
                Assert.IsTrue(WebDriver.FindElement(
                    By.XPath("//*[@id='searchResult']/div["+i+"]/div/p/a/span")).
                    Text.Contains(Make));
            }


    }
}
