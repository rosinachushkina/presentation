﻿using System;
using System.Threading;
using Common;
using NUnit.Core;
using NUnit.Framework;
using OpenQA.Selenium;
using TechTalk.SpecFlow;

namespace MDOclass.AccountScenarios
{
    [Binding]
    public class SendingMessageByCustomerSteps : BaseSteps
    {
        private string messageFromCustomer;
        private string vehicleID;
        private string vehicleTitle;
        private string messageFromDealer;
        private string anonymousName;
        private string anonymousEmail;
        private string messageFromCustomer2;

        [Then(@"I click on checkbox about Invoice")]
        public void ThenIClickOnCheckboxAboutInvoice()
        {
            WebDriver.GetById("wouldLikeToGetInvoice").Click();
        }

        [Then(@"I fill my Name and Email")]
        public void ThenIFillMyNameAndEmail()
        {
            var Handy = new HandyFunctions();
            anonymousName = Handy.RandomStringLatin(6);
            anonymousEmail = Handy.RandomStringLatin(6) + "@test.com";

            WebDriver.GetById("txtMsgName").SendKeys(anonymousName);
            WebDriver.GetById("txtMsgPhone").SendKeys(anonymousEmail);

        }


        [Then(@"I type messages and send it")]
        public void ThenITypeMessagesAndSendIt()
        {
            //---------------Генерация и подготовака случайных корректных данных для сообщения
            var Handy = new HandyFunctions();
            messageFromCustomer = Handy.RandomStringLatinAndNumbers(10);


            WebDriver.GetById("txtNewMsg").SendKeys(messageFromCustomer);//-----набор сообщения
            WebDriver.GetById("CPH_lblSaveMsgCaption").Click();//----отправка сообщения
            Thread.Sleep(3000);
        }

        [Then(@"I type cyryllic messages and send it")]
        public void ThenITypeCyryllicMessagesAndSendIt()
        {
            //---------------Генерация и подготовака случайных корректных данных для сообщения
            var Handy = new HandyFunctions();
            messageFromCustomer = Handy.RandomStringCyrillic(10);


            WebDriver.GetById("txtNewMsg").SendKeys(messageFromCustomer);//-----набор сообщения
            WebDriver.GetById("CPH_lblSaveMsgCaption").Click();//----отправка сообщения
            Thread.Sleep(3000);
        }


        [Then(@"I check that message was sent and get ID and Title")]
        public void ThenICheckThatMessageWasSent()
        {
            string actualMessage =
                (WebDriver.GetByPath("//div[@id = 'divAllComments']//li").Text); //------задание актуального сообщения в переменную

            if ((messageFromCustomer + " • by me just now") != actualMessage)               //-------сравнение отправленного сообщения и актуального
            {
                Assert.Fail("Message must be " + messageFromCustomer + "\n Actual is " + actualMessage);
            }

            vehicleID = WebDriver.GetById("CPH_lblInventoryNumberData2").Text;   //-----задание актуального ID в переменную
            vehicleTitle = WebDriver.GetById("CPH_lblVehicleHeader").Text;       //-----задание актуального Title в переменную
        }

        [Then(@"I type in search message input customer's message and try to find it")]
        public void ThenITypeInSearchMessageInputCustomerSMessageAndTryToFindIt()
        {
            string messageFromCustomerCAPS = messageFromCustomer.ToUpper();
            WebDriver.GetByName("searchStr").SendKeys(messageFromCustomerCAPS);
            WebDriver.GetById("startSearch").Click();
        }

        [Then(@"I check that message was found")]
        public void ThenICheckThatCyrillicMessageWasFound()
        {
            string foundMessage = WebDriver.GetByPath("//section//p/span[3]").Text;
            if ((messageFromCustomer) != foundMessage)               //-------сравнение отправленного сообщения и актуального
            {
                Assert.Fail("Message must be " + messageFromCustomer + "\n Actual is " + foundMessage);
            }
        }


        [Then(@"I check that message was sent and get ID and Title with invoice")]
        public void ThenICheckThatMessageWasSentAndGetIDAndTitleWithInvoice()
        {
            string actualMessage =
    (WebDriver.GetByPath("//div[@id = 'divAllComments']//li").Text); //------задание актуального сообщения в переменную

            if ((messageFromCustomer + " • by me just now • Invoice requested") != actualMessage)               //-------сравнение отправленного сообщения и актуального
            {
                Assert.Fail("Message must be " + messageFromCustomer + "\n Actual is " + actualMessage);
            }

            vehicleID = WebDriver.GetById("CPH_lblInventoryNumberData2").Text;   //-----задание актуального ID в переменную
            vehicleTitle = WebDriver.GetById("CPH_lblVehicleHeader").Text;       //-----задание актуального Title в переменную
        }

        [Then(@"I go to the Dashboard")]
        public void ThenIGoToTheDashboard()
        {
            WebDriver.GetById("ucToolbar_hplMyDash").Click();
        }

        [Then(@"I check messsage from Dashboard")]
        public void ThenICheckMesssageFromDashboard()
        {
            string actualMessage = WebDriver.GetByPath("//div[@id='messagesGroupArea']//p/span[3]").Text;//------задание актуального сообщения в переменную

            if (messageFromCustomer != actualMessage)                                                                //-------сравнение отправленного сообщения и актуального
            {
                Assert.Fail("Message must be " + messageFromCustomer + "\n Actual is " + actualMessage);
            }
        }

        [Then(@"I delete message from Dashboard")]
        public void ThenIDeleteMessageFromDashboard()
        {
            WebDriver.GetByPath("//div[@id='messagesGroupArea']/section").Click();                      //------клик на Vehicle
            WebDriver.GetByPath("//div[@id='messagesGroupArea']/section/div/div/a").Click();            //------клик на крестик удаления
            WebDriver.SwitchTo().Alert().Accept();                                                      //------принятие alert'a
            Assert.True((WebDriver.GetByPath("//*[contains(text(),'You don')]").Enabled));
            Assert.True((WebDriver.GetByPath("//*[contains(text(),'t have any messages')]").Enabled));
        }

        [Then(@"I click on AskDealerAQuestion button")]
        public void ThenIClickOnAskDealerAQuestionButton()
        {
            WebDriver.GetByPath("//article[1]/div[2]/div/p/a[contains(text(), '+ Ask dealer a question.')]").Click();
        }


        [Then(@"I type Name of anon and Email and agree with invoice")]
        public void ThenITypeNameOfAnonAndEmail()
        {
            var Handy = new HandyFunctions();
            anonymousName = Handy.RandomStringLatin(6);
            anonymousEmail = Handy.RandomStringLatin(6) + "@test.com";

            WebDriver.GetByPath("//div[@id='searchResult']/article[1]/div[3]//div[2]/div[1]/input").SendKeys(anonymousName);
            WebDriver.GetByPath("//div[@id='searchResult']/article[1]/div[3]//div[3]/div[1]/input").SendKeys(anonymousEmail);
            WebDriver.GetByPath("//div[@id='searchResult']/article[1]/div[3]//div[4]/div[1]/input").Click();
        }


        [Then(@"I type message in this input and send it")]
        public void ThenITypeMessageInThisInputAndSendIt()
        {
            //---------------Генерация и подготовака случайных корректных данных для сообщения
            var Handy = new HandyFunctions();
            messageFromCustomer = Handy.RandomStringLatinAndNumbers(10);

            WebDriver.GetByPath("//div[@id='searchResult']/article[1]//input").SendKeys(messageFromCustomer); //-------набор сообщения
            WebDriver.GetByPath("//span[contains(text(),'Send')]").Click();                       //-------отправка сообщения
        }


        [Then(@"I check that message was sent from Gallery List and get ID and Title")]
        public void ThenICheckThatMessageWasSentFromGalleryList()
        {
            string actualMessage =
    (WebDriver.GetByPath("//div[@id='searchResult']/article[1]//div[contains(text(),'My Messages')]/following-sibling::*/li").Text);      //------задание актуального сообщения в переменную

            if ((messageFromCustomer + " • by me just now") != actualMessage)                //-------сравнение отправленного сообщения и актуального
            {
                Assert.Fail("Message must be " + messageFromCustomer + " • by me just now" + "\n Actual is " + actualMessage);
            }

            vehicleID = WebDriver.GetByPath("//div[@id='searchResult']/article[1]//div/a/img").GetAttribute("id"); //-----задание актуального ID в переменную
            vehicleID = vehicleID.Substring(1);

            vehicleTitle = WebDriver.GetByPath("//div[@id='searchResult']/article[1]//a/span").Text;                //-----задание актуального Title в переменную


        }

        [Then(@"I check ID and Title")]
        public void ThenICheckIDAndTitle()
        {
            string actualID = WebDriver.GetByPath("//div[@id='messagesListArea']/div/div/div/p/span/span[2]").Text; //-----задание актуального ID в переменную
            if (vehicleID != actualID)                                                                             //-------сравнение ID
            {
                Assert.Fail("ID must be " + vehicleID + "\n Actual is " + actualID);
            }


            /*  string actualTitle = WebDriver.GetByPath("//div[@class='inventory-item']//h1").Text;//-----задание актуального Title в переменную
              if (vehicleTitle != actualTitle)                                                    //-------сравнение Title
              {
                  Assert.Fail("Title must be " + vehicleTitle + "\n Actual is " + actualTitle);
              }*/

            Assert.True(WebDriver.GetByPath("//div[@class='inventory-item']//h1[contains(text(),'" + vehicleTitle + "')]").Enabled);

        }

        [Then(@"I make logout")]
        public void ThenIMakeLogout()
        {
            WebDriver.GetById("ucToolbar_imgUser").Click(); //-----клик на иконку User
            WebDriver.GetByPath("//a[contains(text(), 'Log Out')]").Click(); //-----клик на Logout
        }

        [Then(@"I click on SignIn button")]
        public void ThenIClickOnSignInButton()
        {
            WebDriver.GetById("ucToolbar_lblLogInOut").Click();  //-----нажатие на кнопку Sign In
        }

        [Then(@"I check Notification in Header about message from Customer")]
        public void ThenICheckNotificationInHeader()
        {
            Assert.True(WebDriver.GetByPath("//a[@id='ddlMessageLink']/span[contains(text(),'1')]").Enabled);     //----проверка наличие 1
            WebDriver.GetByPath("//a[@id='ddlMessageLink']/i[1]").Click();  //---- клик на конверт
            Assert.True(WebDriver.GetByPath("//ul/li[1]/a/span/span/div[contains(text(),'" + messageFromCustomer + "')]").Enabled); //---проверка текста сообщения
        }

        [Then(@"I check Notification in Dashboard")]
        public void ThenICheckNotificationInDashboard()
        {
            Assert.True((WebDriver.GetByPath("//a[@href='/Dashboard/Messages/List']/span/span[contains(text(),'1')]").Enabled));

        }

        [Then(@"I open Messages")]
        public void ThenIOpenMessages()
        {
            WebDriver.GetByPath("//a[@href='/Dashboard/Messages/List']/span/span").Click();
        }


        [Then(@"I check message from Customer")]
        public void ThenICheckMessageFromCustomer()
        {
            string actualMessage = WebDriver.GetByPath("//div[@id='messagesGroupArea']//p/span[3]").Text;//------задание актуального сообщения в переменную

            if (messageFromCustomer != actualMessage)                                                                //-------сравнение отправленного сообщения и актуального
            {
                Assert.Fail("Message must be " + messageFromCustomer + "\n Actual is " + actualMessage);
            }
        }

        [Then(@"I type message from Dealer")]
        public void ThenITypeMessageFromDealer()
        {
            var Handy = new HandyFunctions();
            messageFromDealer = Handy.RandomStringLatinAndNumbers(10);
            WebDriver.GetByPath("//input[@placeholder='Write a message']").SendKeys(messageFromDealer);  //----написание нового сообщения

            WebDriver.GetByPath("//a[contains(text(),'Send')]").Click(); //---отправка сообщения
        }

        [Then(@"I type message from Customer again")]
        public void ThenITypeMessageFromCustomerAgain()
        {
            var Handy = new HandyFunctions();
            messageFromCustomer = Handy.RandomStringLatinAndNumbers(10);
            WebDriver.GetByPath("//input[@placeholder='Write a message']").SendKeys(messageFromCustomer);  //----написание нового сообщения

            WebDriver.GetByPath("//a[contains(text(),'Send')]").Click(); //---отправка сообщения
        }


        [Then(@"I click on logo")]
        public void ThenIClickOnLogo()
        {
            WebDriver.GetByPath("//img[@alt='logo']").Click();
        }

        [Then(@"I check messsage from Dealer")]
        public void ThenICheckMesssageFromDealer()
        {
            string actualMessageFromDealer = WebDriver.GetByPath("//ul/li[2]/div/span[5]").Text;

            if (messageFromDealer != actualMessageFromDealer)                //-------сравнение отправленного сообщения и актуального
            {
                Assert.Fail("Message must be " + messageFromDealer + "\n Actual is " + actualMessageFromDealer);
            }
        }

        [Then(@"I check Notification in Header about message from Dealer")]
        public void ThenICheckNotificationInHeaderAboutMessageFromDealer()
        {
            Assert.True(WebDriver.GetByPath("//a[@id='ddlMessageLink']/span[contains(text(),'1')]").Enabled);     //----проверка наличие 1
            WebDriver.GetByPath("//a[@id='ddlMessageLink']/i[1]").Click();  //---- клик на конверт
            Assert.True(WebDriver.GetByPath("//ul/li[1]/a/span/span/div[contains(text(),'" + messageFromDealer + "')]").Enabled); //---проверка текста сообщения
        }

        [Then(@"I check label about Invoice")]
        public void ThenICheckLabelAboutInvoice()
        {
            Assert.True(WebDriver.GetByPath("//strong[contains(text(), 'I would like to get invoice for this vehicle')]").Enabled);
        }

        [Then(@"I check name of Anonymous")]
        public void ThenICheckNameOfAnonymous()
        {
            Assert.True(WebDriver.GetByPath("//div[@class='message']/span[2][contains(text(), '" + anonymousName + "')]").Enabled);
        }

        [Then(@"I check email of Anonymous")]
        public void ThenICheckEmailOfAnonymous()
        {
            Assert.True(WebDriver.GetByPath("//div[@class='message']/div[1]/span[contains(text(), '" + anonymousEmail + "')]").Enabled);
        }



        [Then(@"I open gmail\.com")]
        public void ThenIOpenGmail_Com()
        {
            WebDriver.Url = "https://gmail.com";
        }

        [Then(@"I enter our dealer's email and password and submit them \(for GMAIL\.COM\)")]
        public void ThenIEnterOurDealerSEmailAndPasswordAndSubmitThemForGMAIL_COM()
        {
            WebDriver.FindElement(By.XPath("//input[@id='Email']")).SendKeys(MDOvariables.DealerEmail);
            WebDriver.FindElement(By.XPath("//input[@id='Passwd']")).SendKeys(MDOvariables.DealerEmailPassword);
            WebDriver.FindElement(By.XPath("//input[@id='signIn']")).Click();
            Thread.Sleep(3000);
        }


        [Then(@"I enter  our customer's email and password and submit them \(for GMAIL\.COM\)")]
        public void ThenIEnterOurCustomerSEmailAndPasswordAndSubmitThemForGMAIL_COM()
        {
            WebDriver.FindElement(By.XPath("//input[@id='Email']")).SendKeys(MDOvariables.CustomerGMAILLogin);
            WebDriver.FindElement(By.XPath("//input[@id='Passwd']")).SendKeys(MDOvariables.CustomerGMAILPassword);
            WebDriver.FindElement(By.XPath("//input[@id='signIn']")).Click();
            Thread.Sleep(3000);
        }
        [Then(@"I clean my cookies")]
        public void ThenICleanMyCookies()
        {
            BaseSteps.WebDriver.Manage().Cookies.DeleteAllCookies();
        }


        [Then(@"I open the last message at GMAIL\.COM")]
        public void ThenIOpenTheLastMessageAtGMAIL_COM()
        {
            WebDriver.FindElement(By.XPath("/html/body/div[7]/div[2]/div/div[2]/div/div[2]/div/div/div/div[2]/div/div/div/div/div[7]/div/div/div[2]/div/table/tbody/tr[1]")).Click();
            Thread.Sleep(1000);
            WebDriver.FindElement(By.XPath("/html/body/div[7]/div[2]/div/div[2]/div/div[2]/div/div/div/div[2]/div/div/div/div[2]/div/table/tr/td/div[2]/div[2]/div/div[3]/div[1]/div/div/div/div/div/div[2]")).Click();
            Thread.Sleep(1000);
        }

        [Then(@"I should see notification about message")]
        public void ThenIShouldSeeNotificationAboutMessage()
        {
            Assert.True(WebDriver.GetByPath("//span[contains(text(), 'Message From Customer')]").Enabled);
            Assert.True(WebDriver.GetByPath("//strong/span[contains(text(), '" + MDOvariables.CustomerFirstname + "')]").Enabled);
            Assert.True(WebDriver.GetByPath("//strong/span[contains(text(), '" + MDOvariables.CustomerLastname + "')]").Enabled);
            Assert.True(WebDriver.GetByPath("//span[contains(text(), 'You have a response message from')]").Enabled);
            Assert.True(WebDriver.GetByPath("//span[contains(text(), 'about the vehicle')]").Enabled);
            Assert.True(WebDriver.GetByPath("//p/span[contains(text(), '" + messageFromCustomer + "')]").Enabled);
        }

        [Then(@"I should see notification about message from anon")]
        public void ThenIShouldSeeNotificationAboutMessageFromAnon()
        {
            Assert.True(WebDriver.GetByPath("//span[contains(text(), 'Message From Customer')]").Enabled);
            Assert.True(WebDriver.GetByPath("//span[contains(text(), 'You have a response message from')]").Enabled);
            Assert.True(WebDriver.GetByPath("//span[contains(text(), 'about the vehicle')]").Enabled);
            Assert.True(WebDriver.GetByPath("//p/span[contains(text(), '" + messageFromCustomer + "')]").Enabled);
        }

        [Then(@"I should see notification about message from dealer")]
        public void ThenIShouldSeeNotificationAboutMessageFromDealer()
        {
            Assert.True(WebDriver.GetByPath("//font/span[contains(text(), 'Dear')]").Enabled);
            Assert.True(WebDriver.GetByPath("//strong/span[contains(text(), '" + MDOvariables.CustomerFirstname + " " + MDOvariables.CustomerLastname + "')]").Enabled);
            Assert.True(WebDriver.GetByPath("//span[contains(text(), 'You have a response message from')]").Enabled);
            Assert.True(WebDriver.GetByPath("//span[contains(text(), '" + MDOvariables.DealerFirstname + " " + MDOvariables.DealerLastname + "')]").Enabled);
            Assert.True(WebDriver.GetByPath("//span[contains(text(), 'about the vehicle')]").Enabled);
            // Assert.True(WebDriver.GetByPath("//strong/a[contains(text(), '" + vehicleTitle + "')]").Enabled);
            Assert.True(WebDriver.GetByPath("//p/span[contains(text(), '" + messageFromDealer + "')]").Enabled);


        }

        [Then(@"I open Deleted Tab")]
        public void ThenIOpenDeletedTab()
        {
            WebDriver.GetByPath("//a[contains(text(), 'Deleted Messages')]").Click();
        }

        [Then(@"I type customer name in search input")]
        public void ThenITypeCustomerNameInSearchInput()
        {
            string userName = ""+MDOvariables.CustomerFirstname+" "+MDOvariables.CustomerLastname+"".ToUpper();
            string userNameCAPS = userName.ToUpper();
            WebDriver.GetByName("searchStr").Clear();
            WebDriver.GetByName("searchStr").SendKeys(userNameCAPS);
            WebDriver.GetById("startSearch").Click();
            Thread.Sleep(2000);
        }

        [Then(@"I check that customer name is displayed")]
        public void ThenICheckThatCustomerNameIsDisplayed()
        {
            Assert.True(WebDriver.GetByPath("//section//span[1][contains(text(), '" + MDOvariables.CustomerFirstname + " " + MDOvariables.CustomerLastname + "')]").Enabled);
        }


        [Then(@"I send message of anon")]
        public void ThenISendMessageOfAnon()
        {
            WebDriver.GetByPath("//span[contains(text(),'Send')]").Click();    
        }


    }
}
