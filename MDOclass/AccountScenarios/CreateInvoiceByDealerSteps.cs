﻿using System;
using System.Threading;
using Common;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.Events;
using OpenQA.Selenium.Support.UI;
using TechTalk.SpecFlow;



namespace MDOclass
{
    [Binding]
    public class CreateInvoiceByDealerSteps : BaseSteps
    {
        private string invoiceNumber;
        private string detailDescription;
        private string detailPrice;
        private string detailPrice2;
        private string additionalInstr;
        private string status = "Unpaid";
        private int amount;
        private string description;
        private string currency;
        private string salesPriceString;



        [Then(@"I swith to Invoices")]
        public void ThenISwithToInvoices()
        {
            WebDriver.GetByPath("//a[@href='/Dashboard/Invoices/List']").Click();
        }

        [Then(@"I click Create Invoice button")]
        public void ThenIClickCreateInvoiceButton()
        {
            WebDriver.GetByPath("//button[contains(text(),'Create Invoice')]").Click();
        }
        [Then(@"I fill Invoice Number field")]
        public void ThenIFillInvoiceNumberField()
        {
            var Handy = new HandyFunctions();
            invoiceNumber = Handy.RandomStringLatinAndNumbers(5);

            WebDriver.GetByName("invoiceNumber").SendKeys(invoiceNumber);
        }
        [Then(@"I choose Customer")]
        public void ThenIChooseCustomerTestuser_Com()
        {
            WebDriver.GetByPath("//a/span[2]/b").Click();
            WebDriver.GetByPath("//*[@id='select2-drop']/div/input").SendKeys(MDOvariables.CustomerLogin);
            WebDriver.GetByPath("//*[@id='select2-drop']/div/input").SendKeys(Keys.Enter);

        }
        [Then(@"I choose status Unpaid in InvoiceStatusField")]
        public void ThenIChooseStatusUnpaidInInvoiceStatusField()
        {
            var invoiceselect =
               new SelectElement(WebDriver.GetByName("statusPopupID"));
            invoiceselect.SelectByValue("14");
        }

        [Then(@"I choose Dirham in Currency")]
        public void ThenIChooseUSDollarInCurrency()
        {
            var currencyselect =
              new SelectElement(WebDriver.GetByName("currencyCode"));
            currencyselect.SelectByValue("AED");
        }

        [Then(@"I click Search Inventory ID")]
        public void ThenIClickSearchInventoryID()
        {
            WebDriver.GetByPath("//button/span[contains (text(), 'Search')]").Click();
        }
        [Then(@"I click on Shipment Date and choose current day")]
        public void ThenIClickOnShipmentDate()
        {
            WebDriver.GetByName("shipmentDate").Click();
            WebDriver.GetByPath("//td[@class='active day']").Click();
        }

        [Then(@"I type {{(.*)}} in Search input and click Search")]
        public void ThenITypeAcuraInSearchInput(string searchInput)
        {
            WebDriver.GetByPath("//div[@id='searchContainer']//input").SendKeys(searchInput);
            WebDriver.GetById("searchBtn").Click();
        }

        [Then(@"I type in Sales price {{(.*)}} for first detali")]
        public void ThenITypeInSalesPriceForFirstDetali(string price)
        {
            WebDriver.GetByPath("//table/tbody/tr[1]/td[2]/input").Clear();
            WebDriver.GetByPath("//table/tbody/tr[1]/td[2]/input").SendKeys(price);
        }

        [Then(@"I add Detail")]
        public void ThenIAddDetail()
        {
            WebDriver.GetByPath("//button[contains (text(), '+Add Detail')]").Click();
        }

        [Then(@"I type Description and Price for second detail")]
        public void ThenITypeDescriptionAndPriceForSecondDetail()
        {
            var Handy = new HandyFunctions();
            detailDescription = Handy.RandomStringLatinAndNumbers(5);

            Handy = new HandyFunctions();
            detailPrice = Handy.RandomStringNumbers(4);

            WebDriver.GetByPath("//table/tbody/tr[2]/td[1]/input").SendKeys(detailDescription);
            WebDriver.GetByPath("//table/tbody/tr[2]/td[2]/input").SendKeys(detailPrice);


        }
        [Then(@"I type Description and Price for third detail")]
        public void ThenITypeDescriptionAndPriceForThirdDetail()
        {
            var Handy = new HandyFunctions();
            detailDescription = Handy.RandomStringLatinAndNumbers(2);

            Handy = new HandyFunctions();
            detailPrice = Handy.RandomStringNumberWithOutZero(3);

            WebDriver.GetByPath("//table/tbody/tr[3]/td[1]/input").SendKeys(detailDescription);
            WebDriver.GetByPath("//table/tbody/tr[3]/td[2]/input").SendKeys(detailPrice);
        }


        [Then(@"I remove second detail")]
        public void ThenIRemoveSecondDetail()
        {
            WebDriver.GetByPath("//table/tbody/tr[2]/td[3]/button").Click();
        }

        [Then(@"I add Additional Instruction")]
        public void ThenIAddAdditionalInstruction()
        {
            var Handy = new HandyFunctions();
            additionalInstr = Handy.RandomStringLatinAndNumbers(13);


            WebDriver.GetByPath("//textarea[@class='textareaInvoice form-control']").SendKeys(additionalInstr);
        }

        [Then(@"I click Save and Send to Customer")]
        public void ThenIClickSaveAndSendToCustomer()
        {
            WebDriver.GetById("btnSaveAndShow").Click();
            Thread.Sleep(5000);
        }

        [Then(@"I check info in InvoiceTab by Dealer")]
        public void ThenICheckInfoInInvoiceTab()
        {

            //----------------------------------------------------Invoice#----------------------------------------------------------------------------------
            string actualInvoiceNumber = WebDriver.GetByPath("//table[@class='table table-bordered']/tbody/tr[1]/td[1]/span[3]").Text;//------задание актуального Invoice#

            if (invoiceNumber != actualInvoiceNumber)                                                                //-------сравнение
            {
                Assert.Fail("Invoice Number must be " + invoiceNumber + "\n Actual is " + actualInvoiceNumber);
            }


            //---------------------------------------------------SalePerson------------------------------------------------------------------------------

            string actualSalePerson = WebDriver.GetByPath("//table[@class='table table-bordered']/tbody/tr[1]/td[4]/span[2]").Text;//------задание актуального SalePerson
            string salePerson = (MDOvariables.DealerFirstname + ' ' + MDOvariables.DealerLastname);


            if (salePerson != actualSalePerson)                                                                //-------сравнение
            {
                Assert.Fail("Sale Person must be " + salePerson + "\n Actual is " + actualSalePerson);
            }

            //---------------------------------------------------Customer------------------------------------------------------------------------------
            string actualCustomerPerson = WebDriver.GetByPath("//table[@class='table table-bordered']/tbody/tr[1]/td[5]/span[2]").Text;//------задание актуального Customer
            string customerPerson = (MDOvariables.CustomerFirstname + ' ' + MDOvariables.CustomerLastname);


            if (customerPerson != actualCustomerPerson)                                                                //-------сравнение
            {
                Assert.Fail("Customer must be " + customerPerson + "\n Actual is " + actualCustomerPerson);
            }

            //---------------------------------------------------Date------------------------------------------------------------------------------
            var actualDateInvoiceText = WebDriver.GetByPath("//table[@class='table table-bordered']/tbody/tr[1]/td[6]/span[2]").Text;
            DateTime dateInvoice = DateTime.Today;
            DateTime actualDateInvoice = Convert.ToDateTime(actualDateInvoiceText);

            if (dateInvoice != actualDateInvoice)                                                                //-------сравнение
            {
                Assert.Fail("Date must be " + dateInvoice + "\n Actual is " + actualDateInvoice);
            }


            //---------------------------------------------------Status------------------------------------------------------------------------------
            string actualStatus = WebDriver.GetByPath("//table[@class='table table-bordered']/tbody/tr[1]/td[7]/span[2]").Text;//------задание актуального Customer
            

            if (status != actualStatus)                                                                //-------сравнение
            {
                Assert.Fail("Status must be " + status + "\n Actual is " + actualStatus);
            }

            //-------------------------------------------------Description----------------------------------------------------------------------------
            Assert.True(WebDriver.GetByPath("//table[@class='table table-bordered']/tbody/tr[1]/td[8]/span[2][contains(text(), 'ACURA')]").Enabled);
            

            //-------------------------------------------------Amount---------------------------------------------------------------------------------
            string actualPriceString = WebDriver.GetByPath("//table[@class='table table-bordered']/tbody/tr[1]/td[9]/span[2]").Text; //---------задание актуального Amount
            actualPriceString = actualPriceString.Substring(0, actualPriceString.Length - 4); //---------------------------------обрезка валюты AED
            int actualPrice = Convert.ToInt32(actualPriceString);                             //--------------------------------String > Int
            int salesPrice = Convert.ToInt32(salesPriceString);
            
            int detailPriceReal = Convert.ToInt32(detailPrice);
            amount = (detailPriceReal + salesPrice);                                             //---------------------------------сумма всех деталей

            if (amount != actualPrice)                                                                //-------сравнение
            {
                Assert.Fail("Amount must be " + amount + "\n Actual is " + actualPrice);
            }

            Assert.True(WebDriver.GetByPath("//table[@class='table table-bordered']/tbody/tr[1]/td[9]/span[2][contains(text(), 'AED')]").Enabled);

        }

        [Then(@"I see Notification about new Invoice")]
        public void ThenISeeNotificationAboutNewInvoice()
        {
            Assert.True(WebDriver.GetByPath("//span[@class='invoice badge'][contains(text(), '1')]").Enabled); 
        }

        [Then(@"I see Notifications about new Invoices in Dashboard")]
        public void ThenISeeNotificationsAboutNewInvoicesInDashboard()
        {
            Assert.True(WebDriver.GetByPath("//span[@class='invoice badge pull-right'][contains(text(), '1')]").Enabled);
            Assert.True(WebDriver.GetByPath("//span[@class='invoice badge'][contains(text(), '1')]").Enabled);
        }

        [Then(@"I switch to Invoices")]
        public void ThenISwitchToInvoices()
        {
            WebDriver.GetByPath("//a[@href='/Dashboard/Invoices/List']").Click();
        }

        [Then(@"I see label new")]
        public void ThenISeeLabelNew()
        {
            Assert.True(WebDriver.GetByPath("//span[contains(text(), 'new')]").Enabled);


            var NumberOfLabelsNew = WebDriver.FindElements(By.XPath("//span[contains(text(), 'new')]"));
            int NumberOfLabelsNewSum = (NumberOfLabelsNew.Count);

            if (NumberOfLabelsNewSum != 1)                                                                //-------сравнение
            {
                Assert.Fail("Labels NEW isn't 1");
            }
        }

        [Then(@"I click on Mark As Read")]
        public void ThenIClickOnMarkAsRead()
        {
           WebDriver.GetByPath("//a[contains(text(), 'Mark As Read')]").Click();
        }

        [Then(@"I check InvoiceTab by Customer")]
        public void ThenICheckInvoiceTabByCustomer()
        {
            //----------------------------------------------------Invoice#----------------------------------------------------------------------------------
            string actualInvoiceNumber = WebDriver.GetByPath("//table[@class='table table-bordered']/tbody/tr[1]/td[1]/span[3]").Text;//------задание актуального Invoice#

            if (invoiceNumber != actualInvoiceNumber)                                                                //-------сравнение
            {
                Assert.Fail("Invoice Number must be " + invoiceNumber + "\n Actual is " + actualInvoiceNumber);
            }

            //---------------------------------------------------SalePerson------------------------------------------------------------------------------

            string actualSalePerson = WebDriver.GetByPath("//table[@class='table table-bordered']/tbody/tr[1]/td[4]/span[2]").Text;//------задание актуального SalePerson
            string salePerson = (MDOvariables.DealerFirstname + ' ' + MDOvariables.DealerLastname);


            if (salePerson != actualSalePerson)                                                                //-------сравнение
            {
                Assert.Fail("Sale Person must be " + salePerson + "\n Actual is " + actualSalePerson);
            }
            //---------------------------------------------------Date------------------------------------------------------------------------------
            var actualDateInvoiceText = WebDriver.GetByPath("//table[@class='table table-bordered']/tbody/tr[1]/td[6]/span[2]").Text;
            DateTime dateInvoice = DateTime.Today;
            DateTime actualDateInvoice = Convert.ToDateTime(actualDateInvoiceText);

            if (dateInvoice != actualDateInvoice)                                                                //-------сравнение
            {
                Assert.Fail("Date must be " + dateInvoice + "\n Actual is " + actualDateInvoice);
            }


            //---------------------------------------------------Status------------------------------------------------------------------------------
            string actualStatus = WebDriver.GetByPath("//table[@class='table table-bordered']/tbody/tr[1]/td[7]/span[2]").Text;//------задание актуального Customer
            


            if (status != actualStatus)                                                                //-------сравнение
            {
                Assert.Fail("Status must be " + status + "\n Actual is " + actualStatus);
            }

            //-------------------------------------------------Description----------------------------------------------------------------------------
            Assert.True(WebDriver.GetByPath("//table[@class='table table-bordered']/tbody/tr[1]/td[8]/span[2][contains(text(), 'ACURA')]").Enabled);
            
            //-------------------------------------------------Amount---------------------------------------------------------------------------------
            string actualPriceString = WebDriver.GetByPath("//table[@class='table table-bordered']/tbody/tr[1]/td[9]/span[2]").Text; //---------задание актуального Amount
            actualPriceString = actualPriceString.Substring(0, actualPriceString.Length - 4); //---------------------------------обрезка валюты AED
            int actualPrice = Convert.ToInt32(actualPriceString);                             //--------------------------------String > Int

                        

            if (amount != actualPrice)                                                                //-------сравнение
            {
                Assert.Fail("Amount must be " + amount + "\n Actual is " + actualPrice);
            }
            currency = "AED";

            Assert.True(WebDriver.GetByPath("//table[@class='table table-bordered']/tbody/tr[1]/td[9]/span[2][contains(text(), " + currency + ")]").Enabled);

        }


        [Then(@"I make logout from Dashboard")]
        public void ThenIMakeLogoutFromDashboard()
        {
            WebDriver.GetById("imgLogo").Click();
            WebDriver.GetById("logoff").Click();
        }


 

        [Then(@"I click View")]
        public void ThenIClickView()
        {
            WebDriver.GetByPath("//span[contains(text(), 'View')]").Click();
        }



        [Then(@"I click Save Invoice")]
        public void ThenIClickSave()
        {
            WebDriver.GetById("btnSave").Click();
            Thread.Sleep(5000);
        }

        [Then(@"I click Edit Invoice")]
        public void ThenIClickEditInvoice()
        {
            Thread.Sleep(3000);
            WebDriver.GetByPath("//table/tbody/tr[1]/td[10]//a[@class='btn btn-xs btn-success']/span[contains(text(), 'Edit')]").Click();
            Thread.Sleep(3000);
        }

        [Then(@"I change Invoice Number")]
        public void ThenIChangeInvoiceNumber()
        {
            var Handy = new HandyFunctions();
            invoiceNumber = Handy.RandomStringLatinAndNumbers(5);

            WebDriver.GetByName("invoiceNumber").Clear();
            WebDriver.GetByName("invoiceNumber").SendKeys(invoiceNumber);
        }

        [Then(@"I change Detail Price")]
        public void ThenIChangeDetailPrice()
        {
           var Handy = new HandyFunctions();
            detailPrice = Handy.RandomStringNumbers(4);

            WebDriver.GetByPath("//table/tbody/tr[2]/td[2]/input").Clear();
            WebDriver.GetByPath("//table/tbody/tr[2]/td[2]/input").SendKeys(detailPrice);
        }



        [Then(@"I click on Post Invoice button")]
        public void ThenIClickOnPostInvoiceButton()
        {
            WebDriver.GetById("CPH_ucOpenInvoice_lblPostInvoice").Click();
        }

        [Then(@"I choose Buyer for Invoice")]
        public void ThenIChooseBuyerForInvoice()
        {
            WebDriver.GetByPath("//div[@id='buyerList_chosen']/a/div/b").Click();
            WebDriver.GetByPath("//div[@id='buyerList_chosen']//input").Click();

            WebDriver.GetByPath("//div[@id='buyerList_chosen']//input").SendKeys(MDOvariables.CustomerLogin);
            WebDriver.GetByPath("//div[@id='buyerList_chosen']//input").SendKeys(Keys.Enter);

        }

        [Then(@"I click on Shipment Date and choose today day")]
        public void ThenIClickOnShipmentDateAndChooseTodayDay()
        {
            WebDriver.GetByPath("//div[@id='shippingDate']//span[@class='glyphicon glyphicon-circle-arrow-down']").Click();
            WebDriver.GetByPath("//td[@class='today day']").Click();
        }


        [Then(@"I fill Description and Amount and click Add in Invoice popup")]
        public void ThenIFillDescriptionAndAmountAndClickAdd()
        {
            var Handy = new HandyFunctions();
            detailDescription = Handy.RandomStringLatinAndNumbers(5);

            Handy = new HandyFunctions();
            detailPrice = Handy.RandomStringNumbers(4);



            WebDriver.GetByPath("//input[@placeholder='Description']").SendKeys(detailDescription);
            WebDriver.GetByPath("//input[@placeholder='Amount']").SendKeys(detailPrice);

            WebDriver.GetById("addInvoiceDetail").Click();

            string salesPrice = WebDriver.GetByPath("//tbody[@id='trInvoceDetails']/tr/td[2]").Text;
            salesPrice = salesPrice.Substring(2, salesPrice.Length - 6); //---------------------------------обрезка валюты AED

            var actualPrice = Convert.ToInt32(salesPrice);                             //--------------------------------String > Int
            int detailPriceT = Convert.ToInt32(detailPrice);

            amount = actualPrice + detailPriceT;


        }
        [Then(@"I fill Additional Option in Invoice popup")]
        public void ThenIFillAdditionalOptionInInvoicePopup()
        {
            WebDriver.GetById("additionalInstruction").SendKeys(detailDescription);
        }



        [Then(@"I click Save and Send to Customer from Vehicle page")]
        public void ThenIClickSaveAndSendToCustomerFromVehiclePage()
        {
            WebDriver.GetById("invoiceSaveAndShowToCustomer").Click();
        }
        [Then(@"I check InvoiceTab from VehiclePage by Customer")]
        public void ThenICheckInvoiceTabFromVehiclePageByCustomer()
        {
            //----------------------------------------------------Invoice#----------------------------------------------------------------------------------
            string actualInvoiceNumber = WebDriver.GetByPath("//table[@class='table table-bordered']/tbody/tr[1]/td[1]/span[3]").Text;//------задание актуального Invoice#

            if (invoiceNumber != actualInvoiceNumber)                                                                //-------сравнение
            {
                Assert.Fail("Invoice Number must be " + invoiceNumber + "\n Actual is " + actualInvoiceNumber);
            }

            //---------------------------------------------------SalePerson------------------------------------------------------------------------------

            string actualSalePerson = WebDriver.GetByPath("//table[@class='table table-bordered']/tbody/tr[1]/td[4]/span[2]").Text;//------задание актуального SalePerson
            string salePerson = (MDOvariables.DealerFirstname + ' ' + MDOvariables.DealerLastname);


            if (salePerson != actualSalePerson)                                                                //-------сравнение
            {
                Assert.Fail("Sale Person must be " + salePerson + "\n Actual is " + actualSalePerson);
            }
            //---------------------------------------------------Date------------------------------------------------------------------------------
            var actualDateInvoiceText = WebDriver.GetByPath("//table[@class='table table-bordered']/tbody/tr[1]/td[6]/span[2]").Text;
            DateTime dateInvoice = DateTime.Today;
            DateTime actualDateInvoice = Convert.ToDateTime(actualDateInvoiceText);

            if (dateInvoice != actualDateInvoice)                                                                //-------сравнение
            {
                Assert.Fail("Date must be " + dateInvoice + "\n Actual is " + actualDateInvoice);
            }


            //---------------------------------------------------Status------------------------------------------------------------------------------
            string actualStatus = WebDriver.GetByPath("//table[@class='table table-bordered']/tbody/tr[1]/td[7]/span[2]").Text;//------задание актуального Customer
           status = ("Paid");


            if (status != actualStatus)                                                                //-------сравнение
            {
                Assert.Fail("Status must be " + status + "\n Actual is " + actualStatus);
            }

            //---------------------------------------------------Description------------------------------------------------------------------------------
            string actualDescription = WebDriver.GetByPath("//table[@class='table table-bordered']/tbody/tr[1]/td[8]/span[2]").Text;//------задание актуального Customer


        description =    description.ToUpper();
        actualDescription = actualDescription.ToUpper();
            if (actualDescription != description)                                                                //-------сравнение
            {
                Assert.Fail("Status must be " + description + "\n Actual is " + actualDescription);
            }

            //-------------------------------------------------Amount---------------------------------------------------------------------------------
            string actualPriceString = WebDriver.GetByPath("//table[@class='table table-bordered']/tbody/tr[1]/td[9]/span[2]").Text; //---------задание актуального Amount
            actualPriceString = actualPriceString.Substring(0, actualPriceString.Length - 4); //---------------------------------обрезка валюты
            int actualPrice = Convert.ToInt32(actualPriceString);                             //--------------------------------String > Int
            
          
            if (amount != actualPrice)                                                                //-------сравнение
            {
                Assert.Fail("Amount must be " + amount + "\n Actual is " + actualPrice);
            }
        }

        [Then(@"I save current Description for Invoice from Vehicle page")]
        public void ThenISaveCurrentDescriptionForInvoiceFromVehiclePage()
        {
            description = WebDriver.GetById("CPH_lblVehicleHeader").Text;
            
        }

        [Then(@"I change description in Invoice pop up")]
        public void ThenIChangeDescriptionInInvoicePopUp()
        {
            WebDriver.GetByName("invoiceNumber").SendKeys("47");
            invoiceNumber = (invoiceNumber + "47");
        }

        [Then(@"I type Description and Price for third detail again")]
        public void ThenITypeDescriptionAndPriceForThirdDetailAgain()
        {
            var Handy = new HandyFunctions();
            detailDescription = Handy.RandomStringLatinAndNumbers(2);

            
            detailPrice2 = Handy.RandomStringNumberWithOutZero(3);

            WebDriver.GetByPath("//table/tbody/tr[3]/td[1]/input").SendKeys(detailDescription);
            WebDriver.GetByPath("//table/tbody/tr[3]/td[2]/input").SendKeys(detailPrice2);

            
        }
        [Then(@"I change Status of Description to Paid")]
        public void ThenIChangeStatusOfDescription()
        {
            var invoiceselect =
   new SelectElement(WebDriver.GetByName("statusPopupID"));
            invoiceselect.SelectByValue("13");
        }

        [Then(@"I check info in InvoiceTab by Dealer again")]
        public void ThenICheckInfoInInvoiceTabByDealerAgain()
        {
            Thread.Sleep(2000);
            //----------------------------------------------------Invoice#----------------------------------------------------------------------------------
            string actualInvoiceNumber = WebDriver.GetByPath("//table[@class='table table-bordered']/tbody/tr[1]/td[1]/span[3]").Text;//------задание актуального Invoice#

            if (invoiceNumber != actualInvoiceNumber)                                                                //-------сравнение
            {
                Assert.Fail("Invoice Number must be " + invoiceNumber + "\n Actual is " + actualInvoiceNumber);
            }


            //---------------------------------------------------SalePerson------------------------------------------------------------------------------

            string actualSalePerson = WebDriver.GetByPath("//table[@class='table table-bordered']/tbody/tr[1]/td[4]/span[2]").Text;//------задание актуального SalePerson
            string salePerson = (MDOvariables.DealerFirstname + ' ' + MDOvariables.DealerLastname);


            if (salePerson != actualSalePerson)                                                                //-------сравнение
            {
                Assert.Fail("Sale Person must be " + salePerson + "\n Actual is " + actualSalePerson);
            }

            //---------------------------------------------------Customer------------------------------------------------------------------------------
            string actualCustomerPerson = WebDriver.GetByPath("//table[@class='table table-bordered']/tbody/tr[1]/td[5]/span[2]").Text;//------задание актуального Customer
            string customerPerson = (MDOvariables.CustomerFirstname + ' ' + MDOvariables.CustomerLastname);


            if (customerPerson != actualCustomerPerson)                                                                //-------сравнение
            {
                Assert.Fail("Customer must be " + customerPerson + "\n Actual is " + actualCustomerPerson);
            }

            //---------------------------------------------------Date------------------------------------------------------------------------------
            var actualDateInvoiceText = WebDriver.GetByPath("//table[@class='table table-bordered']/tbody/tr[1]/td[6]/span[2]").Text;
            DateTime dateInvoice = DateTime.Today;
            DateTime actualDateInvoice = Convert.ToDateTime(actualDateInvoiceText);

            if (dateInvoice != actualDateInvoice)                                                                //-------сравнение
            {
                Assert.Fail("Date must be " + dateInvoice + "\n Actual is " + actualDateInvoice);
            }


            //---------------------------------------------------Status------------------------------------------------------------------------------
            string actualStatus = WebDriver.GetByPath("//table[@class='table table-bordered']/tbody/tr[1]/td[7]/span[2]").Text;//------задание актуального Customer

            status = "Paid";
            if (status != actualStatus)                                                                //-------сравнение
            {
                Assert.Fail("Status must be " + status + "\n Actual is " + actualStatus);
            }

            //-------------------------------------------------Description----------------------------------------------------------------------------
            Assert.True(WebDriver.GetByPath("//table[@class='table table-bordered']/tbody/tr[1]/td[8]/span[2][contains(text(), 'ACURA')]").Enabled);


            //-------------------------------------------------Amount---------------------------------------------------------------------------------
            string actualPriceString = WebDriver.GetByPath("//table[@class='table table-bordered']/tbody/tr[1]/td[9]/span[2]").Text; //---------задание актуального Amount
            actualPriceString = actualPriceString.Substring(0, actualPriceString.Length - 4); //---------------------------------обрезка валюты AED
            int actualPrice = Convert.ToInt32(actualPriceString);                             //--------------------------------String > Int
            int salesPrice = Convert.ToInt32(salesPriceString);

            int detailPriceReal = Convert.ToInt32(detailPrice);
            int detailPriceReal2 = Convert.ToInt32(detailPrice2);
            amount = (detailPriceReal + detailPriceReal2 + salesPrice);                                             //---------------------------------сумма всех деталей

            if (amount != actualPrice)                                                                //-------сравнение
            {
                Assert.Fail("Amount must be " + amount + "\n Actual is " + actualPrice);
            }

            Assert.True(
                WebDriver.GetByPath("//table[@class='table table-bordered']/tbody/tr[1]/td[9]/span[2][contains(text(), " + currency + ")]").Enabled);
        }

        [Then(@"I change Currency to British Pound")]
        public void ThenIChangeCurrencyToBritishPound()
        {
            var currencyselect =
  new SelectElement(WebDriver.GetByName("currencyCode"));
            currencyselect.SelectByValue("GBP");
            currency = "GBP";
        }

        [Then(@"I save current price of Vehicle")]
        public void ThenISaveCurrentPriceOfVehicle()
        {
          salesPriceString =  WebDriver.GetByPath("//table/tbody/tr/td[2]/input").GetAttribute("value");
        }


        [Then(@"I check info in InvoiceTab from Vehicle page by Dealer again")]
        public void ThenICheckInfoInInvoiceTabFromVehiclePageByDealerAgain()
        {
            Thread.Sleep(2000);
            //----------------------------------------------------Invoice#----------------------------------------------------------------------------------
            string actualInvoiceNumber = WebDriver.GetByPath("//table[@class='table table-bordered']/tbody/tr[1]/td[1]/span[3]").Text;//------задание актуального Invoice#

            if (invoiceNumber != actualInvoiceNumber)                                                                //-------сравнение
            {
                Assert.Fail("Invoice Number must be " + invoiceNumber + "\n Actual is " + actualInvoiceNumber);
            }


            //---------------------------------------------------SalePerson------------------------------------------------------------------------------

            string actualSalePerson = WebDriver.GetByPath("//table[@class='table table-bordered']/tbody/tr[1]/td[4]/span[2]").Text;//------задание актуального SalePerson
            string salePerson = (MDOvariables.DealerFirstname + ' ' + MDOvariables.DealerLastname);


            if (salePerson != actualSalePerson)                                                                //-------сравнение
            {
                Assert.Fail("Sale Person must be " + salePerson + "\n Actual is " + actualSalePerson);
            }

            //---------------------------------------------------Customer------------------------------------------------------------------------------
            string actualCustomerPerson = WebDriver.GetByPath("//table[@class='table table-bordered']/tbody/tr[1]/td[5]/span[2]").Text;//------задание актуального Customer
            string customerPerson = (MDOvariables.CustomerFirstname + ' ' + MDOvariables.CustomerLastname);


            if (customerPerson != actualCustomerPerson)                                                                //-------сравнение
            {
                Assert.Fail("Customer must be " + customerPerson + "\n Actual is " + actualCustomerPerson);
            }

            //---------------------------------------------------Date------------------------------------------------------------------------------
            var actualDateInvoiceText = WebDriver.GetByPath("//table[@class='table table-bordered']/tbody/tr[1]/td[6]/span[2]").Text;
            DateTime dateInvoice = DateTime.Today;
            DateTime actualDateInvoice = Convert.ToDateTime(actualDateInvoiceText);

            if (dateInvoice != actualDateInvoice)                                                                //-------сравнение
            {
                Assert.Fail("Date must be " + dateInvoice + "\n Actual is " + actualDateInvoice);
            }


            //---------------------------------------------------Status------------------------------------------------------------------------------
            string actualStatus = WebDriver.GetByPath("//table[@class='table table-bordered']/tbody/tr[1]/td[7]/span[2]").Text;//------задание актуального Customer

            status = "Paid";
            if (status != actualStatus)                                                                //-------сравнение
            {
                Assert.Fail("Status must be " + status + "\n Actual is " + actualStatus);
            }

            //-------------------------------------------------Description----------------------------------------------------------------------------
            string actualDescription = WebDriver.GetByPath("//table[@class='table table-bordered']/tbody/tr[1]/td[8]/span[2]").Text;//------задание актуального Customer


            description = description.ToUpper();
            actualDescription = actualDescription.ToUpper();
            if (actualDescription != description)                                                                //-------сравнение
            {
                Assert.Fail("Status must be " + description + "\n Actual is " + actualDescription);
            }


            //-------------------------------------------------Amount---------------------------------------------------------------------------------
            string actualPriceString = WebDriver.GetByPath("//table[@class='table table-bordered']/tbody/tr[1]/td[9]/span[2]").Text; //---------задание актуального Amount
            actualPriceString = actualPriceString.Substring(0, actualPriceString.Length - 4); //---------------------------------обрезка валюты AED
            int actualPrice = Convert.ToInt32(actualPriceString);                             //--------------------------------String > Int
            int salesPrice = Convert.ToInt32(salesPriceString);

            int detailPriceReal = Convert.ToInt32(detailPrice);
            int detailPriceReal2 = Convert.ToInt32(detailPrice2);
            amount = (detailPriceReal + detailPriceReal2 + salesPrice);                                             //---------------------------------сумма всех деталей

            if (amount != actualPrice)                                                                //-------сравнение
            {
                Assert.Fail("Amount must be " + amount + "\n Actual is " + actualPrice);
            }

            Assert.True(
                WebDriver.GetByPath("//table[@class='table table-bordered']/tbody/tr[1]/td[9]/span[2][contains(text(), " + currency + ")]").Enabled);
        }

        [Then(@"I save current price of Vehicle in Invoice")]
        public void ThenISaveCurrentPriceOfVehicleInInvoice()
        {
            string actualPriceString = WebDriver.GetByPath("//table/tbody/tr/td[2]").Text;
            salesPriceString = actualPriceString.Substring(1, actualPriceString.Length - 4);
            
        }

    }

}
