﻿Feature: SendingMessageByCustomer
As a customer I send message and then check it as a Dealer. Send answer and check it as Customer again

@SendingMessageFromVehiclePage
Scenario Outline: Sending messages from Vehicle page
	Given I open <main page> and type <phone>
	And I click on SignIn button
	When I type my login and password of a customer and submit them
	Then I click on search button
	Then I choose 5th vehicle
	Then I type messages and send it
	Then I check that message was sent and get ID and Title
	Then I make logout
	
	Then I click on SignIn button
	When I type my login and password of a dealer and submit them
	Then I check Notification in Header about message from Customer
	Then I go to the Dashboard
	Then I check Notification in Dashboard
	Then I open Messages
	Then I check message from Customer
	Then I type message from Dealer
	Then I delete message from Dashboard
		
	Then I click on logo
	Then I make logout
	Then I click on SignIn button
	When I type my login and password of a customer and submit them
	Then I check Notification in Header about message from Dealer
	Then I go to the Dashboard
	Then I check Notification in Dashboard
	Then I check messsage from Dealer
	Then I check ID and Title
	
	
	Then I wait 60 seconds

	Then I open gmail.com
	Then I enter our dealer's email and password and submit them (for GMAIL.COM)
	Then I open the last message at GMAIL.COM
	Then I should see notification about message

	Given I open <main page> and type <phone>
	Then I go to the Dashboard
	Then I type message from Customer again
	Then I delete message from Dashboard
	Then I click on logo

	Then I make logout
	Then I click on SignIn button
	
	When I type my login and password of a dealer and submit them
	Then I check Notification in Header about message from Customer
	Then I go to the Dashboard
	Then I check Notification in Dashboard
	Then I open Messages
	Then I check message from Customer
	Then I delete message from Dashboard

		
	Examples: 
	| main page       | phone      |
	|      bronze	  | 7327572923 | 
	|      silver     |   0        |
	|       gold      |   0	       |

	@SendingMessageFromGalleryListView
	Scenario Outline: Sending messages from Gallery List
	Given I open <main page> and type <phone>
	And I click on SignIn button
	When I type my login and password of a GMAILcustomer and submit them
	Then I click on search button
	Then I click on List Preview displaying
	Then I click on AskDealerAQuestion button
	Then I type message in this input and send it
	Then I check that message was sent from Gallery List and get ID and Title
	
	
	
	Then I make logout
	Then I click on SignIn button
	When I type my login and password of a dealer and submit them
	Then I check Notification in Header about message from Customer
	Then I go to the Dashboard
	Then I check Notification in Dashboard
	Then I open Messages
	Then I check message from Customer
	Then I type message from Dealer
	Then I delete message from Dashboard
	Then I click on logo
	Then I make logout
	Then I click on SignIn button
	When I type my login and password of a GMAILcustomer and submit them
	Then I check Notification in Header about message from Dealer
	Then I go to the Dashboard
	Then I check Notification in Dashboard
	Then I check messsage from Dealer
	Then I check ID and Title
	Then I delete message from Dashboard

	Then I wait 60 seconds

	Then I open gmail.com
	Then I enter our dealer's email and password and submit them (for GMAIL.COM)
	Then I open the last message at GMAIL.COM
	Then I should see notification about message

	Then I clean my cookies
	Then I open gmail.com
	Then I click sign in different account (gmail)
	Then I click add acount (gmail)
	Then I enter  our customer's email and password and submit them (for GMAIL.COM)
	Then I open the last message at GMAIL.COM
	Then I should see notification about message from dealer

	Examples: 
	| main page       | phone      |
	|      bronze	  | 7327572923 | 
	|      silver     |   0        |
	|       gold      |   0	       |

	@chcekingCheckboxAboutInvoice
	Scenario Outline: Checking label about Invoice
	Given I open <main page> and type <phone>
	And I click on SignIn button
	When I type my login and password of a customer and submit them
	Then I click on search button
	Then I choose 5th vehicle
	Then I click on checkbox about Invoice
	Then I type messages and send it
	Then I check that message was sent and get ID and Title with invoice
	Then I go to the Dashboard
	Then I delete message from Dashboard
	Then I click on logo
	Then I make logout
	Then I click on SignIn button

	When I type my login and password of a dealer and submit them
	Then I check Notification in Header about message from Customer
	Then I go to the Dashboard
	Then I check Notification in Dashboard
	Then I open Messages
	Then I check message from Customer
	Then I check label about Invoice
	Then I delete message from Dashboard

	Then I open Deleted Tab
	Then I type in search message input customer's message and try to find it
	Then I check that message was found



	
	Examples: 
	| main page       | phone      |
	|      bronze	  | 7327572923 | 
	|      silver     |   0        |
	|       gold      |   0	       |

	@checkingSendingMessagesByAnonymous
	Scenario Outline: Sending message from Vehicle page by Anonymous
	Given I open <main page> and type <phone>
	
	Then I click on search button
	Then I choose 7th vehicle
	Then I click on checkbox about Invoice
	Then I fill my Name and Email
	Then I type messages and send it


	Then I click on SignIn button
	When I type my login and password of a dealer and submit them
	Then I check Notification in Header about message from Customer
	Then I go to the Dashboard
	Then I check Notification in Dashboard
	Then I open Messages
	Then I check message from Customer
	Then I check name of Anonymous
	Then I check email of Anonymous
	Then I check label about Invoice
	Then I delete message from Dashboard
		
	Then I wait 60 seconds

	Then I open gmail.com
	Then I enter our dealer's email and password and submit them (for GMAIL.COM)
	Then I open the last message at GMAIL.COM
	Then I should see notification about message from anon
	
		Examples: 
	| main page       | phone      |
	|      bronze	  | 7327572923 | 
	|      silver     |   0        |
	|       gold      |   0	       |

	@checkingSendingMessagesByAnonymousFromSearchPage
	Scenario Outline: Sending message from Search page by Anonymous
	Given I open <main page> and type <phone>

	Then I click on search button
	Then I click on List Preview displaying
	Then I click on AskDealerAQuestion button
	Then I type message in this input and send it
	Then I type Name of anon and Email and agree with invoice
	Then I send message of anon
	
	Then I click on SignIn button
	When I type my login and password of a dealer and submit them
	Then I check Notification in Header about message from Customer
	Then I go to the Dashboard
	Then I check Notification in Dashboard
	Then I open Messages
	Then I check message from Customer
	Then I check name of Anonymous
	Then I check email of Anonymous
	Then I check label about Invoice
	Then I delete message from Dashboard
		
	Then I wait 60 seconds

	Then I open gmail.com
	Then I enter our dealer's email and password and submit them (for GMAIL.COM)
	Then I open the last message at GMAIL.COM
	Then I should see notification about message from anon

			Examples: 
	| main page       | phone      |
	|      bronze	  | 7327572923 | 
	|      silver     |   0        |
	|       gold      |   0	       |


	@CheckCyrillikMessageAndUserNameSearch
	Scenario Outline: Check cyrillic message and username search
	Given I open <main page> and type <phone>
	And I click on SignIn button
	When I type my login and password of a customer and submit them
	Then I click on search button
	Then I choose 5th vehicle
	Then I type cyryllic messages and send it
	Then I check that message was sent and get ID and Title
	Then I go to the Dashboard
	Then I delete message from Dashboard
	Then I click on logo
	Then I make logout


	Then I click on SignIn button
	When I type my login and password of a dealer and submit them
	Then I check Notification in Header about message from Customer
	Then I go to the Dashboard
	Then I check Notification in Dashboard
	Then I open Messages
	Then I check message from Customer
	Then I delete message from Dashboard
	
	Then I open Deleted Tab
	Then I type in search message input customer's message and try to find it
	Then I check that message was found
	
	Then I type customer name in search input
	Then I check that customer name is displayed





		Examples: 
	| main page       | phone      |
	|      bronze	  | 7327572923 | 
	|      silver     |   0        |
	|       gold      |   0	       |