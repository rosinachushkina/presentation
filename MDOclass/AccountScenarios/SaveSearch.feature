﻿Feature: SaveSearch
	
@SaveSearch
Scenario Outline: SaveSearchCustomer
Given I open <webSite> and type <phone>
Then I select USA as a country for the fiter
Then I select a random state
Then I select a random Make for my search
Then I click on a regular search button
Then I click Save Search

When I type my login and password of a customer and submit them
Then My selected search criterias should be refrected in SaveSearch window
Then I click Save
Then I close the window
Then I refresh the page
Then I should have my SavedSearch with all parameters selected
Then I log out

And I click on SignIn button
When I type my login and password of a dealer and submit them
Then I go to Dashboard > MyListings
Then I switch to Customers List
Then I find our test customer in CustomerList
Then I click on his SavedSearch (CustomerList)
Then His SavedSearch should be there
Then I log out

When I type my login and password of a customer and submit them
Then I delete my SavedSearch

	Examples: 
	| webSite         | phone      |
	|      bronze	  | 7327572923 | 
	|      silver     |   0        |
	|       gold      |   0	       |