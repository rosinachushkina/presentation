﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Permissions;
using System.Text;
using System.Threading;
using Common;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.PageObjects;
using TechTalk.SpecFlow;
using System.Windows.Forms;
using NUnit.Framework;

namespace MDOclass.AccountScenarios
{
    [Binding]
    public class ProfileUpdateSteps : BaseSteps
    {
        private string VerName, VerLast, VerEmail, VerPhone, VerCountry, VerState, VerCity, VerAddr1, VerAddr2, VerZIP;

        [Then(@"I go to My Profile")]
        public void ThenIGoToMyProfile()
        {
            WebDriver.FindElement(By.XPath("//img[@id='imgLogo']")).Click();
            WebDriver.FindElement(By.XPath("//img[@id='imgLogo']")).Click();
            WebDriver.FindElement(By.XPath("//a[@href='/Dashboard/Accounts/EditProfile']")).Click();
            
        }

        [Then(@"I enter my new correct customer information (.*),(.*),(.*),(.*),(.*),(.*),(.*),(.*),(.*)")]
        public void ThenIEnterMyNewCorrectCustomerInformation(string p0, string p1, string p2, string p3, string p4, string p5, string p6, string p7, string p8)
        {
            WebDriver.FindElement(By.XPath("//input[@id='FirstName']")).Clear();
            WebDriver.FindElement(By.XPath("//input[@id='FirstName']")).SendKeys(MDOvariables.CustomerFirstname);
            WebDriver.FindElement(By.XPath("//input[@id='LastName']")).Clear();
            WebDriver.FindElement(By.XPath("//input[@id='LastName']")).SendKeys(MDOvariables.CustomerLastname);
            WebDriver.FindElement(By.XPath("//input[@id='Email']")).Clear();
            WebDriver.FindElement(By.XPath("//input[@id='Email']")).SendKeys(MDOvariables.CustomerLogin);
            WebDriver.FindElement(By.XPath("//input[@id='Phone']")).Clear();
            WebDriver.FindElement(By.XPath("//input[@id='Phone']")).SendKeys(MDOvariables.CustomerPhone);
//          WebDriver.FindElement(By.XPath("//input[@id='inputState']")).Clear();
//          WebDriver.FindElement(By.XPath("//input[@id='inputState']")).SendKeys(p4);
            WebDriver.FindElement(By.XPath("//input[@id='City']")).Clear();
            WebDriver.FindElement(By.XPath("//input[@id='City']")).SendKeys(p5);
            WebDriver.FindElement(By.XPath("//input[@id='Address']")).Clear();
            WebDriver.FindElement(By.XPath("//input[@id='Address']")).SendKeys(p6);
            WebDriver.FindElement(By.XPath("//input[@id='Address2']")).Clear();
            WebDriver.FindElement(By.XPath("//input[@id='Address2']")).SendKeys(p7);
            WebDriver.FindElement(By.XPath("//input[@id='PostalCode']")).Clear();
            WebDriver.FindElement(By.XPath("//input[@id='PostalCode']")).SendKeys(p8);

            //-------------------Сбрасываем все входные данные в проперти для сверки позже
            VerName = MDOvariables.CustomerFirstname;
            VerLast = MDOvariables.CustomerLastname;
            VerEmail = MDOvariables.CustomerLogin;
            VerPhone = MDOvariables.CustomerPhone;
            VerCountry = WebDriver.FindElement(By.XPath("//option[@selected='selected']")).Text;
            VerState = WebDriver.FindElement(By.XPath("//input[@id='inputState']")).GetAttribute("value");
            VerCity = p5;
            VerAddr1 = p6;
            VerAddr2 = p7;
            VerZIP = p8;
        }


        [Then(@"I select my new country as a customer")]
        public void ThenISelectMyNewCountryAsACustomer()
        {
            WebDriver.FindElement(By.XPath("//select[@id='CountryCode']")).Click();
            WebDriver.FindElement(By.XPath("//option[@value='US']")).Click();
            WebDriver.FindElement(By.XPath("//option[@value='US']")).Submit();
        }

        [Then(@"I select a state as a customer")]
        public void ThenISelectAStateAsACustomer()
        {
            WebDriver.FindElement(By.XPath("//select[@id='selectState']")).Click();
            WebDriver.FindElement(By.XPath("//select[@id='selectState']//option[@value='ID']")).Click();
            WebDriver.FindElement(By.XPath("//select[@id='selectState']//option[@value='ID']")).Submit();
        }

        [Then(@"I upload my new photo from ""(.*)""")]
        public void ThenIUploadMyNewPhotoFrom(string p0)
        {
            var element = WebDriver.FindElement(By.XPath("//*[@id='btnAddPhoto']"));
            element.Click();

            //-------------------Загружаем картинку, путь которой передали
            Thread.Sleep(1000);
            SendKeys.SendWait(p0);
            Thread.Sleep(1000);
            SendKeys.SendWait(@"{Enter}");

        }
   
        [Then(@"I click the Save button")]
        public void ThenIClickTheSaveButton()
        {
            WebDriver.FindElement(By.XPath("//input[@class='btn btn-success pull-right']")).Click();
        }

        [Then(@"My new customer profile information should be there")]
        public void ThenMyNewCustomerProfileInformationShouldBeThere()
        {
            Assert.IsTrue(VerName == WebDriver.FindElement(By.XPath("//input[@id='FirstName']")).GetAttribute("value"));
            Assert.IsTrue(VerLast == WebDriver.FindElement(By.XPath("//input[@id='LastName']")).GetAttribute("value"));
            Assert.IsTrue(VerEmail == WebDriver.FindElement(By.XPath("//input[@id='Email']")).GetAttribute("value"));
            Assert.IsTrue(VerPhone == WebDriver.FindElement(By.XPath("//input[@id='Phone']")).GetAttribute("value"));
            Assert.IsTrue(VerState == WebDriver.FindElement(By.XPath("//input[@id='inputState']")).GetAttribute("value"));
            Assert.IsTrue(VerCity == WebDriver.FindElement(By.XPath("//input[@id='City']")).GetAttribute("value"));
            Assert.IsTrue(VerAddr1 == WebDriver.FindElement(By.XPath("//input[@id='Address']")).GetAttribute("value"));
            Assert.IsTrue(VerAddr2 == WebDriver.FindElement(By.XPath("//input[@id='Address2']")).GetAttribute("value"));
            Assert.IsTrue(VerZIP == WebDriver.FindElement(By.XPath("//input[@id='PostalCode']")).GetAttribute("value"));
            Assert.IsTrue(VerCountry==WebDriver.FindElement(By.XPath("//option[@selected='selected']")).Text);
        }

        [Then(@"I enter my new correct dealer information")]
        public void ThenIEnterMyNewCorrectDealerInformation()
        {
            WebDriver.FindElement(By.XPath("//input[@id='FirstName']")).Clear();
            WebDriver.FindElement(By.XPath("//input[@id='FirstName']")).SendKeys(MDOvariables.DealerFirstname);
            WebDriver.FindElement(By.XPath("//input[@id='LastName']")).Clear();
            WebDriver.FindElement(By.XPath("//input[@id='LastName']")).SendKeys(MDOvariables.DealerLastname);
            WebDriver.FindElement(By.XPath("//input[@id='Email']")).Clear();
            WebDriver.FindElement(By.XPath("//input[@id='Email']")).SendKeys(MDOvariables.DealerEmail);
            WebDriver.FindElement(By.XPath("//input[@id='Phone']")).Clear();
            WebDriver.FindElement(By.XPath("//input[@id='Phone']")).SendKeys(MDOvariables.DealerPhone);
        }

        [Then(@"I upload my new dealer photo from ""(.*)""")]
        public void ThenIUploadMyNewDealerPhotoFrom(string p0)
        {
            var element = WebDriver.FindElement(By.XPath("//*[@id='btnAddPhoto']"));
            element.Click();

            //-------------------Загружаем картинку, путь которой передали
            Thread.Sleep(1000);
            SendKeys.SendWait(p0);
            Thread.Sleep(1000);
            SendKeys.SendWait(@"{Enter}");
        }

        [Then(@"My new dealer profile information should be there")]
        public void ThenMyNewDealerProfileInformaionShouldBeThere()
        {
            Assert.IsTrue(MDOvariables.DealerFirstname == WebDriver.FindElement(By.XPath("//input[@id='FirstName']")).GetAttribute("value"));
            Assert.IsTrue(MDOvariables.DealerLastname == WebDriver.FindElement(By.XPath("//input[@id='LastName']")).GetAttribute("value"));
            Assert.IsTrue(MDOvariables.DealerEmail == WebDriver.FindElement(By.XPath("//input[@id='Email']")).GetAttribute("value"));
            Assert.IsTrue(MDOvariables.DealerPhone == WebDriver.FindElement(By.XPath("//input[@id='Phone']")).GetAttribute("value"));
        }

    }
}
