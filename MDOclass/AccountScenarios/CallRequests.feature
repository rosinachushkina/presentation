﻿Feature: Sending Call Requests to a dealer.
	I would like to be able to sennapoli pizzad a call request to a dealer
	in case I am interested in some inventory there.

@CallRequestRegistered
Scenario Outline: Sending a call request from a registered customer
	Given I open <main page> and type <phone>
	And I click on SignIn button
	When I type my login and password of a customer and submit them
	Then I click on search button
	Then I choose <vehicleNumber>th vehicle
	Then I click the Request a Phone Call button
	Then There should be our company telephone number displayed
	Then There should be our default telephone phrase displayed
	Then I click the Send button
	Then I log out
	
	Given I open <main page> and type <phone>
	And I click on SignIn button
	When I type my login and password of a dealer and submit them
	Then On the site header there should be a call request notification
	Then I go to the Dashboard
	Then On the Dashboard Sidebar and on the Header there should be a notification
	Then I switch to Call Requests
	Then There should be my call request available
	When I delete the last call request
	Then The last call request should be vaporized
	
	Examples: 
	
	| main page | phone      | vehicleNumber |
	| bronze    | 7327572923 |        1      |
	| silver    | 0          |        1      |
	| gold      | 0          |        1      |

@CallRequestUnregistered
Scenario Outline: Sending a call request from an unregistered customer
	Given I open <main page> and type <phone>
	Then I click on search button
	Then I choose <vehicleNumber>th vehicle
	Then I click the Request a Phone Call button
	Then There should be our company telephone number displayed
	Then There should be our default telephone phrase displayed
	Then I enter my name and telephone
	Then I click the Send button
	
	Given I open <main page> and type <phone>
	And I click on SignIn button
	When I type my login and password of a dealer and submit them
	Then On the site header there should be a call request notification
	Then I go to the Dashboard
	Then On the Dashboard Sidebar and on the Header there should be a notification
	Then I switch to Call Requests
	Then There should be my call request (with 2 verification strings) available
	When I delete the last call request
	Then The last call request should be vaporized
	
	Examples: 
	
	| main page | phone      | vehicleNumber |
	| bronze    | 7327572923 |        1      |
	| silver    | 0          |        1      |
	| gold      | 0          |        1      |