﻿Feature: CheckWidgetPresence
	In order to check that widget enable only on clean vehicle
	I've created this test

	Background: 


@CheckingWidgetPresenceOnCleanInventories
	Scenario Outline: Checking widget presence on clean inventories
	Given I open <webSite> and type <phone>
	Then I click on clean search button
	Then I choose <vehicleNumber>th vehicle
	Then I check that widget displayed

	Then I click on Home button
	Then I click on salvage search button
	Then I choose <vehicleNumber>th vehicle
	Then I check that widget not displayed


	Examples: 
	| webSite | phone      |   vehicleNumber     |
	| bronze    | 7327572923 |   1  |
	| silver    | 0          |   1  |
	| gold      | 0          |   1  |