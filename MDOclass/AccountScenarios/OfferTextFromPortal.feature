﻿Feature: OfferTextFromPortal
	In order to check that it's possible to change default text in placing offer
	+ checking possibility to hide Offer button

@CheckingOfferButtonAndDefaultText
Scenario: Checking Offer displaying button and default text
	Given I open the Portal main page
	Then I click the Login button
	Then I enter my portal email and password for AdditionalPages account and submit them

	Then I open AdditionalPagesSite settings
	Then I click on Website Settings icon
	Then I click on tab Profit Center
	Then I click on checkbox displaying offers
	Then I type new phrase for displaying after placing an offer
	Then I save changes of Profit Center page on website settings
	
	Then I click on preview site button
	Then I swith on next window
	And I click on SignIn button
	When I type my login and password of a customer and submit them
	Then I click on search button
	Then I click on List Preview displaying
	Then I select a vehicle with the Current Offer option
	When I place a higher bid for this inventory with out clicking ok
	Then I check that text of offer is displayed

	Then I switch to first tab
	Then I click Restore Default of Offer text
	Then I save changes of Profit Center page on website settings
	Then I save default text of Offer
	
	Then I swith on next window
	Then I refresh the page
	When I place a higher bid for this inventory with out clicking ok
	Then I check that default text of offer is displayed

	Then I switch to first tab
	Then I disable offer button
	Then I save changes of Profit Center page on website settings

	Then I swith on next window
	Then I refresh the page
	Then I check that button of offer isn't displayed