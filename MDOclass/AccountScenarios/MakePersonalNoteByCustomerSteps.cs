﻿using System;
using Common;
using NUnit.Framework;
using TechTalk.SpecFlow;

namespace MDOclass
{
    [Binding]
    public class MakePersonalNoteByCustomerSteps : BaseSteps
    {
        private string personalNote;
        private string vehicleID;


        [Then(@"I type personal note and publish it")]
        public void ThenITypePersonalNoteAndPublishIt()
        {
            var Handy = new HandyFunctions();
            personalNote = Handy.RandomStringLatinAndNumbers(10);

            WebDriver.GetById("CPH_lblPersNotesCaption").Click(); //-----------выбор блока Personal Notes
            WebDriver.GetById("txtNewNote").SendKeys(personalNote);//----------написание Personal Note
            WebDriver.GetById("CPH_lblSaveCommentCaption").Click();//----------отправка Personal Note
        }

        [Then(@"I check that Personal Note was sent")]
        public void ThenICheckThatPersonalNoteWasSent()
        {

            string actualPersonalNote = WebDriver.GetByPath("//div[@id='divAllComments']//li[1]").Text;

            if ((personalNote + " • by me just now") != actualPersonalNote)               //-------сравнение отправленного сообщения и актуального
            {
                Assert.Fail("Note must be " + personalNote + "\n Actual is " + actualPersonalNote);
            }

            
        }

        [Then(@"I switch to Personal Notes")]
        public void ThenISwitchToPersonalNotes()
        {
            WebDriver.GetByPath("//a[@href='/Dashboard/Notes/List']").Click();
        }


        [Then(@"I check Personal Note and VehicleID")]
        public void ThenICheckPersonalNoteAndVehicleID()
        {
            string actualPersonalNote = WebDriver.GetByPath("//div[@class='message']//span[5]").Text;
            string actualVehicleID = WebDriver.GetByPath("//div[@class='inventory-item']//span/span[2]").Text;

            if ((personalNote) != actualPersonalNote)               //-------сравнение отправленного сообщения и актуального
            {
                Assert.Fail("Note must be " + personalNote + "\n Actual is " + actualPersonalNote);
            }

            if ((vehicleID) != actualVehicleID)               //-------сравнение отправленного ID и актуального
            {
                Assert.Fail("ID must be " + vehicleID + "\n Actual is " + actualVehicleID);
            }

        }

        [Then(@"I delete Personal Note from Dashboard")]
        public void ThenIDeletePersonalNoteFromDashboard()
        {
            WebDriver.GetByPath("//div[@id='listcontainer']/div").Click();
            WebDriver.GetByPath("//div[@id='listcontainer']//a").Click();
            WebDriver.SwitchTo().Alert().Accept();

          Assert.True((WebDriver.GetByPath("//*[contains(text(),'You don')]").Enabled));
            Assert.True((WebDriver.GetByPath("//*[contains(text(),'t have any notes')]").Enabled));
        }

        [Then(@"I click add Personal Note for first vehicle")]
        public void ThenIClickAddPersonalNoteForThVehicle()
        {
            WebDriver.GetByPath("//div[@id='searchResult']/article[1]//p/a[1]").Click();
        }

        [Then(@"I type Personal Note from List preview for first vehicle")]
        public void ThenITypePersonalNoteFromListPreview()
        {
            var Handy = new HandyFunctions();
            personalNote = Handy.RandomStringLatinAndNumbers(10);

            WebDriver.GetByPath("//div[@id='searchResult']/article[1]//input").SendKeys(personalNote);
            WebDriver.GetByPath("//div[@id='searchResult']/article[1]//span/a[1]/span").Click();
        }

        [Then(@"I check that Personal Note was sent from Galery viewing")]
        public void ThenICheckThatPersonalNoteWasSentFromGaleryViewing()
        {
            string actualPersonalNote =
                WebDriver.GetByPath(
                    "//div[@id='searchResult']/article[1]//div[contains(text(),'Personal Notes')]/following-sibling::*/li")
                    .Text;

            if ((personalNote + " • by me just now") != actualPersonalNote)                //-------сравнение отправленного сообщения и актуального
            {
                Assert.Fail("Message must be " + personalNote + " • by me just now" + "\n Actual is " + actualPersonalNote);
            }

        }

        [Then(@"I open this Vehicle")]
        public void ThenIOpenThisVehicle()
        {
            WebDriver.GetByPath("//div[@id='searchResult']/article[1]//a[contains(text(),'Details')]").Click();
        }

        [Then(@"I save ID of this Vehicle")]
        public void ThenISaveIDOfThisVehicle()
        {
            vehicleID = WebDriver.GetById("CPH_lblInventoryNumberData2").Text;            //-------сохранение Vehicle ID для которой создавался Personal Note
        }



    }
}
