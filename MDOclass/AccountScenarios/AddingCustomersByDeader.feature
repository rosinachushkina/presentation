﻿Feature: AddingCustomersByDeader
	

@AddingCustomersByDeaderPositive
Scenario Outline: AddingCustomersByDeaderPositive
	Given I open <webSite> and type <phone>
	Then I click on SignIn button
	When I type my login and password of a dealer and submit them
	Then I go to the Dashboard
	
	Then I save the TotalCustomers number

	Then I switch to Customers List
	Then I click the AddNewCustomer button
	Then I enter all information about my new customer and submit it
	Then I wait 5 seconds
	Then I make a search by new customers ID in CustomersList
	Then My new customer should be successfully added to the customers list
	
	Then I switch to Statistics
	Then My TotalCustomers number should be higher by 1

	Then I switch to Reports
	Then My new customer should be displayed at Reports>CustomerMasterList

	Then I switch to Home
	Then I log out

	Then I click on SignIn button
	Then I enter my new customer credentials and submit them
	Then Then I was logged in successfully to a redirected location

    Examples: 
	|    webSite      | phone      |
	|      bronze	  | 7327572923 | 
	|      silver     |   0        |
	|       gold      |   0	       |