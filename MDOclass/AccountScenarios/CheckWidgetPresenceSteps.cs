﻿using System;
using System.Windows.Forms;
using Common;
using NUnit.Core;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using TechTalk.SpecFlow;

namespace MDOclass.AccountScenarios
{
    [Binding]
    public class CheckWidgetPresenceSteps : BaseSteps
    {
        [Then(@"I check that widget displayed")]
        public void ThenICheckThatWidgetDisplayed()
        {
            Assert.True(WebDriver.GetById("tmvwidget").Displayed);


            /*    string typeselect =
                      new SelectElement(WebDriver.GetByPath("//div[3]/div[7]/div[2]/div/div/div[2]/select[1]")).SelectedOption.Text; */


        }

        [Then(@"I click on salvage search button")]
        public void ThenIClickOnSalvageSearchButton()
        {
            WebDriver.GetById("CPH_ucSearchOptions_lblSalvageVehicleCondition").Click();
        }

        [Then(@"I click on Home button")]
        public void ThenIClickOnHomeButton()
        {
            WebDriver.GetById("ucToolbar_hplHome").Click();
        }

        [Then(@"I check that widget not displayed")]
        public void ThenICheckThatWidgetNotDisplayed()
        {
            try
            {
                if (WebDriver.GetById("tmvwidget").Enabled)
                {
                    Assert.Fail("huinya");
                }


            }
            catch (WebDriverException)
            {
            }


        }



    }
}
