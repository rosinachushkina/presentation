﻿using System;
using System.Diagnostics;
using System.Net.Mime;
using System.Threading;
using Common;
using NUnit.Core;
using NUnit.Framework;
using OpenQA.Selenium;
using TechTalk.SpecFlow;


namespace MDOclass.AccountScenarios
{
    [Binding]
    public class LoginationSteps : BaseSteps
    {
        public static string site = "0";

        [Given(@"I open (.*) and type (.*)")]
        public void GivenIOpenMainPageHttpDiamond_Myvirtualinventory_Com(string webSite, string phone)
        {
            site = "0";

            //---------Если идет тест продакшена
            if (MDOvariables.IsProductionTest == false)
                switch (webSite)
                {
                    case "silver":
                        site = Pages.silverStPackage;
                        break;
                    case "gold":
                        site = Pages.goldStPackage;
                        break;
                }
            //---------Либо адреса продакшена
            else
                switch (webSite)
                {
                    case "silver":
                        site = Pages.silverPackage;
                        break;
                    case "gold":
                        site = Pages.goldPackage;
                        break;
                    case "bronze":
                        site = Pages.bronzePackage;
                        break;
                }

            //----------Если мы не на продакшене - автоматически pass
            if(site=="0" && MDOvariables.IsProductionTest==false)
                Assert.Pass();

            //---------------------ЗАГЛУШКА ПРОДАКШЕНА БРОНЗЫ----------------
            if (site == Pages.bronzePackage && MDOvariables.IsProductionTest == true)
                Assert.Pass();
            //---------------------КОНЕЦ ЗАГЛУШКИ----------------------------

            WebDriver.Go(site);
          
            //процедура для ввода номера на пакете Bronze
            if (Convert.ToDecimal(phone) != 0
                && WebDriver.GetCurrentUrl() == site + "/Login/Default.aspx") 
            {
                Console.WriteLine("entering phone");
                WebDriver.GetById("txtPhone").SendKeys(phone);
                WebDriver.GetById("CPH_btnSignByPhone").Click();
                Thread.Sleep(5000);


            }
        }


        [Given(@"I click on SignIn button")]
        public void GivenIClickOnSignInButton()
        {
            WebDriver.GetById("ucToolbar_lblLogInOut").Click();  // нажатие на кнопку Sign In
        }

        [When(@"I type my login and password of a dealer and submit them")]
        public void WhenITypeMyLoginAndPasswordAndSumbitThem()
        {

            WebDriver.GetByName("username").SendKeys(MDOvariables.DealerLogin);                         // заполнение поля Login
            WebDriver.GetByName("password").SendKeys(MDOvariables.DealerPassword);                              // заполнение поля Password
            WebDriver.GetByPath("//i[@class='m-icon-swapright m-icon-white']").Click();// нажатие на кнопку Login
        }


        [Then(@"Then I was logged in successfully to a redirected location")]
        public void ThenIWasLoggedInSuccessfully()
        {
            WebDriver.WaitFullLoad();
            Assert.True(WebDriver.FindElement(By.Id("ucToolbar_imgUser")).Displayed);          // проверка отображения иконки юзера
        }

        [When(@"I type an INCORRECT (.*) and (.*)")]
        public void WhenITypeAnINCORRECTAnd(string login, string password)
        {
            WebDriver.GetByName("username").SendKeys(login);                                // заполнение поля Login
            WebDriver.GetByName("password").SendKeys(password);                                // заполнение поля Password
            WebDriver.GetByPath("//i[@class='m-icon-swapright m-icon-white']").Click();  // нажатие на кнопку Login
        }

        [When(@"I type my login and password of a customer and submit them")]
        public void GivenIClickOnSignInButtonSigningAsACustomer()
        {
            WebDriver.GetByName("username").SendKeys(MDOvariables.CustomerLogin);                         // заполнение поля Login
            WebDriver.GetByName("password").SendKeys(MDOvariables.CustomerPassword);                              // заполнение поля Password
            WebDriver.GetByPath("//i[@class='m-icon-swapright m-icon-white']").Click();// нажатие на кнопку Login
        }


        [Then(@"I receive the error message")]
        public void ThenIReceiveTheErrorMessage()
        {
            WebDriver.FindElement(By.XPath("//*[contains(text(),'User name or password is not correct')]"));
        }

        [When(@"I type my login and password of a GMAILcustomer and submit them")]
        public void WhenITypeMyLoginAndPasswordOfAGMAILcustomerAndSubmitThem()
        {
            WebDriver.GetByName("username").SendKeys(MDOvariables.CustomerGMAILLogin);                         // заполнение поля Login
            WebDriver.GetByName("password").SendKeys(MDOvariables.CustomerGMAILPassword);                              // заполнение поля Password
            WebDriver.GetByPath("//i[@class='m-icon-swapright m-icon-white']").Click();
        }



        [Then(@"I click sign in different account \(gmail\)")]
        public void ThenIClickSignInDifferentAccountGmail()
        {
            WebDriver.GetByPath("//a[contains(text(), 'Sign in with a different account')]").Click();
        }

        [Then(@"I click add acount \(gmail\)")]
        public void ThenIClickAddAcountGmail()
        {
            WebDriver.GetByPath("//a[contains(text(), 'Add account')]").Click();
        }

    }
}
