﻿Feature: PortalPaymentInstructions
	
@PortalPaymentInstructions
Scenario: PortalPaymentInstructions
    Given I open the Portal main page
	Then I click the Login button
	Then I enter my portal email and password for AdditionalPages account and submit them
	Then I open AdditionalPagesSite settings
	Then I click the Customization Icon
	Then I switch to Website Settings > Profit Center

	Then I fill some information for PaymentInstructions and Save it
	Then I select 'EUR' as a Payment Currency
	Then I add a payment type 'Credit Card' - 'Deposit'
	Then I add a payment type 'Bank Wire Transfer' - 'Full Payment'
	Then I add a payment type 'Check' - 'Deposit'

	Then I click the SaveAll button

	Then I open a new tab and switch there
	Then I open our AdditionalPagesWebsite
	Then I click on a regular search button
	Then I choose 1th vehicle

	Then I check that all entered payment types are here in the inventory

	Then I switch to tab 1
	Then I delete my 3 entered payment types
	Then I check that there are no entered payment types