﻿Feature: SortBy
	
@SortByPriceHighestFirst
Scenario Outline: SortByPriceHighestFirst
	Given I open <webSite> and type <phone>
	Then I click on search button
	Then I set a sorting mode to PRICE:HIGHEST FIRST
	Then All vehicles should be sorted by PRICE:HIGHEST FIRST

	Examples: 
	| webSite | phone      | 
	| bronze  | 7327572923 | 
	| silver  | 0          |    
	| gold    | 0          |

@SortByPriceLowestFirst
Scenario Outline: SortByPriceLowestFirst
	Given I open <webSite> and type <phone>
	Then I click on search button
	Then I set a sorting mode to PRICE:LOWEST FIRST
	Then All vehicles should be sorted by PRICE:LOWEST FIRST

	Examples: 
	| webSite | phone      | 
	| bronze  | 7327572923 |
	| silver  | 0          | 
	| gold    | 0          | 

@SortByTimeEndingSoonest
Scenario Outline: SortByTimeEndingSoonest
	Given I open <webSite> and type <phone>
	Then I click on search button
	Then I set a sorting mode to TIME: ENDING SOONEST
	Then All vehicles should be sorted by TIME: ENDING SOONEST (with Conversion to Sec)

	Examples: 
	| webSite | phone      | 
	| bronze  | 7327572923 | 
	| silver  | 0          | 
	| gold    | 0          | 

@SortByYearMakeNewestFirst
Scenario Outline: SortByYearMakeNewestFirst
	Given I open <webSite> and type <phone>
	Then I click on search button
	Then I set a sorting mode to YEAR, MAKE: NEWEST FIRST
	Then All vehicles should be sorted by YEAR, MAKE: NEWEST FIRST

	Examples: 
	| webSite | phone      | 
	| bronze  | 7327572923 | 
	| silver  | 0          | 
	| gold    | 0          |  

@SortByYearMakeOldersFirst
Scenario Outline: SortByYearMakeOldersFirst
	Given I open <webSite> and type <phone>
	Then I click on search button
	Then I set a sorting mode to YEAR, MAKE: OLDEST FIRST
	Then All vehicles should be sorted by YEAR, MAKE: OLDEST FIRST

	Examples: 
	| webSite | phone      | 
	| bronze  | 7327572923 | 
	| silver  | 0          | 
	| gold    | 0          |

@SortByMakeModelNewestFirst
Scenario Outline: SortByMakeModelNewestFirst
	Given I open <webSite> and type <phone>
	Then I click on search button
	Then I set a sorting mode to MAKE, MODEL: NEWEST FIRST
	Then All vehicles should be sorted by MAKE, MODEL: NEWEST FIRST

	Examples: 
	| webSite | phone      | 
	| bronze  | 7327572923 | 
	| silver  | 0          |   
	| gold    | 0          | 
