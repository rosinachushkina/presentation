﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TechTalk.SpecFlow;
using Common;
using System.Threading;
using NUnit.Framework;

namespace MDOclass.AccountScenarios
    {
    [Binding]
    public class SaveSearchSteps: BaseSteps
        {
        [Then(@"I click Save Search")]
        public void ThenIClickSaveSearch()
            {
            WebDriver.GetByPath("//*[@id='CPH_ucSearchOptions_lblSaveSearch']").Click();
            Thread.Sleep(1000);
            }

        [Then(@"My selected search criterias should be refrected in SaveSearch window")]
        public void ThenMySelectedSearchCriteriasShouldBeRefrectedInSaveSearchWindow()
            {
            WebDriver.SwitchTo().Frame(WebDriver.GetById("contrMessage_frmContract"));
            string s = WebDriver.GetByPath("//span[@id='CPH_lblSearchCriteriaData']").Text;

            Assert.IsTrue(s.Contains(SearchByCriteriasSteps.Country));
            Assert.IsTrue(s.Contains(SearchByCriteriasSteps.Sstate));
            Assert.IsTrue(s.Contains(SearchByCriteriasSteps.Make));
            Assert.IsTrue(s.Contains(SearchByCriteriasSteps.Model));
            }

        [Then(@"I click Save")]
        public void ThenIClickSave()
            {
            WebDriver.GetByPath("//*[@id='lbtSave']").Click();
            WebDriver.SwitchTo().DefaultContent();
            Thread.Sleep(5000);
            }

        [Then(@"I close the window")]
        public void ThenICloseTheWindow()
            {
            WebDriver.GetByPath("//*[@id='contrMessage_OKButton']").Click();
            WebDriver.SwitchTo().DefaultContent();
            }

        [Then(@"I should have my SavedSearch with all parameters selected")]
        public void ThenIShouldHaveMySavedSearchWithAllParametersSelected()
            {
            WebDriver.GetByPath("//*[@id='savedSearchesTopLine']//*[contains(text(),'" + SearchByCriteriasSteps.Country + "')]");
            WebDriver.GetByPath("//*[@id='savedSearchesTopLine']//*[contains(text(),'" + SearchByCriteriasSteps.Sstate + "')]");
            WebDriver.GetByPath("//*[@id='savedSearchesTopLine']//*[contains(text(),'" + SearchByCriteriasSteps.Make + "')]");
            WebDriver.GetByPath("//*[@id='savedSearchesTopLine']//*[contains(text(),'" + SearchByCriteriasSteps.Model + "')]");
            }

        [Then(@"I find our test customer in CustomerList")]
        public void ThenIFindOurTestCustomerInCustomerList()
            {
            WebDriver.GetByPath("//input[@placeholder='Search by Email, Name or Phone Number']").
                SendKeys(MDOvariables.CustomerLogin.Substring(0,9));
            WebDriver.GetByPath("//a[contains(text(),'Search')][@class='btn icn-only']").Click();
            Thread.Sleep(2000);

            WebDriver.GetByPath("//*[@class='scroller customers']/div[1]/div").Click();
            Thread.Sleep(2000);
            }

        [Then(@"I click on his SavedSearch \(CustomerList\)")]
        public void ThenIClickOnHisSavedSearchCustomerList()
            {
            ScenarioContext.Current.Pending();
            }

        [Then(@"His SavedSearch should be there")]
        public void ThenHisSavedSearchShouldBeThere()
            {
            ScenarioContext.Current.Pending();
            }

        [Then(@"I delete my SavedSearch")]
        public void ThenIDeleteMySavedSearch()
            {
            ScenarioContext.Current.Pending();
            }

        }
    }
