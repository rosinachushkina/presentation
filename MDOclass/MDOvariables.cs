﻿using System;                            //-----------Здесь лежат переменные для работы по тестам MDO
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MDOclass
{
    public class MDOvariables
    {
        public static bool IsProductionTest = false; //Тестирование продакшена или стенда
        public static string EnterPhone = "7327572923";

        public static string DealerLogin = "Ginger";
        public static string DealerPassword = "1";
        public static string DealerFirstname = "F";
        public static string DealerLastname = "L";
        public static string DealerEmail = "test1mydealeronline@tsbua.com";
        public static string DealerPhone = "";
        public static string DealerEmailPassword = "mydealeronline1";
        
        public static string BackofficeLoginKathryn = "kathryns@alteso.com";
        public static string BackofficePasswordKathryn = "kers3399";
        public static string BackofficeFirstName = "katya";
        public static string BackofficeLastName = "Repik";

        public static string BackofficeLoginMinh = "minh@alteso.com";
        public static string BackofficePasswordMinh = "1234";

        public static string CustomerLogin = "testuser@1.com";
        public static string CustomerPassword = "1";
        public static string CustomerFirstname = "tester";
        public static string CustomerLastname = "userochok";
        public static string CustomerPhone = "1";

        public static string CustomerGMAILLogin = "test2mydealeronline@tsbua.com";
        public static string CustomerGMAILPassword = "mydealeronline2";

        public static string SpecialCustomerEmail = "kjdfhg47@mailinator.com";
        public static string SpecialCustomerPassword = "1";


        public static string SpecialDealerEmail = "jhsdf47@mailinator.com";
        public static string SpecialDealerPassword = "1";
        
        public static string AvatarPathCustomer = "C:\\111.jpg"; //Пути к картинкам. Можно использовать
        public static string AvatarPathDealer = "C:\\222.jpg"; //вместо таблицы примеров

        public static int VehiclesOnPageDefault = 15;
        public static int VehiclesOnPageDefaultSt = 15;

        public static string PortalLogin = "test1mydealeronline@tsbua.com";
        public static string PortalPassword = "mydealeronline1";
        public static string PortalCompanyName = "Automation-QA";
        public static string PortalPhoneNumber = "7777777";
        public static string PortalFaxNumber = "666666";
        public static string PortalAddress1 = "Marvel str.15";
        public static string PortalCity = "City 17";
        public static string PortalZIP = "123456";
        public static string PortalCountry = "Czech Republic";
        public static string PortalDomain = "automation.qa"; //Используется целиком, а также в одном месте срезаются последние 3 символа

        public static string PortalLoginAdditionalPages = "test1mydealeronline@tsbua.com";
        public static string PortalPasswordAdditionalPages = "mydealeronline1";

        public static string CompanyTelephone = "7327572923";
    }

    public class PortalTemplatesInfo
        {
        //------------------Цвета хедеров темплейтов
        public static string MintCSSHeaderBG = "rgba(91, 204, 91, 1)";
        public static string MiniCSSHeaderBG = "rgba(249, 2, 39, 1)";
        public static string BlueVistaSSHeaderBG = "rgba(76, 131, 186, 1)";
        }

    }
