﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PartsBee
{
    public class PartsBeePages
    {
        public static string DevMainPage =     "http://dev.partsbee.com/";
        public static string DevLoginPage =    "http://dev.partsbee.com/account/login";
        public static string DevRegisterPage = "https://dev.partsbee.com/registration";
        public static string DevSearchPage = "http://dev.partsbee.com/search/";

        public static string ProdMainPage = "http://partsbee.com/";
        public static string ProdLoginPage = "http://partsbee.com/account/login";
        public static string ProdRegisterPage = "https://partsbee.com/registration";
        public static string ProdSearchPage = "http://partsbee.com/search/";
    }
}
