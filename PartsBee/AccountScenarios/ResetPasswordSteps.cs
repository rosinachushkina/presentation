﻿using System;
using System.Linq;
using System.Threading;
using Common;
using NUnit.Framework;
using OpenQA.Selenium;
using TechTalk.SpecFlow;

namespace PartsBee.AccountScenarios
{
    [Binding]
    public class ResetPasswordSteps : BaseSteps
    {
        [Then(@"I login as User with reset email")]
        public void ThenITypeLoginOfUserWithResetEmail()
        {
            WebDriver.GetById("Email").SendKeys(PartsBeeVariables.SellerEmail);
            WebDriver.GetById("Password").SendKeys(PartsBeeVariables.SellerPassword);
            WebDriver.GetByPath("//*[@value='Login']").Click();
        }

        [Then(@"I click My Account button")]
        public void ThenIClickMyAccountButton()
        {
            WebDriver.GetByPath("//*[contains(text(), 'My account')]").Click();
            Thread.Sleep(500);
            WebDriver.GetByPath("//a[contains(text(), 'My Account')]").Click();
        }
        [Then(@"I click Reset Password button in account settings page")]
        public void ThenIClickResetPasswordButtonInAccountSettingsPage()
        {
            WebDriver.GetByPath("//button[contains(text(), 'RESET PASSWORD')]").Click();
        }
        [Then(@"I agree with reseting password")]
        public void ThenIAgreeWithResetingPassword()
        {
            WebDriver.GetByPath("//button[contains(text(), 'Yes')]").Click();
        }
        [Then(@"I see message about sending link with reseting")]
        public void ThenISeeMessageAboutSendingLinkWithReseting()
        {
            Assert.True(WebDriver.GetByPath("//b[contains(text(), 'Recovery link has been sent to your email.')]").Enabled);
        }
        [Then(@"I enter our reseted user's email and password and submit them \(for GMAIL\.COM\)")]
        public void ThenIEnterOurResetedUserSEmailAndPasswordAndSubmitThemForGMAIL_COM()
        {
            WebDriver.GetByPath("//input[@id='Email']").SendKeys(PartsBeeVariables.ResetPasswordEmail);
            WebDriver.GetByPath("//input[@id='Passwd']").SendKeys(PartsBeeVariables.ResetPasswordEmailPass);
            WebDriver.FindElement(By.XPath("//input[@id='signIn']")).Click();
            Thread.Sleep(3000);
        }
        [Then(@"I click on reset password message")]
        public void ThenIClickOnResetPasswordMessage()
        {
            WebDriver.GetByPath("//tbody/tr[1]/td[5]/div[2]/span").Click();
        }
        [Then(@"I check email with reseting")]
        public void ThenICheckEmailWithReseting()
        {
            Assert.True(WebDriver.GetByPath("//img[@alt='PartsBee']").Enabled);
            Assert.True(WebDriver.GetByPath("//p[contains(text(), 'Please')]").Enabled);
            Assert.True(WebDriver.GetByPath("//img[@alt='button']").Enabled);
            Assert.True(WebDriver.GetByPath("//p[contains(text(), 'Thank you for your business!')]").Enabled);

        }
        [Then(@"I click on link for reseting password")]
        public void ThenIClickOnLinkForResetingPassword()
        {
            WebDriver.GetByPath("//img[@alt='button']").Click();
            WebDriver.SwitchTo().Window(WebDriver.WindowHandles.Last());
        }
        [Then(@"I type old password")]
        public void ThenITypeOldPassword()
        {
            Assert.True(WebDriver.GetBy("//p[contains(text(), 'Enter your')]").SetTimer(2000).Find().Displayed);
            Assert.True(WebDriver.GetBy("//span[contains(text(), 'New Password')]").SetTimer(2000).Find().Displayed);
            WebDriver.GetByCss("#password").SendKeys(PartsBeeVariables.ResetPasswordEmailPassOnSite);
            WebDriver.GetById("resetPasswordButton").Click();
        }
        [Then(@"I make logout")]
        public void ThenIMakeLogout()
        {
            WebDriver.GetByPath("//a[contains (text(), 'Logout')]").Click();
        }

        [Then(@"I wait (.*) seconds")]
        public void ThenIWaitSeconds(int p0)
        {
            Thread.Sleep(p0 * 1000);
        }

    }
}
