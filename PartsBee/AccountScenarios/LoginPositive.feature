﻿@PartsBee_LoginationSoc
Feature: LoginPositive
	In order to check login functionality
	As a user
	I want to log in to my account

	Background: 
	Given I open login page

@positiveLogin
Scenario: Positive Login
	Then I login as Seller
	Then I redirect to main page

@positiveLogin#2
Scenario: Positive Login with Upper symbols
	Then I login as Seller with upper symbols
	Then I redirect to main page

@positiveLoginViaGoogle
Scenario: Positive Login via Google
	Then I click on google social icon
	Then I fill information for google pop-up and sumbit
	Then I redirect to main page

	@positiveLoginViaTwiter
	Scenario: Positive Login via Twitter
	Then I click on twitter social icon
	Then i fill login and password
	Then I click submit button for twitter
	Then I redirect to main page

@positiveLoginViaFacebook
Scenario: Positive Login via Facebook
	Then I click on facebook social icon
	Then I fill information for facebook pop-up and sumbit
	Then I redirect to main page


