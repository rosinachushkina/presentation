﻿Feature: SearchFromListingPage
 In order to check buttons next-previous
 from listing page
 I make this test

@SearchResultsFromListingPage
Scenario: Check search results from listing page
 Given I open main page
 Then I go to search page
 Then I type TOYOTA search input
 Then I choose make from autosuggest
 Then I click GO search on search page
 Then I save title of first listing
 Then I open the first listing
 Then I compare titles
 Then I click on search input and check search
 Then I check phrase about next search results
 Then I save title of next Listing
 Then I redirect to next listing
 Then I compare titles
 Then I click on search input and check search
 Then I check phrase about previous listing
 Then I save title of previous listing
 Then I redirect to previous listing
 Then I compare titles
 Then I click go to search results
 Then I redirect to search page with search filter