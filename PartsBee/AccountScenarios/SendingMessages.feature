﻿Feature: SendingMessages
	
@SendingMessagesBuyerSeller
Scenario: SendingMessagesBuyerSeller
	Given I open login page
	Then I login as Buyer
	Then I click on Search
	Then I enter our SellerMessage's company name and click Go
	Then I open the first listing

	Then Switch to Seller tab
	Then I click the SendMessage button
	Then I enter the text message within the part and click Send

	Then I go to Dashboard
	Then I check that my sent message is there (first buyer check)
	Then I make logout

	Then I log in as a MessageSeller
	Then I go to Dashboard
	Then I check that my sent message is there (first buyer check)
	Then I should have 1 new messages
	Then I open my last conversation
	Then I check the title, date, price, name and message parameters from a buyer
	Then I write me response to a message and send it
	Then I should see my latest response in realtime
	Then I make logout

	Then I login as Buyer
	Then I go to Dashboard
	Then I should have 1 new messages
	Then I check that my sent message is there (first seller's check)
	Then I open my last conversation
	Then I check the title, date, price, name and message parameters from a seller

@DeletingMessagesBuyerSeller
Scenario: DeletingMessagesBuyerSeller
Given I open login page
	Then I login as Buyer
	Then I click on Search
	Then I enter our SellerMessage's company name and click Go
	Then I open the first listing

	Then Switch to Seller tab
	Then I click the SendMessage button
	Then I enter the text message 'Message for deletion!' within the part and click Send
	Then I go to Dashboard
	Then I check that my sent message is there (first buyer check)
	Then I make logout

	Then I log in as a MessageSeller
	Then I go to Dashboard
	Then I should have 1 new messages
	Then I open my last conversation
	Then I delete this conversation
	Then I click All Messages

	Then I check that my sent message is NOT there (first buyer check)
	Then I make logout

	Then I login as Buyer
	Then I go to Dashboard
	Then I check that my sent message is there (first buyer check)
	Then I click the checkbox on a conversation 1
	Then I click Bulk Actions
	Then I click Delete (Bulk Actions)
	Then I click All Messages
	Then I check that my sent message is NOT there (first buyer check)