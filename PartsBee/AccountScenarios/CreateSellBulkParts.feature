﻿Feature: SellBulkParts
	In order to check creating list of parts
	As a Seller I make Sell Bulk Parts

	Background: 
	Given I open login page
	Then I login as Seller
	Then I press Sell Your part Button
	Then I press Sell bulk parts Button
	Then I check whether I am on the drafts page and go to bulk parts if I am there
	

	@SellBulkPartsWithVINDecodeing
	Scenario: Creating Sell Bulk Parts List with VIN decoding
	Then I type VIN in VIN input
	Then I click Find My Vehicle button
	Then I check prhrase You are ready to Sell
	Then I check titles of Vehicle
	Then I check that image of found Vehicle displayed

	Then I click continue to Step 2

	Then I check titles of Vehicle
	Then I click 1 part from 2 subcategory
	Then I check that counter of selected parts changed display 1
	Then I click 1 part from 2 subcategory
	Then I check that counter of selected parts changed display 0
	Then I check that all checkboxes unselected

	Then I click 1 part from 2 subcategory
	Then I click 2 part from 2 subcategory
	Then I click 3 part from 2 subcategory
	Then I check that counter of selected parts changed display 3

	Then I delete choosed parts by clicking Clear All button
	Then I check that counter of selected parts changed display 0
	Then I check that all checkboxes unselected

	Then I click 1 part from 2 subcategory
	Then I click on Interior parts
	Then I click 1 part from 2 subcategory
	Then I click on Mechanical parts
	Then I click 1 part from 2 subcategory
	Then I check that counter of selected parts changed display 3

	Then I click continue to Step 3
	Then I check that title of Vehicle displayed
	Then I check number of selected Parts

	Then I click Publish Now button
	Then I check that number of listings in My Listings is correct
	Then I check that counter in My listings display sum of choosed parts

	Then I delete listing from My Listings
	Then I check phrase about empty filter
	Then I check that counter in My listings display sum of choosed parts
	

	@SellBulkPartsWithManualAddingValues
	Scenario Outline: Creating Sell Bulk Parts with manual adding
	Then I type VIN in VIN input
	Then I click Find My Vehicle button
	Then I upload new picture and check that it changes
	Then I click that I dont have VIN
	Then I choose Year 1981
	Then I check that Vehicle Type displayed
	Then I choose Year 2014
	Then I check that Vehicle Type disabled
	Then I choose Make ACURA
	Then I choose model 1.6 EL
	Then I type trim
	Then I check title of added Vehicle in header
	Then I upload picture by Drag&Drop
	Then I upload picture by Add more image button
	Then I check that uploaded 2 images

	Then I click continue to Step 2

	Then I check title of added Vehicle in header
	Then I check title of added Vehicle over a picture
	Then I upload picture by button under image
	Then I check that uploaded 3 images
	Then I type in search Parts input Bug Shield
	Then I check that choosed part checkbox selected
	Then I click on Interior parts
	Then I type in search Parts input Alarms & Security
	Then I check that choosed part checkbox selected
	Then I click on Mechanical parts
	Then I type in search Parts input O-Ring & Kits
	Then I check that choosed part checkbox selected
	Then I click 1 part from 2 subcategory
	Then I click 2 part from 2 subcategory
	Then I check that counter of selected parts changed display 5

	Then I click continue to Step 3
	Then I click Make Revision button
	
	Then I click on checkbox for first part
	
	Then I click Edit for second part
	Then I click on checkbox about having Part Number
	Then I enter {{12}} to List by part input
	Then I fill VIN field
	Then I manually enter a compatible vehicle for a listing
	Then I clear description form
	Then I fill description form default values at listing page
	Then I click on Additional option then fill data <makeSet> <modelSet> <colorSet> <finishSet> <countrySet> <exLinkSet> <widthSet> <heghtSet> <lengthSet> <weigthSet> <weigthSet2> <damageSet> <WarrantySet> <placementSet> and sumbit 
	Then enter price and quantity
	Then I click save part changes
	Then I check price near second price

	Then I type price for third part

	Then I check that status ready enabled
	Then I click Publish My Listings button
	Then I proceed with drafts creation

	Then I check that number of listings in My Listings is correct
	Then I check that counter in My listings display sum of choosed parts

	Then I check prices of created Vehicles

	Then I delete first listing from My Listings
	Then I open open second listing
	Then I am redirected to a listing page and check Additional information for bulk part
	Then I switch to Compatibility within the Listing and check the info
	Then I delete my listing within the listing itself

	Then I go to search page
	Then I open filter My listings
	Then I delete first listing from My Listings

	Then I check phrase about empty filter
	Then I check that counter in My listings display sum of choosed parts

	Then I press Sell Your part Button
	Then I click ListSinglePart from a main dropdown
	Then I check that one group of drafts displayed
	Then I check that in group 2 listings
	Then I open drafts group
	Then I check that in group 2 drafts
	Then I delete drafts from group
	Then I see message about no drafts aviable




		Examples: 
	| makeSet | modelSet | colorSet | finishSet | countrySet | exLinkSet | widthSet | heghtSet | lengthSet | weigthSet | weightSet2 | damageSet | WarrantySet | placementSet |
	| ACURA   |          |		    |           |    null    |   null    |   null   |    null  |   null    |   null    |   null     |   null    |    null     |   null       |
