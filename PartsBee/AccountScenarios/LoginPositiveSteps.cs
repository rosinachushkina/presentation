﻿#region Usings

using System;
using System.Drawing.Text;
using System.Linq;
using System.Security.Policy;
using System.Threading;
using Common;
using NUnit.Framework;
using NUnit.Framework.Constraints;
using TechTalk.SpecFlow;

#endregion

namespace PartsBee.AccountScenarios
{
    [Binding]
    public class LoginPositiveSteps : BaseSteps
    {
        private string currentWindow;
        string site;

        [Given(@"I open login page")]
        public void GivenIOpenLoginPage()
        {

            WebDriver.Manage().Cookies.DeleteAllCookies();

            if (PartsBeeVariables.IsProductionTest == true)
                site = PartsBeePages.ProdLoginPage;
            else site = PartsBeePages.DevLoginPage;

            WebDriver.Url = site;
            WebDriver.WaitFullLoad();
        }

        [Given(@"I open main page")]
        public void GivenIOpenMainPage()
        {
            WebDriver.Manage().Cookies.DeleteAllCookies();

            if (PartsBeeVariables.IsProductionTest == true)
                site = PartsBeePages.ProdMainPage;
            else site = PartsBeePages.DevMainPage;

            WebDriver.Url = site;

            WebDriver.WaitFullLoad();
        }

        [Given(@"I open search page")]
        public void GivenIOpenSearchPage()
        {
            WebDriver.Manage().Cookies.DeleteAllCookies();

            if (PartsBeeVariables.IsProductionTest == true)
                site = PartsBeePages.ProdSearchPage;
            else site = PartsBeePages.DevSearchPage;

            WebDriver.Url = site;

            WebDriver.WaitFullLoad();
        }


        [Given(@"I open register page")]
        public void GivenIOpenRegisterPage()
        {
            WebDriver.Manage().Cookies.DeleteAllCookies();

            if (PartsBeeVariables.IsProductionTest == true)
                site = PartsBeePages.ProdRegisterPage;
            else site = PartsBeePages.DevRegisterPage;

            WebDriver.Url = site;

            WebDriver.WaitFullLoad();
        }

        [Then(@"I redirect to main page")]
        public void ThenIRedirectToMainPage()
        {


            if (PartsBeeVariables.IsProductionTest == true)
                site = PartsBeePages.ProdMainPage;
            else site = PartsBeePages.DevMainPage;

            WebDriver.WaitFullLoad();
            string currentUrl = WebDriver.GetCurrentUrl();
            currentUrl = currentUrl.Substring(currentUrl.IndexOf("/") + 2);

            Assert.True(site.Contains(currentUrl));
            // StringComparison.InvariantCultureIgnoreCase);
        }

        [Then(@"I switch to main page")]
        public void ThenISwitchToMainPage()
        {
            if (PartsBeeVariables.IsProductionTest == true)
                site = PartsBeePages.ProdMainPage;
            else site = PartsBeePages.DevMainPage;

            WebDriver.Url = site;

            WebDriver.WaitFullLoad();
        }

        [Then(@"I login as Seller")]
        public void ThenILoginAsSeller()
        {
            WebDriver.GetByName("Email").SendKeys(PartsBeeVariables.SellerLogin);
            WebDriver.GetByName("Password").SendKeys(PartsBeeVariables.SellerPassword);
            WebDriver.GetByName("submit-button").Click();
            Thread.Sleep(500);
        }

        [Then(@"I login as Seller with upper symbols")]
        public void ThenILoginAsSellerWithUpperSymbols()
        {
            string sellerLogin = PartsBeeVariables.SellerLogin.ToUpper();

            WebDriver.GetByName("Email").SendKeys(sellerLogin);
            WebDriver.GetByName("Password").SendKeys(PartsBeeVariables.SellerPassword);
            WebDriver.GetByName("submit-button").Click();
        }


        [Then(@"I enter {{(.*)}} in email addrees field")]
        public void ThenIEnterTestuser_ComInEmailAddreesField(string userEmail)
        {
            WebDriver.GetByName("Email").SendKeys(userEmail);
        }

        [Then(@"enter small in password field")]
        public void ThenEnterInPasswordField()
        {
            var Handy = new HandyFunctions();
            WebDriver.GetByName("Password").Clear();
            string password = Handy.RandomStringLatin(5);
            WebDriver.GetByName("Password").SendKeys(password);
        }

        [When(@"I press Login Button")]
        public void WhenIPressLoginButton()
        {
            WebDriver.GetByName("submit-button").Click();
        }

        [Then(@"I click on google social icon")]
        public void ThenIClickOnGoogleSocialIcon()
        {
            currentWindow = WebDriver.CurrentWindowHandle;

            WebDriver.GetByPath("//a[@socialname = 'google']").Click();

            Thread.Sleep(1000);
        }

        [Then(@"i fill login and password")]
        public void ThenIFillLoginAndPassword()
        {
            WebDriver.SwitchTo().Window(WebDriver.WindowHandles.LastOrDefault());
            WebDriver.GetById("username_or_email").SendKeys(PartsBeeVariables.SocialEmail);
            WebDriver.GetById("password").SendKeys(PartsBeeVariables.SocialPassword);
            
        }

        [Then(@"I click submit button for twitter")]
        public void ThenIClickSubmitButtonForTwitter()
        {
            WebDriver.GetById("allow").Click();
            Thread.Sleep(2000);
            WebDriver.SwitchTo().Window(WebDriver.WindowHandles.LastOrDefault());
        }

        [Then(@"I fill information for google pop-up and sumbit")]
        public void ThenIFillInformationForGooglePop_Up()
        {
            WebDriver.SwitchTo().Window(WebDriver.WindowHandles.Last());

            WebDriver.GetById("Email").Clear();
            WebDriver.GetById("Email").SendKeys(PartsBeeVariables.SocialEmail);

            WebDriver.GetById("Passwd").Clear();
            WebDriver.GetById("Passwd").SendKeys(PartsBeeVariables.SocialPassword);

            WebDriver.GetById("signIn").Click();

            WebDriver.SwitchTo().Window(currentWindow);

            Thread.Sleep(5000);


            /*   for (int i = 0; i < 10000; i += 100)
               {
                   foreach (string handle in WebDriver.WindowHandles)
                   {
                       if (handle.Equals("Edit listing"))
                       {
                           WebDriver.SwitchTo().Window(handle);
                           break;
                       }
                       Thread.Sleep(100);
                   }
               }
               /*
               foreach (string handle in WebDriver.WindowHandles)
               {
                   if (handle.Equals("Edit listing"))
                   {
                       WebDriver.SwitchTo().Window(handle);
                   }
                
               }
               */
        }

        [Then(@"I click on twitter social icon")]
        public void ThenIClickOnTwitterSocialIcon()
        {
            currentWindow = WebDriver.CurrentWindowHandle;

            WebDriver.GetByPath("//a[@socialname = 'twitter']").Click();

            Thread.Sleep(1000);
        }

        [Then(@"I fill information for twitter pop-up and sumbit")]
        public void ThenIFillInformationForTwitterPop_UpAndSumbit()
        {
            WebDriver.SwitchTo().Window(WebDriver.WindowHandles.Last());

            WebDriver.GetById("username_or_email").Clear();
            WebDriver.GetById("username_or_email").SendKeys(PartsBeeVariables.SocialEmail);

            WebDriver.GetById("password").Clear();
            WebDriver.GetById("password").SendKeys(PartsBeeVariables.SocialPassword);

            WebDriver.GetById("allow").Click();
            WebDriver.GetById("allow").Click();
            WebDriver.SwitchTo().Window(currentWindow);
          //  WebDriver.SwitchTo().Window(currentWindow);

 /*           for (int i = 0; i < 10000; i += 100)
            {
                foreach (string handle in WebDriver.WindowHandles)
                {
                    if (handle.Equals("Edit listing"))
                    {
                        WebDriver.SwitchTo().Window(handle);
                        break;
                    }
                    Thread.Sleep(100);
                }
            }*/

            //Thread.Sleep(1000);
            //WebDriverExtensions.WaitFullLoad();
        }

        [Then(@"I click on facebook social icon")]
        public void ThenIClickOnFacebookSocialIcon()
        {
            currentWindow = WebDriver.CurrentWindowHandle;

            WebDriver.GetByPath("//a[@socialname = 'facebook']").Click();

            Thread.Sleep(1000);
        }

        [Then(@"I fill information for facebook pop-up and sumbit")]
        public void ThenIFillInformationForFacebookPop_UpAndSumbit()
        {
            for (int i = 0; i < 20; i++)
            {
                if (WebDriver.WindowHandles.Contains("Facebook"))
                    WebDriver.SwitchTo().Window("Facebook");
                else
                    Thread.Sleep(500);
            }

            WebDriver.SwitchTo().Window(WebDriver.WindowHandles.Last());

            WebDriver.GetById("email").Clear();
            WebDriver.GetById("email").SendKeys(PartsBeeVariables.SocialEmail);

            WebDriver.GetById("pass").Clear();
            WebDriver.GetById("pass").SendKeys(PartsBeeVariables.SocialPassword);

            WebDriver.GetByName("login").Click();

            WebDriver.SwitchTo().Window(currentWindow);
            Thread.Sleep(1000);
        }

        [When(@"I open another page {{(.*)}}")]
        public void ThenIOpenAnotherPage(string website)
        {
            WebDriver.Manage().Cookies.DeleteAllCookies();
            WebDriver.Go(website);
            WebDriver.WaitFullLoad();
        }


        [Then(@"make logout")]
        public void ThenMakeLogout()
        {
            WebDriver.Go("http://partsbee-dev.azurewebsites.net/account/logout");
            Thread.Sleep(5000);
            WebDriver.Manage().Cookies.DeleteAllCookies();
            WebDriver.Navigate().Refresh();
            WebDriver.Go("http://partsbee-dev.azurewebsites.net/account/login");
        }

        [Then(@"I click SearchParts button")]
        public void ThenIClickSearchPartsButton()
        {
        WebDriver.GetByPath("//input[@class='btn btn-warning']").Click();
        }

    }
}