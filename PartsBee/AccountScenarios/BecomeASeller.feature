﻿Feature: BecomeASeller
	In order to check creating Seller account
	I create a Seller account. lol.

	Background: 
	Given I open register page
	Then I enter values in register fields
	Then accept Term And Services
	Then accept Privacy Policy
	When I press Registr button
	Then I redirect to main page
	Then I click My Account button
	Then I click Become Seller button


@BecomeASellerWithCompanyName
Scenario: Become a Seller with Company name
Then I fill Company Name
Then I fill Address1 input
Then I fill Address2 input
Then I fill City input
Then I choose State Florida
Then I fill ZIP code input
Then I fill Phone input
Then I fill Email input in Become Seller 
Then I fill Website input in Become Seller
Then I fill Additional Info in Become Seller
Then I check that checkboxes about displaying Email and Phone are unselected
Then I click on checkbox about displaying Email
Then I click on checkbpx about displaying Phone
Then I agree with selling Terms of Use
Then I click Save Seller account

 Then I click on list of Category
 And choose first and second and third items and sumbit
 Then I fill description form default values at listing page
 Then enter price and quantity
 When I click on Active listing button
 Then I am redirected to a listing page

 Then I click on Seller info on Listing preview
 Then I check Seller info
 
 Then I delete my listing within the listing itself

 @BecomeASellerAsPrivateSeller
 Scenario: Become a Private Seller
 Then I click on checkbox Private Seller
 Then I choose country Afganistan
 Then I check that field State disabled
 Then I check that field Postal code enabled
 Then I choose country Canada
 Then I check that field Province enabled
 Then I fill Address1 input for Canada
 Then I fill Address2 input
 Then I fill City input for Canada
 Then I choose Province Alberta
 Then I fill ZIP code input
 Then I fill Phone input
 Then I fill Email input in Become Seller 
 Then I fill Website input in Become Seller
 Then I fill Additional Info in Become Seller
 Then I check that checkboxes about displaying Email and Phone are unselected
 Then I agree with selling Terms of Use
 Then I click Save Seller account
 
 Then I click on list of Category
 And choose first and second and third items and sumbit
 Then I fill description form default values at listing page
 Then enter price and quantity
 When I click on Active listing button
 Then I am redirected to a listing page
 
 Then I click on Seller info on Listing preview
 Then I check that Phone and Email don't display
 Then I check Private Seller info

 Then I delete my listing within the listing itself