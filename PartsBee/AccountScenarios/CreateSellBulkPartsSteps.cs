﻿using System;
using System.IO;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Threading;
using System.Windows.Forms;
using Common;
using NUnit.Framework;
using OpenQA.Selenium;
using TechTalk.SpecFlow;

namespace PartsBee.AccountScenarios
{
    [Binding]
    public class CreateSellBulkPartsSteps : FileBasedSteps
    {
        public string vehicleTitle = "2007 ACURA TL";
        public string sumOfChoosedParts;
        public string VehicleMake;
        public string VehicleModel;
        public string VehicleYear;
        public string PartName;
        public int NumberOfDrafts;
        string[] partsNames = new string[3];
        public int x = 0;

        [Then(@"I press Sell bulk parts Button")]
        public void ThenIPressSellBulkPartsButton()
        {
            WebDriver.WaitFullLoad();
            WebDriver.GetByPath("//a[contains(text(), 'Sell bulk parts')]").Click();
        }

        [Then(@"I type VIN in VIN input")]
        public void ThenITypeVINInVINInput()
        {
            WebDriver.GetByPath("//div[@class='container']//div/div/input").SendKeys(PartsBeeVariables.VIN);
        }

        [Then(@"I click Find My Vehicle button")]
        public void ThenIClickFindMyVehicleButton()
        {
            WebDriver.GetByPath("//button[contains(text(), 'Find My Vehicle')]").Click();
        }

        [Then(@"I check titles of Vehicle")]
        public void ThenICheckTitlesOfVehicle()
        {
            Assert.True(WebDriver.GetByPath("//h2[contains (text(), '" + vehicleTitle + "')]").Enabled);
            Assert.True(WebDriver.GetByPath("//span[contains(text(), '" + vehicleTitle + "')]").Enabled);
        }

        [Then(@"I check that image of found Vehicle displayed")]
        public void ThenICheckThatImageOfFoundVehicleDisplayed()
        {
            Assert.True(WebDriver.GetByPath("//div[@class='found-vehicle-image']/div/img").Enabled);
        }

        [Then(@"I click (.*) part from (.*) subcategory")]
        public void ThenIChoosePartFromSubcategory(int p, int s)
        {
            WebDriver.GetByPath("//div/div[1]/div[" + s + "]/div[2]/ul/li[" + p + "]/label/i").Click();
        }

        [Then(@"I check that counter of selected parts changed display (.*)")]
        public void ThenICheckThatCounterOfSelectedPartsChangedDisplay(int p0)
        {
            Assert.True(WebDriver.GetByPath("//div/div[1]/div/b[contains(text(), '" + p0 + "')]").Enabled);
            sumOfChoosedParts = p0.ToString();

        }

        [Then(@"I delete choosed parts by clicking Clear All button")]
        public void ThenIDeleteChoosedPartsByClickingClearAllButton()
        {
            WebDriver.GetByPath("//small[contains(text(), 'Clear All')]").Click();
        }

        [Then(@"I check that all checkboxes unselected")]
        public void ThenICheckThatAllCheckboxesUnselected()
        {
            if (WebDriver.GetByPath("//div[3]//i").Selected)
            {
                Assert.Fail("Checkboxes are not unselected");
            }

        }

        [Then(@"I click on Interior parts")]
        public void ThenIClickOnInteriorParts()
        {
            WebDriver.GetByPath("//span[contains (text(), 'Interior')]").Click();
        }

        [Then(@"I click on Mechanical parts")]
        public void ThenIClickOnMechanicalParts()
        {
            WebDriver.GetByPath("//span[contains (text(), 'Mechanical')]").Click();
        }


        [Then(@"I click continue to Step (.*)")]
        public void ThenIClickContinueToStep(int p0)
        {
            WebDriver.GetByPath("//button[contains(text(), 'Continue to Step " + p0 + "')]").Click();
        }

        [Then(@"I check that title of Vehicle displayed")]
        public void ThenICheckThatTitleOfVehicleDisplayed()
        {
            Assert.True(WebDriver.GetByPath("//section/div/div[2]/h2/span[1][contains(text(), '" + vehicleTitle + "')]").Enabled);

        }
        [Then(@"I check number of selected Parts")]
        public void ThenICheckNumberOfSelectedParts()
        {
            Assert.True(WebDriver.GetByPath("//section/div/div[2]/h2/span[3]/span[1][contains(text(), '" + sumOfChoosedParts + "')]").Enabled);
        }

        [Then(@"I click Publish Now button")]
        public void ThenIClickPublishNowButton()
        {
            WebDriver.GetByPath("//button[contains(text(), 'Publish Now')]").Click();
        }

        [Then(@"I check that number of listings in My Listings is correct")]
        public void ThenICheckThatNumberOfListingsInMyListingsIsCorrect()
        {
            int numberOfVehicle = WebDriver.FindElements(By.XPath("//section/div[2]/div/section/div[1]")).Count;
            string sumOfVehicleInMyListings = numberOfVehicle.ToString();
            Assert.True(sumOfChoosedParts.Equals(sumOfVehicleInMyListings));
        }

        [Then(@"I check that counter in My listings display sum of choosed parts")]
        public void ThenICheckThatCounterInMyListingsDisplaySumOfChoosedParts()
        {
            Assert.True(WebDriver.GetByPath("//header/span[contains(text(), '" + sumOfChoosedParts + "')]").Enabled);
        }

        [Then(@"I delete listing from My Listings")]
        public void ThenIDeleteListingFromMyListings()
        {

            int k = Convert.ToInt32(sumOfChoosedParts);
            for (int i = 1; i <= k; i++)
            {
                WebDriver.GetByPath("//*[@class='resalts-listing-view pull-left']/div/section[1]/div[1]").Click();
                WebDriver.GetByPath("//*[@class='resalts-listing-view pull-left']/div/section[1]/div[1]/div/img").Click();
                WebDriver.GetByPath("//section[1]/div[1]/article/div/div[2]/a[3]").Click();

                WebDriver.GetByPath("//button[@class='btn btn-warning'][contains(text(), 'Yes')]").Click();
                Thread.Sleep(500);
            }



        }
        [Then(@"I check phrase about empty filter")]
        public void ThenICheckPhraseAboutEmptyFilter()
        {
            Assert.True(WebDriver.GetByPath("//section/div[2][contains (text(), 'No items found for this search criteria')]").Enabled);
            sumOfChoosedParts = "0";
        }


        [Then(@"I click on Search")]
        public void ThenIClickOnSearch()
        {
            WebDriver.GetByPath("//a[contains(text(), 'SEARCH')]").Click();
        }

        [Then(@"i open my lisitngs")]
        public void ThenIOpenMyLisitngs()
        {
            WebDriver.GetByPath("//span[contains(text(), 'My Listings')]").Click();
        }

        [Then(@"I upload new picture and check that it changes")]
        public void ThenIUploadNewPictureAndCheckThatItChanges()
        {
            var imgBefore = WebDriver.GetByPath("//article/div[1]/div/img").GetAttribute("src");

            // Get current window handle for switching back after closing the dialog
            string originalHandle = WebDriver.CurrentWindowHandle;

            IWebElement upload = WebDriver.GetByPath("//a[contains(text(), 'Upload it Manually')]");
            upload.Click();

            // Wait for the number of window handles to be greater than 1, or a timeou
            DateTime timeoutEnd = DateTime.Now.Add(TimeSpan.FromSeconds(5));
            while (WebDriver.WindowHandles.Count == 1 && DateTime.Now < timeoutEnd)
            {
                Thread.Sleep(100);
            }

            // Loop through all window handles, finding the first one that isn't
            // the original window handle, and switching to it.
            foreach (string handle in WebDriver.WindowHandles)
            {
                if (handle != originalHandle)
                {
                    WebDriver.SwitchTo().Window(handle);
                    break;
                }
            }

            // Send filename
            Console.Out.WriteLine(Images[0] + String.Format(" exists - {0}", File.Exists(Images[0])));
            SendKeys.SendWait(Images[0]);
            SendKeys.SendWait(@"{Enter}");

            Thread.Sleep(1000);

            // Navigate to original window
            WebDriver.SwitchTo().Window(originalHandle);

            Thread.Sleep(1000);

            var imgAfter = WebDriver.GetByPath("//article/div[1]/div/img").GetAttribute("src");

            if (imgBefore.Equals(imgAfter))
            {
                Assert.Fail("Image didn't changed");
            }
        }

        [Then(@"I click that I dont have VIN")]
        public void ThenIClickThatIDontHaveVIN()
        {
            WebDriver.GetByPath("//a[contains(text(), 'I don’t have VIN or Stock#')]").Click();
        }

        [Then(@"I choose Year (.*)")]
        public void ThenIChooseYear(int vehicleYear)
        {
            WebDriver.GetByPath("//div[2]/div[1]/div[1]/div/div").Click();
            WebDriver.GetByPath("//ul//a/span[contains(text(), '" + vehicleYear + "')]").Click();

            VehicleYear = vehicleYear.ToString();
        }

        [Then(@"I check that Vehicle Type displayed")]
        public void ThenICheckThatVehicleTypeDisplayed()
        {
            Assert.True(WebDriver.GetByPath("//div[contains(text(), 'Vehicle Type')]").Enabled);
        }

        [Then(@"I check that Vehicle Type disabled")]
        public void ThenICheckThatVehicleTypeDisabled()
        {
            try
            {
                WebDriver.GetByPath("//div[contains(text(), 'Vehicle Type')]");
                Assert.Fail("Field Vehicle Type doesn't disappear");
            }
            catch (NoSuchElementException)
            {
            }
        }

        [Then(@"I choose Make (.*)")]
        public void ThenIChooseMakeAcura(string vehicleMake)
        {
            WebDriver.GetByPath("//div[2]/div[1]/div[2]/div/div").Click();
            WebDriver.GetByPath("//ul//a/span[contains(text(), '" + vehicleMake + "')]").Click();

            VehicleMake = vehicleMake;
        }

        [Then(@"I choose model (.*)")]
        public void ThenIChooseModelEl(string vehicleModel)
        {
            WebDriver.GetByPath("//div[2]/div[1]/div[3]/div/div").Click();
            WebDriver.GetByPath("//ul//span[contains(text(), '" + vehicleModel + "')]").Click();

            VehicleModel = vehicleModel;
        }

        [Then(@"I type trim")]
        public void ThenITypeTrim()
        {
            WebDriver.GetByName("trim").SendKeys(PartsBeeVariables.trim);
        }

        [Then(@"I check title of added Vehicle in header")]
        public void ThenICheckTitleOfAddedVehicle()
        {
            Assert.True(WebDriver.GetByPath("//footer/div/div//span[contains(text(), '" + VehicleYear + " " + VehicleMake + " " + VehicleModel + " " + PartsBeeVariables.trim + "')]").Enabled);

        }

        [Then(@"I upload picture by Drag&Drop")]
        public void ThenIUploadPictureToDragDropInput()
        {

            string originalHandle = WebDriver.CurrentWindowHandle;
            IWebElement upload = WebDriver.GetByPath("//div[2]/div[2]/label");
            upload.Click();
            DateTime timeoutEnd = DateTime.Now.Add(TimeSpan.FromSeconds(5));
            while (WebDriver.WindowHandles.Count == 1 && DateTime.Now < timeoutEnd)
            {
                Thread.Sleep(100);
            }
            foreach (string handle in WebDriver.WindowHandles)
            {
                if (handle != originalHandle)
                {
                    WebDriver.SwitchTo().Window(handle);
                    break;
                }
            }
            Console.Out.WriteLine(Images[0] + String.Format(" exists - {0}", File.Exists(Images[0])));
            SendKeys.SendWait(Images[0]);
            SendKeys.SendWait(@"{Enter}");
            Thread.Sleep(1000);
            WebDriver.SwitchTo().Window(originalHandle);
            Thread.Sleep(1000);
        }

        [Then(@"I upload picture by Add more image button")]
        public void ThenIUploadPictureByAddMoreImageButton()
        {
            string originalHandle = WebDriver.CurrentWindowHandle;
            IWebElement upload = WebDriver.GetByPath("//div[1]/span/label");
            upload.Click();
            DateTime timeoutEnd = DateTime.Now.Add(TimeSpan.FromSeconds(5));
            while (WebDriver.WindowHandles.Count == 1 && DateTime.Now < timeoutEnd)
            {
                Thread.Sleep(100);
            }
            foreach (string handle in WebDriver.WindowHandles)
            {
                if (handle != originalHandle)
                {
                    WebDriver.SwitchTo().Window(handle);
                    break;
                }
            }
            Console.Out.WriteLine(Images[0] + String.Format(" exists - {0}", File.Exists(Images[0])));
            SendKeys.SendWait(Images[0]);
            SendKeys.SendWait(@"{Enter}");
            Thread.Sleep(1000);
            WebDriver.SwitchTo().Window(originalHandle);
        }

        [Then(@"I check that uploaded (.*) images")]
        public void ThenICheckThatUploadedImages(int p0)
        {
            int numberOfImages = WebDriver.FindElements(By.XPath("//ul/li/span")).Count;
            Assert.True(numberOfImages.Equals(p0));
        }

        [Then(@"I check title of added Vehicle over a picture")]
        public void ThenICheckTitleOfAddedVehicleOverAPicture()
        {
            Assert.True(WebDriver.GetByPath("//article/h2[contains(text(), '" + VehicleYear + " " + VehicleMake + " " + VehicleModel + " " + PartsBeeVariables.trim + "')]").Enabled);
        }

        [Then(@"I upload picture by button under image")]
        public void ThenIUploadPictureByButtonUnderImage()
        {
            string originalHandle = WebDriver.CurrentWindowHandle;
            IWebElement upload = WebDriver.GetByPath("//button[contains(text(), 'Add More Images')]");
            upload.Click();
            DateTime timeoutEnd = DateTime.Now.Add(TimeSpan.FromSeconds(5));
            while (WebDriver.WindowHandles.Count == 1 && DateTime.Now < timeoutEnd)
            {
                Thread.Sleep(100);
            }
            foreach (string handle in WebDriver.WindowHandles)
            {
                if (handle != originalHandle)
                {
                    WebDriver.SwitchTo().Window(handle);
                    break;
                }
            }
            Console.Out.WriteLine(Images[0] + String.Format(" exists - {0}", File.Exists(Images[0])));
            SendKeys.SendWait(Images[0]);
            SendKeys.SendWait(@"{Enter}");
            Thread.Sleep(1000);
            WebDriver.SwitchTo().Window(originalHandle);
        }

        [Then(@"I type in search Parts input (.*)")]
        public void ThenITypeInSearchPartsInputAntenna(string partName)
        {
            Thread.Sleep(2000);
            WebDriver.GetByPath("//input[@type='search']").Clear();
            WebDriver.GetByPath("//input[@type='search']").SendKeys(partName);
            Thread.Sleep(2000);
            WebDriver.GetByPath("//ul/li[1]/label/span/b").Click();

            PartName = partName;
            partsNames[x] = partName;
            x++;
        }

        [Then(@"I check that choosed part checkbox selected")]
        public void ThenICheckThatCheckboxSelected()
        {
            Thread.Sleep(2000);
            Assert.True(WebDriver.GetByPath("//ul/li/label/span[contains(text(), '" + PartName + "')]/preceding-sibling::*").Selected);
        }

        [Then(@"I click Make Revision button")]
        public void ThenIClickMakeRevisionButton()
        {
            WebDriver.GetByPath("//button[contains(text(), 'Make Revisions')]").Click();
        }

        [Then(@"I click on checkbox for first part")]
        public void ThenIClickOnCheckboxForFirstPart()
        {
            WebDriver.GetByPath("//table[1]/tbody/tr/td[1]/div/div[2]/label/i").Click();
        }

        [Then(@"I click Edit for second part")]
        public void ThenIClickEditForSecondPart()
        {
            WebDriver.GetByPath("//table[2]/tbody/tr/td[2]/span[2]/a/span").Click();
        }

        [Then(@"I click save part changes")]
        public void ThenIClickSavePartChanges()
        {
            WebDriver.GetByPath("//button[contains(text(), 'SAVE')]").Click();
        }




        [Then(@"I check whether I am on the drafts page and go to bulk parts if I am there")]
        public void ThenICheckWhetherIAmOnTheDraftsPageAndGoToBulkPartsIfIAmThere()
        {
            try
            {
                WebDriver.GetByPath("//*[contains(text(),'Available Drafts:')]");
                WebDriver.GetByPath("//a[contains(text(),'List Parts in Bulk')]").Click();
            }
            catch (WebDriverException)
            { }
        }

        [Then(@"I type price for third part")]
        public void ThenITypePriceForThirdPart()
        {
            WebDriver.GetByPath("//table[3]/tbody/tr[1]/td[1]/div/div[2]/input").SendKeys("150");
        }

        [Then(@"I check price near second price")]
        public void ThenICheckPriceNearSecondPrice()
        {
            var price = WebDriver.GetByPath("//table[2]/tbody/tr/td[1]/div/div[2]/input").GetAttribute("value");
            Assert.True(price.Equals("100"));
        }

        [Then(@"I check that status ready enabled")]
        public void ThenICheckThatStatusReadyEnabled()
        {
            Assert.True(WebDriver.GetByPath("//table[1]/tbody/tr/td[2]/span[1]/span[2][contains(text(), 'Ready!')]").Enabled);
            Assert.True(WebDriver.GetByPath("//table[2]/tbody/tr/td[2]/span[1]/span[2][contains(text(), 'Ready!')]").Enabled);
            Assert.True(WebDriver.GetByPath("//table[3]/tbody/tr/td[2]/span[1]/span[2][contains(text(), 'Ready!')]").Enabled);


            int numberOfReadyVehicle = WebDriver.FindElements(By.XPath("//table/tbody/tr/td[2]/span[1]/span[2][contains(text(), 'Ready!')]")).Count;
            sumOfChoosedParts = numberOfReadyVehicle.ToString();

        }

        [Then(@"I clear description form")]
        public void ThenIClearDescriptionForm()
        {
            WebDriver.GetByPath("//textarea[@name='description']").Click();
            WebDriver.GetByPath("//textarea[@name='description']").Clear();
        }


        [Then(@"I click Publish My Listings button")]
        public void ThenIClickPublishMyListingsButton()
        {
            WebDriver.GetByPath("//button[contains(text(), 'Publish My Listings')]").Click();
        }


        [Then(@"I proceed with drafts creation")]
        public void ThenIProceedWithDraftsCreation()
        {
            WebDriver.GetByPath("//button[contains(text(), 'Proceed')]").Click();
        }

        [Then(@"I check prices of created Vehicles")]
        public void ThenICheckPricesOfCreatedVehicles()
        {
            Assert.True(WebDriver.GetByPath("//section[1]/div[2]/span[contains(text(), '$150.00 USD')]").Enabled);
            Assert.True(WebDriver.GetByPath("//section[2]/div[2]/span[contains(text(), '$100.00 USD')]").Enabled);
            Assert.True(WebDriver.GetByPath("//section[3]/div[2]/span[contains(text(), 'Price is negotiable')]").Enabled);
        }

        [Then(@"I delete first listing from My Listings")]
        public void ThenIDeleteFirstListingFromMyListings()
        {
            WebDriver.GetByPath("//*[@class='resalts-listing-view pull-left']/div/section[1]/div[1]").Click();
            WebDriver.GetByPath("//*[@class='resalts-listing-view pull-left']/div/section[1]/div[1]/div/img").Click();
            WebDriver.GetByPath("//section[1]/div[1]/article/div/div[2]/a[3]").Click();

            WebDriver.GetByPath("//button[@class='btn btn-warning'][contains(text(), 'Yes')]").Click();
        }

        [Then(@"I open open second listing")]
        public void ThenIOpenOpenSecondListing()
        {
            WebDriver.GetByPath("//div[@class='resalts-listing-view pull-left']//section[2]/div[1]").Click();
            WebDriver.GetByPath("//div[@class='resalts-listing-view pull-left']//section[2]/div[1]/div/img").Click();
            WebDriver.GetByPath("//div[@class='resalts-listing-view pull-left']//section[2]//a[@class='btn btn-default']").Click();
        }

        [Then(@"I am redirected to a listing page and check Additional information for bulk part")]
        public void ThenIAmRedirectedToAListingPageAndCheckAdditionalInformationForBulkPart()
        {
            Thread.Sleep(3000);
            Assert.True(WebDriver.GetBy("//h2[contains(text(), 'Listing description')]").SetTimer(2000).Find().Displayed);

            Assert.True(WebDriver.GetByPath("//section/div/div/div[3]/div[1]/span/b").Text.Equals("$100.00 USD"));



            Thread.Sleep(3000);
            Assert.True(WebDriver.GetBy("//h2[contains(text(), 'Listing description')]").SetTimer(2000).Find().Displayed);

            // Assert.True(WebDriver.GetByPath("//div[@class='listing-brief-info-price']/div/div").Text.ToUpper().Equals("PRICE IS NEGOTIABLE"));
            //Assert.True(WebDriver.GetByPath("//div[@class='container']/div[1]/div[3]/div[2]/dl[2]/dd").Text.Equals("Test Company Nam1e"));
            Assert.True(WebDriver.GetByPath("//div[@id='listingDescription']/div/article[1]/div[2]/div[1]/table/tbody/tr[2]/td").Text.Equals("Kello 11Kie"));
            Assert.True(WebDriver.GetByPath("//div[@id='listingDescription']/div/article[1]/div[2]/div[1]/table/tbody/tr[1]/td").Text.Equals("New"));
            //Assert.True(WebDriver.GetByPath("//div[@id='listingDescription']/div/article[1]/p").Text.Equals("Notestest"));
            Assert.True(WebDriver.GetByPath("//div[@id='listingDescription']/div/article[1]/div[2]/div[2]/table/tbody/tr[1]/td").Text.Equals("Unknown"));
            Assert.True(WebDriver.GetByPath("//div[@id='listingDescription']/div/article[1]/div[2]/div[2]/table/tbody/tr[2]/td").Text.Equals("3"));
            Assert.True(WebDriver.GetByPath("//div[@id='listingDescription']/div/article[2]/div[1]/table/tbody/tr[1]/td[1]").Text.Equals("TOYOTA"));
            Assert.True(WebDriver.GetByPath("//div[@id='listingDescription']/div/article[2]/div[1]/table/tbody/tr[2]/td[1]").Text.Equals("WAUED64B11N******"));
            Assert.True(WebDriver.GetByPath("//div[@id='listingDescription']/div/article[2]/div[1]/table/tbody/tr[4]/td[1]").Text.Contains("LIGHT BLUE"));
            Assert.True(WebDriver.GetByPath("//div[@id='listingDescription']/div/article[2]/div[1]/table/tbody/tr[5]/td[1]").Text.Equals("Matte"));
            Assert.True(WebDriver.GetByPath("//div[@id='listingDescription']/div/article[2]/div[1]/table/tbody/tr[6]/td[1]").Text.Equals("United States"));
            Assert.True(WebDriver.GetByPath("//div[@id='listingDescription']/div/article[2]/div[1]/table/tbody/tr[7]/td[1]").Text.Equals("TestDamage"));
            Assert.True(WebDriver.GetByPath("//div[@id='listingDescription']/div/article[2]/div[2]/table/tbody/tr[5]/td[1]").Text.Equals("Left, Front, Right, Rear, Bottom, Top"));
            Assert.True(WebDriver.GetByPath("//div[@id='listingDescription']/div/article[2]/div[2]/table/tbody/tr[4]/td[1]").Text.Equals("6 (pounds) 7 (ounces)"));
            Assert.True(WebDriver.GetByPath("//div[@id='listingDescription']/div/article[2]/div[2]/table/tbody/tr[3]/td[1]").Text.Equals("5 (inches)"));
            Assert.True(WebDriver.GetByPath("//div[@id='listingDescription']/div/article[2]/div[2]/table/tbody/tr[2]/td[1]").Text.Equals("4 (inches)"));
            Assert.True(WebDriver.GetByPath("//div[@id='listingDescription']/div/article[2]/div[2]/table/tbody/tr[1]/td[1]").Text.Equals("3 (inches)"));

            /*    Assert.True(WebDriver.GetByPath("//div[@id='listingDescription']/div/article[1]/div[2]/div[1]/table/tbody/tr[1]/td").Text.Equals("New"));
                Assert.True(WebDriver.GetByPath("//div[@id='listingDescription']/div/article[1]/div[2]/div[1]/table/tbody/tr[2]/td").Text.Equals("Kello 11Kie"));
                Assert.True(WebDriver.GetByPath("//div[@id='listingDescription']/div/article[1]/div[2]/div[2]/table/tbody/tr[1]/td").Text.Equals("Unknown"));
                Assert.True(WebDriver.GetByPath("//div[@id='listingDescription']/div/article[1]/div[2]/div[2]/table/tbody/tr[2]/td").Text.Equals("3"));
    //            Assert.True(WebDriver.GetByPath("//div[@id='listingDescription']/div/article[2]/div[1]/table/tbody/tr[1]/td[1]").Text.Equals("TOYOTA"));
                Assert.True(WebDriver.GetByPath("//div[@id='listingDescription']/div/article[2]/div[1]/table/tbody/tr[2]/td[1]").Text.Equals("WAUED64B11N******"));
                Assert.True(WebDriver.GetByPath("//div[@id='listingDescription']/div/article[2]/div[1]/table/tbody/tr[4]/td[1]").Text.Contains("LIGHT BLUE"));
                Assert.True(WebDriver.GetByPath("//div[@id='listingDescription']/div/article[2]/div[1]/table/tbody/tr[5]/td[1]").Text.Equals("Matte"));
                Assert.True(WebDriver.GetByPath("//div[@id='listingDescription']/div/article[2]/div[1]/table/tbody/tr[6]/td[1]").Text.Equals("United States"));
                Assert.True(WebDriver.GetByPath("//div[@id='listingDescription']/div/article[2]/div[1]/table/tbody/tr[7]/td[1]").Text.Equals("TestDamage"));
                Assert.True(WebDriver.GetByPath("//div[@id='listingDescription']/div/article[2]/div[2]/table/tbody/tr[5]/td[1]").Text.Equals("Left, Front, Right, Rear, Bottom, Top"));
                Assert.True(WebDriver.GetByPath("//div[@id='listingDescription']/div/article[2]/div[2]/table/tbody/tr[4]/td[1]").Text.Equals("6 (pounds) 7 (ounces)"));
                Assert.True(WebDriver.GetByPath("//div[@id='listingDescription']/div/article[2]/div[2]/table/tbody/tr[3]/td[1]").Text.Equals("5 (inches)"));
                Assert.True(WebDriver.GetByPath("//div[@id='listingDescription']/div/article[2]/div[2]/table/tbody/tr[2]/td[1]").Text.Equals("4 (inches)"));
                Assert.True(WebDriver.GetByPath("//div[@id='listingDescription']/div/article[2]/div[2]/table/tbody/tr[1]/td[1]").Text.Equals("3 (inches)")); */
        }

        [Then(@"I open filter My listings")]
        public void ThenIOpenFilterMyListings()
        {
            WebDriver.GetByPath("//div[1]/div[2]/div/label[1]/a/span").Click();
        }

        [Then(@"I check that one group of drafts displayed")]
        public void ThenICheckThatOneGroupOfDraftsDisplayed()
        {
            Assert.True(1.Equals(WebDriver.FindElements(By.XPath("//section/div/div/div/div/article/img")).Count));
        }

        [Then(@"I check that in group 2 listings")]
        public void ThenICheckThatInGroupListings()
        {
            var numberOfDrafts = WebDriver.GetByPath("//section/div/div/div/div[1]/article/div[2]/div/span[2]").Text;
            Assert.True("2".Equals(numberOfDrafts));
        }

        [Then(@"I open drafts group")]
        public void ThenIOpenDraftsGroup()
        {
            WebDriver.GetByPath("//a[contains(text(), 'Complete')]").Click();
        }

        [Then(@"I check that in group (.*) drafts")]
        public void ThenICheckThatInGroupDrafts(int p0)
        {
            NumberOfDrafts = WebDriver.FindElements(By.XPath("//article/img")).Count;
            Assert.True(p0.Equals(NumberOfDrafts));
        }

        [Then(@"I delete drafts from group")]
        public void ThenIDeleteDraftsFromGroup()
        {
            for (int i = 1; i <= NumberOfDrafts; i++)
            {
                WebDriver.GetByPath("//article[1]/div[1]/a[2][contains(text(), 'Delete')]").Click();
                Thread.Sleep(200);
            }
        }

        [Then(@"I see message about no drafts aviable")]
        public void ThenISeeMessageAboutNoDraftsAviable()
        {
            Assert.True(WebDriver.GetByPath("//span[contains(text(), 'There are no drafts available')]").Enabled);
        }

        [Then(@"I check parts names")]
        public void ThenICheckPartsNames()
        {
            for (x = 0; x < partsNames.Length; x++)
            {
                Assert.True(partsNames[x].Equals(WebDriver.GetByPath("//section[2]/div/div[1]/div[" + (x + 3) + "]/div[2]/div/div[1]").Text));
            }

        }

        [Then(@"I check title on third step in header")]
        public void ThenICheckTitleOnThirdStepInHeader()
        {
            Assert.True((WebDriver.GetByPath("//h2/strong").Text).Contains(vehicleTitle));
            Assert.True((WebDriver.GetByPath("//h2/strong").Text).Contains(PartsBeeVariables.trim));
        }

        [Then(@"I check title on third step in footer")]
        public void ThenICheckTitleOnThirdStepInFooter()
        {
            Assert.True((WebDriver.GetByPath("//section[2]/div/div[2]/div/div/div[1]/div/span[2]").Text).Contains(vehicleTitle));
            Assert.True((WebDriver.GetByPath("//section[2]/div/div[2]/div/div/div[1]/div/span[2]").Text).Contains(PartsBeeVariables.trim));
        }

        [Then(@"I check number of selected parts on third step in footer")]
        public void ThenICheckNumberOfSelectedPartsOnThirdStepInFooter()
        {
            Assert.True(WebDriver.GetByPath("//section[2]/div/div[2]/div/div/div[1]/div/b").Text.Equals(sumOfChoosedParts));
        }

        [Then(@"I check number of selected parts on third step in header")]
        public void ThenICheckNumberOfSelectedPartsOnThirdStepInHeader()
        {
            Assert.True(WebDriver.GetByPath("//section[2]/div/div[1]/div[2]/span[1]").Text.Equals(sumOfChoosedParts));
        }

        [Then(@"I delete one of parts on third step Parts Wanted")]
        public void ThenIDeleteOneOfPartsOnThirdStepPartsWanted()
        {
            WebDriver.GetByPath("//section[2]/div/div[1]/div[3]/div[2]/div/div[2]/img").Click();
            int SumOfChoosedParts = Convert.ToInt32(sumOfChoosedParts);
            SumOfChoosedParts--;
            sumOfChoosedParts = Convert.ToString(SumOfChoosedParts);
        }

        [Then(@"I check counter of left parts")]
        public void ThenICheckCounterOfLeftParts()
        {
            int numberOfLeftParts = WebDriver.FindElements(By.XPath("//*[@alt='Delete']")).Count;
            Assert.True(sumOfChoosedParts.Equals("" + numberOfLeftParts));
        }

        [Then(@"I see congradulation message")]
        public void ThenISeeCongradulationMessage()
        {
            WebDriver.waitForFindElement(By.XPath("//b[contains(text(), 'Congratulations!')]"), 20);
            Assert.True(WebDriver.GetByPath("//article/h2/span").Text.Equals("All parts are in saved search for your"));
        }

        [Then(@"I see title on confradulation page")]
        public void ThenISeeTitleOnConfradulationPage()
        {
            Assert.True(WebDriver.GetByPath("//article/h2/strong").Text.Contains(PartsBeeVariables.trim));
            Assert.True(WebDriver.GetByPath("//article/h2/strong").Text.Contains(vehicleTitle));
        }

        [Then(@"I see that my Parts Wanted list display in saved search")]
        public void ThenISeeThatMyPartsWantedListDisplayInSavedSearch()
        {
            vehicleTitle = vehicleTitle.Replace(' ', '-');
            Assert.True(WebDriver.GetByPath("//div/div/div/div[1]/div/span/span[contains(text(), '" + vehicleTitle + "-" + PartsBeeVariables.VIN + "')]").Enabled);
        }

        [Then(@"I open list of Parts Wanted")]
        public void ThenIOpenListOfPartsWanted()
        {
            WebDriver.GetByPath("//div[2]/div/div/div/div[1]/div/span/i").Click();
        }

        [Then(@"check count of parts in list of Parts Wanted")]
        public void ThenCheckCountOfPartsInListOfPartsWanted()
        {
            int numberOfPartsInList = WebDriver.FindElements(By.XPath("//div/div/div[2]/div[1]/div/a/span")).Count;
            Assert.True(sumOfChoosedParts.Equals("" + numberOfPartsInList));
        }

        [Then(@"I check names of parts in List")]
        public void ThenICheckNamesOfPartsInList()
        {
            for (x = 1; x < partsNames.Length - 1; x++)
            {
                Assert.True((WebDriver.GetByPath("//div/div/div[2]/div[1]/div[" + x + "]/a/span").Text).Contains(partsNames[x]));
            }
        }

        [Then(@"I delete parts in list")]
        public void ThenIDeletePartsInList()
        {
            for (x = 0; x < partsNames.Length - 1; x++)
            {
                WebDriver.GetByPath("//div/div/div[2]/div[1]/div[1]/a/i").Click();
                x++;
            }
        }

        [Then(@"I check that ads are displayed")]
        public void ThenICheckThatAdsDisplayed()
        {
            for (x = 1; x < partsNames.Length - 1; x++)
            {
                Assert.True(WebDriver.GetByPath("//table/tbody/tr[" + x + "]/td[2]/h2[contains(text(), '" + vehicleTitle + "')]").Enabled);
            }
            for (x = 1; x < partsNames.Length - 1; x++)
            {
                Assert.True((WebDriver.GetByPath("//table/tbody/tr[" + x + "]/td[2]/h2").Text).Contains(partsNames[x]));
            }


        }
        [Then(@"I check dates of created ads")]
        public void ThenICheckDatesOfCreatedAds()
        {
            string s;
           for (x = 1; x < partsNames.Length - 1; x++)
            {
                s = WebDriver.GetByPath("//section/table/tbody/tr[" + x + "]/td[4]/span").Text;
                //   k = DateTime.Now.Subtract(new TimeSpan())

                Assert.IsTrue(Convert.ToDateTime(s).Month - DateTime.Today.Month == 1);
                Assert.IsTrue(Convert.ToDateTime(s).Year == DateTime.Today.Year);
                Assert.IsTrue(Convert.ToDateTime(s).Day == DateTime.Today.Day);

                //var deltaDate = Convert.ToDateTime(s) - DateTime.Today;

                //Assert.IsTrue(deltaDate == new TimeSpan());

            }
        }


    }
}