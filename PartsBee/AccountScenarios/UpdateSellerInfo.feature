﻿@PartsBee_UpdateSellerInfo
Feature: UpdateSellerInfo
	In order to avoid silly mistakes
	As a Sller
	
	@ChangingSellerInfo
Scenario Outline: ChangingSellerInfo
    Given I open login page 
	Then I login as Seller
	Then I click My Account button

	Then I switch metric
	Then I click the Save button (Profile)

	Then I click on Seller Account info
	When I click on checkbox Private Seller and rename company {{Test Company}} and Save it
	Then I see conformation message about Successful saving
	Then I reload page and check Company Name info
	Then I click on checkbox Private Seller and Save it

	Then I click My Account button
	Then I click on Seller Account info

	When I clear all requirement fields
	Then I save changes of Seller Account info
	Then I should NOT see a confirmation message about successful saving

	Then I click My Account button
	Then I click on Seller Account info

	When I fill fields and Save them
	Then I see conformation message about Successful saving
	Then I reload page and check fields
	Then I clean all optional fields
	Then I save changes of Seller Account info
	Then I see conformation message about Successful saving

	When I change field City, State, Phone, Email
	When I fill fields and Save them
	Then I save changes of Seller Account info
	Then I see conformation message about Successful saving

	Then I take information from Seller profile fields and save it for later

	Then I press Sell Your part Button
    Then I click ListSinglePart from a main dropdown
	Then I check whether I am on the drafts page and go to single listing if I am there
    Then I click on list of Category
    And choose first and second and third items and sumbit
    Then I fill description form default values at listing page (with random title)
    Then enter price and quantity
	Then I click on Additional option then fill data <makeSet> <modelSet> <colorSet> <finishSet> <countrySet> <exLinkSet> <widthSet> <heghtSet> <lengthSet> <weigthSet> <weigthSet2> <damageSet> <WarrantySet> <placementSet> and sumbit 
    When I click on Active listing button
    Then I am redirected to a listing page

	Then I check that the metric in listing is shown as METRIC

	Then all required information from my seller account should be included in the listing
	Then I delete my listing within the listing itself

	Then I click My Account button
	Then I switch metric
	Then I click the Save button (Profile)

	Examples: 
	| makeSet | modelSet | colorSet | finishSet | countrySet | exLinkSet | widthSet | heghtSet | lengthSet | weigthSet | weightSet2 | damageSet | WarrantySet | placementSet |
	| ACURA   |          |		    |           |    null    |   null    |   null   |    null  |   null    |   null    |   null     |   null    |    null     |   null       |
