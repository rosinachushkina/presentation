﻿Feature: LinksFromMainPage

@LinksFromMainPage
Scenario: LinksFromMainPage
 Then I switch to main page
 Then I click the Log in button
 Then I login as Seller
 Then I switch to main page

 Then I check the Search Parts Now button
 Then I switch to main page
 Then I check the Search main menu button
 Then I check the About Us main menu button

 Then I press Sell Your part Button
 Then I click ListSinglePart from a main dropdown
 Then I check whether I am on the drafts page and go to single listing if I am there
 Then I check that I am at Sell Your Parts > List Single Part

 Then I press Sell Your part Button
 Then I press Sell bulk parts Button
 Then I check whether I am on the drafts page and go to bulk parts if I am there
 Then I check that I am at Sell Your Parts > List Bulk Parts

 Then I check the Parts Wanted main menu button
 Then I click My Account button
 Then I check that I am a My Account

 Then I go to Dashboard
 Then I check that I am in the Dashboard (Seller)

 Then I switch to main page
 Then I check the Seller Policy footer
 Then I check the Privacy Policy footer
 Then I check the Terms of Service footer
 
 Then I check the Facebook footer
 Then I check the GooglePlus footer
 Then I check the Twitter footer
 Then I check the Pinterest footer

 Then I check the LogOut button