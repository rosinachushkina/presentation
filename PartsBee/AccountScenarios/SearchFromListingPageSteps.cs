﻿using System;
using System.Globalization;
using System.Threading;
using Common;
using NUnit.Framework;
using TechTalk.SpecFlow;

namespace PartsBee
{
    [Binding]
    public class SearchFromListingPageSteps : BaseSteps
    {
        public string listingTitle = "";
        public string listingTitle1 = "";
        public string searchValue = "";

        [Then(@"I type (.*) search input")]
        public void ThenITypeSearchInput(string make)
        {
            WebDriver.GetByPath("//div/div[1]/div[2]/input").SendKeys(make);
            searchValue = make;
        }

        [Then(@"I choose make from autosuggest")]
        public void ThenIChooseMakeFromAutosuggest()
        {
            WebDriver.GetByPath("//div[@class='autocomplete-suggestion-item'][contains(text(), 'Make')]/b").Click();
        }

        [Then(@"I click GO search on search page")]
        public void ThenIClickGOSearchOnSearchPage()
        {
            WebDriver.GetByPath("//button[contains(text(), 'GO')]").Click();
            Thread.Sleep(500);
        }

        [Then(@"I save title of first listing")]
        public void ThenISaveTitleOfFirstListing()
        {
            /*  WebDriver.GetByPath("//*[@class='resalts-listing-view pull-left']/div/section[1]/div[1]").Click();
              WebDriver.GetByPath("//*[@class='resalts-listing-view pull-left']/div/section[1]/div[1]/div/img[1]").Click(); */
            listingTitle = (WebDriver.GetByPath("//*[@class='resalts-listing-view pull-left']/div/section[1]/div[1]/h2").Text);
            listingTitle1 = listingTitle.ToUpper();
        }

        [Then(@"I compare titles")]
        public void ThenICompareTitles()
        {
            string text = WebDriver.GetByPath("//section[1]/div/div/div[3]/header/h2").Text;
            Assert.True((listingTitle1).Contains(text));
        }

        [Then(@"I click on search input and check search")]
        public void ThenIClickOnSearchInputAndCheckSearch()
        {
            Assert.True(WebDriver.GetByPath("//div[1]/input").GetAttribute("value").Equals(searchValue));
        }

        [Then(@"I save title of next Listing")]
        public void ThenISaveTitleOfNextListing()
        {
            listingTitle = WebDriver.GetByPath("//div/div[2]/article/h2").Text;
        }

        [Then(@"I check phrase about next search results")]
        public void ThenICheckPhraseAboutNextSearchResults()
        {
            Assert.True(WebDriver.GetByPath("//article/p").Text.Equals("NEXT SEARCH RESULTS LISTINGS"));
        }

        [Then(@"I redirect to next listing")]
        public void ThenIRedirectToNextListing()
        {
            WebDriver.GetByPath("//div/div[2]/div/div/div[2]/a").Click();
        }

        [Then(@"I check phrase about previous listing")]
        public void ThenICheckPhraseAboutPreviousListing()
        {
            Assert.True(WebDriver.GetByPath("//div/div/div[1]/article/p").Text.Equals("PREVIOUS SEARCH RESULTS LISTINGS"));
        }

        [Then(@"I save title of previous listing")]
        public void ThenISaveTitleOfPreviousListing()
        {
            listingTitle = WebDriver.GetByPath("//article/h2/span[1]").Text;
        }

        [Then(@"I redirect to previous listing")]
        public void ThenIRedirectToPreviousListing()
        {
            WebDriver.GetByPath("//div[3]/div/div[2]/div/div/div[1]/a").Click();
        }

        [Then(@"I click go to search results")]
        public void ThenIClickGoToSearchResults()
        {
            WebDriver.GetByPath("//div[2]/a/span[2]").Click();
        }

        [Then(@"I redirect to search page with search filter")]
        public void ThenIRedirectToSearchPageWithSearchFilter()
        {
            string site;
            if (PartsBeeVariables.IsProductionTest == true)
                site = PartsBeePages.ProdSearchPage;
            else site = PartsBeePages.DevSearchPage;

            Assert.True(WebDriver.GetCurrentUrl().Contains(site));
        }

    }
}