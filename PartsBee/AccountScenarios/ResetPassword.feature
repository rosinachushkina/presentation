﻿Feature: ResetPassword
	In order to check reseting password
	I reset password from account settings page

@resetPasswordFromAccountSettingsPage
Scenario: Reseting password from account settings page
	Given I open login page
	Then I login as User with reset email
	Then I click My Account button
	Then I click Reset Password button in account settings page
	Then I agree with reseting password
	Then I see message about sending link with reseting
	
	
	Then I open gmail.com
	Then I type sign in button in Gmail
	Then I enter our reseted user's email and password and submit them (for GMAIL.COM)
	Then I click on reset password message
	Then I check email with reseting
	Then I click on link for reseting password
	Then I type new password 
	Then I see conformation message about Successful reseting
	Then I redirect to main page
	Then I make logout
	Then I login with new password with email after reseting password


	Then I click My Account button
	Then I click Reset Password button in account settings page
	Then I agree with reseting password
	Then I see message about sending link with reseting
	
	Then I wait 30 seconds

	Then I open gmail.com
	Then I click on reset password message
	Then I check email with reseting
	Then I click on link for reseting password
	Then I type old password 
	Then I see conformation message about Successful reseting
	Then I redirect to main page
	Then I make logout
	Then I login as User with reset email
	Then I redirect to main page