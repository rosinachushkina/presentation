﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TechTalk.SpecFlow;
using Common;
using System.Threading;
using NUnit.Framework;
using System.Windows.Forms;
using OpenQA.Selenium;

namespace PartsBee.AccountScenarios
    {
    [Binding]
    public class SendingMessages : BaseSteps
        {
        string testmessage1 = "Привет! QA байерочок проверка кириллицы и заодно some English text in here...";
        string testresponse1 = "И тебе привет! QA селлерочок шлёт тебе ответ! Check it out!";

        string testtitle = "QA Automation - listing for the message test";
        string price = "200";
        string date = DateTime.Today.ToString();

        [Then(@"I enter our SellerMessage's company name and click Go")]
        public void ThenIEnterOurSellerMessageSCompanyNameAndClickGo()
            {
            WebDriver.GetByPath("//*[@id='autocomplete']").Clear();
            Thread.Sleep(500);
            WebDriver.GetByPath("//*[@id='autocomplete']").SendKeys(PartsBeeVariables.SellerMessageCompany);
            Thread.Sleep(2000);
            WebDriver.GetByPath("//div[@class='search-block input-group']/span/button").Click();
            Thread.Sleep(2000);
            }

        [Then(@"Switch to Seller tab")]
        public void ThenSwitchToSellerTab()
            {
            WebDriver.GetByPath("//span[contains(text(),'Seller')]/..").Click();
            Thread.Sleep(1000);
            }

        [Then(@"I click the SendMessage button")]
        public void ThenIClickTheSendMessageButton()
            {
            WebDriver.GetByPath("//button[contains(text(),'Send a Private Message')]").Click();
            }

        [Then(@"I enter the text message within the part and click Send")]
        public void ThenIEnterTheTextMessageWithinThePartAndClickSend()
            {
            WebDriver.GetByPath("//textarea").SendKeys(testmessage1);
            WebDriver.GetByPath("//div[@class='modal-footer']/div/button[2]").Click();
            Thread.Sleep(1000);
            }

        [Then(@"I enter the text message '(.*)' within the part and click Send")]
        public void ThenIEnterTheTextMessageWithinThePartAndClickSend(string p0)
            {
            WebDriver.GetByPath("//textarea").SendKeys(p0);
            testmessage1 = p0;
            WebDriver.GetByPath("//div[@class='modal-footer']/div/button[2]").Click();
            Thread.Sleep(1000);
            }


        [Then(@"I check that my sent message is there \(first buyer check\)")]
        public void ThenIClickThatMySentMessageIsThereFirstBuyerCheck()
            {
            string s="//table[@class='dashboard-messages']/tbody/tr[1]/td[2]";
            Assert.IsTrue(WebDriver.GetByPath(s).Text.Contains(PartsBeeVariables.CustomerFirstname));
            //Assert.IsTrue(WebDriver.GetByPath(s).Text.Contains(PartsBeeVariables.CustomerLastname));

            s = "//table[@class='dashboard-messages']/tbody/tr[1]/td[3]/b";
            Assert.IsTrue(WebDriver.GetByPath(s).Text.Contains(testtitle));

            s = "//table[@class='dashboard-messages']/tbody/tr[1]/td[3]/div";
            Assert.IsTrue(WebDriver.GetByPath(s).Text.Contains(testmessage1));

            //--------Проверим, что отправлено только сейчас и оно точно ОНО
            s = "//table[@class='dashboard-messages']/tbody/tr[1]/td[4]";
            Assert.IsTrue(WebDriver.GetByPath(s).Text.Contains("a few seconds"));
            }

        [Then(@"I log in as a MessageSeller")]
        public void ThenILogInAsAMessageSeller()
            {
            WebDriver.GetByName("Email").SendKeys(PartsBeeVariables.SellerMessageLogin);
            WebDriver.GetByName("Password").SendKeys(PartsBeeVariables.SellerMessagePassword);
            WebDriver.GetByName("submit-button").Click();
            }

        [Then(@"I should have (.*) new messages")]
        public void ThenIShouldHaveNewMessage(int p0)
            {
            Assert.IsTrue(WebDriver.GetByPath("//*[@class='list-group-item'][1]/span[1]/span/span[1]").Text.Contains(p0.ToString()));
            Assert.IsTrue(WebDriver.GetByPath("//*[@class='list-group-item'][1]/span[1]/span/span[2]").Text.Contains("New"));
            }

        [Then(@"I open my last conversation")]
        public void ThenIOpenMyLastConversation()
            {
            WebDriver.GetByPath("//table[@class='dashboard-messages']/tbody/tr[1]/td[3]/b").Click();
            }

        [Then(@"I check the title, date, price, name and message parameters from a buyer")]
        public void ThenICheckTheTitleDatePriceNameAndMessageParametersFromABuyer()
            {
            Assert.IsTrue(WebDriver.GetByPath("//*[@class='dashboard-listing-info-inner']/a/h2").Text == testtitle);
            string s = WebDriver.GetByPath("//*[@class='dashboard-listing-info-inner']/span[3]/span[2]").Text;
            //Assert.IsTrue(WebDriver.GetByPath("//*[@class='dashboard-listing-info-inner']/span[3]/span[2]").Text == date);
            Assert.IsTrue(Convert.ToDateTime(WebDriver.GetByPath("//*[@class='dashboard-listing-info-inner']/span[3]/span[2]").Text).Date
                ==DateTime.Today.Date);
            Assert.IsTrue(WebDriver.GetByPath("//*[@class='dashboard-listing-info-inner']/span[1]/span[1]/span[2]/span[2]").
                Text.Trim() == price);
            Assert.IsTrue(WebDriver.GetByPath("//*[@class='comments']/div[1]/div[2]").Text == testmessage1);
            Assert.IsTrue(WebDriver.GetByPath("//*[@class='comments']/div[1]/div[1]/b").
                Text.Contains(PartsBeeVariables.CustomerFirstname));
            Assert.IsTrue(WebDriver.GetByPath("//*[@class='comments']/div[1]/div[1]/b").
                Text.Contains(PartsBeeVariables.CustomerLastname));
            }

        [Then(@"I write me response to a message and send it")]
        public void ThenIWriteMeResponseToAMessageAndSendIt()
            {
            Assert.IsTrue(WebDriver.GetByPath("//textarea").GetAttribute("placeholder") == "Write a Reply...");
            WebDriver.GetByPath("//textarea").SendKeys(testresponse1);
            SendKeys.SendWait(@"{Enter}");
            Thread.Sleep(500);
            }

        [Then(@"I should see my latest response in realtime")]
        public void ThenIShouldSeeMyLatestResponseInRealtime()
            {
            Assert.IsTrue(WebDriver.GetByPath("//*[@class='comments']/div[2]/div[2]").Text == testresponse1);
            }

        [Then(@"I check that my sent message is there \(first seller's check\)")]
        public void ThenICheckThatMySentMessageIsThereFirstSellerSCheck()
            {
            string s = "//table[@class='dashboard-messages']/tbody/tr[1]/td[2]";
            //Assert.IsTrue(WebDriver.GetByPath(s).Text.Contains(PartsBeeVariables.CustomerFirstname));
            //Assert.IsTrue(WebDriver.GetByPath(s).Text.Contains(PartsBeeVariables.CustomerLastname));

            s = "//table[@class='dashboard-messages']/tbody/tr[1]/td[3]/b";
            Assert.IsTrue(WebDriver.GetByPath(s).Text.Contains(testtitle));

            s = "//table[@class='dashboard-messages']/tbody/tr[1]/td[3]/div";
            Assert.IsTrue(WebDriver.GetByPath(s).Text.Contains(testresponse1));

            //--------Проверим, что отправлено только сейчас и оно точно ОНО
            s = "//table[@class='dashboard-messages']/tbody/tr[1]/td[4]";
            Assert.IsTrue(WebDriver.GetByPath(s).Text.Contains("a few seconds"));
            }

        [Then(@"I check the title, date, price, name and message parameters from a seller")]
        public void ThenICheckTheTitleDatePriceNameAndMessageParametersFromASeller()
            {
            Assert.IsTrue(WebDriver.GetByPath("//*[@class='dashboard-listing-info-inner']/a/h2").Text == testtitle);
            //Assert.IsTrue(WebDriver.GetByPath("//*[@class='dashboard-listing-info-inner']/span[3]/span[2]").Text == date);
            Assert.IsTrue(Convert.ToDateTime(WebDriver.GetByPath("//*[@class='dashboard-listing-info-inner']/span[3]/span[2]").Text).Date
                == DateTime.Today.Date);
            Assert.IsTrue(WebDriver.GetByPath("//*[@class='dashboard-listing-info-inner']/span[1]/span[1]/span[2]/span[2]").
                Text.Trim() == price);
            Assert.IsTrue(WebDriver.GetByPath("//*[@class='comments']/div[2]/div[2]").Text == testresponse1);
            Assert.IsTrue(WebDriver.GetByPath("//*[@class='comments']/div[2]/div[1]/b").
                Text.Contains(PartsBeeVariables.SellerMessageFirstname));
            Assert.IsTrue(WebDriver.GetByPath("//*[@class='comments']/div[2]/div[1]/b").
                Text.Contains(PartsBeeVariables.SellerMessageLastname));
            }

        [Then(@"I delete this conversation")]
        public void ThenIDeleteThisConversation()
            {
            WebDriver.GetByPath(".//*[@id='dashboard']/div/div/div/div[2]/div[1]/a[2]").Click();
            Thread.Sleep(500);
            }

        [Then(@"I click All Messages")]
        public void ThenIClickAllMessages()
            {
            WebDriver.GetByPath(".//*[@id='dashboard']/div/div/div/div[1]/div/section/ul/a[1]/span[2]").Click();
            Thread.Sleep(500);
            }

        [Then(@"I check that my sent message is NOT there \(first buyer check\)")]
        public void ThenICheckThatMySentMessageIsNOTThereFirstBuyerCheck()
            {
            WebDriver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(3));
            try
                {
                WebDriver.GetByPath("//*[contains(text(),'Your Messages Folder is Empty')]");
                }
            catch(WebDriverException)
                {
                string s = "//table[@class='dashboard-messages']/tbody/tr[1]/td[2]";
                s = "//table[@class='dashboard-messages']/tbody/tr[1]/td[3]/div";
                Assert.IsTrue(!WebDriver.GetByPath(s).Text.Contains(testmessage1));

                //--------Проверим, что отправлено только сейчас и оно точно ОНО
                s = "//table[@class='dashboard-messages']/tbody/tr[1]/td[4]";
                Assert.IsTrue(!WebDriver.GetByPath(s).Text.Contains("a few seconds"));
                }
            WebDriver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(BaseSteps.StandardTimeOut));
            }

        [Then(@"I click the checkbox on a conversation (.*)")]
        public void ThenIClickTheCheckboxOnAConversation(int p0)
            {
            WebDriver.GetByPath(".//*[@id='dashboard']/div/div/div/div[2]/table/tbody/tr["+p0.ToString()+"]/td[1]/input").Click();
            Thread.Sleep(500);
            }

        [Then(@"I click Bulk Actions")]
        public void ThenIClickBulkActions()
            {
            WebDriver.GetByPath(".//*[@id='dashboard']/div/div/div/div[2]/div/div[1]/div/button").Click();
            Thread.Sleep(500);
            }

        [Then(@"I click Delete \(Bulk Actions\)")]
        public void ThenIClickDeleteBulkActions()
            {
            WebDriver.GetByPath(".//*[@id='dashboard']/div/div/div/div[2]/div/div[1]/div/ul/li[3]/a").Click();
            Thread.Sleep(500);
            }

        }
    }
