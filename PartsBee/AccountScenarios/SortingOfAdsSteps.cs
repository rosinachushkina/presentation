﻿using System;
using System.Threading;
using Common;
using NUnit.Framework;
using OpenQA.Selenium;
using TechTalk.SpecFlow;

namespace PartsBee.AccountScenarios
{
    [Binding]
    public class SortingOfAdsSteps : BaseSteps
    {
        [Then(@"I click on Expiration Date")]
        public void GivenIClickOnExpirationDate()
        {
            WebDriver.GetByPath("//section/table//span[contains(text(), 'Expiration')]").Click();
        }

        [Then(@"I check that ads display by descending date")]
        public void ThenICheckThatAdsDisplayByDescendingDate()
        {
            int numberOfAds = WebDriver.FindElements(By.XPath("//section/table/tbody/tr/td[4]/span")).Count;
            string firstDate;
            string nextDate;

            for (int i = 1; i <= numberOfAds-1; i++)
            {
                firstDate =
                    WebDriver.GetByPath("//section/table/tbody/tr[" + i + "]/td[4]/span").Text;
                nextDate =
                    WebDriver.GetByPath("//section/table/tbody/tr[" + (i+1) + "]/td[4]/span").Text;

                Assert.IsTrue(Convert.ToDateTime(firstDate) >= Convert.ToDateTime(nextDate));
            }


        }

        [Then(@"I check that ads display by ascending date")]
        public void ThenICheckThatAdsDisplayByAscendingDate()
        {
            Thread.Sleep(500);
            int numberOfAds = WebDriver.FindElements(By.XPath("//section/table/tbody/tr/td[4]/span")).Count;
            string firstDate;
            string nextDate;

            for (int i = 1; i <= numberOfAds - 1; i++)
            {
                firstDate =
                    WebDriver.GetByPath("//section/table/tbody/tr[" + i + "]/td[4]/span").Text;
                nextDate =
                    WebDriver.GetByPath("//section/table/tbody/tr[" + (i + 1) + "]/td[4]/span").Text;

                Assert.IsTrue(Convert.ToDateTime(firstDate) <= Convert.ToDateTime(nextDate));
            }


        }

    }
}
