﻿using System;
using System.Linq;
using System.Security.Policy;
using System.Threading;
using Common;
using Common.Generators;
using NUnit.Framework;
using TechTalk.SpecFlow;

namespace PartsBee
{
    [Binding]
    public class ForgotPasswordSteps : BaseSteps
    {
        private string currentWindow;
        public static string password = EmailGenerator.GenerateEmail();
        [Then(@"I click on link forgot password")]
        public void GivenClickOnLinkForgotPassword()
        {
            WebDriver.GetByPath("//a[contains (text(), 'Forgot password ?')]").Click();
        }
        [Then(@"I type in input email from mailinator")]
        public void ThenITypeInInputEmailTrtrtrqwertyMailinator_Com()
        {
            WebDriver.GetByPath("//input[@id='email']").SendKeys(PartsBeeVariables.PasswordRecoveryEmail);

        }

        [Then(@"I click Send button")]
        public void ThenIClickSendButton()
        {

            WebDriver.GetById("submitButton").Click();
            Thread.Sleep(2000);
        }

        [Then(@"I type email mailinator")]
        public void ThenITypeEmailMailinator()
        {
            WebDriver.GetById("inboxfield").SendKeys(PartsBeeVariables.PasswordRecoveryEmail);
            WebDriver.GetByPath("//btn[@onclick='changeInbox();']").Click();

        }

        [Then(@"I see forgot  pass letter and open it")]
        public void ThenISeeWelcomeLetterAndOpenIt()
        {
            WebDriver.GetByPath("//div[contains (text(), 'less than a minute ago')]").Click();
        }

        [Then(@"I see error about empty field")]
        public void ThenISeeErrorAboutEmptyField()
        {
            Assert.True(WebDriver.GetBy("//span[contains(text(), 'Invalid Email Address.')]").SetTimer(2000).Find().Displayed);
        }



        [Then(@"I see error message about unexisted email")]
        public void ThenISeeErrorMessageAboutUnexistedEmail()
        {
            Assert.True(WebDriver.GetBy("//span[contains(text(), 'Email is not found in the database or account is disabled.')]").SetTimer(2000).Find().Displayed);
        }

        [Then(@"I click on Reset password")]
        public void ThenIClickOnResetPasswordAndSee()
        {
            Thread.Sleep(3000);
            WebDriver.SwitchTo().Frame(WebDriver.GetByName("rendermail"));
            WebDriver.GetById("reset").Click();
            WebDriver.SwitchTo().Window(WebDriver.WindowHandles.Last());
        }

        [Then(@"I see conformation message")]
        public void ThenISeeConformationMessage()
        {
            Assert.True(WebDriver.GetBy("//p[contains(text(), 'Enter your')]").SetTimer(2000).Find().Displayed);
            Assert.True(WebDriver.GetBy("//span[contains(text(), 'New Password')]").SetTimer(2000).Find().Displayed);


        }

        [Then(@"I type new password")]
        public void ThenITypeNewPassword()
        {
            Assert.True(WebDriver.GetBy("//p[contains(text(), 'Enter your')]").SetTimer(2000).Find().Displayed);
            Assert.True(WebDriver.GetBy("//span[contains(text(), 'New Password')]").SetTimer(2000).Find().Displayed);
            WebDriver.GetById("password").SendKeys(password);
            WebDriver.GetById("resetPasswordButton").Click();
           }

        [Then(@"I see conformation message about Successful reseting")]
        public void ThenISeeConformationMessageAboutSuccessfulReseting()
        {
            Assert.True(WebDriver.GetByPath("//p[contains(text(), 'Your password has been reset')]").Enabled);
            WebDriver.GetByPath("//a[@class='btn btn-warning reset']").Click();
        }

        [Then(@"I login with new password")]
        public void ThenILoginWithNewPassword()
        {
            WebDriver.GetByName("Email").SendKeys(PartsBeeVariables.PasswordRecoveryEmail);
            WebDriver.GetByName("Password").SendKeys(password);
            WebDriver.GetByName("submit-button").Click();
        }

        [Then(@"I type in input unexisted email")]
        public void ThenITypeInInputUnexistedEmail()
        {
            WebDriver.GetByPath("//input[@id='email']").SendKeys(PartsBeeVariables.UnexistedEmail);
        }

        [Then(@"I see message about successful sending recovery email from login page")]
        public void ThenISeeMessageAboutSuccessfulSendingRecoveryEmailFromLoginPage()
        {
            Assert.True(WebDriver.GetByPath("//span[contains(text(), 'Recovery link has been sent to your email.')]").Enabled);
        }
        [Then(@"I login with new password with email after reseting password")]
        public void ThenILoginWithNewPasswordWithEmailAfterResetingPassword()
        {
            WebDriver.GetByName("Email").SendKeys(PartsBeeVariables.ResetPasswordEmail);
            WebDriver.GetByName("Password").SendKeys(password);
            WebDriver.GetByName("submit-button").Click();
        }

    }
}
