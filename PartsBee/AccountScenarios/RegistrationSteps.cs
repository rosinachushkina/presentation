﻿using System;
using System.Linq;
using System.Threading;
using Common;
using NUnit.Framework;
using TechTalk.SpecFlow;
using TechTalk.SpecFlow.Assist;
using OpenQA.Selenium;

namespace PartsBee.AccountScenarios
{
    [Binding]
    public class RegistrationSteps : BaseSteps
    {
        public static string nameValue;
        public static string email;
        public static string firstName;
        public static string lastName;
        public static string password;
        public static string address;
        public static string mailSite = "mailinator.com";
        public static string site;
        

        [Given(@"I open homepage {{(.*)}}")]
        public void GivenIOpenHomepageHttpPartsbee_Dev_Grynco_Com_UaRegistration(string website)
        {
            WebDriver.Manage().Cookies.DeleteAllCookies();
            WebDriver.Go(website);
        }

        [Then(@"enter my data to registrarion form")]
        public void GivenEnterMyDataToRegistrarionForm(Table table)
        {   
            var fields = table.CreateSet<FormField>().ToList();
            foreach (var formField in fields)
            {
                WebDriver.FillField(formField);
            }
        }
        [Then(@"I enter values in register fields")]
        public void ThenIEnterValuesInRegisterFields()
        {
            var Handy = new HandyFunctions();
            email = Handy.RandomStringLatin(6) + "@mailinator.com";
            firstName = Handy.RandomStringLatin(6);
            lastName = Handy.RandomStringLatin(6);
            password = Handy.RandomStringLatin(6);
            address = Handy.RandomStringLatin(5);

            WebDriver.GetByName("Email").SendKeys(email);
            WebDriver.GetByName("FirstName").SendKeys(firstName);
            WebDriver.GetByName("LastName").SendKeys(lastName);
            WebDriver.GetByName("Password").SendKeys(password);
            WebDriver.GetByName("Location.Address").SendKeys(address);
        }

        [Then(@"I enter values in register fields for smoke registration")]
        public void ThenIEnterValuesInRegisterFieldsForSmokeRegistration()
        {
            var Handy = new HandyFunctions();
            email = Handy.RandomStringLatin(6) + "@mailinator.com";
            firstName = Handy.RandomStringLatin(6);
            lastName = Handy.RandomStringLatin(6);
            password = Handy.RandomStringLatin(7);

            WebDriver.GetByName("Email").SendKeys(email);
            WebDriver.GetByName("FirstName").SendKeys(firstName);
            WebDriver.GetByName("LastName").SendKeys(lastName);
            WebDriver.GetByName("Password").SendKeys(password);
        }
        [Then(@"I open mail site")]
        public void ThenIOpenMailinatorSite()
        {
            WebDriver.Go(mailSite);
        }

        [Then(@"I type email in mail input")]
        public void ThenITypeEmailInMailInput()
        {
            WebDriver.GetById("inboxfield").SendKeys(email);
            WebDriver.GetByPath("//*[@class='btn btn-success']").Click();
        }

        [Then(@"I open email notification")]
        public void ThenCheckEmailNotification()
        {
            WebDriver.GetByPath("//div[contains(text(), 'Welcome to PartsBee')]").Click();
        }


        [Then(@"I check autosuggest and choose Kiev")]
        public void ThenICheckAutosuggestAndChooseKiev()
        {
            WebDriver.GetByPath("//span[@class = 'pac-matched']").Click();
        }
        [When(@"I press Registr button")]
        public void WhenIPressRegistrButton()
        {
            WebDriver.GetByName("submit-button").Click();
        }


        [Then(@"accept Term And Services")]
        public void GivenAcceptTermAndServicesAgreement()
        {
            WebDriver.GetByPath("//label/i").Click();
        }




        [Then(@"accept Privacy Policy")]
        public void ThenAcceptPrivacyPolicy()
        {
            WebDriver.GetByPath("//label[2]/i").Click();
        }

        [Then(@"I copy fields info")]
        public void ThenICopyFieldsInfo()
        {
            
            Thread.Sleep(3000);
            string emailValue =
               WebDriver.FindElement(By.XPath("//*[@id='Email']")).Text;
            nameValue =
               WebDriver.FindElement(By.XPath("//*[@id='FirstName']")).GetAttribute("value");
            string nameValue2 =
               WebDriver.FindElement(By.XPath("//*[@id='LastName']")).Text;
        }


        [Then(@"I verificate names")]
        public void ThenIVerificateNames()
        {
            Thread.Sleep(3000);
            var verificateName =
                WebDriver.FindElement(By.XPath("//*[@id='navbar-right']/div[1]/ul/li[1]/div/span/span")).Text.ToString();
        Console.Write(verificateName);
        Console.Write(nameValue);
             //Assert.True(nameValue.Equals(verificateName));
        }

        [Then(@"I verificate button My Account")]
        public void ThenIVerificateButtonMyAccount()
        {
            Assert.True(WebDriver.GetByPath("//a[contains(text(), 'My Account')]").Enabled); 
        }

        [Then(@"I click Sign In button in mail")]
        public void ThenIClickSignInButtonInMail()
        {
            WebDriver.SwitchTo().Frame(WebDriver.GetByName("rendermail"));
            WebDriver.GetByPath("//*[@alt='button']").Click();
        //    string url = WebDriver.GetByPath("//p[3]/a[2]").GetAttribute("href");
        //    WebDriver.Go(url);
            Thread.Sleep(2000);
            WebDriver.SwitchTo().Window(WebDriver.WindowHandles.Last());
            
        }
        [Then(@"I type my email address on second register page")]
        public void ThenITypeMyEmailAddressOnSecondRegisterPage()
        {
            WebDriver.GetById("Email").SendKeys(PartsBeeVariables.SocialEmail);
        }

        [Then(@"I type my last name on second register page")]
        public void ThenITypeMyLastNameOnSecondRegisterPage()
        {
         WebDriver.GetById("LastName").SendKeys(PartsBeeVariables.SellerLastname);
        }
        [Then(@"I verify my data on second register page")]
        public void ThenIVerifyMyDataOnSecondRegisterPage()
        {
            WebDriver.GetByPath("//*[@value='Verify my data']").Click();
        }
        [Then(@"I open gmail\.com")]
        public void ThenIOpenGmail_Com()
        {
            WebDriver.Url = "https://gmail.com";
        }

        [Then(@"I click Send me verification mail")]
        public void ThenIClickSendMeVerificationMail()
        {
            WebDriver.GetByPath("//input[@value='Send me verification mail']").Click();
        }

        [Then(@"I see see message about sent message")]
        public void ThenISeeSeeMessageAboutSentMessage()
        {
            Assert.True(
                WebDriver.GetByPath(
                    "//span[contains(text(), 'We`ve already sent you an email with confirmation link. Please check your mail and finish registration.')]")
                    .Enabled);
        }

        [Then(@"I type social email and submit")]
        public void ThenITypeSocialEmailAndSubmit()
        {
            WebDriver.FindElement(By.XPath("//input[@id='Email']")).SendKeys(PartsBeeVariables.SocialEmail);
            WebDriver.FindElement(By.XPath("//input[@id='Passwd']")).SendKeys(PartsBeeVariables.SocialPassword);
            WebDriver.FindElement(By.XPath("//input[@id='signIn']")).Click();
            Thread.Sleep(3000);
        }
        [Then(@"I open the last message at GMAIL\.COM")]
        public void ThenIOpenTheLastMessageAtGMAIL_COM()
        {
            WebDriver.FindElement(By.Name("alteso.test")).Click();
            Thread.Sleep(1000);
       
        }

        [Then(@"I must see phrase about confirmation")]
        public void ThenIMustSeePhraseAboutConfirmation()
        {
            Assert.True(WebDriver.GetByPath("//p[contains(text(), 'requested this confirmation link to verify your email account with PartsBee.')]").Enabled);
        }

        [Then(@"I click on verify link")]
        public void ThenIClickOnVerifyLink()
        {
            WebDriver.GetByPath("//a[contains(text(), 'Here')]").Click();
            WebDriver.SwitchTo().Window(WebDriver.WindowHandles.Last());
        }
        [Then(@"I click button that Im sure")]
        public void ThenIClickButtonThatIMSure()
        {
            WebDriver.GetByPath("//input[@value='Yes, sure']").Click();
        }
        [Then(@"I type sign in button in Gmail")]
        public void ThenITypeSignInButton()
        {
            WebDriver.GetByPath("//*[contains(text(), 'Sign in')]").Click();
        }


        }
    }

