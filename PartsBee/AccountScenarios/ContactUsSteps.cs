﻿using System;
using System.Threading;
using Common;
using NUnit.Core;
using NUnit.Framework;
using OpenQA.Selenium;
using TechTalk.SpecFlow;

namespace PartsBee.AccountScenarios
{
    [Binding]
    public class ContactUsSteps:BaseSteps
    {
        [Then(@"I click contact us page")]
        public void ThenIClickContactUsPage()
        {
            WebDriver.GetByPath("//a[contains(text(), 'CONTACT')]").Click();
        }

        [Then(@"I check that name of Buyer is displayed")]
        public void ThenICheckThatNameOfBuyerIsDisplayed()
        {
            Assert.True(WebDriver.GetById("Name").GetAttribute("value").Equals(""+PartsBeeVariables.CustomerFirstname+" "+PartsBeeVariables.CustomerLastname+""));
        }

        [Then(@"I check that email of  Buyer is displayed")]
        public void ThenICheckThatEmailOfBuerIsDisplayed()
        {
            Assert.True(WebDriver.GetById("Email").GetAttribute("value").Equals(PartsBeeVariables.CustomerLogin));
        }

        [Then(@"I check that placeholder Phone is displayed")]
        public void ThenICheckThatPlaceholderPhoneIsDisplayed()
        {
            Assert.True(WebDriver.GetById("Phone").GetAttribute("placeholder").Equals("Phone#"));
        }

        [Then(@"I check that placeholder of message to support is displayed")]
        public void ThenICheckThatPlaceholderOfMessageToSupportIsDisplayed()
        {
            Assert.True(WebDriver.GetById("content").GetAttribute("placeholder").Equals("Questions, concerns or comments..."));
        }

        [Then(@"I type test message and send it")]
        public void ThenITypeTestMessageAndSendIt()
        {
            WebDriver.GetById("content").SendKeys(PartsBeeVariables.SellerAddInfo);
            WebDriver.GetByPath("//input[@value='Send']").Click();
        }

        [Then(@"I type Phone on Contact us page")]
        public void ThenITypePhoneOnContactUsPage()
        {
           WebDriver.GetById("Phone").SendKeys(PartsBeeVariables.CustomerPhone);
        }

        [Then(@"I see message about successful sending message to support")]
        public void ThenISeeMessageAboutSuccessfulSendingMessageToSupport()
        {
            Assert.True(WebDriver.GetByPath("//div[contains(text(), 'Your message has been sent. Thank you for your feedback!')]").Enabled);
        }
        [Then(@"I type support email and submit")]
        public void ThenITypeSupportEmailAndSubmit()
        {
            WebDriver.FindElement(By.XPath("//input[@id='Email']")).SendKeys(PartsBeeVariables.ContactUsSupportEmail);
            WebDriver.FindElement(By.XPath("//input[@id='Passwd']")).SendKeys(PartsBeeVariables.ContacUsSupportPassword);
            WebDriver.FindElement(By.XPath("//input[@id='signIn']")).Click();
            Thread.Sleep(3000);
        }

        [Then(@"I open email from buyer on support mail")]
        public void ThenIOpenEmailFromBuyerOnSupportMail()
        {
            WebDriver.GetByPath("//span/b[contains(text(), 'New Letter from "+PartsBeeVariables.CustomerFirstname+" "+PartsBeeVariables.CustomerLastname+"')]").Click();
        }

        [Then(@"I check email of Buyer in mail")]
        public void ThenICheckEmailOfBuyerInMail()
        {
            Assert.True(WebDriver.GetByPath("//div[2]/div[6]/div/div[1]/a").Text.Equals(PartsBeeVariables.CustomerLogin));
        }

        [Then(@"I check Phone of Buyer in mail")]
        public void ThenICheckPhoneOfBuyerInMail()
        {
            ScenarioContext.Current.Pending();
        }

        [Then(@"I check message of Buyer in mail")]
        public void ThenICheckMessageOfBuerInMail()
        {
            Assert.True(WebDriver.GetByPath("//span[contains(text(),'Phone: "+PartsBeeVariables.CustomerPhone+" "+PartsBeeVariables.SellerAddInfo+"')]").Enabled);
        }
        [Then(@"I delete message from gmail")]
        public void ThenIDeleteMessageFromGmail()
        {
            WebDriver.GetByPath("//div[2]/div/div[2]/div[1]/div[2]/div/div/div/div[1]/div[2]/div[1]/div/div[2]/div[3]/div/div").Click();
        }

    }
}
