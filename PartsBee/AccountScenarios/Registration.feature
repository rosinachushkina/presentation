﻿@PartsBee_Registration
Feature: Registration
	In order to use PartsBee
	As a customer
	I want to registr


	Background: 
	Given I open register page

@positiveRegistration
Scenario: Registration as a new user with 6 symbols in password

	Then I enter values in register fields
	Then accept Term And Services
	Then accept Privacy Policy
	When I press Registr button
	Then I redirect to main page
	Then I verificate button My Account
	Then I open mail site
	Then I type email in mail input
	Then I open email notification
	Then I click Sign In button in mail
	Then I redirect to main page

@positiveRegistration2
Scenario: Registration as a new user without address with 7 symbols in password
	Then I enter values in register fields for smoke registration
	Then accept Term And Services
	Then accept Privacy Policy
	When I press Registr button
	Then I redirect to main page

@socialRegistrationByaGmail
Scenario: Registration as a new user using A Gmail account
	Then accept Term And Services
	Then accept Privacy Policy
	Then I click on google social icon
	Then I fill information for google pop-up and sumbit
	Then I redirect to main page

@socialRegistrationByTwitter
Scenario: Registration as a new user using Twitter account
	Then accept Term And Services
	Then accept Privacy Policy
	Then I click on twitter social icon
	Then I fill information for twitter pop-up and sumbit
	Then I type my email address on second register page
	Then I type my last name on second register page
	Then I verify my data on second register page
	Then I click Send me verification mail
	Then I see see message about sent message
	Then I open gmail.com
	Then I type sign in button in Gmail
	Then I type social email and submit
	Then I open the last message at GMAIL.COM
	Then I must see phrase about confirmation
	Then I click on verify link
	Then I click button that Im sure
	Then I redirect to main page

@socialRegistrationByFacebook
Scenario: Registration as a new user using Facebook account
	Then accept Term And Services
	Then accept Privacy Policy
	Then I click on facebook social icon
	Then I fill information for facebook pop-up and sumbit
	Then I type my email address on second register page
	Then I type my last name on second register page
	Then I verify my data on second register page
	Then I click Send me verification mail
	Then I see see message about sent message
	Then I open gmail.com
	Then I type sign in button in Gmail
	Then I type social email and submit
	Then I open the last message at GMAIL.COM
	Then I must see phrase about confirmation
	Then I click on verify link
	Then I click button that Im sure
	Then I redirect to main page