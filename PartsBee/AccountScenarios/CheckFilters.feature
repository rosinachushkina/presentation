﻿Feature: CheckFilters
	In order to check working Filters
	As Anonymous

	Background: 
	Given I open main page
	Then I go to search page

@partTypeCheckFilters
Scenario: I check Part Type filters
	Then I check Part Type filters

@conditionsCheck
Scenario: I check Condition filters
	Then I check Condition filters

@shippingPolicyFilters
	Scenario: I check shippingPolicyFilters
	Then I check Shipping Policy filters

@PaymentPolicyFilters
	Scenario: I check PaymentPolicyFilters
	Then I check Payment Policy filters

@ReturnPolicyFilters
	Scenario: I check ReturnPolicyFilters
	Then I check Return Policy filters

@CategoryFilters
	Scenario: I check CategoryFilters
	Then I switch to List Date: Oldest First
	Then I check category filters using 'Car' > 'Cooling' > 'Blower Motor' categories

@PlacementFilters
	Scenario: I check PlacementFilters
	Then I check placement filters
	
@ColorFilters
	Scenario: I check ColorFilters
	Then I check color filters using 'LIGHT BLUE', 'BLACK', 'ORANGE'

@MakeFilters
	Scenario: I check MakeFilters
	Then I check Make filters using 'Lincoln'

@PartsWithVINFilters
	Scenario: I check PartsWithVINFilters
	Then I check PartsWithVINFilters filters

@ResetFiltersButton
    Scenario: I check the Reset filters button
	Then I remember 1 st part title
	Then I set a category to 'Car' > 'Cooling' > 'Blower Motor'
	Then I click the Reset button
	Then The inventory which was added to my watchlist should be there (I check the title)

@HideNegoriableFilters
	Scenario: I check HideNegoriableFilters
	Given I open login page
	Then I login as Seller

	Then I press Sell Your part Button
    Then I click ListSinglePart from a main dropdown
	Then I check whether I am on the drafts page and go to single listing if I am there
    Then I click on list of Category
    And choose first and second and third items and sumbit
    Then I fill description form default values at listing page (with random title)
    Then enter price and quantity
    When I click on Active listing button
    Then I am redirected to a listing page

	Then I go to search page
	Then I switch to My Listings
	Then I click the HideNegotiable filter checkbox
	Then I open the first listing
	Then this listing should not be marked as PriceNegotiable
	Then I delete my listing within the listing itself

@FilterInterface
    Scenario: I check FilterInterface
	Then I check that categories are there
	Then I collapse Categories
	Then I check that categories are hidden
	Then I refresh the page
	Then I check that categories are hidden
	Then I expand categories
	Then I check that categories are there

	Then I check that Part Types are there
	Then I collapse Part Types
	Then I check that Part Types are hidden
	Then I refresh the page
	Then I check that Part Types are hidden
	Then I expand Part Types
	Then I check that Part Types are there

	Then I check that NegotiablePrice is there
	Then I collapse NegotiablePrice
	Then I check that NegotiablePrice is hidden
	Then I refresh the page
	Then I check that NegotiablePrice is hidden
	Then I expand NegotiablePrice
	Then I check that NegotiablePrice is there

	Then I check that Condition is there
	Then I collapse Condition
	Then I check that Condition is hidden
	Then I refresh the page
	Then I check that Condition is hidden
	Then I expand Condition
	Then I check that Condition is there

	Then I check that Placement is there
	Then I collapse Placement
	Then I check that Placement is hidden
	Then I refresh the page
	Then I check that Placement is hidden
	Then I expand Placement
	Then I check that Placement is there

	Then I check that Colors are there
	Then I collapse Colors
	Then I check that Colors are hidden
	Then I refresh the page
	Then I check that Colors are hidden
	Then I expand Colors
	Then I check that Colors are there

	Then I check that Make is there
	Then I collapse Make
	Then I check that Make is hidden
	Then I refresh the page
	Then I check that Make is hidden
	Then I expand Make
	Then I check that Make is there

	Then I check that PaymentPolicy is there
	Then I collapse PaymentPolicy
	Then I check that PaymentPolicy is hidden
	Then I refresh the page
	Then I check that PaymentPolicy is hidden
	Then I expand PaymentPolicy
	Then I check that PaymentPolicy is there

	Then I check that ShippingPolicy is there
	Then I collapse ShippingPolicy
	Then I check that ShippingPolicy is hidden
	Then I refresh the page
	Then I check that ShippingPolicy is hidden
	Then I expand ShippingPolicy
	Then I check that ShippingPolicy is there

	Then I check that ReturnPolicy is there
	Then I collapse ReturnPolicy
	Then I check that ReturnPolicy is hidden
	Then I refresh the page
	Then I check that ReturnPolicy is hidden
	Then I expand ReturnPolicy
	Then I check that ReturnPolicy is there

	Then I check that ListingDate is there
	Then I collapse ListingDate
	Then I check that ListingDate is hidden
	Then I refresh the page
	Then I check that ListingDate is hidden
	Then I expand ListingDate
	Then I check that ListingDate is there

	Then I check that ShowOnly is there
	Then I collapse ShowOnly
	Then I check that ShowOnly is hidden
	Then I refresh the page
	Then I check that ShowOnly is hidden
	Then I expand ShowOnly
	Then I check that ShowOnly is there

	Then I check that Price is there
	Then I collapse Price
	Then I check that Price is hidden
	Then I refresh the page
	Then I check that Price is hidden
	Then I expand Price
	Then I check that Price is there

	Then I collapse All filters
	Then I check that categories are hidden
	Then I check that Part Types are hidden
	Then I check that NegotiablePrice is hidden
	Then I check that Condition is hidden
	Then I check that Placement is hidden
	Then I check that Colors are hidden
	Then I check that Make is hidden
	Then I check that PaymentPolicy is hidden
	Then I check that ShippingPolicy is hidden
	Then I check that ReturnPolicy is hidden
	Then I check that ListingDate is hidden
	Then I check that ShowOnly is hidden
	Then I check that Price is hidden
	
	Then I close the whole filters panel
	Then I check that the filters panel is closed
	Then I reopen the filters panel
	Then I check that the filters panel is opened