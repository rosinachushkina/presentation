﻿using System;
using System.Data;
using System.Globalization;
using System.Threading;
using Common;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using TechTalk.SpecFlow;

namespace PartsBee.AccountScenarios
{
    [Binding]
    public class SortingBySteps : BaseSteps
    {
        private DateTime date1;
        private DateTime date2;
        private int k = 0;

        [When(@"I choose sorting from Oldest")]
        public void WhenIChooseSortingFromOldest()
        {
            WebDriver.GetByPath("//section/div[1]/div[3]/div/div/div/div").Click();
            WebDriver.GetByPath("//a[contains(text(), 'List Date: Oldest First')]").Click();
            Thread.Sleep(2000);
        }

        [When(@"I choose sorting from Newest")]
        public void WhenIChooseSortingFromNewest()
        {
            WebDriver.GetByPath("//section/div[1]/div[3]/div/div/div/div").Click();
            WebDriver.GetByPath("//a[contains(text(), 'List Date: Newest First')]").Click();
            Thread.Sleep(2000);
        }

        [When(@"I choose sorting from Highest")]
        public void WhenIChooseSortingFromHighest()
        {
            WebDriver.GetByPath("//section/div[1]/div[3]/div/div/div/div").Click();
            WebDriver.GetByPath("//a[contains(text(), 'Price: Highest First')]").Click();
            Thread.Sleep(2000);
        }

        [When(@"I choose sorting from Lowest")]
        public void WhenIChooseSortingFromLowest()
        {
            WebDriver.GetByPath("//section/div[1]/div[3]/div/div/div/div").Click();
            WebDriver.GetByPath("//div/div/ul/li/a[contains(text(), 'Price: Lowest First')]").Click();
            Thread.Sleep(2000);
        }


        [Then(@"I check that first inventory more expensive than second")]
        public void ThenICheckThatFirstInventoryMoreExpensiveThanSecond()
        {
            double firstPrice = 0;
            double secondPrice = 0;
            int i = 2;
            while (firstPrice.Equals(secondPrice))
            {
                string s = "//section[" + i + "]/div[2]/span";
                firstPrice =
                    Convert.ToDouble(HandyFunctions.TrimToDigitsAndSeparators(WebDriver.GetByPath(s).Text));

                s = "//section[" + (i + 1) + "]/div[2]/span";
                secondPrice =
                    Convert.ToDouble(HandyFunctions.TrimToDigitsAndSeparators(WebDriver.GetByPath(s).Text));

                if (secondPrice > firstPrice)
                    Assert.Fail("First price is " + firstPrice + ", second is " + secondPrice + "");
                i++;
            }
        }

        [Then(@"I check that first inventory less expensive than second")]
        public void ThenICheckThatFirstInventoryLessExpensiveThanSecond()
        {

            double firstPrice = 0;
            double secondPrice = 0;
            int i = 2;

            while (firstPrice.Equals(secondPrice))
            {
                string s = "//section[" + i + "]/div[2]/span";
                firstPrice =
                    Convert.ToDouble(HandyFunctions.TrimToDigitsAndSeparators(WebDriver.GetByPath(s).Text));

                s = "//section[" + (i + 1) + "]/div[2]/span";
                secondPrice =
                    Convert.ToDouble(HandyFunctions.TrimToDigitsAndSeparators(WebDriver.GetByPath(s).Text));

                if (secondPrice < firstPrice)
                    Assert.Fail("First price is " + firstPrice + ", second is " + secondPrice + "");
                i++;
            }
        }

        [Then(@"I click on filter which hide negotiable price")]
        public void ThenIClickOnFilterWhichHideNegotiablePrice()
        {
            WebDriver.GetByPath("//*[@name='NegotiableFilter_']/following-sibling::i").Click();
            Thread.Sleep(1500);
        }

        [Then(@"I save date")]
        public void ThenISaveDate()
        {
            string date =
            WebDriver.GetByPath("//section[1]/div/div/div[3]/table/tbody/tr[1]/td").Text;

            date1 = Convert.ToDateTime(date);
            WebDriver.Navigate().Back();
        }
        [Then(@"I save second date")]
        public void ThenISaveSecondDate()
        {
            string date = WebDriver.GetByPath("//section[1]/div/div/div[3]/table/tbody/tr[1]/td").Text;

            date2 = Convert.ToDateTime(date);
        }

        [Then(@"I compare dates")]
        public void ThenICompareDates()
        {

            k = DateTime.Compare(date1, date2);

            Assert.True(k == 1);
        }

        [Then(@"I check that dates of listing from newest to oldest")]
        public void ThenICheckThatDatesOfListingFromNewestToOldest()
        {
            int i = 1;
            while (k == 0)
            {

                Thread.Sleep(1000);
                WebDriver.GetByPath("//section["+i+"]/div[1]").Click();
                WebDriver.GetByPath("//section["+i+"]/div[1]/div/img").Click();
                WebDriver.GetByPath("//section["+i+"]//a[@class='btn btn-default']").Click();

                string date = WebDriver.GetByPath("//section[1]/div/div/div[3]/table/tbody/tr[1]/td").Text;
                date1 = Convert.ToDateTime(date);
                WebDriver.Navigate().Back();

                WebDriver.GetByPath("//section[" + (i+1) + "]/div[1]").Click();
                WebDriver.GetByPath("//section[" + (i+1) + "]/div[1]/div/img").Click();
                WebDriver.GetByPath("//section[" + (i+1) + "]//a[@class='btn btn-default']").Click();

                date = WebDriver.GetByPath("//section[1]/div/div/div[3]/table/tbody/tr[1]/td").Text;
                date2 = Convert.ToDateTime(date);
                WebDriver.Navigate().Back();

                k = DateTime.Compare(date1, date2);
                if (k == -1)
                    Assert.Fail("First date is " + date1 + ", second is " + date2 + "");
                i++;

            }
        }



    }
}
