﻿Feature: SortingBy
	In order to use PartsBee
	As a Seller
	I want to check different Sorting

	Background: 
	Given I open search page

	@sortingByPriceHight
	Scenario: Sorting from Highest Price
	When I choose sorting from Highest
	Then I check that first inventory more expensive than second

	@SortingByPriceLow
	Scenario: Sorting from Lowest Price
	When I choose sorting from Lowest 
	Then I click on filter which hide negotiable price
	Then I check that first inventory less expensive than second

	@SortingByDateNewest
	Scenario: Sorting from Newest Price
	When I choose sorting from Newest
	Then I check that dates of listing from newest to oldest