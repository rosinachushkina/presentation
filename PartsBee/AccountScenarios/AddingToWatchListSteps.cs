﻿using System;
using System.Threading;
using Common;
using NUnit.Framework;
using OpenQA.Selenium;
using TechTalk.SpecFlow;

namespace PartsBee
    {
    [Binding]
    public class WatchListSteps : BaseSteps
        {

        public static string SelectedTitle = "";

        [Then(@"I add the (.*) st part to my watchlist")]
        public void ThenIAddTheStPartToMyWatchlist(int p0)
            {
            Thread.Sleep(2000);
            WebDriver.GetByPath("//*[@class='resalts-listing-view pull-left']/div/section[" + p0 + "]/div[2]/button").
                Click();
            }

        [Then(@"I remember (.*) st part title")]
        public void ThenIRememberStPartTitle(int p0)
            {
            SelectedTitle = WebDriver.GetByPath("//*[@class='resalts-listing-view pull-left']/div/section[" + p0 + "]/div[1]/h2").
                Text;

            }

        [Then(@"I click on icon My Watchlist")]
        public void ThenIClickOnIconMyWatchlist()
            {
            WebDriver.GetByPath("//div[@class='user-filters pull-left']//*[contains(text(),'My Watchlist')]").Click();
            }

        [Then(@"The inventory which was added to my watchlist should be there \(I check the title\)")]
        public void ThenTheInventoryWhichWasAddedToMyWatchlistShouldBeThereICheckTheTitle()
            {
            Assert.IsTrue(WebDriver.GetByPath("//*[@class='resalts-listing-view pull-left']/div/section[1]/div[1]/h2").
                Text.ToUpper() == SelectedTitle.ToUpper());
            }

        [Then(@"I remove the (.*) st inventory from the watchlist")]
        public void ThenIRemoveTheStInventoryFromTheWatchlist(int p0)
            {
            Thread.Sleep(500);
            WebDriver.GetByPath("//*[@class='resalts-listing-view pull-left']/div/section[" + p0 + "]/div[2]/button").
                Click();
            Thread.Sleep(500);
            }

        [Then(@"I refresh the page")]
        public void ThenIRefreshThePage()
            {
            WebDriver.Url = WebDriver.Url;
            }

        [Then(@"My watchlist should be empty")]
        public void ThenMyWatchlistShouldBeEmpty()
            {
            try
                {
                WebDriver.GetByPath("//div[contains(text(),'No items found for this search criteria')]");
                }
            catch (WebDriverException)
                {
                Assert.Fail("Most likely something is still present in our Watchlist!(((");
                }
            }

        [Then(@"I switch to a List view mode")]
        public void ThenISwitchToAListViewMode()
            {
            WebDriver.GetByPath("//span[@class='results-view-icons pull-right']/i[2]").Click();
            }

        [Then(@"I add the (.*) st part to my watchlist \(List View\)")]
        public void ThenIAddTheStPartToMyWatchlistListView(int p0)
            {
            WebDriver.GetByPath("//*[@class='resalts-listing-view pull-left']/div[1]/section[" + p0 + "]/div[2]/button").Click();
            }

        [Then(@"I remember (.*) st part title \(List View\)")]
        public void ThenIRememberStPartTitleListView(int p0)
            {
            string s = "//*[@class='resalts-listing-view pull-left']/div[1]/section[" + p0 + "]/div[1]/header/h2";
            SelectedTitle = WebDriver.GetByPath(s).Text;
            }

        [Then(@"The inventory which was added to my watchlist should be there \(I check the title\)\(ListMode\)")]
        public void ThenTheInventoryWhichWasAddedToMyWatchlistShouldBeThereICheckTheTitleListMode()
            {
            string s = "//*[@class='resalts-listing-view pull-left']/div[1]/section[1]/div[1]/header/h2";
            Assert.IsTrue(SelectedTitle == WebDriver.GetByPath(s).Text);
            }

        [Then(@"I remove the (.*) st inventory from the watchlist \(ListMode\)")]
        public void ThenIRemoveTheStInventoryFromTheWatchlistListMode(int p0)
            {
            Thread.Sleep(500);
            WebDriver.GetByPath("//*[@class='resalts-listing-view pull-left']/div[1]/section[" + p0 + "]/div[2]/button").Click();
            Thread.Sleep(500);
            }

        [Then(@"I switch to Map view mode")]
        public void ThenISwitchToMapViewMode()
            {
            WebDriver.GetByPath("//span[@class='results-view-icons pull-right']/i[3]").Click();
            Thread.Sleep(500);
            }

        [Then(@"I select an item from the map")]
        public void ThenISelectAnItemFromTheMap()
            {
            Thread.Sleep(3000);
            var error = false;

            CHECKTITLE:
            WebDriver.GetByPath("//*[@class='map-marker-label'][1]").Click();
            Thread.Sleep(2000);

            try
                {
                WebDriver.GetByPath("//*[@class='map-info-window listing-state-active']/div[2]/h2");
                }
            catch (WebDriverException)
                {
                if (error == true)
                    Assert.Fail("Cannot open the item from the map!((");
                error = true;
                goto CHECKTITLE;
                }
            }

        [Then(@"I remember its title \(map\)")]
        public void ThenIRememberItsTitleMap()
            {
            SelectedTitle = WebDriver.GetByPath("//*[@class='map-info-window listing-state-active']/div[2]/h2").Text;
            }

        [Then(@"I add the item to my watchlist \(map\)")]
        public void ThenIAddTheItemToMyWatchlistMap()
            {
            WebDriver.GetByPath("//*[@class='map-info-window listing-state-active']/div[2]/div[2]/button").Click();
            Thread.Sleep(500);
            }

        [Then(@"The inventory which was added to my watchlist should be there \(I check the title\)\(MapMode\)")]
        public void ThenTheInventoryWhichWasAddedToMyWatchlistShouldBeThereICheckTheTitleMapMode()
            {
            string s = "//*[@class='results-gallery-item clearfix']/h2";
            Assert.IsTrue(SelectedTitle == WebDriver.GetByPath(s).Text);
            }

        [Then(@"I remove the (.*) st inventory from the watchlist \(MapMode\)")]
        public void ThenIRemoveTheStInventoryFromTheWatchlistMapMode(int p0)
            {
            Thread.Sleep(500);
            WebDriver.GetByPath("//*[@class='results-gallery-item clearfix']//button").Click();
            Thread.Sleep(500);
            }

        [Then(@"My watchlist should be empty \(map\)")]
        public void ThenMyWatchlistShouldBeEmptyMap()
            {
            string s = "//div[@class='search-results-panel']//header/span";
            Assert.IsTrue(WebDriver.GetByPath(s).Text.Trim() == "(0)");
            }

        [Then(@"I remember this listing's Title")]
        public void ThenIRememberThisListingSTitle()
            {
            SelectedTitle = WebDriver.GetByPath("//section[@class='listing-brief-info']/div/div/div[3]/header/h2").Text;
            }

        [Then(@"I add this listing to my watchlist")]
        public void ThenIAddThisListingToMyWatchlist()
            {
            WebDriver.GetByPath("//div[@class='listing-brief-info-actions']/button[2]").Click();
            }

        [Then(@"I click on icon My Watchlist \(within the item\)")]
        public void ThenIClickOnIconMyWatchlistWithinTheItem()
            {
            WebDriver.GetByPath("//span[contains(text(),'My Watchlist')]").Click();
            }




        }
    }
