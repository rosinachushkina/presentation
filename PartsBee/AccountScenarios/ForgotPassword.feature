﻿Feature: ForgotPassword
	In order to use ParstBee
	As a Anonymous
	I want to check function "Forgot Password"

	Background: 
		Given I open login page
		Then I click on link forgot password


@ForgotPassPositive
Scenario: sending link on email
	Then I type in input email from mailinator
	Then I click Send button
	Then I see message about successful sending recovery email from login page
	Then I open mail site
	Then I type email mailinator
	Then I see forgot  pass letter and open it
	Then I click on Reset password
	Then I type new password 
	Then I see conformation message about Successful reseting
	Then I login with new password
	Then I redirect to main page

	@emptyField
	Scenario: checking empty field sending
	Then I click Send button
	Then I see error about empty field

	@unexistedEmail
	Scenario: type unexisted email
	Then I type in input unexisted email
	Then I click Send button
	Then I see error message about unexisted email


