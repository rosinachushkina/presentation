﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading;
using Common;
using Common.Generators;
using NUnit.Framework;
using OpenQA.Selenium;
using TechTalk.SpecFlow;

namespace PartsBee.AccountScenarios
{
    [Binding]
    public class UpdateUserProfileSteps : BaseSteps
    {

        private string oldPass;
        private string newPass;

        string fname, lname, addr, email;

        [Then(@"I am redirected to a user profile info page {{(.*)}}")]
        public void ThenIAmRedirectedToAUserProfileInfoPage(string expectedDestination)
        {
            Assert.True(expectedDestination.Equals(WebDriver.GetCurrentUrl(),
            StringComparison.InvariantCultureIgnoreCase));
        }

        [Then(@"I click on User Profile Settings button")]
        public void ThenIClickOnUserProfileSettingsButton()
        {
           WebDriver.GetByPath("//a[@class='btn btn-link dropdown-toggle dropdown-button-wrapper']").Click();
        }

        [Then(@"I click in Drop Menu My Account")]
        public void ThenIClickInDropMenuMyAccount()
        {
            WebDriver.GetByPath("//a[contains(text(), 'My Account')]").Click();
        }

        
        [Then(@"I click on User Profile Info button")]
        public void ThenIClickOnUserProfileInfoButton()
        {
            WebDriver.GetByPath("//span[contains(text(), 'User Profile')]").Click();
        }

        [When(@"I enter password {{(.*)}} to old password input and new password {{(.*)}} to new password input and Save")]
        public void ThenIEnterPasswordToOldPasswordInputAndNewPasswordToNewPasswordInputAndSave(string oldPass, string newPass)
        {
            WebDriver.GetByName("oldPassword").Clear();
            WebDriver.GetByName("oldPassword").SendKeys(oldPass);
            WebDriver.GetByName("newPassword").Clear();
            WebDriver.GetByName("newPassword").SendKeys(newPass);

            this.oldPass = oldPass;
            this.newPass = newPass;

            WebDriver.GetByPath("//input[@value='Save']").Click();
        }

        [Then(@"I accept allert with info about successful chanhing")]
        public void ThenIAcceptAllertWithInfoAboutSuccessfulChanhing()
        {
            Assert.True(WebDriver.GetBy("//b[contains(text(), 'Profile was saved')]").SetTimer(2000).Find().Displayed);


           /* var passAlert = WebDriver.SwitchTo().Alert();
            Assert.True(passAlert.Text.Equals("Profile saved"));
            passAlert.Accept(); */

        }

        [Then(@"I return old password as a main")]
        public void ThenIReturnOldPasswordAsAMain()
        {
            WebDriver.GetByName("oldPassword").SendKeys(newPass);
            WebDriver.GetByName("newPassword").SendKeys(oldPass);
            WebDriver.GetByPath("//input[@value='Save']").Click();
            var Alert = WebDriver.SwitchTo().Alert();
            Alert.Accept();
        }


        [When(@"I enter new first name, last name and click Save")]
        public void WhenIEnterNewFirstNameNameLastNameLastnameAndClickSave()
        {
            var firstName = NamesGenerator.GetFirstName();
            var lastName = NamesGenerator.GetLastName();

            WebDriver.GetByName("firstName").Clear();
            WebDriver.GetByName("firstName").SendKeys(firstName);

            WebDriver.GetByName("lastName").Clear();
            WebDriver.GetByName("lastName").SendKeys(lastName);

            WebDriver.GetByPath("//button[contains(text(), 'SAVE')]").Click();

            //Вынести в отдельный метод
            //Assert.True(WebDriver.GetByName("firstName").Text.Equals(firstName));
            //Assert.True(WebDriver.GetByName("lastName").Text.Equals(lastName));

        }


        [When(@"I enter new address and pick first and Save")]
        public void WhenIEnterNewAddressAndPickFirstAndSave()
        {
            WebDriver.GetByName("address").Clear();
            WebDriver.GetByName("address").SendKeys(PartsBeeVariables.SellerUserAddress);

            WebDriver.GetByPath("//span[@class = 'pac-matched']").Click();

            WebDriver.GetByPath("//button[contains(text(), 'SAVE')]").Click();
        }

        [When(@"I enter new email and click Save")]
        public void WhenIEnterNewEmailAmdClickSave()
        {
            WebDriver.GetByName("email").Clear();
            WebDriver.GetByName("email").SendKeys(EmailGenerator.GenerateEmail());

            WebDriver.GetByPath("//button[contains(text(), 'SAVE')]").Click();
        }

        [Then(@"I return old email")]
        public void ThenIReturnOldEmail()
        {
            WebDriver.GetByName("email").Clear();
            WebDriver.GetByName("email").SendKeys("testuser@4.com");

            WebDriver.GetByPath("//button[contains(text(), 'SAVE')]").Click();
            Thread.Sleep(1000);
            Assert.True(WebDriver.GetBy("//b[contains(text(), 'Profile was saved')]").SetTimer(2000).Find().Displayed);
           
            /* var Alert = WebDriver.SwitchTo().Alert();
            Alert.Accept(); */
        }

        [When(@"I clean all inputs and click Save")]
        public void WhenICleanAllInputsAndClickSave()
        {
            WebDriver.GetByName("email").Clear();
            WebDriver.GetByName("lastName").Clear();
            WebDriver.GetByName("firstName").Clear();

            WebDriver.GetByPath("//button[contains(text(), 'SAVE')]").Click();

        }

        [Then(@"I recieved {{(.*)}} error notification")]
        public void ThenIRecievedErrorNotification(int errorNumbers)
        {
            var errorNotification = WebDriver.FindElements(By.XPath("//li[contains(text(), 'This value is required.')]"));
            Assert.True(errorNotification.Count == errorNumbers);
        }

        [When(@"I enter used email {{(.*)}} and click Save")]
        public void WhenIEnterUsedEmailAndClickSave(string usedEmail)
        {
            WebDriver.GetByName("email").Clear();
            WebDriver.GetByName("email").SendKeys(usedEmail);

            WebDriver.GetByPath("//button[contains(text(), 'SAVE')]").Click();
        }

        [Then(@"I accept allert about used error")]
        public void ThenIAcceptAllertAboutUsedError()
        {

            Thread.Sleep(1000);
            Assert.True(WebDriver.GetBy("//b[contains(text(), 'Email is in use')]").SetTimer(2000).Find().Displayed);

           /*var alert = WebDriver.SwitchTo().Alert();
            Assert.True(alert.Text.Equals("Email in use"));
            alert.Accept();*/
        }

        [When(@"I click on dimensions changer and save it")]
        public void WhenIClickOnDimensionsChanger()
        {
            WebDriver.GetByPath("//label[@class='pull-right']").Click();
            WebDriver.GetByPath("//button[contains(text(), 'SAVE')]").Click();
            Assert.True(WebDriver.GetBy("//b[contains(text(), 'Profile was saved')]").SetTimer(2000).Find().Displayed);
            }


        [Then(@"I click on Additional option and check US Metric")]
        public void ThenIClickOnAdditionalOptionAndCheckUSMetric()
        {
            WebDriver.GetByPath("//input[@value='Edit']").Click();
        }




        [Then(@"I see US metric")]
        public bool ThenISeeUSMetric()
        {
            return WebDriver.GetByPath("//label[@class='pull-right']").Selected;
        }

        [Then(@"I change firstname, lastname,address and email")]
        public void ThenIChangeFirstnameLastnameAddressAndEmail()
            {
            var h = new HandyFunctions();
            fname = h.RandomStringLatin(10);
            WebDriver.GetByPath("//input[@name='firstName']").Clear();
            WebDriver.GetByPath("//input[@name='firstName']").SendKeys(fname);
            lname = h.RandomStringLatin(10);
            WebDriver.GetByPath("//input[@name='lastName']").Clear();
            WebDriver.GetByPath("//input[@name='lastName']").SendKeys(lname);
            addr = h.RandomStringLatinAndNumbers(25);
            WebDriver.GetByPath("//input[@name='address']").Clear();
            WebDriver.GetByPath("//input[@name='address']").SendKeys(addr);
            email = h.RandomStringLatinAndNumbers(8)+"@test.tsbua.com";
            WebDriver.GetByPath("//input[@name='email']").Clear();
            WebDriver.GetByPath("//input[@name='email']").SendKeys(email);
            }

        [Then(@"I check that user info was changed to its new values")]
        public void ThenICheckThatUserInfoWasChangedToItsNewValues()
            {
            Assert.IsTrue(
            WebDriver.GetByPath("//input[@name='firstName']").GetAttribute("value") == fname &&
            WebDriver.GetByPath("//input[@name='lastName']").GetAttribute("value") == lname &&
            WebDriver.GetByPath("//input[@name='address']").GetAttribute("value") == addr &&
            WebDriver.GetByPath("//input[@name='email']").GetAttribute("value") == email
            );
            }

        [Then(@"I roll back all user info to defaults")]
        public void ThenIRollBackAllUserInfoToDefaults()
            {
            WebDriver.GetByPath("//input[@name='firstName']").Clear();
            WebDriver.GetByPath("//input[@name='firstName']").SendKeys(PartsBeeVariables.CustomerFirstname);
            WebDriver.GetByPath("//input[@name='lastName']").Clear();
            WebDriver.GetByPath("//input[@name='lastName']").SendKeys(PartsBeeVariables.CustomerLastname);
            WebDriver.GetByPath("//input[@name='address']").Clear();
            WebDriver.GetByPath("//input[@name='address']").SendKeys(PartsBeeVariables.CustomerAddress);
            WebDriver.GetByPath("//input[@name='email']").Clear();
            WebDriver.GetByPath("//input[@name='email']").SendKeys(PartsBeeVariables.CustomerLogin);
            }

        [Then(@"I check that user info rolled back successfully")]
        public void ThenICheckThatUserInfoRolledBackSuccessfully()
            {
            Assert.IsTrue(
           WebDriver.GetByPath("//input[@name='firstName']").GetAttribute("value") == PartsBeeVariables.CustomerFirstname &&
           WebDriver.GetByPath("//input[@name='lastName']").GetAttribute("value") == PartsBeeVariables.CustomerLastname &&
           WebDriver.GetByPath("//input[@name='address']").GetAttribute("value") == PartsBeeVariables.CustomerAddress &&
           WebDriver.GetByPath("//input[@name='email']").GetAttribute("value") == PartsBeeVariables.CustomerLogin 
           );
            }



        }



    }



