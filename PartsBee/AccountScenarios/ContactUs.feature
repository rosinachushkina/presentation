﻿Feature: ContactUs
	In order to check sending message to support
	I as a Customer
	Send it from contact us page

@CheckContactUsPage
Scenario: Checking Contact us page
	Given I open login page
	Then I login as Buyer
	Then I click contact us page
	Then I check that name of Buyer is displayed
	Then I check that email of  Buyer is displayed
	Then I check that placeholder Phone is displayed
	Then I check that placeholder of message to support is displayed
	Then I type Phone on Contact us page
	Then I type test message and send it
	Then I see message about successful sending message to support
	Then I open gmail.com
	Then I type sign in button in Gmail
	Then I type support email and submit
	Then I open email from buyer on support mail
	Then I check email of Buyer in mail
	Then I check message of Buyer in mail
	Then I delete message from gmail
