﻿Feature: SortingOfAds
   Checking sorting of Ads

	Background: 
	Given I open login page
	Then I login as Buyer
	Then I click My Account > Dashboard button
	Then I switch to Manage my ads


@sortingAdsByDate
Scenario: Sorting Ads By Date
	Then I click on Expiration Date
	Then I check that ads display by descending date
	Then I click on Expiration Date
	Then I check that ads display by ascending date