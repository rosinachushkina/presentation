﻿@PartsBee_Registration
Feature: NegativeRegistration

Background: 
	Given I open register page

@negativeRegistration
Scenario: Negative Registration
	When I press Registr button
	Then I recieved error notification
	Then I enter correct email to email adress input
	When I press Registr button
	Then I recieved error notification without email error
	
	Then I enter first name to First Name input
	When I press Registr button
	Then I recieved error notification without email/Fn error
	
	Then I enter last name to Last Name input
	When I press Registr button
	Then I recieved error notification without email/Fn/Ln error
	
	Then I enter password to Password input
	When I press Registr button
	Then I recieved error notification without email/Fn/Ln/Ps error

	Then I enter password to Password input
	When I press Registr button

	Then accept Term And Services
	Then I recieved error notification without email/Fn/Ln/Ps/Term&Services error

	Then I enter existed email
	And accept Privacy Policy
	When I press Registr button
	Then I recieved error notification about existed email

	Then I enter correct email to email adress input
	And enter small in password field 
	When I press Registr button
	Then I recieved error notification about icorrect password
