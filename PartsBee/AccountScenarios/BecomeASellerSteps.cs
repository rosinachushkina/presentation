﻿using System;
using Common;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using TechTalk.SpecFlow;

namespace PartsBee
{
    [Binding]
    public class BecomeASellerSteps: BaseSteps
    {
        [Then(@"I click Become Seller button")]
        public void ThenIClickBecomeSellerButton()
        {
            WebDriver.GetByPath("//span[contains(text(), 'Become Seller')]").Click();
        }

        [Then(@"I fill Company Name")]
        public void ThenIFillCompanyName()
        {
            WebDriver.GetByName("companyName").SendKeys(PartsBeeVariables.SellerCompany);
        }

        [Then(@"I fill Address1 input")]
        public void ThenIFillAddress1Input()
        {
            WebDriver.GetByName("address1").SendKeys(PartsBeeVariables.SellerAddress1);
        }

        [Then(@"I fill Address2 input")]
        public void ThenIFillAddress2Input()
        {
            WebDriver.GetByName("address2").SendKeys(PartsBeeVariables.SellerAddress2);
        }


        [Then(@"I fill City input")]
        public void ThenIFillCityInput()
        {
            WebDriver.GetByName("city").SendKeys(PartsBeeVariables.SellerCity);
        }

        [Then(@"I fill ZIP code input")]
        public void ThenIFillZIPCodeInput()
        {
            WebDriver.GetByName("zip").SendKeys(PartsBeeVariables.SellerZIP);
        }

        [Then(@"I fill Website input in Become Seller")]
        public void ThenIFillWebsiteInputInBecomeSeller()
        {
            WebDriver.GetByName("site").SendKeys(PartsBeeVariables.SellerWebSite);
        }

        [Then(@"I choose State Florida")]
        public void ThenIChooseStateFlorida()
        {
            var stateSelect =
               new SelectElement(WebDriver.GetByName("stateId"));
            stateSelect.SelectByValue("10");
        }

        [Then(@"I fill Phone input")]
        public void ThenIFillPhoneInput()
        {
            WebDriver.GetByName("phone1").SendKeys(PartsBeeVariables.SellerPhone);
        }

        [Then(@"I fill Email input in Become Seller")]
        public void ThenIFillEmailInputInBecomeSeller()
        {
            WebDriver.GetByName("cEmail").SendKeys(PartsBeeVariables.SellerEmail);
        }

        [Then(@"I fill Additional Info in Become Seller")]
        public void ThenIFillAdditionalInfoInBecomeSeller()
        {
            WebDriver.GetByPath("//div[2]/textarea").Click();
            WebDriver.GetByPath("//div[2]/textarea").SendKeys(PartsBeeVariables.SellerAddInfo);
        }


        [Then(@"I agree with selling Terms of Use")]
        public void ThenIAgreeWithSellingTermsOfUse()
        {
            WebDriver.GetByPath("//div[@id='sellerAccountInfo']/form/div/div/label/i").Click();
        }

        [Then(@"I click Save Seller account")]
        public void ThenIClickSaveSellerAccount()
        {
            WebDriver.GetByPath("//button[contains(text(), 'SAVE')]").Click();
        }

        [Then(@"I click on Seller info on Listing preview")]
        public void ThenIClickOnSellerInfoOnListingPreview()
        {
            WebDriver.GetByPath("//span[contains(text(), 'Seller')]").Click();
        }

        [Then(@"I check Seller info")]
        public void ThenICheckSellerInfo()
        {
            Assert.True(WebDriver.GetByPath("//tr[1]/td[contains(text(), '" + PartsBeeVariables.SellerCompany + "')]").Enabled);
            Assert.True(WebDriver.GetByPath("//tr[2]/td[contains(text(), '" + PartsBeeVariables.SellerAddress1 + "')]").Enabled);
            Assert.True(WebDriver.GetByPath("//tr[3]/td[contains(text(), '" + PartsBeeVariables.SellerAddress2 + "')]").Enabled);
            Assert.True(WebDriver.GetByPath("//tr[4]/td[contains(text(), '" + PartsBeeVariables.SellerCity + "')]").Enabled);
            Assert.True(WebDriver.GetByPath("//tr[4]/td[contains(text(), 'Florida')]").Enabled);
            Assert.True(WebDriver.GetByPath("//tr[4]/td[contains(text(), 'United States')]").Enabled);
            Assert.True(WebDriver.GetByPath("//tr[5]/td[contains(text(), '" + PartsBeeVariables.SellerZIP + "')]").Enabled);
            Assert.True(WebDriver.GetByPath("//tr[6]/td[contains(text(), '" + PartsBeeVariables.SellerPhone + "')]").Enabled);
            Assert.True(WebDriver.GetByPath("//tr[7]/td[contains(text(), '" + PartsBeeVariables.SellerEmail + "')]").Enabled);
            Assert.True(WebDriver.GetByPath("//tr[8]/td[contains(text(), '" + PartsBeeVariables.SellerWebSite + "')]").Enabled);
        }
        [Then(@"I check that checkboxes about displaying Email and Phone are unselected")]
        public void ThenICheckThatCheckboxesAboutDisplayingEmailAndPhoneAreUnselected()
        {
            if (WebDriver.GetByPath("//div[8]/div[2]/div/div[2]/label/i").Selected)
            {
                Assert.Fail("Checkbox Display Phone selected");
            }

            if (WebDriver.GetByPath("//fieldset/div[9]/div[3]/label/i").Selected)
            {
                Assert.Fail("Checkbox Display Email selected");
            }
        }
        [Then(@"I click on checkbox about displaying Email")]
        public void ThenIClickOnCheckboxAboutDisplayingEmail()
        {
            WebDriver.GetByPath("//div[8]/div[2]/div/div[2]/label/i").Click();
        }

        [Then(@"I click on checkbpx about displaying Phone")]
        public void ThenIClickOnCheckbpxAboutDisplayingPhone()
        {
            WebDriver.GetByPath("//fieldset/div[9]/div[3]/label/i").Click();
        }


        [Then(@"I click on checkbox Private Seller")]
        public void ThenIClickOnCheckboxPrivateSeller()
        {
           WebDriver.GetByPath("//fieldset/div[1]/div[3]/label/i").Click();
        }

        [Then(@"I choose country Afganistan")]
        public void ThenIChooseCountryAfganistan()
        {
            var countrySelect =
               new SelectElement(WebDriver.GetByName("countryId"));
            countrySelect.SelectByValue("2");
        }

        [Then(@"I check that field State disabled")]
        public void ThenICheckThatFieldStateDisabled()
        {
            try
            {
             WebDriver.GetByPath("//span[contains(text(), 'State')]");
             Assert.Fail("Field State doesn't disappear");
            }
            catch (NoSuchElementException)
            {
            }
            
            
    
        }

        [Then(@"I check that field Postal code enabled")]
        public void ThenICheckThatFieldPostalCodeEnabled()
        {
            Assert.True(WebDriver.GetByPath("//span[contains(text(), 'Postal code')]").Enabled);
        }

        [Then(@"I choose country Canada")]
        public void ThenIChooseCountryCanada()
        {
            var countrySelect =
               new SelectElement(WebDriver.GetByName("countryId"));
            countrySelect.SelectByValue("41");
        }

        [Then(@"I check that field Province enabled")]
        public void ThenICheckThatFieldProvinceEnabled()
        {
            Assert.True(WebDriver.GetByPath("//span[contains(text(), 'Province')]").Enabled);
        }

        [Then(@"I choose Province Alberta")]
        public void ThenIChooseProvinceAlberta()
        {
            var stateSelect =
               new SelectElement(WebDriver.GetByName("stateId"));
            stateSelect.SelectByValue("52");
        }

        [Then(@"I check Private Seller info")]
        public void ThenICheckPrivateSellerInfo()
        {
            Assert.True(WebDriver.GetByPath("//tr[1]/td[contains(text(), '" + PartsBeeVariables.SellerAddress1ForCanada + "')]").Enabled);
            Assert.True(WebDriver.GetByPath("//tr[2]/td[contains(text(), '" + PartsBeeVariables.SellerAddress2 + "')]").Enabled);
            Assert.True(WebDriver.GetByPath("//tr[3]/td[contains(text(), '" + PartsBeeVariables.SellerCityForCanada + "')]").Enabled);
            Assert.True(WebDriver.GetByPath("//tr[3]/td[contains(text(), 'Alberta')]").Enabled);
            Assert.True(WebDriver.GetByPath("//tr[3]/td[contains(text(), 'Canada')]").Enabled);
            Assert.True(WebDriver.GetByPath("//tr[4]/td[contains(text(), '" + PartsBeeVariables.SellerZIP + "')]").Enabled);
         //   Assert.True(WebDriver.GetByPath("//dl[2]/dd[2][contains(text(), '" + PartsBeeVariables.SellerEmail + "')]").Enabled);
            Assert.True(WebDriver.GetByPath("//tr[5]/td[contains(text(), '" + PartsBeeVariables.SellerWebSite + "')]").Enabled);
        }

        [Then(@"I fill Address1 input for Canada")]
        public void ThenIFillAddressInputForCanada()
        {
            WebDriver.GetByName("address1").SendKeys(PartsBeeVariables.SellerAddress1ForCanada);
        }

        [Then(@"I fill City input for Canada")]
        public void ThenIFillCityInputForCanada()
        {
            WebDriver.GetByName("city").SendKeys(PartsBeeVariables.SellerCityForCanada);
        }

        [Then(@"I check that Phone and Email don't display")]
        public void ThenICheckThatPhoneAndEmailDonTDisplay()
        {
            try
            {
                WebDriver.GetByPath("//dl[contains(text(), '" + PartsBeeVariables.SellerEmail + "')]");
                Assert.Fail("Email displayed");
            }
            catch (NoSuchElementException)
            {
            }

            try
            {
                WebDriver.GetByPath("//dl[contains(text(), '" + PartsBeeVariables.SellerPhone + "')]");
                Assert.Fail("Email displayed");
            }
            catch (NoSuchElementException)
            {
            }
            
        }


    }
}
