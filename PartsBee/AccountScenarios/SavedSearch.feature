﻿Feature: SavedSearch
	
Background: 
Given I open login page
Then I login as Buyer

@SavedSearch
Scenario: SavedSearch testing
	Then I click SearchParts button
	Then I make a search using the following key phrase: 'chrysler'
	Then I remember 1 st part title
	Then I remember a number of items found
	Then I Save my current search
	Then I click on Search
	Then I click saved searches
	Then I select my Saved Search 

	Then I wait 5 seconds

	Then The inventory which was added to my watchlist should be there (I check the title)
	Then The number of items found should be the same
	
	Then I delete my saved search
	Then I should not have any saved searches