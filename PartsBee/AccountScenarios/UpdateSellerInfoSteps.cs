﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading;
using Common;
using Common.Generators;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using TechTalk.SpecFlow;


namespace PartsBee.AccountScenarios
{
    [Binding]
    public class UpdateSellerInfoSteps : BaseSteps
    {
        string companyname, country, addr1, addr2, city, state, zipcode, phone, email, website, addinfo;

        [Then(@"I click on Seller Account info")]
        public void WhenIClickOnSellerAccountInfo()
        {
           WebDriver.GetByPath("//span[contains(text(), 'Seller Account')]").Click();
        }

        [When(@"I click on checkbox Private Seller and rename company {{(.*)}} and Save it")]
        public void WhenIClickOnCheckboxPrivateSeller(string companyName)
            {
            if(!WebDriver.GetByPath("//div[1]/div[3]/label/i").Selected)
            WebDriver.GetByPath("//div[1]/div[3]/label/i").Click();
            Thread.Sleep(500);

            WebDriver.GetByPath("//span[contains(text(),'Company Name')]/../../following-sibling::div[1]/input").Clear();
            WebDriver.GetByPath("//span[contains(text(),'Company Name')]/../../following-sibling::div[1]/input").SendKeys(companyName);
            WebDriver.GetByPath("//button[contains(text(), 'SAVE')]").Click();
        }

        [Then(@"I see conformation message about Successful saving")]
        public void ThenISeeConformationMessageAboutSuccessfulSaving()
        {
            Assert.True(WebDriver.GetBy("//b[contains(text(), 'Account saved')]").SetTimer(2000).Find().Displayed);
        }

        [Then(@"I reload page and check Company Name info")]
        public void ThenIReloadPageAndCheckSavedInfo()
        {
            WebDriver.Navigate().Refresh();
            WebDriver.GetByPath("//span[contains(text(), 'Seller Account')]").Click();
            Assert.True(WebDriver.GetBy("//div[@class='form-group'][1]/div[2]/input[@value='Test Company']").Find().Displayed);
        }

        [Then(@"I click on checkbox Private Seller and Save it")]
        public void ThenIClickOnCheckboxPrivateSellerAndSaveIt()
        {
            Thread.Sleep(1000);
            WebDriver.GetByPath("//div[@class='form-group'][1]/div[3]/label/i").Click();
            WebDriver.GetByPath("//button[contains(text(), 'SAVE')]").Click();

        }

        [When(@"I fill fields and Save them")]
        public void WhenIFillFieldsAndSaveThem()
        {
            WebDriver.GetByName("address1").Clear();
            WebDriver.GetByName("address1").SendKeys(PartsBeeVariables.SellerAddress1);

            WebDriver.GetByName("address2").Clear();
            WebDriver.GetByName("address2").SendKeys(PartsBeeVariables.SellerAddress2);

            WebDriver.GetByName("zip").SendKeys("");
            WebDriver.GetByName("zip").SendKeys(PartsBeeVariables.SellerZIP);

            WebDriver.GetByName("site").Clear();
            WebDriver.GetByName("site").SendKeys(PartsBeeVariables.SellerWebSite);

            WebDriver.GetByPath("//textarea[@name='description']").Clear();
            WebDriver.GetByPath("//textarea[@name='description']").SendKeys(PartsBeeVariables.SellerAddInfo);
            WebDriver.GetByPath("//button[contains(text(), 'SAVE')]").Click();
        }

        [Then(@"I reload page and check fields")]
        public void ThenIReloadPageAndCheckFields()
        {
            WebDriver.Navigate().Refresh();
            WebDriver.GetByPath("//span[contains(text(), 'Seller Account')]").Click();
          var NullAddress1 =
              WebDriver.FindElement(By.Name("address1")).GetAttribute("value");
          if (NullAddress1.Equals(""))
          Assert.Fail();

          var NullAddress2 =
              WebDriver.FindElement(By.Name("address2")).GetAttribute("value");
          if (NullAddress2.Equals(""))
          Assert.Fail();

          var NullZip =
             WebDriver.FindElement(By.Name("zip")).GetAttribute("value");
          if (NullZip.Equals(""))
          Assert.Fail();

          var NullSite =
            WebDriver.FindElement(By.Name("site")).GetAttribute("value");
          if (NullSite.Equals(""))
          Assert.Fail();

          var NullDescription =
WebDriver.FindElement(By.XPath("//textarea[@name='description']")).GetAttribute("value");
          if (NullDescription.Equals(""))
              Assert.Fail();
           
        }

        [Then(@"I clean all optional fields")]
        public void ThenICleanAllOptionalFields()
        {
        /*    WebDriver.GetByName("address1").Clear();
            WebDriver.GetByName("address1").SendKeys(" "); */

            WebDriver.GetByName("address2").Clear();
            WebDriver.GetByName("address2").SendKeys(" ");

            WebDriver.GetByName("zip").Clear();
            WebDriver.GetByName("zip").SendKeys(" ");

            WebDriver.GetByName("site").Clear();
            WebDriver.GetByName("site").SendKeys(" ");

            WebDriver.GetByPath("//textarea[@name='description']").Clear();
            WebDriver.GetByPath("//textarea[@name='description']").SendKeys(" ");
         }

        [Then(@"I save changes of Seller Account info")]
        public void ThenISaveChangesSellerAccountInfo()
        {
            WebDriver.GetByPath("//button[contains(text(), 'SAVE')]").Click();
        }

        [When(@"I change field City, State, Phone, Email")]
        public void WhenIChangeFieldCityStatePhoneEmail()
        {
            WebDriver.GetByName("city").Clear();
            WebDriver.GetByName("city").SendKeys("Miami");


            var conditionSelect =
    new SelectElement(WebDriver.GetByPath("//div[@id='sellerAccountInfo']/form//select[@name='stateId']"));
            conditionSelect.SelectByText("Florida");

            WebDriver.GetByName("phone1").Clear();
            WebDriver.GetByName("phone1").SendKeys("12345");

            WebDriver.GetByName("cEmail").Clear();
            WebDriver.GetByName("cEmail").SendKeys(PartsBeeVariables.SellerLogin);
            
        }

        [When(@"I clear all requirement fields")]
        public void WhenIClearAllRequirementFields()
        {
            WebDriver.GetByName("city").Clear();
            WebDriver.GetByName("city").SendKeys(" ");

            WebDriver.GetByName("phone1").Clear();
            WebDriver.GetByName("phone1").SendKeys(" ");

            WebDriver.GetByName("cEmail").Clear();
            WebDriver.GetByName("cEmail").SendKeys(" ");

            WebDriver.GetByPath("//div[@class='form-group'][1]/div[3]/label/i").Click(); //Checkbox Private Seller
            WebDriver.GetByName("companyName").Clear();
            WebDriver.GetByName("companyName").SendKeys(" ");

            WebDriver.GetByPath("//div[@class='form-group e-mail']/div[3]/label/i").Click(); //Checkbox Display Email
          
            WebDriver.GetByName("address1").Clear();
            WebDriver.GetByName("address1").SendKeys(" ");
         }

        [Then(@"I see {{(.*)}} Alert about needi for filling fields")]
        public void ThenISeeAlertAboutNeediForFillingFields(int numberRequirements)
        {
            WebDriver.GetById("userProfileContainer").Click();
            var checkNumberErrorNotification =
                WebDriver.FindElements(By.ClassName("parsley-error"));
            
            if
            
                (checkNumberErrorNotification.Count != numberRequirements)
            




            Assert.Fail("Expected error notification is " + numberRequirements + "\n Actual is " + checkNumberErrorNotification.Count);
            
           
        }

        [Then(@"I should NOT see a confirmation message about successful saving")]
        public void ThenIShouldNOTSeeAConfirmationMessageAboutSuccessfulSaving()
            {
            WebDriver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(3));

            try
                {
                if(WebDriver.GetByPath("//b[contains(text(), 'Account saved')]").Displayed);
                Assert.Fail("We have saved our profile without a required field!(((");
                }
            catch(WebDriverException)
            { }

            WebDriver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(BaseSteps.StandardTimeOut));
            }

        [Then(@"I switch metric")]
        public void ThenISwitchMetric()
            {
            Thread.Sleep(500);
            WebDriver.GetByPath("//span[@class='switch-input-checkbox']/b/label").Click();
            Thread.Sleep(500);
            }

        [Then(@"I take information from Seller profile fields and save it for later")]
        public void ThenITakeInformationFromSellerProfileFieldsAndSaveItForLater()
            {
            //companyname=
            country=WebDriver.GetByPath("//select[@name='countryId']").Text;
            addr1=WebDriver.GetByPath("//input[@name='address1']").GetAttribute("value");
            addr2=WebDriver.GetByPath("//input[@name='address2']").GetAttribute("value");
            city=WebDriver.GetByPath("//input[@name='city']").GetAttribute("value");
            state=WebDriver.GetByPath("//select[@name='stateId']").Text;
            zipcode=WebDriver.GetByPath("//input[@name='zip']").GetAttribute("value");
            phone=WebDriver.GetByPath("//input[@name='phone1']").GetAttribute("value");
            email=WebDriver.GetByPath("//input[@name='cEmail']").GetAttribute("value");
            website=WebDriver.GetByPath("//input[@name='site']").GetAttribute("value");
            addinfo = WebDriver.GetByPath("//textarea[@name='description']").GetAttribute("value");
            }

        [Then(@"all required information from my seller account should be included in the listing")]
        public void ThenAllRequiredInformationFromMySellerAccountShouldBeIncludedInTheListing()
            {
            WebDriver.GetByPath("//span[contains(text(),'Seller')]/..").Click();
            Thread.Sleep(3000);

            Assert.IsTrue(WebDriver.GetByPath("//th[contains(text(),'Address 1')]/following-sibling::*").Text
                ==addr1);
            Assert.IsTrue(WebDriver.GetByPath("//th[contains(text(),'Address 2')]/following-sibling::*").Text
                == addr2);

            Assert.IsTrue(WebDriver.GetByPath("//th[contains(text(),'Description')]/following-sibling::*").Text
                == addinfo);

            Assert.IsTrue(
                
                WebDriver.GetByPath("//th[contains(text(),'Seller Address')]/following-sibling::*").Text.
                Contains(city));

            Assert.IsTrue(WebDriver.GetByPath("//th[contains(text(),'Phone')]/following-sibling::*").Text
                ==phone);
            Assert.IsTrue(WebDriver.GetByPath("//th[contains(text(),'Email')]/following-sibling::*").Text
                == email);
            }

        [Then(@"I click the Save button \(Profile\)")]
        public void ThenIClickTheSaveButtonProfile()
            {
            WebDriver.GetByPath("//button[@type='button'][contains(text(),'SAVE')]").Click();
            Thread.Sleep(2000);
            }

        [Then(@"I check that the metric in listing is shown as METRIC")]
        public void ThenICheckThatTheMetricInListingIsShownAsMETRIC()
            {
            WebDriver.GetByPath("//*[contains(text(),'cm')]");
            WebDriver.GetByPath("//*[contains(text(),'kg')]");
            }




    }
}
