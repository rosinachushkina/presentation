﻿@PartsBee_CreateListingPage
Feature: CreateListingPage
	In order to checl create list functionality
	As a user
	I want to create a list

	Background: 
	Given I open login page
	Then I login as Seller

@CreateListingPage_AddOptionsDraftsPreview
Scenario Outline: CreateListingPage_AddOptionsDraftsPreview
	Then I press Sell Your part Button
	Then I click ListSinglePart from a main dropdown

	Then I check whether I am on the drafts page and go to single listing if I am there

	Then I click on checkbox about having Part Number
	Then I enter {{12}} to List by part input
	Then I click on list of Category
	And choose first and second and third items and sumbit
	Then I fill VIN field
	Then I manually enter a compatible vehicle for a listing
	Then I fill description form default values at listing page
	Then I click on Additional option then fill data <makeSet> <modelSet> <colorSet> <finishSet> <countrySet> <exLinkSet> <widthSet> <heghtSet> <lengthSet> <weigthSet> <weigthSet2> <damageSet> <WarrantySet> <placementSet> and sumbit 
	Then I fill Shipping Polices
	Then I fill in Return Policy
	Then I upload picture
	Then enter price and quantity
	Then I click on Negotiable checkbox

	Then I click the Preview button for my listing
    Then I am redirected to a listing page and check Additional information
	Then I click on Return Policy and check info
	Then I click on Shipping Policy and check info
	
	Then I switch back to my listing from Preview
	Then I save the listing for later (to drafts)
	Then I select my draft from the drafts page (complete button)
	When I click on Active listing button

	Then I am redirected to a listing page and check Additional information
	
	Then I click on Return Policy and check info
	Then I click on Shipping Policy and check info
	Then I switch to Compatibility within the Listing and check the info
	Then I click on GO FOR IT
	Then I redirect to site page {{http://diamond.myvirtualinventory.com/}}

	Then I close current browser window and switch to the first one

	Then I click the Print button
	Then I check that all information is there (in the print page)

	Then I close current browser window and switch to the first one
	Then I delete my listing within the listing itself

	Examples: 
	| makeSet | modelSet | colorSet | finishSet | countrySet | exLinkSet | widthSet | heghtSet | lengthSet | weigthSet | weightSet2 | damageSet | WarrantySet | placementSet |
	| ACURA   |          |		    |           |    null    |   null    |   null   |    null  |   null    |   null    |   null     |   null    |    null     |   null       |


	
