﻿Feature: Report
	In order to avoid silly mistakes
	As a math idiot
	I want to be told the sum of two numbers

@ReportingWithNotAvailable
Scenario: ReportingWithNotAvailable
Given I open login page
	Then I login as Seller
	Then I press Sell Your part Button
    Then I click ListSinglePart from a main dropdown
	Then I check whether I am on the drafts page and go to single listing if I am there
    Then I click on list of Category
    And choose first and second and third items and sumbit
    Then I fill description form default values at listing page (with random title)
    Then enter price and quantity
    When I click on Active listing button
    Then I am redirected to a listing page
	Then I log out

	Given I open login page
	Then I log in as a buyer
	Then I go to search page
	Then I make search of the created item title
	Then I open the first listing
	Then I report the listing as NotAvailable
	Then I log out

	Then I wait 10 seconds
	Then I open mail site
	Then I enter a seller's email in mailinator and check it
	Then I see forgot  pass letter and open it
	Then There should be my listing's unique title
	Then I go to fix the listing using CheckListing button from the email

	Then I login as Seller
	Then I mark my listing as Price Negotiable
	Then I click SubmitRevisions button
	Then My listing should be available after fixing
	Then I delete my listing within the listing itself

@ReportingWithIncorrectListing
Scenario: ReportingWithIncorrectListing
Given I open login page
	Then I login as Seller
	Then I press Sell Your part Button
    Then I click ListSinglePart from a main dropdown
	Then I check whether I am on the drafts page and go to single listing if I am there
    Then I click on list of Category
    And choose first and second and third items and sumbit
    Then I fill description form default values at listing page (with random title)
    Then enter price and quantity
    When I click on Active listing button
    Then I am redirected to a listing page
	Then I log out

	Given I open login page
	Then I log in as a buyer
	Then I go to search page
	Then I make search of the created item title
	Then I open the first listing
	Then I report the listing as IncorrectListing
	Then I log out

    Then I wait 10 seconds
	Then I open mail site
	Then I enter a seller's email in mailinator and check it
	Then I see forgot  pass letter and open it
	Then There should be my listing's unique title
	Then I go to fix the listing using CheckListing button from the email

	Then I login as Seller
	Then I mark my listing as Price Negotiable
	Then I click SubmitRevisions button
	Then My listing should be available after fixing
	Then I delete my listing within the listing itself

	@ReportingWithFraud
Scenario: ReportingWithFraud
Given I open login page
	Then I login as Seller
	Then I press Sell Your part Button
    Then I click ListSinglePart from a main dropdown
	Then I check whether I am on the drafts page and go to single listing if I am there
    Then I click on list of Category
    And choose first and second and third items and sumbit
    Then I fill description form default values at listing page (with random title)
    Then enter price and quantity
    When I click on Active listing button
    Then I am redirected to a listing page
	Then I log out

	Given I open login page
	Then I log in as a buyer
	Then I go to search page
	Then I make search of the created item title
	Then I open the first listing
	Then I report the listing as Fraud
	Then I log out

	Then I wait 10 seconds
	Then I open mail site
	Then I enter a seller's email in mailinator and check it
	Then I see forgot  pass letter and open it
	Then There should be my listing's unique title