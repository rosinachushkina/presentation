﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TechTalk.SpecFlow;
using Common;
using System.Threading;
using NUnit.Framework;

namespace PartsBee.AccountScenarios
    {
    [Binding]
    public class SavedSearchSteps : BaseSteps
        {
        string SavedItemTitle;
        string SavedSearchName = "Automation - Test Saved Search";
        string SavedItemsCount;

        [Then(@"I make a search using the following key phrase: '(.*)'")]
        public void ThenIMakeASearchUsingTheFollowingKeyPhrase(string p0)
            {
            WebDriver.GetByPath("//*[@id='autocomplete']").SendKeys(p0);
            WebDriver.GetByPath("//button[@class='btn btn-warning go-search']").Click();
            Thread.Sleep(2000);
            }

        [Then(@"I remember a number of items found")]
        public void ThenIRememberANumberOfItemsFound()
            {
            SavedItemsCount = HandyFunctions.TrimToDigits
                (WebDriver.GetByPath("//div[@class='search-results-panel']//h2/following-sibling::span").Text);
            }

        [Then(@"I Save my current search")]
        public void ThenISaveMyCurrentSearch()
            {
            WebDriver.GetByPath("//div[@class='save-search pull-left']").Click();
            Thread.Sleep(1000);
            WebDriver.GetByPath("//input[@class='form-control input-sm']").Clear();
            WebDriver.GetByPath("//input[@class='form-control input-sm']").SendKeys(SavedSearchName);
            WebDriver.GetByPath(".//*[@id='searchContainer']/div/div/div[2]/div[1]/div/div/button").Click();
            Thread.Sleep(200);
            }

        [Then(@"I select my Saved Search")]
        public void ThenISelectMySavedSearch()
            {
            //WebDriver.GetByPath("//button[@class='btn btn-default saved-searches dropdown-button-wrapper']").Click();
            WebDriver.GetByPath("//span[contains(text(),'"+SavedSearchName+"')]").Click();
            Thread.Sleep(500);
            }

        [Then(@"The number of items found should be the same")]
        public void ThenTheNumberOfItemsFoundShouldBeTheSame()
            {
            Assert.IsTrue(SavedItemsCount == HandyFunctions.TrimToDigits
                (WebDriver.GetByPath("//div[@class='search-results-panel']//h2/following-sibling::span").Text));
            }

        [Then(@"I delete my saved search")]
        public void ThenIDeleteMySavedSearch()
            {
            WebDriver.GetByPath(".//*[@id='searchContainer']/div/div/div[2]/div[2]/button").Click();
            WebDriver.GetByPath("//i[@class='glyphicon glyphicon-remove pull-right']").Click();
            Thread.Sleep(200);
            }

        [Then(@"I should not have any saved searches")]
        public void ThenIShouldNotHaveAnySavedSearches()
            {
            WebDriver.GetByPath("//span[contains(text(),'You do not have any search saved yet')]");
            }

        }
    }
