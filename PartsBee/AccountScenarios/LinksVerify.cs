﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using Common;
using NUnit.Framework;
using TechTalk.SpecFlow;
using OpenQA.Selenium;

namespace PartsBee.AccountScenarios
{
    [Binding]
    public class LinksVerify : BaseSteps
    {
        [Given(@"I go to registration page")]
        public void GivenIGoToRegistrationPage()
        {
            WebDriver.Go("https://dev.partsbee.com/registration");
        }

        [Then(@"I verify links")]
        public void ThenIVerifyLinks()
        {
            Assert.True(WebDriver.GetByPath("//*[@id='agreements']/label[1]/span/a").Displayed);
            Assert.True(WebDriver.GetByPath("//*[@id='agreements']/label[2]/span/a").Displayed);
            Assert.True(WebDriver.GetByPath("//footer/div/div[1]/div/span[2]/a[1]/span").Displayed);
            Assert.True(WebDriver.GetByPath("//footer/div/div[1]/div/span[2]/a[2]/span").Displayed);
            Assert.True(WebDriver.GetByPath("//footer/div/div[1]/div/span[2]/a[3]/span").Displayed);
            Assert.True(WebDriver.GetByPath("//*[@id='socialNetworks']/span[2]/a[1]").Displayed);
            Assert.True(WebDriver.GetByPath("//*[@id='socialNetworks']/span[2]/a[2]").Displayed);
            Assert.True(WebDriver.GetByPath("//*[@id='socialNetworks']/span[2]/a[3]").Displayed);
            Assert.True(WebDriver.GetByPath("//*[@id='socialNetworks']/span[2]/a[4]").Displayed);
            Console.WriteLine("Links on page are present");

        }

        [Then(@"I go body links")]
        public void ThenIGoBodyLinks()
        {
            WebDriver.GetByPath("//*[@id='agreements']/label[1]/span/a").Click();
            WebDriver.SwitchTo().Window(WebDriver.WindowHandles[1]);
            Assert.True(WebDriver.GetByPath("//*[@class='legal-contract-header']/h2").Displayed);
            WebDriver.Close();
            WebDriver.SwitchTo().Window(WebDriver.WindowHandles[0]);
            Thread.Sleep(3000);
            WebDriver.GetByPath("//*[@id='agreements']/label[2]/span/a").Click();
            WebDriver.SwitchTo().Window(WebDriver.WindowHandles[1]);
            Assert.True(WebDriver.GetByPath("//*[@class='legal-contract-header']/h2").Displayed);
            WebDriver.Close();
            WebDriver.SwitchTo().Window(WebDriver.WindowHandles[0]);
            Console.WriteLine("Body links are OK");
        }

        [Then(@"I verify  Seller Policies")]
        public void ThenIVerifyFooterLinks()
        {
            WebDriver.GetByPath("//footer/div/div[1]/div/span[2]/a[1]/span").Click();
            WebDriver.SwitchTo().Window(WebDriver.WindowHandles[1]);
            Thread.Sleep(1000);
            Assert.True(WebDriver.GetByPath("//*[@class='legal-contract-header']/h2").Displayed);
            WebDriver.Close();
            WebDriver.SwitchTo().Window(WebDriver.WindowHandles[0]);
            Thread.Sleep(1000);
           }
            [Then(@"I verify  Privacy Policy")]
            public void ThenIVerifyPrivacyPolicy()
            {
              WebDriver.GetByPath("//footer/div/div[1]/div/span[2]/a[2]/span").Click();
              WebDriver.SwitchTo().Window(WebDriver.WindowHandles[1]);
              Thread.Sleep(1000);
            Assert.True(WebDriver.GetByPath("//*[@class='legal-contract-header']/h2").Displayed);
            WebDriver.Close();
            WebDriver.SwitchTo().Window(WebDriver.WindowHandles[0]);
            Thread.Sleep(1000);
            
            }

            
                    [Then(@"I verify  Terms of Use")]
                    public void ThenIVerifyTermsOfUse()
                    {
                         WebDriver.GetByPath("//footer/div/div[1]/div/span[2]/a[3]/span").Click();
                         WebDriver.SwitchTo().Window(WebDriver.WindowHandles[1]);
                         Thread.Sleep(1000);
            Assert.True(WebDriver.GetByPath("//*[@class='legal-contract-header']/h2").Displayed);
            
            WebDriver.Close();
            WebDriver.SwitchTo().Window(WebDriver.WindowHandles[0]);
            Console.WriteLine("3 first links are OK");
            Thread.Sleep(1000);
                    }
                [Then(@"I verify  Facebook")]
                public void ThenIVerifyFacebook()
                {
                   WebDriver.GetByPath("//*[@id='socialNetworks']/span[2]/a[1]").Click();
                   WebDriver.SwitchTo().Window(WebDriver.WindowHandles[1]);
            Assert.True(WebDriver.GetBy("//*[@id='fbProfileCover']/div[2]/div[1]/h2/span[1][contains(text(), 'PartsBee')]").Find().Displayed);
            WebDriver.Close();
            WebDriver.SwitchTo().Window(WebDriver.WindowHandles[0]);
            Console.WriteLine("Facebook link are OK");
            Thread.Sleep(1000);
                }
                [Then(@"I verify  google\+")]
                public void ThenIVerifyGoogle()
            {
                       WebDriver.GetByPath("//*[@id='socialNetworks']/span[2]/a[2]").Click();
             
             WebDriver.SwitchTo().Window(WebDriver.WindowHandles[1]);
             Thread.Sleep(2000);
             Assert.True(WebDriver.GetBy("//*[@class='zi']/a[contains(text(), 'PartsBee.com')]").Find().Displayed);
             WebDriver.Close();
             WebDriver.SwitchTo().Window(WebDriver.WindowHandles[0]);
            Console.WriteLine("Goggle+ link are OK");
            Thread.Sleep(1000);
                }
                [Then(@"I verify  twitter")]
                public void ThenIVerifyTwitter()
                 {
                     
            WebDriver.GetByPath("//*[@id='socialNetworks']/span[2]/a[3]").Click();
            WebDriver.SwitchTo().Window(WebDriver.WindowHandles[1]);
            WebDriver.GetByPath("//*[@id='signup-dialog-dialog']/button").Click();
            Thread.Sleep(1000);
            Assert.True(WebDriver.GetBy("//*[@class='ProfileHeaderCard']/h1/a[contains(text(), 'PartsBee.com')]").Find().Displayed);
            WebDriver.Close();
            WebDriver.SwitchTo().Window(WebDriver.WindowHandles[0]);
            Console.WriteLine("Twitter link are OK");
            Thread.Sleep(1000);
                }
                [Then(@"I verify  pinterest")]
            public void ThenIVerifyPinterest()
            {
             WebDriver.GetByPath("//*[@id='socialNetworks']/span[2]/a[4]").Click();
             WebDriver.SwitchTo().Window(WebDriver.WindowHandles[1]);
             Thread.Sleep(2000);
             Assert.True(WebDriver.GetBy("//*[@class='name'][contains(text(), 'PartsBee.com')]").Find().Displayed);
             WebDriver.Close();
             WebDriver.SwitchTo().Window(WebDriver.WindowHandles[0]);
             Console.WriteLine("Pinterest link are OK");
             Thread.Sleep(1000);
            }

        //--------------------

                [Then(@"I click the Log in button")]
                public void ThenIClickTheLogInButton()
                    {
                    try
                        {
                        WebDriver.GetByPath("//a[@href='http://dev.partsbee.com/account/login']").Click();
                        }
                    catch(WebDriverException)
                        {
                        WebDriver.GetByPath("//a[@href='http://partsbee.com/account/login']").Click();
                        }
                    }

                [Then(@"I check the Search Parts Now button")]
                public void ThenICheckTheSearchPartsNowButton()
                    {
                    WebDriver.GetByPath("//input[@value='Search Parts Now']").Click();
                    Thread.Sleep(2000);
                    WebDriver.GetByPath("//b[contains(text(),'Search')]");
                    WebDriver.GetByPath("//b[contains(text(),'Search')]/following-sibling::*[contains(text(),'Results')]");
                    }

                [Then(@"I check the Search main menu button")]
                public void ThenICheckTheSearchMainMenuButton()
                    {
                    WebDriver.GetByPath("//a[@href='/search']").Click();
                    Thread.Sleep(2000);
                    WebDriver.GetByPath("//b[contains(text(),'Search')]");
                    WebDriver.GetByPath("//b[contains(text(),'Search')]/following-sibling::*[contains(text(),'Results')]");
                    }

                [Then(@"I check the About Us main menu button")]
                public void ThenICheckTheAboutUsMainMenuButton()
                    {
                    WebDriver.GetByPath("//div[@id='mainMenu']//a[@href='/aboutus']").Click();
                    Thread.Sleep(2000);
                    WebDriver.GetByPath("//h2//*[contains(text(),'ABOUT')]");
                    }

                [Then(@"I check that I am at Sell Your Parts > List Single Part")]
                public void ThenICheckThatIAmAtSellYourPartsListSinglePartOptions()
                    {
                    Assert.IsTrue(WebDriver.Title=="Creating new listing");
                    }

                [Then(@"I check that I am at Sell Your Parts > List Bulk Parts")]
                public void ThenICheckThatIAmAtSellYourPartsListBulkPartsOptions()
                    {
                    Assert.IsTrue(WebDriver.Url.Contains("partsbee.com/matching/seller/identify-vehicle"));
                    }


                [Then(@"I check the Parts Wanted main menu button")]
                public void ThenICheckThePartsWantedMainMenuButton()
                    {
                    WebDriver.GetByPath("//a[@href='/matching/buyer/identify-vehicle']").Click();
                    Thread.Sleep(2000);
                    Assert.IsTrue(WebDriver.Url.Contains("partsbee.com/matching/buyer/identify-vehicle"));
                    }

                [Then(@"I check that I am a My Account")]
                public void ThenICheckThatIAmAMyAccount()
                    {
                    WebDriver.GetByPath("//legend[contains(text(),'Account Information')]");
                    }

                [Then(@"I go to Dashboard")]
                public void ThenIGoToDashboard()
                    {
                    WebDriver.GetByPath("//*[contains(text(), 'My account')]").Click();
                    Thread.Sleep(500);
                    WebDriver.GetByPath("//a[contains(text(), 'Dashboard')]").Click();
                    }

                [Then(@"I check that I am in the Dashboard \(Seller\)")]
                public void ThenICheckThatIAmInTheDashboardSeller()
                    {
                    Assert.IsTrue(WebDriver.Url.Contains("partsbee.com/dashboard/seller/threads/"));
                    }


                [Then(@"I check the Seller Policy footer")]
                public void ThenICheckTheSellerPolicyFooter()
                    {
                    WebDriver.GetByPath("//a[@href='/contracts/selling-policies']").Click();
                    Thread.Sleep(2000);
                    WebDriver.SwitchTo().Window(WebDriver.WindowHandles.Last());
                    Assert.IsTrue(WebDriver.Url.Contains("partsbee.com/contracts/selling-policies"));
                    WebDriver.Close();
                    WebDriver.SwitchTo().Window(WebDriver.WindowHandles[0]);
                    }

                [Then(@"I check the Privacy Policy footer")]
                public void ThenICheckThePrivacyPolicyFooter()
                    {
                    WebDriver.GetByPath("//a[@href='/contracts/privacy-policy']").Click();
                    Thread.Sleep(2000);
                    WebDriver.SwitchTo().Window(WebDriver.WindowHandles.Last());
                    Assert.IsTrue(WebDriver.Url.Contains("partsbee.com/contracts/privacy-policy"));
                    WebDriver.Close();
                    WebDriver.SwitchTo().Window(WebDriver.WindowHandles[0]);
                    }

                [Then(@"I check the Terms of Service footer")]
                public void ThenICheckTheTermsOfServiceFooter()
                    {
                    WebDriver.GetByPath("//a[@href='/contracts/terms-of-use']").Click();
                    Thread.Sleep(2000);
                    WebDriver.SwitchTo().Window(WebDriver.WindowHandles.Last());
                    Assert.IsTrue(WebDriver.Url.Contains("partsbee.com/contracts/terms-of-use"));
                    WebDriver.Close();
                    WebDriver.SwitchTo().Window(WebDriver.WindowHandles[0]);
                    }

                [Then(@"I check the Facebook footer")]
                public void ThenICheckTheFacebookFooter()
                    {
                    WebDriver.GetByPath("//a[@href='https://www.facebook.com/pages/PartsBee/1536519049931122?skip_nax_wizard=true']").
                        Click();
                    Thread.Sleep(2000);
                    WebDriver.SwitchTo().Window(WebDriver.WindowHandles.Last());
                    Assert.IsTrue(WebDriver.Title.Contains("Facebook"));
                    WebDriver.Close();
                    WebDriver.SwitchTo().Window(WebDriver.WindowHandles[0]);
                    }

                [Then(@"I check the GooglePlus footer")]
                public void ThenICheckTheGooglePlusFooter()
                    {
                    WebDriver.GetByPath("//a[@href='https://plus.google.com/u/0/116701380105478001849/posts']").
                        Click();
                    Thread.Sleep(2000);
                    WebDriver.SwitchTo().Window(WebDriver.WindowHandles.Last());
                    Assert.IsTrue(WebDriver.Title.Contains("Google"));
                    WebDriver.Close();
                    WebDriver.SwitchTo().Window(WebDriver.WindowHandles[0]);
                    }

                [Then(@"I check the Twitter footer")]
                public void ThenICheckTheTwitterFooter()
                    {
                    WebDriver.GetByPath("//a[@href='https://twitter.com/PartsBee']").
                         Click();
                    Thread.Sleep(2000);
                    WebDriver.SwitchTo().Window(WebDriver.WindowHandles.Last());
                    Assert.IsTrue(WebDriver.Title.Contains("Twitter"));
                    WebDriver.Close();
                    WebDriver.SwitchTo().Window(WebDriver.WindowHandles[0]);
                    }

                [Then(@"I check the Pinterest footer")]
                public void ThenICheckThePinterestFooter()
                    {
                    WebDriver.GetByPath("//a[@href='https://pinterest.com/PartsBee/']").
                         Click();
                    Thread.Sleep(2000);
                    WebDriver.SwitchTo().Window(WebDriver.WindowHandles.Last());
                    Assert.IsTrue(WebDriver.Title.Contains("Pinterest"));
                    WebDriver.Close();
                    WebDriver.SwitchTo().Window(WebDriver.WindowHandles[0]);
                    }

                [Then(@"I check the LogOut button")]
                public void ThenICheckTheLogOutButton()
                    {
                    WebDriver.GetByPath("//a[contains(text(),'Logout')]").Click();
                    Thread.Sleep(2000);
                    Assert.IsTrue(WebDriver.Url.Contains("partsbee.com/account/login"));
                    }


    }
}
