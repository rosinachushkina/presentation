﻿using System;
using System.Threading;
using Common;
using NUnit.Core;
using NUnit.Framework;
using TechTalk.SpecFlow;

namespace PartsBee
{
    [Binding]
    public class PartsWantedCreationSteps : BaseSteps
    {


        [Then(@"I login as Buyer")]
        public void ThenILoginAsBuyer()
        {
            WebDriver.GetByName("Email").SendKeys(PartsBeeVariables.CustomerLogin);
            WebDriver.GetByName("Password").SendKeys(PartsBeeVariables.CustomerPassword);
            WebDriver.GetByName("submit-button").Click();
        }

        [Then(@"I press PARTS WANTED Button")]
        public void ThenIPressPARTSWANTEDButton()
        {
            WebDriver.WaitFullLoad();
            WebDriver.GetByPath("//a[contains(text(), 'PARTS WANTED')]").Click();
        }

        [Then(@"I check prhrase You are ready to (.*)")]
        public void ThenICheckPrhraseYouAreReadyToBuy(string phrase)
        {
            Assert.True(WebDriver.GetByPath("//footer/div/div/span[2]/span[2][contains (text(), '" + phrase + "')]").Enabled);
        }

        [Then(@"I type trim for parts wanted")]
        public void ThenITypeTrimForPartsWanted()
        {
            WebDriver.GetByPath("//article/div[2]/input").Clear();
            WebDriver.GetByPath("//article/div[2]/input").SendKeys(PartsBeeVariables.trim);
        }

        [Then(@"I check VIN on third step in header")]
        public void ThenICheckVINOnThirdStepInHeader()
        {
            Assert.True(WebDriver.GetByPath("//h2/span[2]/span[2]").Text.Equals(PartsBeeVariables.VIN));
        }

        [Then(@"I click Publish My Listings of Parts Wanted")]
        public void ThenIClickPublishMyListingsOfPartsWanted()
        {
            WebDriver.GetByPath("//section[2]/div/div[2]/div/div/div[2]/button[2]").Click();
            WebDriver.WaitFullLoad();
        }

        [Then(@"I check trim")]
        public void ThenICheckTrim()
        {
            Assert.True(WebDriver.GetByPath("//h2[contains (text(), '" + PartsBeeVariables.trim + "')]").Enabled);
            Assert.True(WebDriver.GetByPath("//span[contains(text(), '" + PartsBeeVariables.trim + "')]").Enabled);
        }

        [Then(@"I check trim in header")]
        public void ThenICheckTrimInHeader()
        {
            Assert.True(WebDriver.GetByPath("//span[contains(text(), '" + PartsBeeVariables.trim + "')]").Enabled);
        }


        [Then(@"I check that names of groups enabled")]
        public void ThenICheckThatNamesOfGroupsEnabled()
        {
            string[] array = new string[] { "EXTERIOR", "INTERIOR", "MECHANICAL" };

            for (int i = 0; i < array.Length; i++)
            {
                Assert.True(WebDriver.GetByPath("//section[2]/div/div[1]/div[" + (i + 3) + "]/div[1]/strong").Text.Contains(array[i]));
            }
        }

        [Then(@"I click Go to search results page button")]
        public void ThenIClickGoToSearchResultsPageButton()
        {
            WebDriver.GetByPath("//a[contains(text(), 'Go to search results page')]").Click();
        }

        [Then(@"I check that parts wanted filter is working")]
        public void ThenICheckThatPartsWantedFilterIsWorking()
        {
            int numberOfListingsAll = 0;
            int numberOfListingsFound = 0;

            string numberOfListingsAllStr = WebDriver.GetByPath("//div[@class='search-results-panel']//h2/following-sibling::span").Text;
            numberOfListingsAll = Convert.ToInt32(numberOfListingsAllStr.Substring(1, numberOfListingsAllStr.Length-2));

            WebDriver.GetByPath("//div/div[2]/div[2]/button").Click();
            WebDriver.GetByPath("//div[2]/div/div/div/div[1]/div/span/span").Click();
            Thread.Sleep(500);

            string numberOfFoundedListingsStr = WebDriver.GetByPath("//div[@class='search-results-panel']//h2/following-sibling::span").Text;
            numberOfListingsFound = Convert.ToInt32(numberOfFoundedListingsStr.Substring(1, numberOfFoundedListingsStr.Length-2));

            Assert.True(numberOfListingsAll > numberOfListingsFound);

        }

        [Then(@"I click saved searches")]
        public void ThenIClickSavedSearches()
        {
        WebDriver.GetByPath(".//*[@id='searchContainer']/div/div/div[2]/div[2]/button").Click();
        }

        [Then(@"I click delete this search")]
        public void ThenIClickDeleteThisSearch()
        {
            WebDriver.GetByPath("//div/a[contains(text(), 'Delete This Search')]").Click();
        }

        [Then(@"I see phrase that I dont have any search saved yet")]
        public void ThenISeePhraseThatIDontHaveAnySearchSavedYet()
        {
            Assert.True(WebDriver.GetByPath("//span[contains(text(), 'You do not have any search saved yet')]").Enabled);
        }

        [Then(@"I click My Account > Dashboard button")]
        public void ThenIClickMyAccountDashboardButton()
        {
            WebDriver.GetByPath("//*[contains(text(), 'My account')]").Click();
            Thread.Sleep(500);
            WebDriver.GetByPath("//*[contains(text(), 'Dashboard')]").Click();
        }

        [Then(@"I switch to Manage my ads")]
        public void ThenISwitchToManageMyAds()
        {
            WebDriver.GetByPath("//header/div/button").Click();
            Thread.Sleep(500);
            WebDriver.GetByPath("//ul/li[1]/a[contains(text(), 'MANAGE MY ADS')]").Click();
        }

   



    }
}
