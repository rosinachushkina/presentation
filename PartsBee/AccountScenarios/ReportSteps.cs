﻿using System.Threading;
using Common;
using NUnit.Framework;
using OpenQA.Selenium;
using TechTalk.SpecFlow;
using OpenQA.Selenium.Support.UI;

namespace PartsBee.AccountScenarios
{
    [Binding]
    public class ReportSteps : BaseSteps
    {

        string EnteredTitle = "";

        [Then(@"I go to search page")]
        public void ThenIGoToSearchPage()
        {
            WebDriver.GetByPath("//a[@href='/search']").Click();
        }

        [Then(@"I type for search")]
        public void ThenITypeForSearch()
        {
            WebDriver.GetByPath("//*[@id='searchContainer']/div/div[1]/div[1]/input").SendKeys("test");
            Thread.Sleep(2000);
            WebDriver.GetByPath("//*[@id='searchContainer']/div/div[1]/span/button").Click();
            Thread.Sleep(5000);
            int position = 1;
            int claimedListingPosition = -1;
            IWebElement element = WebDriver.GetByPath("//*[@id='resaltsListing']/section[" + position + "]/div");

            while (element != null && claimedListingPosition < 0)
            {
                element.Click();
                if (ClaimListing())
                {
                    claimedListingPosition = position;
                }
                //navigate back to the search page
                WebDriver.Navigate().Back();
                //Give search engine time to return results
                Thread.Sleep(500);
                //move to next listing item
                position++;
                element = WebDriver.GetByPath("//*[@id='resaltsListing']/section[" + position + "]/div");
            }
        }


        public bool ClaimListing()
        {
            WebDriver.GetByPath("//*[@class='pull-right']/a[2]").Click();
            //1 report exists - go to next listing
            if (WebDriver.Exists(By.XPath("//*[@class='report-warning']/div[contains(text(), 'Report exist')]")))
            {
                return false;
            }
            //2 report listing
            WebDriver.GetByPath("//*[@id='report']/div/div/div[1]/div[1]/label/i").Click();
            WebDriver.GetByPath("//*[@id='report']/div/div/div[2]/input").Click();
            Thread.Sleep(500);
            //Text div that equals to "Your report has been sent to the seller"
            Assert.NotNull(WebDriver.GetByPath("//*[@id='listingsBriefInfo']/div[3]/div"));
            return true;
        }
        [Then(@"I log out")]
        public void ThenILogOut()
        {
        WebDriver.GetByPath("//a[contains(text(),'Logout')]").Click();
        }

        [Then(@"I fill description form default values at listing page \(with random title\)")]
        public void ThenIFillDescriptionFormDefaultValuesAtListingPageWithRandomTitle()
            {
            var h=new HandyFunctions();
            EnteredTitle = h.RandomStringLatinAndNumbers(25);
            WebDriver.GetByName("title").SendKeys(EnteredTitle);

            Thread.Sleep(2000);
            var conditionSelect =
                new SelectElement(WebDriver.GetByPath("//*[@class='listing-description-fieldset']/div[2]/div[2]/select"));
            conditionSelect.SelectByValue("0");

            var typeselect =
                new SelectElement(WebDriver.GetByPath("//*[@class='listing-description-fieldset']/div[2]/div[4]/select"));
            typeselect.SelectByValue("2");
            Thread.Sleep(2000);

            WebDriver.GetByPath("//textarea[@name='description']").SendKeys("test");

            WebDriver.GetByName("location").SendKeys("Kie");
            }

        [Then(@"I go to my listings")]
        public void ThenIGoToMyListings()
            {
            WebDriver.GetByPath("//*[@id='userFilters']/div[2]/label[1]/a/span").Click();
            //get to my listings

            var foundListing = false;
            int position = 0;
            //Cycle for find deeded listings
            while (!foundListing && WebDriver.Exists(
                By.XPath("//*[@id='resaltsListing']/section[" + ++position + "]/div[1]/article/div/div[1]/div")))
                {
                foundListing = CreateListingPageSteps.Url
                               == WebDriver.GetByPath("//*[@id='resaltsListing']/section[1]/div[1]/article/div/div[2]/a").GetAttribute("href");
                }
            Thread.Sleep(2000);
            Assert.True(foundListing);
            }

        [Then(@"I switch to My Listings")]
        public void ThenISwitchToMyListings()
            {
            WebDriver.GetByPath("//span[contains(text(),'My Listings')]").Click();
            }


        [Then(@"I log in as a buyer")]
        public void ThenILogInAsABuyer()
            {
            WebDriver.GetByName("Email").SendKeys(PartsBeeVariables.CustomerLogin);
            WebDriver.GetByName("Password").SendKeys(PartsBeeVariables.CustomerPassword);
            WebDriver.GetByName("submit-button").Click();
            }

        [Then(@"I make search of the created item title")]
        public void ThenIMakeSearchOfTheCreatedItemTitle()
            {
            WebDriver.GetByPath("//*[@id='autocomplete']").SendKeys(EnteredTitle);
            WebDriver.GetByPath("//button[@class='btn btn-warning go-search']").Click();
            }

        [Then(@"I open the first listing")]
        public void ThenIOpenTheFirstListing()
            {
            Thread.Sleep(2000);
            WebDriver.GetByPath("//*[@class='resalts-listing-view pull-left']/div/section[1]/div[1]").Click();
            WebDriver.GetByPath("//*[@class='resalts-listing-view pull-left']/div/section[1]/div[1]/div/img").Click();
            Thread.Sleep(3000);
            WebDriver.GetByPath("//*[@class='resalts-listing-view pull-left']/div/section[1]//a[@class='btn btn-default']").Click();
            }

        [Then(@"I report the listing as NotAvailable")]
        public void ThenIReportTheListingAsNotAvailable()
            {
            WebDriver.GetByPath("//a[contains(text(),'Report Item')]").Click();
            Thread.Sleep(1000);
            WebDriver.GetByPath("//*[@class='report-item']/div/div/div[1]/div[1]/div[2]/label").Click();
            WebDriver.GetByPath("//*[@class='report-item']/div/div/div[2]/div/input").SendKeys("Test report!");
            WebDriver.GetByPath("//*[@class='report-item']/div/div/div[2]/input").Click();
            Thread.Sleep(2000);

            try
                {
                WebDriver.GetByPath("//div[contains(text(),'Your report has been sent to the seller')]");
                }
            catch(WebDriverException)
                {
                Assert.Fail("Most likely our reporting wasn't quite successful!(((");
                }
            }

        [Then(@"I report the listing as IncorrectListing")]
        public void ThenIReportTheListingAsIncorrectListing()
            {
            WebDriver.GetByPath("//a[contains(text(),'Report Item')]").Click();
            Thread.Sleep(1000);
            WebDriver.GetByPath("//*[@class='report-item']/div/div/div[1]/div[1]/div[2]/label").Click();
            WebDriver.GetByPath("//*[@class='report-item']/div/div/div[2]/div/input").SendKeys("Test report!");
            WebDriver.GetByPath("//*[@class='report-item']/div/div/div[2]/input").Click();
            Thread.Sleep(2000);

            try
                {
                WebDriver.GetByPath("//div[contains(text(),'Your report has been sent to the seller')]");
                }
            catch (WebDriverException)
                {
                Assert.Fail("Most likely our reporting wasn't quite successful!(((");
                }
            }

        [Then(@"I report the listing as Fraud")]
        public void ThenIReportTheListingAsFraud()
            {
            WebDriver.GetByPath("//a[contains(text(),'Report Item')]").Click();
            Thread.Sleep(1000);
            WebDriver.GetByPath("//*[@class='report-item']/div/div/div[1]/div[1]/div[3]/label").Click();
            WebDriver.GetByPath("//*[@class='report-item']/div/div/div[2]/div/input").SendKeys("Test report!");
            WebDriver.GetByPath("//*[@class='report-item']/div/div/div[2]/input").Click();
            Thread.Sleep(2000);

            try
                {
                WebDriver.GetByPath("//div[contains(text(),'Your report has been sent to the seller')]");
                }
            catch (WebDriverException)
                {
                Assert.Fail("Most likely our reporting wasn't quite successful!(((");
                }
            }


        [Then(@"I click Fix for my last listing")]
        public void ThenIClickFixForMyLastListing()
            {
            Thread.Sleep(2000);
            WebDriver.GetByPath("//*[contains(text(),'Fix Listing')]").Click();
            }

        [Then(@"I mark my listing as Price Negotiable")]
        public void ThenIMarkMyListingAsPriceNegotiable()
            {
            WebDriver.GetByPath("//*[@class='negotiable-checkbox col-xs-3']/label/i").Click();
            }

        [Then(@"I click SubmitRevisions button")]
        public void ThenIClickSubmitRevisionsButton()
            {
            WebDriver.GetByPath("//*[@class='buttonbar-container']/div/div/button[1]").Click();
            }

        [Then(@"My listing should be available after fixing")]
        public void ThenMyListingShouldBeAvailableAfterFixing()
            {
            WebDriver.GetByPath("//*[contains(text(),'Your listing is ')]");
            WebDriver.GetByPath("//*[contains(text(),'Active')]");
            WebDriver.GetByPath("//*[contains(text(),'!')]");
            }

        [Then(@"There should be no listing with my EnteredTitle \(Frauded to death\)")]
        public void ThenThereShouldBeNoListingWithMyEnteredTitleFraudedToDeath()
            {
            WebDriver.GetByPath("//div[contains(text(),'No items found for this search criteria')]");
            }

        [Then(@"I enter a seller's email in mailinator and check it")]
        public void ThenIEnterASellerSEmailInMailinatorAndCheckIt()
            {
            WebDriver.GetByPath("//*[@id='inboxfield']").SendKeys(PartsBeeVariables.SellerLogin);
            WebDriver.GetByPath("//btn[@onclick='changeInbox();']").Click();
            }

        [Then(@"There should be my listing's unique title")]
        public void ThenThereShouldBeMyListingSUniqueTitle()
            {
            Thread.Sleep(1000);
            WebDriver.SwitchTo().Frame(WebDriver.GetByName("rendermail"));
            WebDriver.GetByPath("//*[contains(text(),'"+EnteredTitle+"')]");
            }


        [Then(@"I go to fix the listing using CheckListing button from the email")]
        public void ThenIGoToFixTheListingUsingCheckListingButtonFromTheEmail()
            {
            WebDriver.GetByPath("//img[@title='Check Listing']/..").Click();
            Thread.Sleep(1000);
            WebDriver.SwitchTo().Window(WebDriver.WindowHandles[1]);
            }


    }
}
