﻿Feature: Watchlist

	Background: 
	Given I open login page
	Then I login as Seller
	Then I redirect to main page
	Then I click SearchParts button

	@AddingToWatchListFromGallery
	Scenario: AddingToWatchListFromGallery
	Then I add the 1 st part to my watchlist
	Then I remember 1 st part title 
	Then I click on icon My Watchlist
	Then The inventory which was added to my watchlist should be there (I check the title)
	Then I remove the 1 st inventory from the watchlist
	Then I refresh the page
	Then I wait 2 seconds
	Then My watchlist should be empty

	@AddingToWatchListFromList
	Scenario: AddingToWatchListFromList
    Then I switch to a List view mode
	Then I add the 1 st part to my watchlist (List View)
	Then I remember 1 st part title (List View)
	Then I click on icon My Watchlist
	Then The inventory which was added to my watchlist should be there (I check the title)(ListMode)
	Then I remove the 1 st inventory from the watchlist (ListMode)
	Then I refresh the page
	Then I wait 2 seconds
	Then My watchlist should be empty

	@AddingToWatchListFromMap
	Scenario: AddingToWatchListFromMap
	Then I switch to Map view mode
	Then I select an item from the map
	Then I remember its title (map)
	Then I add the item to my watchlist (map)
	Then I click on icon My Watchlist
	Then The inventory which was added to my watchlist should be there (I check the title)(MapMode)
	Then I remove the 1 st inventory from the watchlist (MapMode)
	Then I refresh the page
	Then I wait 2 seconds
	Then My watchlist should be empty (map)

	@AddingToWatchListWithinThePart
	Scenario: AddingToWatchListWithinThePart
	Then I open the first listing
	Then I remember this listing's Title
	Then I add this listing to my watchlist
	Then I click on icon My Watchlist (within the item)
	Then The inventory which was added to my watchlist should be there (I check the title)
	Then I remove the 1 st inventory from the watchlist
	Then I refresh the page
	Then I wait 2 seconds
	Then My watchlist should be empty

	
