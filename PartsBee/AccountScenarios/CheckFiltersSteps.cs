﻿using System;
using System.Data.SqlTypes;
using System.Threading;
using System.Windows.Forms;
using Common;
using NUnit.Framework;
using TechTalk.SpecFlow;
using OpenQA.Selenium;

namespace PartsBee.AccountScenarios
{
    [Binding]
    public class CheckFiltersSteps : BaseSteps
    {
        private string actualFilter;
        string[] nocategoryauctions= new string[] {"AMAZON","LKQ","EBAY"};

        [When(@"I press button to hide filters")]
        public void WhenIPressButtonToHideFilters()
        {
            Assert.True(WebDriver.GetBy("//h2[contains(text(), 'SEARCH FILTERS')]").SetTimer(2000).Find().Displayed);
            WebDriver.GetByPath("//a[@class='hide-filters-icon pull-right']").Click();
        }

        [Then(@"I check that filters are hide")]
        public void ThenICheckThatFiltersAreHide()
        {
           Assert.True(WebDriver.GetBy("//div[@class='show-filters-panel-icon pull-left']").SetTimer(2000).Find().Displayed);
           WebDriver.GetByCss(".show-filters-panel-icon.pull-left").Click();
           Thread.Sleep(500);
        }

        [When(@"I click on checkbox OEM")]
        public void WhenIClickOnCheckboxOEM()
        {
            if (!WebDriver.GetByName("PartTypeFilter_0").Selected)
            {
                WebDriver.GetByPath("//label[@for='PartTypeFilter_0']/i").Click();
            }

            Thread.Sleep(3000);
            WebDriver.GetByPath("//div[@id='resaltsListing']/section[1]/div[1]").Click();
            Assert.True(("OEM").Equals(WebDriver.GetByPath("//section[1]/div[1]/article/div/div[1]/dl[3]/dd").Text));

            WebDriver.GetByPath("//label[@for='PartTypeFilter_0']/i").Click();
            Thread.Sleep(500);
            /*
              if (!WebDriver.GetByPath("//div[@id='placement']/div[2]/label[" + i + "]/input").Selected)
                    WebDriver.GetByPath("//div[@id='placement']/div[2]/label[" + i + "]/i").Click();
             */
            
        }

   

        [When(@"I click on checkbox Aftermarket")]
        public void WhenIClickOnCheckboxAftermarket()
        {
            if (!WebDriver.GetByName("PartTypeFilter_1").Selected)
            {
                WebDriver.GetByPath("//label[@for='PartTypeFilter_1']/i").Click();


            }

            Thread.Sleep(3000);
            WebDriver.GetByPath("//div[@id='resaltsListing']/section[1]/div[1]").Click();
            Assert.True(("Aftermarket").Equals(WebDriver.GetByPath("//section[1]/div[1]/article/div/div[1]/dl[3]/dd").Text));
            WebDriver.GetByPath("//label[@for='PartTypeFilter_1']/i").Click();
        }

        [When(@"I click on checkbox Unknown")]
        public void WhenIClickOnCheckboxUnknown()
        {
            if (!WebDriver.GetByName("PartTypeFilter_2").Selected)
            {
                WebDriver.GetByPath("//label[@for='PartTypeFilter_2']/i").Click();


            }

            Thread.Sleep(3000);
            WebDriver.GetByPath("//div[@id='resaltsListing']/section[1]/div[1]").Click();
            Assert.True(("Unknown").Equals(WebDriver.GetByPath("//section[1]/div[1]/article/div/div[1]/dl[3]/dd").Text));
            WebDriver.GetByPath("//label[@for='PartTypeFilter_2']/i").Click();
        }



        [Then(@"I check Part Type filters")]
        public void ThenITryToBeFunny()
        {
            string[] array = new string[] {"OEM", "Aftermarket", "Unknown" };

            for (int i = 0; i < array.Length; i++)
            {
                if (!WebDriver.GetByName("PartTypeFilter_" + i + "").Selected)
                    WebDriver.GetByPath("//label[@for='PartTypeFilter_" + i + "']/i").Click();
                Thread.Sleep(3000);
                WebDriver.GetByPath("//*[@class='resalts-listing-view pull-left']/div/section[1]/div[1]").Click();
                WebDriver.GetByPath("//*[@class='resalts-listing-view pull-left']/div/section[1]/div[1]/div/img").Click();
                Thread.Sleep(500);
                actualFilter =
                    (WebDriver.GetByPath(
                        "//section[1]/div[1]/article/div/div/dl/dt[contains (text(), 'Part Type')]/following-sibling::*")
                        .Text);
                if ((array[i]) != actualFilter)
                {
                    Assert.Fail("Filter is " + array[i] + "\n Actual is " + actualFilter);
                }
                
                WebDriver.GetByPath("//label[@for='PartTypeFilter_" + i + "']/i").Click();
                Thread.Sleep(3000);
            }
         
        }


        [Then(@"I check Condition filters")]
        public void ThenICheckConditionFilters()
        {
            string[] array = new string[] { "New", "Used", "Remanufactured" };

            for (int i = 0; i < array.Length; i++)
            {
                if (!WebDriver.GetByName("ConditionFilter_" + i + "").Selected)
                    WebDriver.GetByPath("//label[@for='ConditionFilter_" + i + "']/i").Click();
                Thread.Sleep(3000);
                WebDriver.GetByPath("//*[@class='resalts-listing-view pull-left']/div/section[1]/div[1]").Click();
                WebDriver.GetByPath("//*[@class='resalts-listing-view pull-left']/div/section[1]/div[1]/div/img").Click();
                Thread.Sleep(500);
                actualFilter =
                    (WebDriver.GetByPath(
                        "//section[1]/div[1]/article/div/div/dl/dt[contains (text(), 'Condition')]/following-sibling::*")
                        .Text);
                if ((array[i]) != actualFilter)
                {
                    Assert.Fail("Filter is " + array[i] + "\n Actual is " + actualFilter);
                }
                
                WebDriver.GetByPath("//label[@for='ConditionFilter_" + i + "']/i").Click();
                Thread.Sleep(3000);

             }
        }

        [Then(@"I check Shipping Policy filters")]
        public void ThenICheckPolicyFilters() 
        {
            WebDriver.GetByPath("//div[@class='filter-sections col-xs-12']/section[10]/div/ul/a").Click();
            string[] array = new string[] { "Local Pickup Only", "US Domestic", "Worldwide - US Based", "Worldwide" };
            for (int i = 0; i < array.Length; i++)
            {
                if (!WebDriver.GetByName("ShippingFilter_" + i + "").Selected)
                    WebDriver.GetByPath("//label[@for='ShippingFilter_" + i + "']/i").Click();
                Thread.Sleep(3000);
                WebDriver.ExecuteJavaScript("window.scrollTo(0, 0)");
                WebDriver.GetByPath("//*[@class='resalts-listing-view pull-left']/div/section[1]/div[1]").Click();
                WebDriver.GetByPath("//*[@class='resalts-listing-view pull-left']/div/section[1]/div[1]/div/img").Click();
                Thread.Sleep(500);
                actualFilter =
                    (WebDriver.GetByPath("//section[1]/div[1]/article/div/div/dl/dt[contains (text(), 'Shipping')]/following-sibling::*").Text);
                if ((array[i]) != actualFilter)
                {
                    Assert.Fail("Filter is " + array[i] + "\n but Actual is " + actualFilter);
                }
                
                WebDriver.GetByPath("//label[@for='ShippingFilter_" + i + "']/i").Click();
                Thread.Sleep(3000);

            }

        }

        [Then(@"I check Payment Policy filters")]
        public void ThenICheckPaymentPolicyFilters()
            {
            WebDriver.GetByPath("//div[@class='filter-sections col-xs-12']/section[9]/div/ul/a").Click();
            string[] array = new string[] { "Cash", "PayPal", "Check", "Money Order", "Wire Transfer", "ACH" };
            for (int i = 0; i < array.Length; i++)
                {
                if (!WebDriver.GetByName("PaymentFilter_" + i + "").Selected)
                    WebDriver.GetByPath("//label[@for='PaymentFilter_" + i + "']/i").Click();
                Thread.Sleep(3000);
                WebDriver.ExecuteJavaScript("window.scrollTo(0, 0)");
                WebDriver.GetByPath("//*[@class='resalts-listing-view pull-left']/div/section[1]/div[1]").Click();
                WebDriver.GetByPath("//*[@class='resalts-listing-view pull-left']/div/section[1]/div[1]/div/img").Click();
                Thread.Sleep(3000);
                WebDriver.GetByPath("//*[@class='resalts-listing-view pull-left']/div/section[1]//a[@class='btn btn-default']").Click();
                Thread.Sleep(500);

                WebDriver.GetByPath("//div[@class='listing-details-info']/div[1]/div[1]/div[1]/ul/li[4]/a").Click();

                try
                    {
                    WebDriver.GetByPath("//td[contains(text(),'"+array[i]+"')]");
                    }
                catch (WebDriverException)
                    {
                    Assert.Fail("Selected payment method is not available for this listing!(((");
                    }

                WebDriver.GetByPath("//span[contains(text(),'Back To Search Results')]").Click();
                Thread.Sleep(1000);

                WebDriver.GetByPath("//div[@class='filter-sections col-xs-12']/section[9]/div/ul/a").Click();
                Thread.Sleep(500);
                WebDriver.GetByPath("//label[@for='PaymentFilter_" + i + "']/i").Click();
                Thread.Sleep(3000);

                }
            }

        [Then(@"I check Return Policy filters")]
        public void ThenICheckReturnPolicyFilters()
            {
            string[] array = new string[] { "Returns not Accepted", "Returns Accepted" };
            for (int i = 0; i < array.Length; i++)
                {
                if (!WebDriver.GetByName("ReturnFilter_" + i + "").Selected)
                    WebDriver.GetByPath("//label[@for='ReturnFilter_" + i + "']/i").Click();
                Thread.Sleep(3000);
                WebDriver.ExecuteJavaScript("window.scrollTo(0, 0)");
                WebDriver.GetByPath("//*[@class='resalts-listing-view pull-left']/div/section[1]/div[1]").Click();
                WebDriver.GetByPath("//*[@class='resalts-listing-view pull-left']/div/section[1]/div[1]/div/img").Click();
                Thread.Sleep(3000);
                WebDriver.GetByPath("//*[@class='resalts-listing-view pull-left']/div/section[1]//a[@class='btn btn-default']").Click();
                Thread.Sleep(500);

                WebDriver.GetByPath("//div[@class='listing-details-info']/div[1]/div[1]/div[1]/ul/li[5]/a").Click();

                try
                    {
                    WebDriver.GetByPath("//h2[contains(text(),'" + array[i] + "')]");
                    }
                catch (WebDriverException)
                    {
                    Assert.Fail("Return policy is incorrect for this listing!(((");
                    }

                WebDriver.GetByPath("//span[contains(text(),'Back To Search Results')]").Click();
                Thread.Sleep(1000);

                WebDriver.GetByPath("//label[@for='ReturnFilter_" + i + "']/i").Click();
                Thread.Sleep(3000);

                }
            }


        [Then(@"I check Colors filters")]
        public void ThenICheckColorsFilters()
        {
            string[] array = new string[] { "New", "Used", "Remanufactured" };

            for (int i = 0; i < array.Length; i++)
            {
                if (!WebDriver.GetByName("ConditionFilter_" + i + "").Selected)
                    WebDriver.GetByPath("//label[@for='ConditionFilter_" + i + "']/i").Click();
                Thread.Sleep(3000);
                WebDriver.GetByPath("//div[@id='resaltsListing']/section[1]/div[1]").Click();
                Assert.True((array[i]).Equals(WebDriver.GetByPath("//section[1]/div[1]/article/div/div/dl/dt[contains (text(), 'Condition')]/following-sibling::*").Text));
                WebDriver.GetByPath("//label[@for='ConditionFilter_" + i + "']/i").Click();
                Thread.Sleep(3000);

            }
        }

        [Then(@"I switch to List Date: Oldest First")]
        public void ThenISwitchToListDateOldestFirst()
            {
            WebDriver.GetByPath("//div[@class='form-control dropdown-button-wrapper']/div").Click();
            Thread.Sleep(500);
            WebDriver.GetByPath("//a[contains(text(),'List Date: Oldest First')]").Click();
            Thread.Sleep(2000);
            }


        [Then(@"I check category filters using '(.*)' > '(.*)' > '(.*)' categories")]
        public void ThenICheckCategoryFiltersUsingCategories(string p0, string p1, string p2)
            {
            //-----------Входим в категорию
            WebDriver.GetByPath("//section[@class=' categories-section']//*[contains(text(),'"+p0+"')]").Click();
            Thread.Sleep(2000);

            WebDriver.GetByPath("//*[@class='resalts-listing-view pull-left']/div/section[1]/div[1]").Click();
            WebDriver.GetByPath("//*[@class='resalts-listing-view pull-left']/div/section[1]/div[1]/div/img").Click();
            Thread.Sleep(3000);
            WebDriver.GetByPath("//*[@class='resalts-listing-view pull-left']/div/section[1]//a[@class='btn btn-default']").Click();
            Thread.Sleep(500);

            try
                {
                WebDriver.GetByPath("//span[@class='breadcrumb-block']//*[contains(text(),'"+p0+"')]");
                }
            catch (WebDriverException)
                {
                Assert.Fail("Category is incorrect!");
                }

            WebDriver.GetByPath("//span[contains(text(),'Back To Search Results')]").Click();
            Thread.Sleep(1000);

            //-----------Входим в подкатегорию
            /*
            WebDriver.GetByPath("//section[@class=' categories-section']//*[contains(text(),'" + p0 + "')]").Click();
            Thread.Sleep(2000);
             */
            WebDriver.GetByPath("//section[@class=' categories-section']//*[contains(text(),'" + p1 + "')]").Click();
            Thread.Sleep(2000);

            WebDriver.GetByPath("//*[@class='resalts-listing-view pull-left']/div/section[1]/div[1]").Click();
            WebDriver.GetByPath("//*[@class='resalts-listing-view pull-left']/div/section[1]/div[1]/div/img").Click();
            Thread.Sleep(3000);
            WebDriver.GetByPath("//*[@class='resalts-listing-view pull-left']/div/section[1]//a[@class='btn btn-default']").Click();
            Thread.Sleep(500);

            try
                {
                WebDriver.GetByPath("//span[@class='breadcrumb-block']//*[contains(text(),'" + p1 + "')]");
                }
            catch (WebDriverException)
                {
                WebDriver.GetByPath(".//*[@id='listingView']/div/div[3]/div/div/div/ul/li[3]/a").Click();
                Thread.Sleep(500);
                
                bool ok=false;

                foreach(string element in nocategoryauctions)
                    {
                    if (WebDriver.GetByPath(".//*[@id='sellerInfo']/div/div[1]/table/tbody/tr/td").Text == element)
                        ok = true;
                    }
                if (ok == false)
                    Assert.Fail("Category problem!(((");
                }

            WebDriver.GetByPath("//span[contains(text(),'Back To Search Results')]").Click();
            Thread.Sleep(1000);
        
            //-----------Входим во вторую подкатегорию
            /*
            WebDriver.GetByPath("//section[@class=' categories-section']//*[contains(text(),'" + p0 + "')]").Click();
            Thread.Sleep(2000);
            WebDriver.GetByPath("//section[@class=' categories-section']//*[contains(text(),'" + p1 + "')]").Click();
            Thread.Sleep(2000);
            */
            WebDriver.GetByPath("//section[@class=' categories-section']//*[contains(text(),'" + p2 + "')]").Click();
            Thread.Sleep(2000);

            WebDriver.GetByPath("//*[@class='resalts-listing-view pull-left']/div/section[1]/div[1]").Click();
            WebDriver.GetByPath("//*[@class='resalts-listing-view pull-left']/div/section[1]/div[1]/div/img").Click();
            Thread.Sleep(3000);
            WebDriver.GetByPath("//*[@class='resalts-listing-view pull-left']/div/section[1]//a[@class='btn btn-default']").Click();
            Thread.Sleep(500);

            try
                {
                WebDriver.GetByPath("//span[@class='breadcrumb-block']//*[contains(text(),'" + p2 + "')]");
                }
            catch (WebDriverException)
                {
                WebDriver.GetByPath(".//*[@id='listingView']/div/div[3]/div/div/div/ul/li[3]/a").Click();
                Thread.Sleep(500);

                bool ok = false;

                foreach (string element in nocategoryauctions)
                    {
                    if (WebDriver.GetByPath(".//*[@id='sellerInfo']/div/div[1]/table/tbody/tr/td").Text == element)
                        ok = true;
                    }
                if (ok == false)
                    Assert.Fail("Category problem!(((");
                //Assert.Fail("Sub-subcategory is incorrect!");
                }
            }

        [Then(@"I check placement filters")]
        public void ThenICheckPlacementFilters()
            {
            string[] array = new string[] { "Left", "Front", "Right", "Rear", "Bottom", "Top" };

            for (int i = 0; i < array.Length; i++)
                {
                if (!WebDriver.GetByPath("//section[@class=' placement-filter']/div/ul/li["+(i+1)+"]/label/input").Selected)
                    WebDriver.GetByPath("//section[@class=' placement-filter']/div/ul/li[" + (i + 1) + "]/label/i").Click();
                Thread.Sleep(3000);
                WebDriver.GetByPath("//*[@class='resalts-listing-view pull-left']/div/section[1]/div[1]").Click();
                WebDriver.GetByPath("//*[@class='resalts-listing-view pull-left']/div/section[1]/div[1]/div/img").Click();
                Thread.Sleep(500);
                actualFilter =
                    (WebDriver.GetByPath(
                        "//section[1]/div[1]/article/div/div/dl/dt[contains (text(), 'Placement')]/following-sibling::*")
                        .Text);
                if (!actualFilter.Contains(array[i]))
                    {
                    Assert.Fail("Filter is " + array[i] + "\n Actual is " + actualFilter);
                    }

                WebDriver.GetByPath("//section[@class=' placement-filter']/div/ul/li[" + (i + 1) + "]/label/i").Click();
                Thread.Sleep(3000);
                }
            }

        [Then(@"I check color filters using '(.*)', '(.*)', '(.*)'")]
        public void ThenICheckColorFiltersUsing(string p0, string p1, string p2)
            {
            string[] array = new string[3];

            array[0] = p0;
            array[1] = p1;
            array[2] = p2;

            WebDriver.GetByPath("//ul[@class='color-filter']/../*[contains(text(),'Show More...')]").Click();

            Thread.Sleep(500);

            for(int i=0; i<3; i++)
                {
                Thread.Sleep(500);
                WebDriver.GetByPath("//ul[@class='color-filter']//span[contains(text(),'"+array[i]+"')]/../label").Click();
                Thread.Sleep(3000);

                WebDriver.GetByPath("//*[@class='resalts-listing-view pull-left']/div/section[1]/div[1]").Click();
                WebDriver.GetByPath("//*[@class='resalts-listing-view pull-left']/div/section[1]/div[1]/div/img").Click();
                Thread.Sleep(3000);
                WebDriver.GetByPath("//*[@class='resalts-listing-view pull-left']/div/section[1]//a[@class='btn btn-default']").Click();
                Thread.Sleep(500);

                try
                    {
                    WebDriver.GetByPath("//div[@id='listingDescription']//*[contains(text(),'"+array[i]+"')]");
                    }
                catch(WebDriverException)
                    {
                    Assert.Fail("Color is not present where it should be.");
                    }

                WebDriver.GetByPath("//span[contains(text(),'Back To Search Results')]").Click();
                Thread.Sleep(1000);
                WebDriver.GetByPath("//ul[@class='color-filter']/../*[contains(text(),'Show More...')]").Click();
                Thread.Sleep(500);
                WebDriver.GetByPath("//ul[@class='color-filter']//span[contains(text(),'" + array[i] + "')]/../label").Click();
                Thread.Sleep(3000);
                }
            
            }

        [Then(@"I check Make filters using '(.*)'")]
        public void ThenICheckMakeFiltersUsing(string p0)
            {
            string SelectedMake = p0;
            WebDriver.GetByPath("//section[@class=' make-filter']/div/input").SendKeys(SelectedMake);
            Thread.Sleep(1000);

            WebDriver.GetByPath("//*[@class='resalts-listing-view pull-left']/div/section[1]/div[1]").Click();
            WebDriver.GetByPath("//*[@class='resalts-listing-view pull-left']/div/section[1]/div[1]/div/img").Click();
            Thread.Sleep(3000);
            WebDriver.GetByPath("//*[@class='resalts-listing-view pull-left']/div/section[1]//a[@class='btn btn-default']").Click();
            Thread.Sleep(500);

            Assert.IsTrue(WebDriver.GetByPath("//section[@class='listing-brief-info']//h2").Text.Contains(SelectedMake.ToUpper()));
            }

        [Then(@"I check PartsWithVINFilters filters")]
        public void ThenICheckPartsWithVINFiltersFilters()
            {
            WebDriver.GetByPath("//span[contains(text(),'Parts with VIN')]/../i").Click();
            Thread.Sleep(3000);

            WebDriver.GetByPath("//*[@class='resalts-listing-view pull-left']/div/section[1]/div[1]").Click();
            WebDriver.GetByPath("//*[@class='resalts-listing-view pull-left']/div/section[1]/div[1]/div/img").Click();
            Thread.Sleep(3000);
            WebDriver.GetByPath("//*[@class='resalts-listing-view pull-left']/div/section[1]//a[@class='btn btn-default']").Click();
            Thread.Sleep(500);

            Assert.IsTrue(WebDriver.GetByPath("//th[contains(text(),'VIN')]/following-sibling::*").Text != "");
            }

        [Then(@"I set a category to '(.*)' > '(.*)' > '(.*)'")]
        public void ThenISetACategoryTo(string p0, string p1, string p2)
            {
            //-----------Входим во вторую подкатегорию

            WebDriver.GetByPath("//section[@class=' categories-section']//*[contains(text(),'" + p0 + "')]").Click();
            Thread.Sleep(2000);
            WebDriver.GetByPath("//section[@class=' categories-section']//*[contains(text(),'" + p1 + "')]").Click();
            Thread.Sleep(2000);
            WebDriver.GetByPath("//section[@class=' categories-section']//*[contains(text(),'" + p2 + "')]").Click();
            Thread.Sleep(2000);
            }

        [Then(@"I click the Reset button")]
        public void ThenIClickTheResetButton()
            {
            WebDriver.GetByPath("//*[contains(text(),'Reset')]").Click();
            Thread.Sleep(2000);
            }

        [Then(@"I click the HideNegotiable filter checkbox")]
        public void ThenIClickTheHideNegotiableFilterCheckbox()
            {
            WebDriver.GetByPath("//span[contains(text(),'Reset')]/../i").Click();
            Thread.Sleep(3000);
            }


        [Then(@"this listing should not be marked as PriceNegotiable")]
        public void ThenThisListingShouldNotBeMarkedAsPriceNegotiable()
            {
            WebDriver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(2));
            try
                {
                WebDriver.GetByPath("//*[contains(text(),'Price is Negotiable')]");
                Assert.Fail("This listing should NOT be marked as Price Negotiable!(((");
                }
            catch(WebDriverException)
            { }
            WebDriver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(BaseSteps.StandardTimeOut));
            }

        [Then(@"I check that categories are there")]
        public void ThenICheckThatCategoriesAreThere()
            {
            WebDriver.GetByPath("//section[@class=' categories-section']");
            }

        [Then(@"I collapse Categories")]
        public void ThenICollapseCategories()
            {
            WebDriver.GetByPath("//h2[contains(text(),'Category')]/following-sibling::*").Click();
            Thread.Sleep(500);
            }

        [Then(@"I check that categories are hidden")]
        public void ThenICheckThatCategoriesAreHidden()
            {
            WebDriver.GetByPath("//section[@class='collapsed-filter categories-section']");
            }

        [Then(@"I expand categories")]
        public void ThenIExpandCategories()
            {
            WebDriver.GetByPath("//h2[contains(text(),'Category')]/following-sibling::*").Click();
            Thread.Sleep(500);
            }

        [Then(@"I check that Part Types are there")]
        public void ThenICheckThatPartTypesAreThere()
            {
            Assert.IsTrue(WebDriver.GetByPath("//h2[contains(text(),'Part Type')]/following-sibling::*").
                GetAttribute("class")=="pull-right glyphicon glyphicon-menu-down");
            }

        [Then(@"I collapse Part Types")]
        public void ThenICollapsePartTypes()
            {
            WebDriver.GetByPath("//h2[contains(text(),'Part Type')]/following-sibling::*").Click();
            Thread.Sleep(500);
            }

        [Then(@"I check that Part Types are hidden")]
        public void ThenICheckThatPartTypesAreHidden()
            {
            Assert.IsTrue(WebDriver.GetByPath("//h2[contains(text(),'Part Type')]/following-sibling::*").
                GetAttribute("class") == "pull-right glyphicon glyphicon-menu-right");
            }

        [Then(@"I expand Part Types")]
        public void ThenIExpandPartTypes()
            {
            WebDriver.GetByPath("//h2[contains(text(),'Part Type')]/following-sibling::*").Click();
            Thread.Sleep(500);
            }

        [Then(@"I check that NegotiablePrice is there")]
        public void ThenICheckThatNegotiablePriceIsThere()
            {
            Assert.IsTrue(WebDriver.GetByPath("//h2[contains(text(),'Negotiable Price')]/following-sibling::*").
                GetAttribute("class") == "pull-right glyphicon glyphicon-menu-down");
            }

        [Then(@"I collapse NegotiablePrice")]
        public void ThenICollapseNegotiablePrice()
            {
            WebDriver.GetByPath("//h2[contains(text(),'Negotiable Price')]/following-sibling::*").Click();
            Thread.Sleep(500);
            }

        [Then(@"I check that NegotiablePrice is hidden")]
        public void ThenICheckThatNegotiablePriceIsHidden()
            {
            Assert.IsTrue(WebDriver.GetByPath("//h2[contains(text(),'Negotiable Price')]/following-sibling::*").
                GetAttribute("class") == "pull-right glyphicon glyphicon-menu-right");
            }

        [Then(@"I expand NegotiablePrice")]
        public void ThenIExpandNegotiablePrice()
            {
            WebDriver.GetByPath("//h2[contains(text(),'Negotiable Price')]/following-sibling::*").Click();
            Thread.Sleep(500);
            }

        [Then(@"I check that Condition is there")]
        public void ThenICheckThatConditionIsThere()
            {
            Assert.IsTrue(WebDriver.GetByPath("//h2[contains(text(),'Condition')]/following-sibling::*").
                GetAttribute("class") == "pull-right glyphicon glyphicon-menu-down");
            }

        [Then(@"I collapse Condition")]
        public void ThenICollapseCondition()
            {
            WebDriver.GetByPath("//h2[contains(text(),'Condition')]/following-sibling::*").Click();
            Thread.Sleep(500);
            }

        [Then(@"I check that Condition is hidden")]
        public void ThenICheckThatConditionIsHidden()
            {
            Assert.IsTrue(WebDriver.GetByPath("//h2[contains(text(),'Condition')]/following-sibling::*").
               GetAttribute("class") == "pull-right glyphicon glyphicon-menu-right");
            }

        [Then(@"I expand Condition")]
        public void ThenIExpandCondition()
            {
            WebDriver.GetByPath("//h2[contains(text(),'Condition')]/following-sibling::*").Click();
            Thread.Sleep(500);
            }

        [Then(@"I check that Placement is there")]
        public void ThenICheckThatPlacementIsThere()
            {
            Assert.IsTrue(WebDriver.GetByPath("//h2[contains(text(),'Placement')]/following-sibling::*").
                GetAttribute("class") == "pull-right glyphicon glyphicon-menu-down");
            }

        [Then(@"I collapse Placement")]
        public void ThenICollapsePlacement()
            {
            WebDriver.GetByPath("//h2[contains(text(),'Placement')]/following-sibling::*").Click();
            Thread.Sleep(500);
            }

        [Then(@"I check that Placement is hidden")]
        public void ThenICheckThatPlacementIsHidden()
            {
            Assert.IsTrue(WebDriver.GetByPath("//h2[contains(text(),'Placement')]/following-sibling::*").
                GetAttribute("class") == "pull-right glyphicon glyphicon-menu-right");
            }

        [Then(@"I expand Placement")]
        public void ThenIExpandPlacement()
            {
            WebDriver.GetByPath("//h2[contains(text(),'Placement')]/following-sibling::*").Click();
            Thread.Sleep(500);
            }

        [Then(@"I check that Colors are there")]
        public void ThenICheckThatColorsAreThere()
            {
            Assert.IsTrue(WebDriver.GetByPath("//h2[contains(text(),'Colors')]/following-sibling::*").
                GetAttribute("class") == "pull-right glyphicon glyphicon-menu-down");
            }

        [Then(@"I collapse Colors")]
        public void ThenICollapseColors()
            {
            WebDriver.GetByPath("//h2[contains(text(),'Colors')]/following-sibling::*").Click();
            Thread.Sleep(500);
            }

        [Then(@"I check that Colors are hidden")]
        public void ThenICheckThatColorsAreHidden()
            {
            Assert.IsTrue(WebDriver.GetByPath("//h2[contains(text(),'Colors')]/following-sibling::*").
                GetAttribute("class") == "pull-right glyphicon glyphicon-menu-right");
            }

        [Then(@"I expand Colors")]
        public void ThenIExpandColors()
            {
            WebDriver.GetByPath("//h2[contains(text(),'Colors')]/following-sibling::*").Click();
            Thread.Sleep(500);
            }

        [Then(@"I check that Make is there")]
        public void ThenICheckThatMakeIsThere()
            {
            Assert.IsTrue(WebDriver.GetByPath("//h2[contains(text(),'Make')]/following-sibling::*").
                GetAttribute("class") == "pull-right glyphicon glyphicon-menu-down");
            }

        [Then(@"I collapse Make")]
        public void ThenICollapseMake()
            {
            WebDriver.GetByPath("//h2[contains(text(),'Make')]/following-sibling::*").Click();
            Thread.Sleep(500);
            }

        [Then(@"I check that Make is hidden")]
        public void ThenICheckThatMakeIsHidden()
            {
            Assert.IsTrue(WebDriver.GetByPath("//h2[contains(text(),'Make')]/following-sibling::*").
                GetAttribute("class") == "pull-right glyphicon glyphicon-menu-right");
            }

        [Then(@"I expand Make")]
        public void ThenIExpandMake()
            {
            WebDriver.GetByPath("//h2[contains(text(),'Make')]/following-sibling::*").Click();
            Thread.Sleep(500);
            }

        [Then(@"I check that PaymentPolicy is there")]
        public void ThenICheckThatPaymentPolicyIsThere()
            {
            Assert.IsTrue(WebDriver.GetByPath("//h2[contains(text(),'Payment Policy')]/following-sibling::*").
                GetAttribute("class") == "pull-right glyphicon glyphicon-menu-down");
            }

        [Then(@"I collapse PaymentPolicy")]
        public void ThenICollapsePaymentPolicy()
            {
            WebDriver.GetByPath("//h2[contains(text(),'Payment Policy')]/following-sibling::*").Click();
            Thread.Sleep(500);
            }

        [Then(@"I check that PaymentPolicy is hidden")]
        public void ThenICheckThatPaymentPolicyIsHidden()
            {
            Assert.IsTrue(WebDriver.GetByPath("//h2[contains(text(),'Payment Policy')]/following-sibling::*").
                GetAttribute("class") == "pull-right glyphicon glyphicon-menu-right");
            }

        [Then(@"I expand PaymentPolicy")]
        public void ThenIExpandPaymentPolicy()
            {
            WebDriver.GetByPath("//h2[contains(text(),'Payment Policy')]/following-sibling::*").Click();
            Thread.Sleep(500);
            }

        [Then(@"I check that ShippingPolicy is there")]
        public void ThenICheckThatShippingPolicyIsThere()
            {
            Assert.IsTrue(WebDriver.GetByPath("//h2[contains(text(),'Shipping Policy')]/following-sibling::*").
                GetAttribute("class") == "pull-right glyphicon glyphicon-menu-down");
            }

        [Then(@"I collapse ShippingPolicy")]
        public void ThenICollapseShippingPolicy()
            {
            WebDriver.GetByPath("//h2[contains(text(),'Shipping Policy')]/following-sibling::*").Click();
            Thread.Sleep(500);
            }

        [Then(@"I check that ShippingPolicy is hidden")]
        public void ThenICheckThatShippingPolicyIsHidden()
            {
            Assert.IsTrue(WebDriver.GetByPath("//h2[contains(text(),'Shipping Policy')]/following-sibling::*").
                GetAttribute("class") == "pull-right glyphicon glyphicon-menu-right");
            }

        [Then(@"I expand ShippingPolicy")]
        public void ThenIExpandShippingPolicy()
            {
            WebDriver.GetByPath("//h2[contains(text(),'Shipping Policy')]/following-sibling::*").Click();
            Thread.Sleep(500);
            }

        [Then(@"I check that ReturnPolicy is there")]
        public void ThenICheckThatReturnPolicyIsThere()
            {
            Assert.IsTrue(WebDriver.GetByPath("//h2[contains(text(),'Return Policy')]/following-sibling::*").
                GetAttribute("class") == "pull-right glyphicon glyphicon-menu-down");
            }

        [Then(@"I collapse ReturnPolicy")]
        public void ThenICollapseReturnPolicy()
            {
            WebDriver.GetByPath("//h2[contains(text(),'Return Policy')]/following-sibling::*").Click();
            Thread.Sleep(500);
            }

        [Then(@"I check that ReturnPolicy is hidden")]
        public void ThenICheckThatReturnPolicyIsHidden()
            {
            Assert.IsTrue(WebDriver.GetByPath("//h2[contains(text(),'Return Policy')]/following-sibling::*").
                GetAttribute("class") == "pull-right glyphicon glyphicon-menu-right");
            }

        [Then(@"I expand ReturnPolicy")]
        public void ThenIExpandReturnPolicy()
            {
            WebDriver.GetByPath("//h2[contains(text(),'Return Policy')]/following-sibling::*").Click();
            Thread.Sleep(500);
            }

        [Then(@"I check that ListingDate is there")]
        public void ThenICheckThatListingDateIsThere()
            {
            Assert.IsTrue(WebDriver.GetByPath("//h2[contains(text(),'Listing Date')]/following-sibling::*").
                GetAttribute("class") == "pull-right glyphicon glyphicon-menu-down");
            }

        [Then(@"I collapse ListingDate")]
        public void ThenICollapseListingDate()
            {
            WebDriver.GetByPath("//h2[contains(text(),'Listing Date')]/following-sibling::*").Click();
            Thread.Sleep(500);
            }

        [Then(@"I check that ListingDate is hidden")]
        public void ThenICheckThatListingDateIsHidden()
            {
            Assert.IsTrue(WebDriver.GetByPath("//h2[contains(text(),'Listing Date')]/following-sibling::*").
                GetAttribute("class") == "pull-right glyphicon glyphicon-menu-right");
            }

        [Then(@"I expand ListingDate")]
        public void ThenIExpandListingDate()
            {
            WebDriver.GetByPath("//h2[contains(text(),'Listing Date')]/following-sibling::*").Click();
            Thread.Sleep(500);
            }

        [Then(@"I check that ShowOnly is there")]
        public void ThenICheckThatShowOnlyIsThere()
            {
            Assert.IsTrue(WebDriver.GetByPath("//h2[contains(text(),'Show only')]/following-sibling::*").
                GetAttribute("class") == "pull-right glyphicon glyphicon-menu-down");
            }

        [Then(@"I collapse ShowOnly")]
        public void ThenICollapseShowOnly()
            {
            WebDriver.GetByPath("//h2[contains(text(),'Show only')]/following-sibling::*").Click();
            Thread.Sleep(500);
            }

        [Then(@"I check that ShowOnly is hidden")]
        public void ThenICheckThatShowOnlyIsHidden()
            {
            Assert.IsTrue(WebDriver.GetByPath("//h2[contains(text(),'Show only')]/following-sibling::*").
                GetAttribute("class") == "pull-right glyphicon glyphicon-menu-right");
            }

        [Then(@"I expand ShowOnly")]
        public void ThenIExpandShowOnly()
            {
            WebDriver.GetByPath("//h2[contains(text(),'Show only')]/following-sibling::*").Click();
            Thread.Sleep(500);
            }

        [Then(@"I check that Price is there")]
        public void ThenICheckThatPriceIsThere()
            {
            Assert.IsTrue(WebDriver.GetByPath("//h2[contains(text(),'Price')]/following-sibling::*").
               GetAttribute("class") == "pull-right glyphicon glyphicon-menu-down");
            }

        [Then(@"I collapse Price")]
        public void ThenICollapsePrice()
            {
            WebDriver.GetByPath("//h2[contains(text(),'Price')]/following-sibling::*").Click();
            Thread.Sleep(500);
            }

        [Then(@"I check that Price is hidden")]
        public void ThenICheckThatPriceIsHidden()
            {
            Assert.IsTrue(WebDriver.GetByPath("//h2[contains(text(),'Price')]/following-sibling::*").
               GetAttribute("class") == "pull-right glyphicon glyphicon-menu-right");
            }

        [Then(@"I expand Price")]
        public void ThenIExpandPrice()
            {
            WebDriver.GetByPath("//h2[contains(text(),'Price')]/following-sibling::*").Click();
            Thread.Sleep(500);
            }

        [Then(@"I collapse All filters")]
        public void ThenICollapseAllFilters()
            {
            WebDriver.GetByPath("//a[contains(text(),'Collapse all')]").Click();
            Thread.Sleep(500);
            }
        
        [Then(@"I close the whole filters panel")]
        public void ThenICloseTheWholeFiltersPanel()
            {
            WebDriver.GetByPath("//h2[contains(text(),'SEARCH FILTERS')]/following-sibling::*").Click();
            Thread.Sleep(500);
            }

        [Then(@"I check that the filters panel is closed")]
        public void ThenICheckThatTheFiltersPanelIsClosed()
            {
            WebDriver.GetByPath("//b[@class='glyphicon glyphicon-menu-right pull-right']");
            }

        [Then(@"I reopen the filters panel")]
        public void ThenIReopenTheFiltersPanel()
            {
            WebDriver.GetByPath("//b[@class='glyphicon glyphicon-menu-right pull-right']").Click();
            Thread.Sleep(500);
            }

        [Then(@"I check that the filters panel is opened")]
        public void ThenICheckThatTheFiltersPanelIsOpened()
            {
            Thread.Sleep(500);
            WebDriver.GetByPath("//a[@class='glyphicon glyphicon-menu-left']");
            }


        }
 }


