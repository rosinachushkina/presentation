﻿using System;
using System.Security.Cryptography;
using Common;
using NUnit.Framework;
using TechTalk.SpecFlow;

namespace PartsBee.AccountScenarios
{
    [Binding]
    public class LoginNegativeSteps : BaseSteps
    {
        [Then(@"I recieved error notification for login page")]
        public void ThenIRecievedErrorNotificationForLoginPage()
        {
            Assert.True(WebDriver.GetBy("//span[contains(text(), 'The Email field is required.')]").SetTimer(1000).Find().Displayed);
            Assert.True(WebDriver.GetBy("//span[contains(text(), 'The Password field is required.')]").SetTimer(1000).Find().Displayed);
        }

        [Then(@"I recieved password error notification for login page")]
        public void ThenIRecievedPasswordErrorNotificationForLoginPage()
        {
            Assert.True(WebDriver.GetBy("//span[contains(text(), 'The Password field is required.')]").SetTimer(1000).Find().Displayed);
        }

        [Then(@"I recieved email error notification for login page")]
        public void ThenIRecievedEmailErrorNotificationForLoginPage()
        {
            Assert.True(WebDriver.GetBy("//span[contains(text(), 'This value seems to be invalid.')]").SetTimer(1000).Find().Displayed);
        }

        [Then(@"I enter incorrect email (.*) to email adress input")]
        public void ThenIEnterIncorrectEmailAasdasdToEmailAdressInput(string inEmail)
        {
            WebDriver.GetById("Email").SendKeys(inEmail);
        }

        [Then(@"I recieved email error notification for login page about correct email type")]
        public void ThenIRecievedEmailErrorNotificationForLoginPageAboutCorrectEmailType()
        {
            Assert.True(WebDriver.GetBy("//span[contains(text(), 'Please type correct email address.')]").SetTimer(1000).Find().Displayed);
            Assert.True(WebDriver.GetBy("//span[contains(text(), 'The Password field is required.')]").SetTimer(1000).Find().Displayed);
        }

        [Then(@"I recieved email error notification for login page for null email")]
        public void ThenIRecievedEmailErrorNotificationForLoginPageForNullEmail()
        {
            Assert.True(WebDriver.GetBy("//span[contains(text(), 'The Email field is required.')]").SetTimer(1000).Find().Displayed);
            
        }

        [Then(@"I clean email input")]
        public void ThenICleanEmailInput()
        {
            WebDriver.GetById("Email").Clear();
        }

        [Then(@"I enter incorrect password to Password input")]
        public void ThenIEnterIncorrectPasswordToPasswordInput()
        {
            var Handy = new HandyFunctions();
            string password = Handy.RandomStringLatin(10);
            WebDriver.GetByName("Password").SendKeys(password);
        }
        [Then(@"I recieved error notification about incorrect password")]
        public void ThenIRecievedErrorNotificationAboutIncorrectPassword()
        {
            Assert.True(WebDriver.GetBy("//span[contains(text(), 'Invalid username or password!')]").SetTimer(1000).Find().Displayed);
        }

    }
}
