﻿using Common;
using Common.Generators;
using NUnit.Framework;
using TechTalk.SpecFlow;

namespace PartsBee.AccountScenarios
{
    [Binding]
    public class NegativeRegistrationSteps : BaseSteps
    {
        [Then(@"I enter correct email to email adress input")]
        public void ThenIEnterCorrectEmailEmailToEmailAdressInput()
        {
            WebDriver.GetByName("Email").Clear();
            WebDriver.GetByName("Email").SendKeys(EmailGenerator.GenerateEmail());
        }

        [Then(@"I enter first name to First Name input")]
        public void ThenIEnterFirstNameToFirstNameInput()
        {
            WebDriver.GetByName("FirstName").SendKeys(NamesGenerator.GetFirstName());
        }

        [Then(@"I enter last name to Last Name input")]
        public void ThenIEnterLastNameToLastNameInput()
        {
            WebDriver.GetByName("LastName").SendKeys(NamesGenerator.GetLastName());
        }

        [Then(@"I enter password to Password input")]
        public void ThenIEnterPasswordToPasswordInput()
        {
            WebDriver.GetByName("Password").SendKeys(PasswordGenerator.GeneratePassword());
        }


        [Then(@"I recieved error notification")]
        public void ThenIRecievedErrorNotification()
        {
            Assert.True(WebDriver.GetBy("//span[contains(text(), 'The Email field is required.')]").SetTimer(1000).Find().Displayed);
            Assert.True(WebDriver.GetBy("//span[contains(text(), 'The First name field is required.')]").SetTimer(1000).Find().Displayed);
            Assert.True(WebDriver.GetBy("//span[contains(text(), 'The Last name field is required.')]").SetTimer(1000).Find().Displayed);
            Assert.True(WebDriver.GetBy("//span[contains(text(), 'The Password field is required.')]").SetTimer(1000).Find().Displayed);
            Assert.True(WebDriver.GetBy("//span[contains(text(), 'Please read and accept agreement before proceeding!')]").SetTimer(1000).Find().Displayed);
            Assert.True(WebDriver.GetBy("//span[contains(text(), 'Please read and accept privacy policy before proceeding!')]").SetTimer(1000).Find().Displayed);
        }

        [Then(@"I recieved error notification without email error")]
        public void ThenIRecievedErrorNotificationWithoutEmailError()
        {
            Assert.True(WebDriver.GetBy("//span[contains(text(), 'The First name field is required.')]").SetTimer(1000).Find().Displayed);
            Assert.True(WebDriver.GetBy("//span[contains(text(), 'The Last name field is required.')]").SetTimer(1000).Find().Displayed);
            Assert.True(WebDriver.GetBy("//span[contains(text(), 'The Password field is required.')]").SetTimer(1000).Find().Displayed);
            Assert.True(WebDriver.GetBy("//span[contains(text(), 'Please read and accept agreement before proceeding!')]").SetTimer(1000).Find().Displayed);
            Assert.True(WebDriver.GetBy("//span[contains(text(), 'Please read and accept privacy policy before proceeding!')]").SetTimer(1000).Find().Displayed);
        }

        [Then(@"I recieved error notification without email/Fn error")]
        public void ThenIRecievedErrorNotificationWithoutEmailFnError()
        {
            Assert.True(WebDriver.GetBy("//span[contains(text(), 'The Last name field is required.')]").SetTimer(1000).Find().Displayed);
            Assert.True(WebDriver.GetBy("//span[contains(text(), 'The Password field is required.')]").SetTimer(1000).Find().Displayed);
            Assert.True(WebDriver.GetBy("//span[contains(text(), 'Please read and accept agreement before proceeding!')]").SetTimer(1000).Find().Displayed);
            Assert.True(WebDriver.GetBy("//span[contains(text(), 'Please read and accept privacy policy before proceeding!')]").SetTimer(1000).Find().Displayed);
        }

        [Then(@"I recieved error notification without email/Fn/Ln error")]
        public void ThenIRecievedErrorNotificationWithoutEmailFnLnError()
        {
            Assert.True(WebDriver.GetBy("//span[contains(text(), 'The Password field is required.')]").SetTimer(1000).Find().Displayed);
            Assert.True(WebDriver.GetBy("//span[contains(text(), 'Please read and accept agreement before proceeding!')]").SetTimer(1000).Find().Displayed);
            Assert.True(WebDriver.GetBy("//span[contains(text(), 'Please read and accept privacy policy before proceeding!')]").SetTimer(1000).Find().Displayed);
        }

        [Then(@"I recieved error notification without email/Fn/Ln/Ps error")]
        public void ThenIRecievedErrorNotificationWithoutEmailFnLnPsError()
        {
            Assert.True(WebDriver.GetBy("//span[contains(text(), 'Please read and accept agreement before proceeding!')]").SetTimer(1000).Find().Displayed);
            Assert.True(WebDriver.GetBy("//span[contains(text(), 'Please read and accept privacy policy before proceeding!')]").SetTimer(1000).Find().Displayed);
        }

        [Then(@"I recieved error notification without email/Fn/Ln/Ps/Term&Services error")]
        public void ThenIRecievedErrorNotificationWithoutEmailFnLnPsTermServicesError()
        {
            Assert.True(WebDriver.GetBy("//span[contains(text(), 'Please read and accept privacy policy before proceeding!')]").SetTimer(1000).Find().Displayed);
        }

        [Then(@"I enter existed email")]
        public void ThenIEnterExistedEmailTestuser_Com()
        {
            WebDriver.GetByPath("//input[@id='Email']").Clear();
            WebDriver.GetByPath("//input[@id='Email']").SendKeys(PartsBeeVariables.SellerLogin);
        }

        [Then(@"I recieved error notification about existed email")]
        public void ThenIRecievedErrorNotificationAboutExistedEmail()
        {
            Assert.True(WebDriver.GetBy("//span[contains(text(), 'Email already exist')]").SetTimer(1000).Find().Displayed);
        }


        [Then(@"I recieved error notification about icorrect password")]
        public void ThenIRecievedErrorNotificationAboutIcorrectPassword()
        {
            Assert.True(WebDriver.GetBy("//span[contains(text(), 'The Password must be up to 6 characters long.')]").SetTimer(1000).Find().Displayed);
        }
       
        
    }
}
