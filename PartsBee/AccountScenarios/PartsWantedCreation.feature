﻿Feature: PartsWantedCreation
	In order create search list
	I as a customer
	Create Parts Wanted list

	Background: 
	Given I open login page
	Then I login as Buyer
	Then I press PARTS WANTED Button
	

@CreationPartsWantedWithAutomatedDecodingVIN
Scenario: Creation Parts Wanted List with automated decoding VIN
	Then I type VIN in VIN input
	Then I click Find My Vehicle button
	Then I check prhrase You are ready to Buy
	Then I type trim for parts wanted
	Then I check titles of Vehicle
	Then I check trim in header
	Then I check that image of found Vehicle displayed

	Then I click continue to Step 2

	Then I check titles of Vehicle
	Then I check trim
	Then I check prhrase You are ready to Buy
	Then I click 1 part from 2 subcategory
	Then I check that counter of selected parts changed display 1
	Then I click 1 part from 2 subcategory
	Then I check that counter of selected parts changed display 0
	Then I check that all checkboxes unselected
	
	Then I click 1 part from 2 subcategory
	Then I click 2 part from 2 subcategory
	Then I click 3 part from 2 subcategory
	Then I check that counter of selected parts changed display 3

	Then I delete choosed parts by clicking Clear All button
	Then I check that counter of selected parts changed display 0
	Then I check that all checkboxes unselected

	Then I type in search Parts input Bug Shield
	Then I check that choosed part checkbox selected
	Then I click on Interior parts
	Then I type in search Parts input Alarms & Security
	Then I check that choosed part checkbox selected
	Then I click on Mechanical parts
	Then I type in search Parts input O-Ring & Kits
	Then I check that choosed part checkbox selected

	Then I check that counter of selected parts changed display 3
	
	Then I click continue to Step 3
	Then I check parts names
	Then I check title on third step in header
	Then I check VIN on third step in header
	Then I check number of selected parts on third step in header
	Then I check title on third step in footer
	Then I check number of selected parts on third step in footer
	Then I check that names of groups enabled
	Then I delete one of parts on third step Parts Wanted
	Then I check counter of left parts

	Then I click Publish My Listings of Parts Wanted
	Then I see congradulation message
	Then I see title on confradulation page
	Then I click Go to search results page button

	Then I click My Account > Dashboard button
	Then I switch to Manage my ads
	Then I check that ads are displayed
	Then I check dates of created ads


	#Then I see that my Parts Wanted list display in saved search
	#Then I open list of Parts Wanted
	#Then check count of parts in list of Parts Wanted
	#Then I check names of parts in List

	#Then I click on Search
	#Then I check that parts wanted filter is working
	#Then I click saved searches
	#Then I open list of Parts Wanted
	#Then I delete parts in list 
	#Then I click delete this search
	#Then I see phrase that I dont have any search saved yet
