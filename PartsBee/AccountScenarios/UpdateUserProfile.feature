﻿@PartsBee_UpdateProfileInfo
Feature: UpdateUserProfile
	In order to avoid silly mistakes
	As a user
	I want to be change an user profile info

Background: 
	Given I open login page
	Then I log in as a buyer
	Then I click My Account button
	Then I click on User Profile Info button

@PositiveUpdate
Scenario: ChangingUserInfoPositive
Then I change firstname, lastname,address and email
Then I click the Save button (Profile)
Then I refresh the page
Then I check that user info was changed to its new values
Then I roll back all user info to defaults
Then I click the Save button (Profile)
Then I refresh the page
Then I check that user info rolled back successfully

@usedEmail
Scenario: ChangingUserInfoUsedEmail
	When I enter used email {{testuser@1.com}} and click Save
	Then I accept allert about used error

@negativeEmptyInputs
Scenario: ChangingUserInfoemptyInputs
	When I clean all inputs and click Save 
	Then I recieved {{3}} error notification





