﻿#region Usings

using System;
using System.IO;
using System.Linq;
using System.Threading;
using System.Windows.Forms;
using Common;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using TechTalk.SpecFlow;
using Keys = OpenQA.Selenium.Keys;

#endregion

namespace PartsBee.AccountScenarios
{
    [Binding]
    public class CreateListingPageSteps : FileBasedSteps
    {
        private string color = "";
        private string finish = "";
        private string make = "";
        private string model = "";
        public static string EnteredTitle = "test title!";
        static string compyearfrom = "2002";
        static string compyearto = "2010";
        static string compmake = "ACURA";
        static string compmodel = "CL";
        static string comptrim = "TestCompTrim";
        static string compcomments = "TestCompComments";

        private static string url;

        public static string Url
        {
            get { return url; }
            set { url = value; }
        }

        [BeforeScenario]
        public void Start()
        {
            InitFiles();
        }

        [Then(@"I click on checkbox about having Part Number")]
        public void ThenIClickOnCheckboxAboutHavingPartNumber()
        {
            WebDriver.GetByPath("//label[@for='partNumberCheckbox']").Click();
        }

        [Then(@"I check whether I am on the drafts page and go to single listing if I am there")]
        public void ThenICheckWhetherIAmOnTheDraftsPageAndGoToSingleListingIfIAmThere()
            {
            try
                {
                WebDriver.GetByPath("//*[contains(text(),'Available Drafts:')]");
                WebDriver.GetByPath("//a[contains(text(),'List a Single Part')]").Click();
                }
            catch(WebDriverException)
            { }
            }


        [Then(@"I enter {{(.*)}} to List by part input")]
        public void ThenIEnterToListByPartInput(string listByPartNumberQuery)
        {
            WebDriver.GetByName("partNumber").SendKeys(listByPartNumberQuery);
            //WebDriver.GetByPath("//i[@class='glyphicon glyphicon-search']").Click();
            //WebDriver.GetByPath("//button[contains(text(), 'Search')]").Click();
        }

        [Then(@"I manually enter a compatible vehicle for a listing")]
        public void ThenIManuallyEnterACompatibleVehicleForAListing()
            {
            WebDriver.GetByPath("//input[@name='yearStart']").
                SendKeys(compyearfrom);
            WebDriver.GetByPath("//input[@name='yearEnd']").
                SendKeys(compyearto);

            var selector=new SelectElement(
            WebDriver.GetByPath("//select[@name='makeId']"));
            selector.SelectByText(compmake);

            selector = new SelectElement(WebDriver.GetByPath("//select[@name='partModelId']"));
            selector.SelectByText(compmodel);

            WebDriver.GetByPath("//input[@name='trim']").
                SendKeys(comptrim);
            WebDriver.GetByPath("//input[@name='comments']").
                SendKeys(compcomments);

            WebDriver.GetByPath("//input[@class='btn gray-button']").Click();
            Thread.Sleep(1000);
            }


        [Then(@"choose first item and sumbit")]
        public void ThenChooseFirstItemAndSumbit()
        {
            WebDriver.GetByPath("//input[@value='Confirm Part Number']").Click();

            /*WebDriver.GetByPath("//div[@class='col-xs-6 radio-option scroller-follow-focus'][1]//i[1]").Click();
            WebDriver.GetByPath("//input[@value='Select']").Click(); */
        }

        [Then(@"I fill description form default values at listing page")]
        public void ThenIFillDescriptionFormDefaultValuesAtListingPage()
        {
            WebDriver.GetByName("title").SendKeys(EnteredTitle);

            Thread.Sleep(2000);
            var conditionSelect =
                new SelectElement(WebDriver.GetByPath("//*[@class='listing-description-fieldset']/div[2]/div[2]/select"));
            conditionSelect.SelectByValue("0");

            var typeselect =
                new SelectElement(WebDriver.GetByPath("//*[@class='listing-description-fieldset']/div[2]/div[4]/select"));
            typeselect.SelectByValue("2");
            Thread.Sleep(2000);

            WebDriver.GetByPath("//textarea[@name='description']").SendKeys("test");

            WebDriver.GetByName("location").SendKeys("Kie");

            // WebDriver.GetByPath("//span[@class = 'pac-matched']").Click();
        }

        [Then(@"enter price and quantity")]
        public void ThenEnterPriceAndQuantity()
        {
            WebDriver.GetByPath("//span[@class='price-number']/span").Click();
            WebDriver.GetByPath("//span[@class='price-number']/input").SendKeys("100");
            WebDriver.GetByPath("//div[@class='quantity label-wrapper pull-right']/div/input").Clear();
            WebDriver.GetByPath("//div[@class='quantity label-wrapper pull-right']/div/input").SendKeys("3");
        }

        [When(@"I click on Active listing button")]
        public void WhenIClickOnActiveListingButton()
        {
            WebDriver.GetBy("//span[contains(text(), 'Activate listing')]").SetTimer(5000).Find().Click();
        }

        /*     // original version - 20140912
                [Then(@"I upload picture")]
                public void ThenIUploadPicture()
                {
                    IWebElement upload = WebDriver.GetById("addPicture");  
                    upload.Click();
                    Console.Out.WriteLine(Images[0] + String.Format(" exists - {0}", File.Exists(Images[0])));
                    SendKeys.SendWait(Images[0]);
                    SendKeys.SendWait(@"{Enter}"); // !!! - TODO - TeamCity Upload Error - !!!

                   // WebDriver.GetBy("//div[@class='progress-bar progress-bar-success' and @style='width: 100%;']").SetTimer(10000).Find();

                    try
                    {
                        WebDriver.waitForFindElement(
                            By.XPath("//div[@class='progress-bar progress-bar-success' and @style='width: 100%;']"), 10);
                    }
                    catch (NoSuchElementException)
                    {
                        Assert.Fail("Photo is not upload");
                    }
                }
        */

        [Then(@"I upload picture")]
        public void ThenIUploadPicture()
        {
            // Get current window handle for switching back after closing the dialog
            string originalHandle = WebDriver.CurrentWindowHandle;

            IWebElement upload = WebDriver.GetByPath("//label[@for='uploadImage']/span");
            upload.Click();

            // Wait for the number of window handles to be greater than 1, or a timeou
            DateTime timeoutEnd = DateTime.Now.Add(TimeSpan.FromSeconds(5));
            while (WebDriver.WindowHandles.Count == 1 && DateTime.Now < timeoutEnd)
            {
                Thread.Sleep(100);
            }

            // Loop through all window handles, finding the first one that isn't
            // the original window handle, and switching to it.
            foreach (string handle in WebDriver.WindowHandles)
            {
                if (handle != originalHandle)
                {
                    WebDriver.SwitchTo().Window(handle);
                    break;
                }
            }

            // Send filename
            Console.Out.WriteLine(Images[0] + String.Format(" exists - {0}", File.Exists(Images[0])));
            SendKeys.SendWait(Images[0]);
            SendKeys.SendWait(@"{Enter}");

            Thread.Sleep(5000);

            // Navigate to original window
            WebDriver.SwitchTo().Window(originalHandle);

            /*try
            {
                WebDriver.waitForFindElement(
                    By.XPath("//div[@class='progress-bar progress-bar-success' and @style='width: 100%;']"), 10);
            }
            catch (NoSuchElementException)
            {
                Assert.Fail("Photo is not uploaded");
            }*/
        }

        [Then(@"I am redirected to a listing page")]
        public void ThenIAmRedirectedToAListingPage()
        {
            Thread.Sleep(3000);
            Assert.True(WebDriver.GetBy("//h2[contains(text(), 'Listing description')]").SetTimer(2000).Find().Displayed);
   /*         WebDriver.GetByPath("//*[@id='listingsBriefInfo']/div[2]/div/div[4]/div[1]/span[1]/a").Click();
            Thread.Sleep(3000);
            //Go to edit page
            CreateListingPageSteps.Url = WebDriver.GetCurrentUrl();
            //Get this URL */

        }

         [Then(@"I click on Additional option then fill data (.*) (.*) (.*) (.*) (.*) (.*) (.*) (.*) (.*) (.*) (.*) (.*) (.*) (.*) and sumbit")]
  public void ThenIClickOnAdditionalOptionThenFillDataACURAAndSumbit(string makeQuery, string modelQuery,
            string colorQuery, string finishQuery, string countryQuery,
            string exLinkQuery, string widthQuery, string heightQuery, string lengthQuery, string weightQuery,
            string weightQuery2, string damageQuery, string warrantyQuery, string placementSet)
        {
            WebDriver.GetByPath("//input[@value='Edit']").Click();

            //WebDriver.waitForFindElement(By.Name("makeId"), 10);

            /*     var makeSelect =
                     new SelectElement(
                         WebDriver.GetByPath("//div[@id='additionalOptions']//div[@class='form-group'][1]//select[1]"));
                 makeSelect.SelectByText(makeQuery);
                 make = makeSelect.SelectedOption.Text;

                 var modelSelect =
                     new SelectElement(
                         WebDriver.GetByPath("//div[@id='additionalOptions']//div[@class='form-group'][2]//select[1]"));
                 if (modelQuery.Equals(""))
                     modelSelect.SelectByValue("1");
                 model = modelSelect.SelectedOption.Text; */


            WebDriver.GetByName("brand").SendKeys("TOYOTA");

            var colorSelect =
                new SelectElement(
                    WebDriver.GetByName(
                        "colorId"));
            if (colorQuery.Equals(""))
                colorSelect.SelectByValue("1");
            color = colorSelect.SelectedOption.Text;

            var countrySelect =
                new SelectElement(
                    WebDriver.GetByName(
                        "manufactureCountryId"));
            if (finishQuery.Equals(""))
                countrySelect.SelectByValue("1");
            finish = countrySelect.SelectedOption.Text;


            var finishSelect =
                 new SelectElement(
                  WebDriver.GetByName(
                        "finishId"));
            if (finishQuery.Equals(""))
                finishSelect.SelectByValue("1");
            finish = finishSelect.SelectedOption.Text;

            WebDriver.GetByName("externalLink").SendKeys("http://diamond.myvirtualinventory.com/");
            for (int i = 3; i < 6; i++)
                WebDriver.GetByPath("//form[@class='form-horizontal']/div[6]/div[" + i + "]/div/input").SendKeys(i.ToString()); //Dimensions
  


            WebDriver.GetByPath("//form[@class='form-horizontal']/div[7]/div[3]/div/input").SendKeys("6");
            WebDriver.GetByPath("//form[@class='form-horizontal']/div[7]/div[4]/div/input").SendKeys("7"); //Weight

            WebDriver.GetByPath("//div[8]/div[2]/textarea").SendKeys("TestDamage");
            WebDriver.GetByPath("//div[9]/div[2]/textarea").SendKeys("TestDisclaimer");


            //Placements of Vehicles
            for (int i = 1; i < 7; i++)
                if (!WebDriver.GetByPath("//div[@id='placement']/div[2]/label[" + i + "]/input").Selected)
                    WebDriver.GetByPath("//div[@id='placement']/div[2]/label[" + i + "]/i").Click();

            WebDriver.GetByPath("//input[@value='Ok']").Click();
        }

        [Then(@"I am redirected to a listing page and check Additional information")]
        public void ThenIAmRedirectedToAListingPageAndCheckAdditionalInformation()
        {
            Thread.Sleep(3000);
            Assert.True(WebDriver.GetBy("//h2[contains(text(), 'Listing description')]").SetTimer(2000).Find().Displayed);
            Assert.True(WebDriver.GetByPath("//header/h2").Text.Equals(EnteredTitle.ToUpper()));
            Assert.True(WebDriver.GetByPath("//section[1]/div/div/div[3]/div[1]/span/b").Text.Equals("$100.00 USD"));
            Assert.True(WebDriver.GetByPath("//section[1]/div/div/div[3]/div[1]/span/span").Text.ToUpper().Equals("PRICE IS NEGOTIABLE"));
            //Assert.True(WebDriver.GetByPath("//div[@class='container']/div[1]/div[3]/div[2]/dl[2]/dd").Text.Equals("Test Company Nam1e"));
            Assert.True(WebDriver.GetByPath("//div[@id='listingDescription']/div/article[1]/div[2]/div[1]/table/tbody/tr[2]/td").Text.Equals("Kie"));
            Assert.True(WebDriver.GetByPath("//div[@id='listingDescription']/div/article[1]/div[2]/div[1]/table/tbody/tr[1]/td").Text.Equals("New"));
            //Assert.True(WebDriver.GetByPath("//div[@id='listingDescription']/div/article[1]/p").Text.Equals("Notestest"));
            Assert.True(WebDriver.GetByPath("//div[@id='listingDescription']/div/article[1]/div[2]/div[2]/table/tbody/tr[1]/td").Text.Equals("Unknown"));
            Assert.True(WebDriver.GetByPath("//div[@id='listingDescription']/div/article[1]/div[2]/div[2]/table/tbody/tr[2]/td").Text.Equals("3"));
            Assert.True(WebDriver.GetByPath("//div[@id='listingDescription']/div/article[2]/div[1]/table/tbody/tr[1]/td[1]").Text.Equals("TOYOTA"));
            Assert.True(WebDriver.GetByPath("//div[@id='listingDescription']/div/article[2]/div[1]/table/tbody/tr[2]/td[1]").Text.Equals("WAUED64B11N******"));
            Assert.True(WebDriver.GetByPath("//div[@id='listingDescription']/div/article[2]/div[1]/table/tbody/tr[4]/td[1]").Text.Contains("LIGHT BLUE"));
            Assert.True(WebDriver.GetByPath("//div[@id='listingDescription']/div/article[2]/div[1]/table/tbody/tr[5]/td[1]").Text.Equals("Matte"));
            Assert.True(WebDriver.GetByPath("//div[@id='listingDescription']/div/article[2]/div[1]/table/tbody/tr[6]/td[1]").Text.Equals("United States"));
            Assert.True(WebDriver.GetByPath("//div[@id='listingDescription']/div/article[2]/div[1]/table/tbody/tr[7]/td[1]").Text.Equals("TestDamage"));
            Assert.True(WebDriver.GetByPath("//div[@id='listingDescription']/div/article[2]/div[2]/table/tbody/tr[5]/td[1]").Text.Equals("Left, Front, Right, Rear, Bottom, Top"));
            Assert.True(WebDriver.GetByPath("//div[@id='listingDescription']/div/article[2]/div[2]/table/tbody/tr[4]/td[1]").Text.Equals("6 (pounds) 7 (ounces)"));
            Assert.True(WebDriver.GetByPath("//div[@id='listingDescription']/div/article[2]/div[2]/table/tbody/tr[3]/td[1]").Text.Equals("5 (inches)"));
            Assert.True(WebDriver.GetByPath("//div[@id='listingDescription']/div/article[2]/div[2]/table/tbody/tr[2]/td[1]").Text.Equals("4 (inches)"));
            Assert.True(WebDriver.GetByPath("//div[@id='listingDescription']/div/article[2]/div[2]/table/tbody/tr[1]/td[1]").Text.Equals("3 (inches)"));

            //Assert.True(WebDriver.GetByPath("//div[@class='greyWrapper']//dd[1]").Text.Equals(make));
            //Assert.True(WebDriver.GetByPath("//div[@class='greyWrapper']//dd[2]").Text.Equals(model));
            //Assert.True(WebDriver.GetByPath("//div[@class='greyWrapper']//dd[4]").Text.Equals(color));
            //Assert.True(WebDriver.GetByPath("//div[@class='greyWrapper']//dd[5]").Text.Equals(finish));
            //Assert.True(WebDriver.GetByPath("//div[@class='greyWrapper']//dd[contains(text(), '"+ make +"')][1]").Enabled);
            //Assert.True(WebDriver.GetByPath("//div[@class='greyWrapper']//dd[contains(text(), '" + model + "')][2]").Enabled);
        }

        [Then(@"I redirect to main page {{(.*)}}")]
        public void ThenIRedirectTo(string expectedDestination)
        {
            Thread.Sleep(3000);
            Console.Error.WriteLine(WebDriver.GetCurrentUrl());
            WebDriver.WaitFullLoad();
            Assert.True(expectedDestination.Equals(WebDriver.GetCurrentUrl(),
            StringComparison.InvariantCultureIgnoreCase));
        }

        [Then(@"I press Sell Your part Button")]
        public void GivenIPressSellYourPartButton()
        {
            WebDriver.WaitFullLoad();
            WebDriver.GetByPath("//a[contains(text(), 'SELL YOUR PARTS')]").Click();
        }

        [Then(@"I click on list of Category")]
        public void ThenIClickOnListOfCategory()
        {
            WebDriver.GetByPath("//button[contains(text(), 'Browse all')]").Click();
        }

        [Then(@"choose first and second and third items and sumbit")]
        public void ThenChooseFirstAndSecondItemAndSumbit()
        {


            var conditionSelect =
               new SelectElement(WebDriver.GetByPath("//div[@id='categories']/div/div/select"));
            conditionSelect.SelectByValue("2");

            var conditionSelect2 =
               new SelectElement(WebDriver.GetByPath("//div[@id='categories']/div[2]/div/select"));
            conditionSelect2.SelectByValue("4");

            var conditionSelect3 =
               new SelectElement(WebDriver.GetByPath("//div[@id='categories']/div[3]/div/select"));
            conditionSelect3.SelectByValue("5");

            WebDriver.GetByPath("//input[@value='Done']").Click();
        }


        [Then(@"I fill VIN field")]
        public void ThenIFillVINField()
        {
            WebDriver.GetByName("txtVin").SendKeys("WAUED64B11N139015");
        }

       
        [Then(@"I click on Negotiable checkbox")]
        public void ThenIClickOnNegotiableCheckbox()
        {
            WebDriver.GetByPath("//label[@for='negotiable']/i").Click();
        }

        [Then(@"I fill Shipping Polices")]
        public void ThenIFillPolices()
        {
            WebDriver.GetByPath("//fieldset[@class='listing-policies']/div[1]/div[2]/a").Click();
            WebDriver.GetByPath("//div[@class='col-sm-6'][1]/div/div[4]/label").Click();
            WebDriver.GetByPath("//input[@placeholder='Type Country Name']").SendKeys("Ukraine");
            WebDriver.GetByPath("//input[@value='Save']").Click();
            Thread.Sleep(1000);
        }

        [Then(@"I fill in payment policies")]
        public void ThenIFillInPaymentPolicies()
            {
            WebDriver.GetByPath("/html/body/div[3]/div/div/form/fieldset[6]/div[2]/div[2]/a").Click();
            }

        [Then(@"I fill in Return Policy")]
        public void ThenIWillInReturnPolicy()
            {
            WebDriver.GetByPath("//*[contains(text(),'Return')]/../../div[2]/a").Click();
            Thread.Sleep(2000);
            WebDriver.GetByPath("//div[@class='form-control dropdown-button-wrapper']").Click();
            Thread.Sleep(1000);
            WebDriver.GetByPath("//div[@class='dropdown has-feedback']/div[3]").Click();
            Thread.Sleep(1000);
            WebDriver.GetByPath("//input[@value='Save']").Click();
            }


        [Then(@"I click on Payment and check info")]
        public void ThenIClickOnPaymentAndCheckInfo()
        {
            WebDriver.GetByPath("//span[contains(text(), 'Payment')]").Click();
            Assert.True(WebDriver.GetByPath("//div[@id='paymentPolicy']/section/div[1]/div[2]").Text.Equals("PayPal"));
            Assert.True(WebDriver.GetByPath("//div[@id='paymentPolicy']/section/div/div[3]/div[2]").Text.Equals("CashTest"));
            Assert.True(WebDriver.GetByPath("//div[@id='paymentPolicy']/section/div/div[5]/div[2]").Text.Equals("CheckTest"));
            Assert.True(WebDriver.GetByPath("//div[@id='paymentPolicy']/section/div/div[7]/div[2]").Text.Equals("WireTest"));
            Assert.True(WebDriver.GetByPath("//div[@id='paymentPolicy']/section/div/div[8]/div[2]").Text.Equals("ACHTest"));
        }

        [Then(@"I click on Return Policy and check info")]
        public void ThenIClickOnReturnPolicyAndCheckInfo()
        {
            WebDriver.GetByPath("//span[contains(text(), 'Return')]").Click();
            Assert.True(WebDriver.GetByPath("//div[@id='returnPolicy']/div[1]/table/tbody/tr/td").Text.Equals("accepted"));
            //Assert.True(WebDriver.GetByPath("//div[@id='returnPolicy']/div/span").Text.Equals("Five days for the win!"));
            Assert.True(WebDriver.GetByPath("//div[@id='returnPolicy']/div[1]/div[1]/div[1]/div[1]").Text.Equals("Buyer pays for return shipping"));
        }

        [Then(@"I click on Shipping Policy and check info")]
        public void ThenIClickOnShippingPolicyAndCheckInfo()
        {
            WebDriver.GetByPath("//span[contains(text(), 'Shipping')]").Click();
            Assert.True(WebDriver.GetByPath("//div[@id='shippingPolicy']/div/div[1]/b").Text.Equals("Worldwide"));
            Assert.True(WebDriver.GetByPath("//div[@id='shippingPolicy']/div/div[2]/span[2]").Text.Equals("Ukraine"));
        //Assert.True(WebDriver.GetByPath("//div[@id='shippingPolicy']/div/div[3]/span[2]").Text.Equals("gdf"));
        
        }

        [Then(@"I switch to Compatibility within the Listing and check the info")]
        public void ThenISwitchToCompatibilityWithinTheListingAndCheckTheInfo()
            {
            WebDriver.GetByPath("//div[@class='listing-details-info']/div/div/div/ul/li[2]/a/span").Click();
            Thread.Sleep(2000);
            WebDriver.GetByPath("//*[contains(text(),'"+compyearfrom+"')]");
            WebDriver.GetByPath("//*[contains(text(),'" + compyearto + "')]");
            WebDriver.GetByPath("//*[contains(text(),'" + compmake + "')]");
            WebDriver.GetByPath("//*[contains(text(),'" + compmodel + "')]");
            WebDriver.GetByPath("//*[contains(text(),'" + comptrim + "')]");
            WebDriver.GetByPath("//*[contains(text(),'" + compcomments + "')]");
            }

        [Then(@"I click on GO FOR IT")]
        public void ThenIClickOnGOFORITAndRedirectToSite()
        {
            WebDriver.GetByPath("//div/div/div[3]/div[1]/a").Click();

        }

        [Then(@"I redirect to site page {{(.*)}}")]
        public void ThenIRedirectToSite(string expectedDestination)
        {
            WebDriver.SwitchTo().Window(WebDriver.WindowHandles.Last());
            Thread.Sleep(3000);
            Console.Error.WriteLine(WebDriver.GetCurrentUrl());
            WebDriver.WaitFullLoad();
            Assert.True(expectedDestination.Equals(WebDriver.GetCurrentUrl(),
            StringComparison.InvariantCultureIgnoreCase));
        }

        [Then(@"I click ListSinglePart from a main dropdown")]
        public void ThenIClickListSinglePartFromAMainDropdown()
            {
            Thread.Sleep(500);
            WebDriver.GetByPath("//a[contains(text(), 'List single part')]").Click();
            }

        [Then(@"I click the Preview button for my listing")]
        public void ThenIClickThePreviewButtorForMyListing()
            {
            Thread.Sleep(1000);
            WebDriver.GetByPath("//*[@class='button-group']/div/div/div/button[3]").Click();
            WebDriver.SwitchTo().Window(WebDriver.WindowHandles.Last());
            Thread.Sleep(2000);
            }

        [Then(@"I switch back to my listing from Preview")]
        public void ThenISwitchBackToMyListingFromPreview()
            {
            WebDriver.Close();
            WebDriver.SwitchTo().Window(WebDriver.WindowHandles[0]);
            }

        [Then(@"I save the listing for later \(to drafts\)")]
        public void ThenISaveTheListingForLaterToDrafts()
            {
                WebDriver.GetByPath("//span[contains(text(), 'Save for later')]").Click();
            }

        [Then(@"I select my draft from the drafts page \(complete button\)")]
        public void ThenISelectMyDraftFromTheDraftsPageCompleteButton()
            {
            WebDriver.GetByPath("//div[@class='nano-content']/article[1]/div[1]/a[1]").Click();
            }

        [Then(@"I close current browser window and switch to the first one")]
        public void ThenICloseCurrentBrowserWindowAndSwitchToTheFirstOne()
            {
            WebDriver.Close();
            WebDriver.SwitchTo().Window(WebDriver.WindowHandles[0]);
            }

        [Then(@"I delete my listing within the listing itself")]
        public void ThenIDeleteMyListingWithinTheListingItself()
            {
            Thread.Sleep(500);
            WebDriver.GetByPath("//a[@class='delete-item-icon']").Click();
            Thread.Sleep(500);
            WebDriver.GetByPath("//button[@class='btn btn-warning']").Click();
            Thread.Sleep(500);
            try
                {
                WebDriver.GetByPath("//strong[contains(text(),'Deleted Listing')]");
                }
            catch(WebDriverException)
                {
                Assert.Fail("Most likely our listing was not properly deleted.");
                }
            }

        [Then(@"I click the Print button")]
        public void ThenIClickThePrintButton()
            {
            WebDriver.GetByPath("//div[@class='pull-right']/a[1]").Click();
            Thread.Sleep(3000);
            }

        [Then(@"I check that all information is there \(in the print page\)")]
        public void ThenICheckThatAllInformationIsThereInThePrintPage()
            {
            WebDriver.SwitchTo().Window(WebDriver.WindowHandles.Last());
            WebDriver.GetByPath("//*[contains(text(),'" + EnteredTitle + "')]");
            WebDriver.GetByPath("//*[contains(text(),'test')]");
            WebDriver.GetByPath("//*[contains(text(),'Unknown')]");
            WebDriver.GetByPath("//*[contains(text(),'TOYOTA')]");
            WebDriver.GetByPath("//*[contains(text(),'WAUED64B11N******')]");
            WebDriver.GetByPath("//*[contains(text(),'LIGHT BLUE')]");
            WebDriver.GetByPath("//*[contains(text(),'Matte')]");
            WebDriver.GetByPath("//*[contains(text(),'TestDamage')]");
            WebDriver.GetByPath("//*[contains(text(),'3')]");
            WebDriver.GetByPath("//*[contains(text(),'inches')]");
            WebDriver.GetByPath("//*[contains(text(),'Left, Front, Right, Rear, Bottom, Top')]");
            
            WebDriver.GetByPath("//*[contains(text(),'"+PartsBeeVariables.SellerAddress1+"')]");
            WebDriver.GetByPath("//*[contains(text(),'"+PartsBeeVariables.SellerZIP+"')]");
            WebDriver.GetByPath("//*[contains(text(),'12345')]");
            WebDriver.GetByPath("//*[contains(text(),'" + PartsBeeVariables.SellerWebSite+ "')]");
            WebDriver.GetByPath("//*[contains(text(),'" + PartsBeeVariables.SellerCity + "')]");
            WebDriver.GetByPath("//*[contains(text(),'days')]");
            WebDriver.GetByPath("//*[contains(text(),'3')]");
            //---------Payment Instructions
            WebDriver.GetByPath("//*[contains(text(),'12312312312')]");
            WebDriver.GetByPath("//*[contains(text(),'21312312')]");
            WebDriver.GetByPath("//*[contains(text(),'312312312')]");
            WebDriver.GetByPath("//*[contains(text(),'1231231231')]");
            WebDriver.GetByPath("//*[contains(text(),'12312312')]");
            WebDriver.GetByPath("//*[contains(text(),'1231231')]");

            WebDriver.GetByPath("//*[contains(text(),'Worldwide (Ukraine)')]");
            WebDriver.GetByPath("//*[contains(text(),'100')]");
            WebDriver.GetByPath("//*[contains(text(),'$')]");
            }


        [AfterScenario]
        public void Done()
        {
            DisposeFiles();
        }
    }
}
