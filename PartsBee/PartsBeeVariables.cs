﻿using System;               //-----------Данный класс хранит переменные для работы по тестам PartsBee
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PartsBee
{
    public class PartsBeeVariables
    {
        public static bool IsProductionTest = false; //Тестирование продакшена или стенда

        public static string SellerLogin = "testsellerpartsbee@mailinator.com";
        public static string SellerPassword = "123321";
        public static string SellerFirstname = "F";
        public static string SellerLastname = "L";
        public static string SellerUserAddress = "New York, NY, United States";

        public static string SellerMessageLogin = "testsellermessage@mailinator.com";
        public static string SellerMessagePassword = "123321";
        public static string SellerMessageFirstname = "Sellerochok";
        public static string SellerMessageLastname = "Sellerskii";
        public static string SellerMessageUserAddress = "New York, NY, United States";
        public static string SellerMessageCompany = "MessagionCorporation";

        public static string SellerEmail = "test1mydealeronline@tsbua.com";
        public static string SellerEmailPassword = "mydealeronline1";
        public static string SellerPhone = "12345";
        public static string SellerCompany = "MezioCorporation";
        public static string SellerZIP = "33550";
        public static string SellerAddress1 = "Kello 11";
        public static string SellerAddress1ForCanada = "135 Fraser Ave";
        public static string SellerAddress2 = "Mironova 3";
        public static string SellerCity = "Miami";
        public static string SellerCityForCanada = "Fort McMurray";
        public static string SellerWebSite = "http://diamond.myvirtualinventory.com/";
        public static string SellerAddInfo = "Test test test test test";

        public static string VIN = "19UUA76507A013864";
        public static string trim = "TESTtrim";
        
        public static string CustomerLogin = "buyer@test.com";
        public static string CustomerPassword = "123321";
        public static string CustomerFirstname = "testovy";
        public static string CustomerLastname = "buyerochok";
        public static string CustomerPhone = "47143552";
        public static string CustomerEmail = "testuser@1.com";
        public static string CustomerAddress = "New York, NY, United States";


        public static string SocialEmail = "test1mydealer@gmail.com";
        public static string SocialPassword = "mydealer1";

        public static string ContactUsSupportEmail = "support@partsbee.com ";
        public static string ContacUsSupportPassword = "b4J6dE=b";

        public static string PasswordRecoveryEmail = "asdfgrfdsppoooppoofkfvl@mailinator.com";
        public static string UnexistedEmail = "3123gtrgtds123sal@mailinator.com";

        public static string ResetPasswordEmail = "test1mydealeronline@tsbua.com";
        public static string ResetPasswordEmailPassOnSite = "123321";
        public static string ResetPasswordEmailPass = "mydealeronline1";


        public static string Image1Path = "C:\\111.jpg"; //Пути к картинкам. Можно использовать
        public static string Image2Path = "C:\\222.jpg"; //вместо таблицы примеров

        
    }
}
