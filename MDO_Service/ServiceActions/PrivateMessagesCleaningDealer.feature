﻿Feature: PrivateMessagesCleaning
   Cleaning our private messages
   before regression testing

@ClearingPrivateMessagesForDealer
Scenario Outline: PrivateMessagesCleaningDealer
	Given I open <webSite> and type <phone>
	And I click on SignIn button
	When I type my login and password of a dealer and submit them
	Then I go to the Dashboard
	Then I open Messages
    And I erase all public messages there (service)


		Examples:
	| webSite         | phone      |
	|      bronze	  | 7327572923 | 
	|      silver     |   0        |
	|       gold      |   0	       |
