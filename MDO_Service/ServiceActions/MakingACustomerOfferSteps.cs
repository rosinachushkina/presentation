﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using Common;
using NUnit.Core;
using NUnit.Framework;
using NUnit.Framework.Constraints;
using OpenQA.Selenium;
using TechTalk.SpecFlow;

namespace MDOclass.AccountScenarios
{
    [Binding]
    public class MakingACustomerOfferSteps : BaseSteps

    {
        string sbid, sfullname;
        Double MyBid; //Переменные для проверок
        private string s;

        [When(@"I place a higher bid for this inventory")]
        public void WhenIPlaceAHigherBidForThisInventory()
        {
            //------------------Забираем текущую цену и вычленяем саму сумму из строки
            s = WebDriver.FindElement(By.XPath("//*[@id='CPH_ucOpenInvoice_divCurrentBid']")).Text;
            s=s.Substring(1,s.Length-5);
            
            //------------------Запоминаем и придумываем свою ставку
            MyBid = Convert.ToDouble(s) +25;

            //------------------Чистим инпут и записываемся в него
            WebDriver.FindElement(By.XPath("//input[@id='txtCustomerBid']")).Clear();
            WebDriver.FindElement(By.XPath("//input[@id='txtCustomerBid']")).SendKeys(MyBid.ToString());
            WebDriver.FindElement(By.XPath("//a[@class='btn btn-primary pull-left']")).Click();
            WebDriver.FindElement(By.XPath("//a[@href='javascript:BidConfirmation()']")).Click();
            WebDriver.FindElement(By.XPath("//a[@href='javascript:BidCloseInstructions()']")).Click();
            
        }

        [Then(@"I select a vehicle with the Current Offer option")]
        public void ThenISelectAVehicleWithTheCurrentOfferOption()
        {
            //-------Работает только в режиме отображения list
            WebDriver.GetByPath("//div[contains(text(), 'Current Offer')]/../div[3]/a[1]").Click();
        }


        [Then(@"My new bid should be placed successfully")]
        public void ThenMyNewBidShouldBePlacedSuccessfully()
        {
            Thread.Sleep(5000);
            WebDriver.FindElement(By.XPath("//*[contains(text(), 'THANK YOU FOR PLACING AN OFFER!')]"));
        }

        [Then(@"I check whether I have a new offer from the main page")]
        public void ThenICheckWhetherIHaveANewOfferFromTheMainPage()
        {
            //-------------Кликаем наверх
            WebDriver.GetByPath("//*[@id='ddlOffersLink']").Click();

            //-------------Смотрим есть ли там наш новый оффер
            s = WebDriver.FindElement(By.XPath("//li[@id='dropdownOffer']/ul/li[1]/a/span/b"))
                    .Text;
            Assert.IsTrue(s.Contains(MDOvariables.CustomerFirstname+" "+MDOvariables.CustomerLastname));

            s = WebDriver.FindElement(By.XPath("//li[@id='dropdownOffer']/ul/li[1]/a/span/span/div[2]"))
                    .Text;
            Assert.IsTrue(s.Contains(MyBid.ToString()));  
        }

        [Then(@"I switch to Offers")]
        public void ThenISwitchToOffers()
        {
            WebDriver.FindElement(By.XPath("//span[@ class='biddinghistory badge pull-right']")).Click();
        }

        [Then(@"I switch to CustomerOffers")]
        public void ThenISwitchToCustomerOffers()
        {
            WebDriver.FindElement(By.XPath("//a[@href='/Dashboard/BiddingCustomerHistory/List']")).Click();
        }


        [Then(@"I check whether a new offer is there in Offers")]
        public void ThenICheckWhetherANewOfferIsThereInOffers()
        {
           
            //string element = "//tr[1]//span[contains(Text()," + "'" + MyBid + " USD" + "'" + ")]";
            //WebDriver.FindElement(By.XPath(element));

            
            //------Нормальный путь в драйвере не отрабатывает, хотя должен. Поэтому делаем костыли
            //------Проверка по полному хпасу на вхождение нужных нам данных

            //----Проверка бида
            sbid= WebDriver.FindElement(
                By.XPath(
                    "/html/body/div[2]/div/div/div[2]/div/div/div/div/div/div[2]/div/div[2]/div/div/table/tbody/tr[1]/td[2]/span[2]"))
                .Text;
          
            Assert.IsTrue(sbid.Contains(MyBid + " USD"));

            //----Проверка имени
            sfullname = WebDriver.FindElement(
                By.XPath(
                    "/html/body/div[2]/div/div/div[2]/div/div/div/div/div/div[2]/div/div[2]/div/div/table/tbody/tr/td[3]/span[2]"))
                .Text;
         
            Assert.IsTrue(sfullname.Contains(MDOvariables.CustomerFirstname+" "+MDOvariables.CustomerLastname));

        }

        [Then(@"I mark this new offer as read")]
        public void ThenIMarkThisNewOfferAsRead()
        {
            WebDriver.FindElement(By.XPath("//a[@class='btn btn-success btn-xs']")).Click();
        }
       
        [Then(@"I switch to Home")]
        public void ThenISwitchToHome()
        {
            WebDriver.FindElement(By.XPath("//a[@href='/']")).Click();
        }

        [Then(@"I check whether my customer_s offer is in my offers list")]
        public void ThenICheckWhetherMyCustomer_SOfferIsInMyOffersList()
        {
            //----Проверка бида
            sbid = WebDriver.FindElement(
                By.XPath(
                    "/html/body/div[2]/div/div/div[2]/div/div/div/div/div/div/div/section/div/div[1]/div[1]/p[2]/span[3]"))
                .Text;

            Assert.IsTrue(sbid.Contains(MyBid.ToString()));

        }

        [Then(@"I switch to Reports")]
        public void ThenISwitchToReports()
        {
            WebDriver.FindElement(By.XPath("//a[@href='/Dashboard/Reports/List']")).Click();
        }

        [Then(@"I switch to Reports>Customer Offers")]
        public void ThenISwitchToReportsCustomerOffers()
        {
            WebDriver.FindElement(By.XPath("//a[@href='#customerOffersHistory']")).Click();
        }


        [Then(@"I check whether this new offer is reflected there")]
        public void ThenICheckWhetherThisNewOfferIsReflectedThere()
        {
            //----Проверка имени
            sfullname = WebDriver.FindElement(
                By.XPath(
                    "/html/body/div[2]/div/div/div[2]/div/div/div/div/div/div/div/div/section/div/table/tbody/tr[1]/td[10]/span[2]"))
                .Text;
         
            Assert.IsTrue(sfullname.Contains(MDOvariables.CustomerFirstname+" "+MDOvariables.CustomerLastname));

            //----Проверка бида
            sbid = WebDriver.FindElement(
                By.XPath(
                    "/html/body/div[2]/div/div/div[2]/div/div/div/div/div/div/div/div/section/div/table/tbody/tr[1]/td[4]/span[2]"))
                .Text;

            Assert.IsTrue(sbid.Contains(MyBid.ToString()));
        }


    }
}
