﻿Feature: CallRequestsCleaning
	Call requests clean up
	before regression testing

@CallRequestsCleaning
Scenario Outline: CallRequestsCleaning

 Given I open <webSite> and type <phone>
 And I click on SignIn button
 When I type my login and password of a dealer and submit them
 Then I go to the Dashboard
 And I switch to Call Requests
 Then I erase all public messages there (service)

  	Examples:
	| webSite         | phone      |
	|      bronze	  | 7327572923 | 
	|      silver     |   0        |
	|       gold      |   0	       |