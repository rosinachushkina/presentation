﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using Common;
using Microsoft.SqlServer.Server;
using NUnit.Framework.Constraints;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using TechTalk.SpecFlow;

namespace MDO_Service.ServiceActions
{
    [Binding]
    public class PublicMessagesCleaningSteps : BaseSteps
    {
        [Then(@"I erase all public messages there \(service\)")]
        public void ThenIEraseAllPublicMessagesThereService()
        {
            bool b = true;

            //--------------Кликаем на удаление до победного конца с вываливанием теста
            while (b==true)
            {
             WebDriver.FindElement(By.XPath("//div[@class='scroller']//*[@class='item read']")).Click(); 
             WebDriver.FindElement(By.XPath("//div[@class='item read']//*[@class='remove-icon']")).Click();  
             WebDriver.SwitchTo().Alert().Accept();
             Thread.Sleep(1000);
            }
           
        }

    }
}
