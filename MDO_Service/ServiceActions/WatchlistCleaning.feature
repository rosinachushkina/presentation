﻿Feature: WatchlistCleaning
	Watchlist clean up 
	before regression testing

@WatchlistCleaning
Scenario Outline: WatchlistCleaning

Given I open <webSite> and type <phone>
And I click on SignIn button
When I type my login and password of a customer and submit them
Then I go to the Dashboard
Then I go to Watchlist from Dashboard  
Then I erase all all watchlist values from there (service)

  	Examples:
	| webSite         | phone      |
	|      bronze	  | 7327572923 | 
	|      silver     |   0        |
	|       gold      |   0	       |