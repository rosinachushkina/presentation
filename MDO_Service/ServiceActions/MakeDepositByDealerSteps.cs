﻿using System;
using System.Threading;
using Common;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using TechTalk.SpecFlow;

namespace MDOclass.AccountScenarios
{
    [Binding]
    public class MakeDepositByDealerSteps : BaseSteps
    {
        [Then(@"I switch to Customer List")]
        public void ThenISwitchToCustomerList()
        {
            WebDriver.GetByPath("//a[@href='/Dashboard/Customers/List']").Click();

        }

        [Then(@"I click on Financial Block")]
        public void ThenIClickOnFinancialBlock()
        {
            WebDriver.GetByPath("//a[@href='#financial']").Click();
        }

        [Then(@"I click on Deposit Block")]
        public void ThenIClickOnDepositBlock()
        {
            WebDriver.GetByPath("//a[@href='#deposit']").Click();
        }

        [Then(@"I choose current date")]
        public void ThenIChooseCurrentDate()
        {
            WebDriver.GetByPath("//input[@class='form-control form-filter']").Click();
            WebDriver.GetByPath("//td[@class='active day']").Click(); //----выбор текущей даты
        }

  
        [Then(@"I click on Search Inventory Button")]
        public void ThenIClickOnSearchInventoryButton()
        {
            WebDriver.GetByPath("//button[@class='btn btn-warning dropdown-toggle']/span").Click();
        }


        [Then(@"I type Inventory number")]
        public void ThenITypeInventoryNumber()
        {
            WebDriver.GetByPath("//div[@class='modal-body']//input[@class='form-control']").SendKeys("12401641");
        }

        [Then(@"I click SearchButton in this dropdown")]
        public void ThenIClickSearchButtonInThisDropdown()
        {
            WebDriver.GetByPath("//button[@id='searchBtn']").Click();
        }


        [Then(@"I choose from results first Vehicle")]
        public void ThenIChooseFromResultsFirstVehicle()
        {
            WebDriver.GetByPath("//section[@class='searchInventoryResult scroller']/div/div[1]//img").Click();
        }

        [Then(@"I send InvoiceNumber in Invoice input")]
        public void ThenISendInvoiceNumberInInvoiceInput()
        {
            WebDriver.GetByName("invoiceNumber").SendKeys("TestInvoice");
        }

        [Then(@"I send Amount in AmouuntInput")]
        public void ThenISendAmountInAmouuntInput()
        {
            WebDriver.GetByName("amount").SendKeys("123");
        }

        [Then(@"I choose status Applied in StatusField")]
        public void ThenISendStatusInStatusInput()
        {
            var typeselect =
                new SelectElement(WebDriver.GetByName("statusID"));
            typeselect.SelectByValue("18");
            
        }


        [Then(@"I click on saving Deposit")]
        public void ThenISaveMyDeposit()
        {
            WebDriver.GetById("btnSave").Click();
        }


        [Then(@"I delete Deposit")]
        public void ThenIDeleteDeposit()
        {
            WebDriver.GetByPath("//a[contains(text(), 'Remove')]").Click();
        }
        [Then(@"I see {{(.*)}} notification about requirement field")]
        public void ThenISeeAndNotificationAboutRequirementField(int numberRequirements)
        {
            var checkNumberRequirements=
                WebDriver.FindElements(By.XPath("//div[@class='col-lg-8 col-xs-7 form-validation-error']"));
            int checkNumberErrorNotificationSum = checkNumberRequirements.Count;

            if (numberRequirements != checkNumberErrorNotificationSum)

                Assert.Fail("Expected error notification is " + numberRequirements + "\n Actual is " + checkNumberErrorNotificationSum);
        }


    }

}
