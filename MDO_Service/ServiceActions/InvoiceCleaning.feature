﻿Feature: InvoiceCleaning
	In order as Customer 
	I want to mark all new Invoices
	as read

@MarkingNewInvoices
Scenario Outline: Cleaning new Invoices
	Given I open <main page> and type <phone>
	Then I click on SignIn button
	When I type my login and password of a customer and submit them
	Then I go to the Dashboard
	Then I switch to Invoices
	Then I click on Mark As Read
	
	
	
	
	
	
	Examples: 
	| main page       | phone      |
	|      bronze	  | 7327572923 | 
	|      silver     |   0        |
	|       gold      |   0	       |