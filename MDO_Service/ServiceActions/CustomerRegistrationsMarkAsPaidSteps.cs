﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using Common;
using OpenQA.Selenium;
using TechTalk.SpecFlow;

namespace MDO_Service.ServiceActions
{
    [Binding]
    public class CustomerRegistrationsMarkAsPaidSteps : BaseSteps
    {
        [Then(@"I mark all customer registrations as read")]
        public void ThenIMarkAllCustomerRegistrationsAsRead()
        {
            bool b = true;

            while (b == true)
                WebDriver.FindElement(By.XPath("//a[@class='btn btn-success btn-xs']")).Click();
           
        }

    }
}
