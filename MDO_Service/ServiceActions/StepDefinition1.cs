﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using Common;
using TechTalk.SpecFlow;
using OpenQA.Selenium;
using NUnit.Framework;

namespace MDO_Service.ServiceActions
    {
    [Binding]
    public class StepDefinition1 : BaseSteps
        {
        [Given(@"I have trimmed my string by spaces")]
        public void GivenIHaveTrimmedMyStringBySpaces()
            {
            string wow = "Some shit \b and more shit. Actually the\nre is more s\nhi\bt.";
            string output = wow;
            output = HandyFunctions.SubstringRemove(output, "\n");
            output = HandyFunctions.SubstringRemove(output, "\b");
            output = HandyFunctions.DoubleSpaceDeletion(output);
            }

        }
    }
