﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common;
using TechTalk.SpecFlow;
using System.Threading;
using OpenQA.Selenium;

namespace MDO_Service.ServiceActions
{
    [Binding]
    public class WatchlistCleaningSteps : BaseSteps
    {
        [Then(@"I go to Watchlist from Dashboard")]
        public void ThenIGoToWatchlistFromDashboard()
        {
            WebDriver.FindElement(By.XPath("//a[@href='/Dashboard/WatchLists/List']")).Click();
        }
        
        [Then(@"I erase all all watchlist values from there \(service\)")]
        public void ThenIEraseAllAllWatchlistValuesFromThereService()
        {
            {
                bool b = true;

                //--------------Кликаем на удаление до победного конца с вываливанием теста
                while (b == true)
                {
                    WebDriver.FindElement(By.XPath("//div[@class='scroller']//*[@class='item read']")).Click();
                    WebDriver.FindElement(By.XPath("//div[@class='item read']//*[@class='remove-icon']")).Click();
                    WebDriver.SwitchTo().Alert().Accept();
                    Thread.Sleep(1000);
                }

            }

        }

    }
}
