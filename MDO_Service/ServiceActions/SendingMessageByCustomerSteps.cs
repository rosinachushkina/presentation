﻿using System;
using System.Threading;
using Common;
using NUnit.Core;
using NUnit.Framework;
using TechTalk.SpecFlow;

namespace MDOclass.AccountScenarios
{
    [Binding]
    public class SendingMessageByCustomerSteps : BaseSteps
    {
        private string messageFromCustomer;
        private string vehicleID;
        private string vehicleTitle;
        private string messageFromDealer;


        [Then(@"I type messages and send it")]
        public void ThenITypeMessagesAndSendIt()
        {
            //---------------Генерация и подготовака случайных корректных данных для сообщения
            var Handy = new HandyFunctions();
            messageFromCustomer = Handy.RandomStringLatinAndNumbers(10);


            WebDriver.GetById("txtNewMsg").SendKeys(messageFromCustomer);//-----набор сообщения
            WebDriver.GetById("CPH_lblSaveMsgCaption").Click();//----отправка сообщения
            Thread.Sleep(3000);
        }

        [Then(@"I check that message was sent and get ID and Title")]
        public void ThenICheckThatMessageWasSent()
        {
            string actualMessage =
                (WebDriver.GetByPath("//div[@id = 'divAllComments']//li").Text); //------задание актуального сообщения в переменную

            if ((messageFromCustomer + " • by me just now") != actualMessage)               //-------сравнение отправленного сообщения и актуального
            {
                Assert.Fail("Message must be " + messageFromCustomer + "\n Actual is " + actualMessage);
            }

            vehicleID = WebDriver.GetById("CPH_lblInventoryNumberData2").Text;   //-----задание актуального ID в переменную
            vehicleTitle = WebDriver.GetById("CPH_lblVehicleHeader").Text;       //-----задание актуального Title в переменную



        }


        [Then(@"I go to the Dashboard")]
        public void ThenIGoToTheDashboard()
        {
            WebDriver.GetById("ucToolbar_hplMyDash").Click();
        }

        [Then(@"I check messsage from Dashboard")]
        public void ThenICheckMesssageFromDashboard()
        {
            string actualMessage = WebDriver.GetByPath("//div[@id='messagesGroupArea']//p/span[3]").Text;//------задание актуального сообщения в переменную

            if (messageFromCustomer != actualMessage)                                                                //-------сравнение отправленного сообщения и актуального
            {
                Assert.Fail("Message must be " + messageFromCustomer + "\n Actual is " + actualMessage);
            }
        }

        [Then(@"I delete message from Dashboard")]
        public void ThenIDeleteMessageFromDashboard()
        {
            WebDriver.GetByPath("//div[@id='messagesGroupArea']/section").Click();                      //------клик на Vehicle
            WebDriver.GetByPath("//div[@id='messagesGroupArea']/section/div/div/a").Click();            //------клик на крестик удаления
            WebDriver.SwitchTo().Alert().Accept();                                                      //------принятие alert'a
            Assert.True((WebDriver.GetByPath("//*[contains(text(),'You don')]").Enabled));
            Assert.True((WebDriver.GetByPath("//*[contains(text(),'t have any messages')]").Enabled));
        }

        [Then(@"I click on AskDealerAQuestion button")]
        public void ThenIClickOnAskDealerAQuestionButton()
        {
            WebDriver.GetByPath("//div[@id='searchResult']/article[1]/div[2]/div/p/a[2]/img").Click();
        }




        [Then(@"I type message in this input and send it")]
        public void ThenITypeMessageInThisInputAndSendIt()
        {
            //---------------Генерация и подготовака случайных корректных данных для сообщения
            var Handy = new HandyFunctions();
            messageFromCustomer = Handy.RandomStringLatinAndNumbers(10);

            WebDriver.GetByPath("//div[@id='searchResult']/article[1]//input").SendKeys(messageFromCustomer); //-------набор сообщения
            WebDriver.GetByPath("//span[contains(text(),'Send')]").Click();                       //-------отправка сообщения
        }


        [Then(@"I check that message was sent from Gallery List and get ID and Title")]
        public void ThenICheckThatMessageWasSentFromGalleryList()
        {
            string actualMessage =
    (WebDriver.GetByPath("//div[@id='searchResult']/article[1]//div[contains(text(),'My Messages')]/following-sibling::*/li").Text);      //------задание актуального сообщения в переменную

            if ((messageFromCustomer + " • by me just now") != actualMessage)                //-------сравнение отправленного сообщения и актуального
            {
                Assert.Fail("Message must be " + messageFromCustomer + " • by me just now" + "\n Actual is " + actualMessage);
            }

            vehicleID = WebDriver.GetByPath("//div[@id='searchResult']/article[1]//div/a/img").GetAttribute("id"); //-----задание актуального ID в переменную
            vehicleID = vehicleID.Substring(1);

            vehicleTitle = WebDriver.GetByPath("//div[@id='searchResult']/article[1]//a/span").Text;                //-----задание актуального Title в переменную


        }

        [Then(@"I check ID and Title")]
        public void ThenICheckIDAndTitle()
        {
            string actualID = WebDriver.GetByPath("//div[@id='messagesListArea']/div/div/div/p/span/span[2]").Text; //-----задание актуального ID в переменную
            if (vehicleID != actualID)                                                                             //-------сравнение ID
            {
                Assert.Fail("ID must be " + vehicleID + "\n Actual is " + actualID);
            }


            string actualTitle = WebDriver.GetByPath("//div[@class='inventory-item']//h1").Text;//-----задание актуального Title в переменную
            if (vehicleTitle != actualTitle)                                                    //-------сравнение Title
            {
                Assert.Fail("Title must be " + vehicleTitle + "\n Actual is " + actualTitle);
            }

        }

        [Then(@"I make logout")]
        public void ThenIMakeLogout()
        {
            WebDriver.GetById("ucToolbar_imgUser").Click(); //-----клик на иконку User
            WebDriver.GetByPath("//a[contains(text(), 'Log Out')]").Click(); //-----клик на Logout
        }

        [Then(@"I click on SignIn button")]
        public void ThenIClickOnSignInButton()
        {
            WebDriver.GetById("ucToolbar_lblLogInOut").Click();  //-----нажатие на кнопку Sign In
        }

        [Then(@"I check Notification in Header about message from Customer")]
        public void ThenICheckNotificationInHeader()
        {
            Assert.True(WebDriver.GetByPath("//a[@id='ddlMessageLink']/span[contains(text(),'1')]").Enabled);     //----проверка наличие 1
            WebDriver.GetByPath("//a[@id='ddlMessageLink']/i[1]").Click();  //---- клик на конверт
            Assert.True(WebDriver.GetByPath("//ul/li[1]/a/span/span/div[contains(text(),'" + messageFromCustomer + "')]").Enabled); //---проверка текста сообщения
        }

        [Then(@"I check Notification in Dashboard")]
        public void ThenICheckNotificationInDashboard()
        {
            Assert.True((WebDriver.GetByPath("//a[@href='/Dashboard/Messages/List']/span/span[contains(text(),'1')]").Enabled));

        }

        [Then(@"I open Messages")]
        public void ThenIOpenMessages()
        {
            WebDriver.GetByPath("//div[2]/aside/ul/li[3]/a/span").Click();
        }


        [Then(@"I check message from Customer")]
        public void ThenICheckMessageFromCustomer()
        {
            string actualMessage = WebDriver.GetByPath("//div[@id='messagesGroupArea']//p/span[3]").Text;//------задание актуального сообщения в переменную

            if (messageFromCustomer != actualMessage)                                                                //-------сравнение отправленного сообщения и актуального
            {
                Assert.Fail("Message must be " + messageFromCustomer + "\n Actual is " + actualMessage);
            }
        }

        [Then(@"I type message from Dealer")]
        public void ThenITypeMessageFromDealer()
        {
            var Handy = new HandyFunctions();
            messageFromDealer = Handy.RandomStringLatinAndNumbers(10);
            WebDriver.GetByPath("//input[@placeholder='Write a message']").SendKeys(messageFromDealer);  //----написание нового сообщения

            WebDriver.GetByPath("//a[contains(text(),'Send')]").Click(); //---отправка сообщения
        }



        [Then(@"I click on logo")]
        public void ThenIClickOnLogo()
        {
            WebDriver.GetByPath("//img[@alt='logo']").Click();
        }

        [Then(@"I check messsage from Dealer")]
        public void ThenICheckMesssageFromDealer()
        {
            string actualMessageFromDealer = WebDriver.GetByPath("//ul/li[2]/div/span[5]").Text;

            if (messageFromDealer != actualMessageFromDealer)                //-------сравнение отправленного сообщения и актуального
            {
                Assert.Fail("Message must be " + messageFromDealer + "\n Actual is " + actualMessageFromDealer);
            }
        }

        [Then(@"I check Notification in Header about message from Dealer")]
        public void ThenICheckNotificationInHeaderAboutMessageFromDealer()
        {
            Assert.True(WebDriver.GetByPath("//a[@id='ddlMessageLink']/span[contains(text(),'1')]").Enabled);     //----проверка наличие 1
            WebDriver.GetByPath("//a[@id='ddlMessageLink']/i[1]").Click();  //---- клик на конверт
            Assert.True(WebDriver.GetByPath("//ul/li[1]/a/span/span/div[contains(text(),'" + messageFromDealer + "')]").Enabled); //---проверка текста сообщения
        }



    }
}
