﻿Feature: PrivateMessagesCleaningCustomer
	 Cleaning our private messages
   before regression testing

@ClearingPrivateMessagesForCustomer
Scenario Outline: PrivateMessagesCleaningCustomer
	Given I open <webSite> and type <phone>
	Then I click on SignIn button
	When I type my login and password of a customer and submit them
	Then I go to the Dashboard
	And I erase all public messages there (service)
			Examples:
	| webSite         | phone      |
	|      bronze	  | 7327572923 | 
	|      silver     |   0        |
	|       gold      |   0	       |