﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OpenQA.Selenium;
using TechTalk.SpecFlow;
using OpenQA.Selenium;
using NUnit;
using System.IO;
using Common;
using NUnit.Core;
using NUnit.Framework;

namespace MDOclass.AccountScenarios
{
    [Binding]
    public class RegistrationSteps : BaseSteps
    {

        string first = "1234567890";  //строковые данные для регистрации (будут изменены генератором по ходу дела)
        string last = "1234567890";
        string email = "12345678901234567890123"; //после генерации будет добавлено @test.tsbua.com
        string phone = "1234567890";
        string pass = "1234567890";
        string passconfirm = "1234567890";
        private string VerificationString;

        [Given(@"I click the Register button")]
        public void GivenIClickTheRegisterButton()
        {
         WebDriver.FindElement(By.XPath("//*[contains(text(),'Register')]")).Click();
        }

        [Given(@"I fill in some correct credentials and click Register")]
        public void GivenIFillInAndClickRegister()
        {
           
            //---------------Генерация и подготовака случайных корректных данных для регистрации
            var Handy = new HandyFunctions();

            first=Handy.RandomStringLatinAndNumbers(10);
            last = Handy.RandomStringLatinAndNumbers(10);
            email = Handy.RandomStringLatinAndNumbers(8) + "@test.tsbua.com";
          //phone = Handy.RandomStringLatinAndNumbers(10);
            pass = Handy.RandomStringLatinAndNumbers(10);
            passconfirm = pass;

            //-----Для проверки регистрации потом сверим имя с хедерным текстом
            VerificationString = first;

            //----------------Заполняем поля корректными данными
            WebDriver.FindElement(By.XPath(("//*[@id='CPH_txtFN']"))).SendKeys(first);
            WebDriver.FindElement(By.XPath(("//*[@id='CPH_txtLN']"))).SendKeys(last);
            WebDriver.FindElement(By.XPath(("//*[@id='CPH_txtEml']"))).SendKeys(email);
            WebDriver.FindElement(By.XPath(("//*[@id='CPH_txtPhone']"))).SendKeys(phone);
            WebDriver.FindElement(By.XPath(("//*[@id='CPH_txtPWD']"))).SendKeys(pass);
            WebDriver.FindElement(By.XPath(("//*[@id='CPH_txtPWDConf']"))).SendKeys(passconfirm);

            //-----------------Кликаем на Register
            WebDriver.FindElement(By.XPath(("//*[@id='CPH_btnRegister']"))).Click();
        }

        [Then(@"I should be redirected to main with my name on the top")]
        public void ThenIShouldBeRedirectedToMainWithMyNameOnTheTop()
        {

            Assert.IsNotNull(WebDriver.FindElement(By.Id("ucToolbar_imgUser")));
        }

        [When(@"I enter wrong (.*),(.*),(.*),(.*),(.*),(.*) and click Register")]
        public void WhenIEnterWrongAndClickRegister(string p0, string p1, string p2, string p3, string p4, string p5)
        {
            //-------------------Проверяем возможно не заполнены все поля, тогда пропускаем такой кейс
            if (p0 == "" && p1 == "" && p2 == "" && p3 == "" && p4 == "" && p5 == "")
            {
                //Заполняем поля НЕкорректными данными
                WebDriver.FindElement(By.XPath(("//*[@id='CPH_txtFN']"))).SendKeys(p0);
                WebDriver.FindElement(By.XPath(("//*[@id='CPH_txtLN']"))).SendKeys(p1);
                WebDriver.FindElement(By.XPath(("//*[@id='CPH_txtEml']"))).SendKeys(p2);
                WebDriver.FindElement(By.XPath(("//*[@id='CPH_txtPhone']"))).SendKeys(p3);
                WebDriver.FindElement(By.XPath(("//*[@id='CPH_txtPWD']"))).SendKeys(p4);
                WebDriver.FindElement(By.XPath(("//*[@id='CPH_txtPWDConf']"))).SendKeys(p5);

                //Кликаем на Register
                WebDriver.FindElement(By.XPath(("//*[@id='CPH_btnRegister']"))).Click();
            }

            //------------------Иначе догенерируем остальные поля случайными значениями
            else
            {
                var Handy = new HandyFunctions();

                if (p0 == "")
                    p0 = Handy.RandomStringLatinAndNumbers(8);
                if (p1 == "")
                    p0 = Handy.RandomStringLatinAndNumbers(8);
                if (p2 == "")
                    p0 = Handy.RandomStringLatinAndNumbers(8) + "@test.tsbua.com";
                if (p3 == "")
                    p0 = Handy.RandomStringLatinAndNumbers(8);
                if (p4 == "")
                    p0 = Handy.RandomStringLatinAndNumbers(8);
                if (p5 == "")
                    p0 = Handy.RandomStringLatinAndNumbers(8);

                //Заполняем поля НЕкорректными данными
                WebDriver.FindElement(By.XPath(("//*[@id='CPH_txtFN']"))).SendKeys(p0);
                WebDriver.FindElement(By.XPath(("//*[@id='CPH_txtLN']"))).SendKeys(p1);
                WebDriver.FindElement(By.XPath(("//*[@id='CPH_txtEml']"))).SendKeys(p2);
                WebDriver.FindElement(By.XPath(("//*[@id='CPH_txtPhone']"))).SendKeys(p3);
                WebDriver.FindElement(By.XPath(("//*[@id='CPH_txtPWD']"))).SendKeys(p4);
                WebDriver.FindElement(By.XPath(("//*[@id='CPH_txtPWDConf']"))).SendKeys(p5);

                //Кликаем на Register
                WebDriver.FindElement(By.XPath(("//*[@id='CPH_btnRegister']"))).Click();
            }

        }


        [Then(@"I should received the appropriate error message")]
        public void ThenIShouldReceivedTheAppropriateErrorMessage()
        {
          WebDriver.FindElement(By.XPath(("//*[@id='CPH_divErorrMessage']")));
        }

        [Then(@"I check whether all parameters of our new user are correct")]
        public void ThenICheckWhetherAllParametersOfOurNewUserAreCorrect()
        {
            Assert.IsTrue(WebDriver.FindElement(By.XPath("//table[@class='table table-bordered table-striped']/tbody/tr[1]/td[2]/span[2]"))
                .Text==first+" "+last);
            Assert.IsTrue(WebDriver.FindElement(By.XPath("//table[@class='table table-bordered table-striped']/tbody/tr[1]/td[3]/span[2]"))
                .Text == email);
            Assert.IsTrue(WebDriver.FindElement(By.XPath("//table[@class='table table-bordered table-striped']/tbody/tr[1]/td[4]/span[2]"))
                .Text == phone);
            Assert.IsTrue(WebDriver.FindElement(By.XPath("//table[@class='table table-bordered table-striped']/tbody/tr[1]/td[5]/span[2]"))
                .Text == "Active");
        }

        [Then(@"I mark this registration entry as read")]
        public void ThenIMarkThisRegistrationEntryAsRead()
        {
            WebDriver.FindElement(By.XPath(("//a[@class='btn btn-success btn-xs']"))).Click();
            
        }


      }
}
