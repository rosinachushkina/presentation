﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using TechTalk.SpecFlow;
using Common;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using NUnit.Framework;

namespace MDOclass.AccountScenarios
{
    [Binding]
    public class CallRequestsSteps : BaseSteps
    {
        private string NameForVerifying, PhoneForVerifying;

        [Then(@"I click the Request a Phone Call button")]
        public void ThenIClickTheRequestAPhoneCallButton()
        {
            WebDriver.FindElement(By.XPath("//*[@id='CPH_lblPhoneCallCaption']")).Click();
        }

        [Then(@"I click the Send button")]
        public void ThenIClickTheSendButton()
        {
            WebDriver.FindElement(By.XPath("//*[@id='CPH_lblCallCaption']")).Click();
            Thread.Sleep(5000);
        }

        [Then(@"I log out")]
        public void ThenILogOut()
        {
            //----Костыль: т.к. layout отличается для бронзы и остальных, делаем проверку
            String CurLink = WebDriver.GetCurrentUrl();
          
            CurLink = CurLink.Substring(7, CurLink.Length - 7);
            var index = CurLink.IndexOf('/');
            CurLink = CurLink.Substring(0, index);
            CurLink = "http://" + CurLink;

            //----Логаут для бронзы
            if (CurLink == Pages.bronzePackage)
            {
                WebDriver.FindElement(By.XPath("//*[@id='ucToolbar_imgUser']")).Click();
                Thread.Sleep(1000);
                WebDriver.FindElement(By.XPath("//a[@href='/Logout.aspx']")).Click();
            }

            else
            //----Логаут для серебра и голда
            {
               // WebDriver.FindElement(By.XPath("/html/body/div/form/nav/div/div/div/div/div[2]/ul[1]/li[1]/a")).Click();
                  WebDriver.FindElement(By.XPath("//a[@class='dropdown-toggle dropdown-disabled']")).Click();          
                  WebDriver.FindElement(By.XPath("//a[@href='/Logout.aspx']")).Click();
            }
        }

        [Then(@"On the site header there should be a call request notification")]
        public void ThenOnTheSiteHeaderThereShouldBeACallRequestNotification()
        {
            Assert.IsTrue(WebDriver.FindElement(By.XPath("//span[@class='callrequest badge']")).Text == "1");
        }

        [Then(@"On the Dashboard Sidebar and on the Header there should be a notification")]
        public void ThenOnTheDashboardSidebarAndOnTheHeaderThereShouldBeANotification()
        {
            Assert.IsTrue(WebDriver.FindElement(By.XPath("//span[@class='callrequest badge pull-right']")).Text == "1");
            Assert.IsTrue(WebDriver.FindElement(By.XPath("//span[@class='callrequest badge']")).Text == "1");

        }

        [Then(@"I switch to Call Requests")]
        public void ThenISwitchToCallRequests()
        {
            WebDriver.FindElement(By.XPath("/html/body/div[2]/aside/ul/li[6]/a/span")).Click();
        }

        [Then(@"There should be my call request available")]
        public void ThenThereShouldBeMyCallRequestAvailable()
        {
            WebDriver.FindElement(By.XPath("//*[contains(text()," + "'" + MDOvariables.CustomerFirstname + "')]"));
            WebDriver.FindElement(By.XPath("//*[contains(text()," + "'" + MDOvariables.CustomerLastname + "')]"));
        }

        [When(@"I delete the last call request")]
        public void WhenIDeleteTheLastCallRequest()
        {
            WebDriver.FindElement(By.XPath("//div[@class='item read']")).Click();
            WebDriver.FindElement(By.XPath("//div[@class='item read']//a[@class='remove-icon']")).Click();
            WebDriver.SwitchTo().Alert().Accept();

        }

        [Then(@"The last call request should be vaporized")]
        public void ThenTheLastCallRequestShouldBeVaporized()
        {
            WebDriver.FindElement(By.XPath("//*[contains(text(),'You don')]"));
            WebDriver.FindElement(By.XPath("//*[contains(text(),'t have call requests at the moment')]"));
        }

        [Then(@"I enter my name and telephone")]
        public void ThenIEnterMyNameAndTelephone()
        {
            //-----------Генерация вводных параметров
            var Handy = new HandyFunctions();
            NameForVerifying = Handy.RandomStringLatinAndNumbers(10);
            PhoneForVerifying = Handy.RandomStringNumbers(10);

            //-----------Вводим данные
            WebDriver.FindElement(By.XPath("//input[@id='txtCallName']")).SendKeys(NameForVerifying);
            WebDriver.FindElement(By.XPath("//input[@id='txtCallPhone']")).SendKeys(PhoneForVerifying);

        }

        [Then(@"There should be my call request \(with (.*) verification strings\) available")]
        public void ThenThereShouldBeMyCallRequestWithVerificationStringsAvailable(int p0)
        {
            WebDriver.FindElement(By.XPath("//*[contains(text()," + "'" + NameForVerifying + "')]"));
            WebDriver.FindElement(By.XPath("//*[contains(text()," + "'" + PhoneForVerifying + "')]"));
        }


    }
}
