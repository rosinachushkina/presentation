﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using Common;
using NUnit.Framework;
using TechTalk.SpecFlow;

namespace MDO_Service.ServiceActions
{
    [Binding]
    public class StepDefinition2 : BaseSteps
    {
        string s = "krubtvgr";
        string stroka;
        string fullemail;
        //WebDriver.Url = "http://ya.ru";
        //WebDriver.GetByPath("hgfhy");
        //WebDriver.GetByPath("hgfhy").Click();
        //WebDriver.GetByPath("hgfhy").SendKeys("khjgjkhgfjgfhg");
        // Assert.True( WebDriver.GetByPath("//tbody/tr[1]/td[1]/b[contains(text(), 'E-mail')]").Enabled);


        [Given(@"TEST i have open povarenok main page")]
        public void GivenTESTIHaveOpenPovarenokMainPage()
        {
            WebDriver.Url = "http://www.povarenok.ru/";
        }

        [Given(@"TEST i click on login button")]
        public void GivenTESTIClickOnLoginButton()
        {
            WebDriver.GetByPath("//input[@value='Войти на сайт']").Click();
        }

        [Then(@"TEST i enter the login cerdantions and confirm it")]
        public void ThenTESTIEnterTheLoginCerdantionsAndConfirmIt()
        {
            WebDriver.GetByPath("//tbody/tr[1]/td[1]/b[contains(text(), 'E-mail')]");

            WebDriver.GetByPath("//*[@id='login_id']").SendKeys(s);
            WebDriver.GetByPath("//tbody/tr[2]/td[1]/strong[contains(text(), 'Пароль')]");
            WebDriver.GetByPath("//*[@id='password_id']").SendKeys(s);
            WebDriver.GetByPath("//tbody/tr[3]/td[2]/input[@value='Войти!']").Click();
        }
        [Then(@"TEST i check my profile page")]
        public void ThenTESTICheckMyProfilePage()
        {
            Thread.Sleep(5000);
            WebDriver.GetByPath("//*[@id='sideLeft']/noindex/div/div[1]/div[contains(text(), 'Здравствуйте," + " " + s + "')]");

        }
        [Given(@"TEST I should have ability generating new email on the guerrilla mail")]
        public void GivenTESTGenerationNewEmailOnTheGuerrillaMail()
        {

            WebDriver.GetByPath("//*[@id='inbox-id']").Click();
            var h = new HandyFunctions();
            stroka = h.RandomStringLatin(5);
            WebDriver.GetByPath("//*[@id='inbox-id']/input").Clear();
            WebDriver.GetByPath("//*[@id='inbox-id']/input").SendKeys(stroka);
            WebDriver.GetByPath("//*[@id='inbox-id']/button[1]").Click();
            Thread.Sleep(5000);
            Assert.True(WebDriver.GetByPath("//*[@id='inbox-id']").Text == stroka);
        }


        [When(@"TEST I go to register page")]
        public void ThenTESTIGoToRegisterPge()
        {
            WebDriver.GetByPath("//a[@class='biglink']").Click();
            Thread.Sleep(5000);
            WebDriver.GetByPath("//td[1]/strong[contains(text(),'E-mail:')]");

        }

        [Then(@"TEST i have entered my saved email on guerrilla mail")]
        public void ThenTESTIShouldEnterEmailAddressHasGeneratedOnGuerrillaMail()
        {
            fullemail = stroka + "@sharklasers.com";
            WebDriver.GetByPath("//*[@id='email_id']").SendKeys(fullemail);
            WebDriver.GetByPath("//td[2]/input[@value='Продолжить']").Click();
            Thread.Sleep(1000);

        }
        [Given(@"TEST I have opened Guerrila mail servise")]
        public void GivenTESTOpenGuerrilaMailServise()
        {
            WebDriver.Url = "https://www.guerrillamail.com/";
        }

        [Then(@"TEST i have checked confirmation message")]
        public void ThenTESTIHaveCheckConfirmationMessage()
        {
            WebDriver.GetByPath("//div[3]/div/div/h1");
            WebDriver.GetByPath("//div[3]/div/div/strong");
            WebDriver.GetByPath("//div[3]/div/div/div[1]");
        }

        [Given(@"TEST I enter my generated email on guerrila mail\.")]
        public void GivenTESTIEnterMyGeneratedEmailOnGuerrilaMail_()
        {
            WebDriver.GetByPath("//*[@id='inbox-id']").Click();
            WebDriver.GetByPath("//*[@id='inbox-id']").SendKeys(fullemail);
            WebDriver.GetByPath("//*[@id='inbox-id']/button[1]").Click();
            Thread.Sleep(5000);
        }

        [Given(@"TEST i refresh page")]
        public void GivenTESTIRefreshPage()
        {
            WebDriver.Navigate().Refresh();
        }

        [Given(@"TEST i check my generated email")]
        public void GivenTESTICheckMyGeneratedEmail()
        {


        }
        [Given(@"TEST I check new email that generated on GM")]
        public void GivenTESTICheckNewEmailThatGeneratedOnGM()
        {
            Assert.True(WebDriver.GetByPath("//*[@id='inbox-id']").Text == stroka);
        }
    }
}
