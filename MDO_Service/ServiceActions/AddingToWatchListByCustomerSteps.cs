﻿using System;
using System.Threading;
using Common;
using NUnit.Core;
using NUnit.Framework;
using TechTalk.SpecFlow;

namespace MDOclass
{
    [Binding]

    public class AddingToWatchListByCustomerSteps : BaseSteps
    {
       private string vehicleName;
       private  string currentPrice;
        [Then(@"I click on search button")]
        public void ThenIClickOnSearchButton()
        {
            WebDriver.GetById("quickSearchButton").Click();
        }

        [Then(@"I click on clean search button")]
        public void ThenIClickOnCleanSearchButton()
        {
            WebDriver.GetById("CPH_ucSearchOptions_lblCleanVehicleCondition").Click();
        }

    
        [Then(@"I search for Vehicle with preview mode")]
        public void ThenISearchForVehicleWithPreviewMode()
        {
            WebDriver.GetByPath("//span[contains(text(), 'Preview')]/../../../..//img").Click();
        }



        [Then(@"I choose (.*)th vehicle")]
        public void ThenIChooseThInventory(int vehicleNumber)
        {
            WebDriver.GetByPath("//div[@id='searchResult']/div[" + vehicleNumber + "]//img").Click(); //-----выбор Vehicle с заданным номером
        }

        [Then(@"I click Add to Watchlist")]
        public void ThenIClickAddToWatchlist()
        {
            WebDriver.GetById("CPH_ucOpenInvoice_hplWL").Click();    //----------добавление Vehicle в Watchlist
            vehicleName = WebDriver.GetById("CPH_lblVehicleHeader").Text;  //----сохранение имени Vehicle для сравнения
            Assert.True(WebDriver.GetByPath("//span[contains(text(), 'On My Watchlist')]").Enabled); //--проверка появления надписи "On My Watchlist"
        }

        [Then(@"I go to Watchlist")]
        public void ThenIGoToWatchlistAndCheckThatVehicleAdded()
        {
            WebDriver.GetById("CPH_ucOpenInvoice_hplWL").Click(); //----редирект со страницы Vehicle в Watchlist
        }

        [Then(@"I check name of added Vehicle")]
        public void ThenICheckNameOfAddedVehicle()
        {
            string vehicleTitleInRightBlock = WebDriver.GetByPath("//article//h1").Text;  //-------сохранение в переменную названия Vehicle в списке справа
            string vehicleTitleInLeftBlock = WebDriver.GetByPath("//section/div[1]/div[1]//h2[1]").Text; //--сохраненение в переменную названия Vehicle в его preview

            Assert.True((vehicleTitleInRightBlock).Equals(vehicleTitleInLeftBlock)); //---сравнение названий справа и в preview
         //   Assert.True((vehicleTitleInRightBlock).Equals(vehicleName)); //-----сравнение сохраненного Vehicle на странице поиска и названия в Watchlist'e
            Assert.True(WebDriver.GetByPath("//h1[contains(text(), '"+ vehicleName +"')]").Enabled);
        }


        [Then(@"I check that current bid equals 0")]
        public void ThenICheckThatCurrentBidEquals()
        {
            Assert.True(WebDriver.GetByPath("//span[1][contains(text(), '$ 0 USD')]").Enabled);
        }


        [Then(@"I delete Vehicle from Watchlist")]
        public void ThenIDeleteVehicleFromWatchlist()
        {
            WebDriver.GetByPath("//button[contains(text(), 'Remove')]").Click();   //-----нажатие на кнопку Remove
            WebDriver.SwitchTo().Alert().Accept();                                 //-----нажатие Ок на Alert'e
        }

        [Then(@"I see phrase about empty Watchlist")]
        public void ThenISeePhraseAboutEmptyWatchlist()
        {
            Thread.Sleep(3000);
            //section[@id='listcontainer']/div/p/span[2]
            Assert.True(WebDriver.GetByPath("//section[@id='listcontainer']/div/p/span[2]").Text.Equals("You don't have any watchlists"));
            
        }

        [Then(@"I click on Buy Now search button")]
        public void ThenIClickOnBuyNowSearchButton()
        {
            WebDriver.GetById("CPH_ucSearchOptions_chkBuyNow").Click(); //-------клик на чекбокс Buy Now
            WebDriver.GetById("CPH_ucSearchOptions_hplSearch").Click(); //-------клик на кнопку Search
        }

        [Then(@"I search for Vehicle with ByuNow price")]
        public void ThenISearchForVehicleWithByuNowPrice()
        {

            Assert.True(WebDriver.GetByPath("//div[@id='searchResult']/div[1]//div[contains(text(), 'Buy Now')]").Enabled); //---Проверка, что у первого Vehicle присутствует Buy Now
            currentPrice = WebDriver.GetByPath("//div[@id='searchResult']/div[1]/div/div[2]/div[2]/span").Text; //----сохранение цены первого Vehicle на странице Gallery
            WebDriver.GetByPath("//div[@id='searchResult']/div[1]//img").Click(); //----выбор первого Vehicle с Buy Now
        }

        [Then(@"I compare prices on on List Page and preview Vehicle page")]
        public void ThenIComparePricesOnOnListPageAndPreviewVehiclePage()
        {
            Assert.True(currentPrice.Equals(WebDriver.GetById("CPH_ucOpenInvoice_lblBuyNowData").Text)); //--------------сравнение цен на странице Галлереи и Vehicle Preview
        }

        [Then(@"I check price of added Vehicle")]
        public void ThenICheckPriceOfAddedVehicle()
        {
            currentPrice = currentPrice.Substring(1);     //--------обрезка первого символа $
            currentPrice = currentPrice.Replace(",", ""); //--------обрезка символа "," в цене
            string actualVehiclePrice =
                WebDriver.GetByPath("//div[@id='inventoryContent']/div[1]/div[1]/div[2]/p/span[1]").Text.Substring(2); //-----обрезка первых двух символов в цене Vehicle в Watchlist'e

            if (currentPrice != actualVehiclePrice)
            {
                Assert.Fail("Price is " + currentPrice + "\n Actual is " + actualVehiclePrice);
            }
            

         //   Assert.True(currentPrice.Equals(WebDriver.GetByPath("//div[@id='inventoryContent']/div[1]/div[1]/div[2]/p/span[1]").Text));
        }

        [Then(@"I click on List Preview displaying")]
        public void ThenIClickOnListPreviewDisplaying()
        {
            WebDriver.GetByPath("//span[@class='glyphicon glyphicon-align-justify']").Click();
        }

        [Then(@"I search for Vehicle with Current Offer and add it to Watchlist")]
        public void ThenISearchForVehicleWithCurrentOffer()
        {

            WebDriver.GetByPath("//div[contains(text(), 'Current Offer')]/../div[3]/a[2]").Click();
        }
        [Then(@"I save current offer")]
        public void ThenISaveCurrentOffer()
        {
            currentPrice = WebDriver.GetByPath("//div[contains(text(), 'Current Offer')]/span").Text;
        }

        [Then(@"I go to Watchlist from List")]
        public void ThenIGoToWatchlistFromList()
        {
            WebDriver.GetByPath("//div[contains(text(), 'Current Offer')]/../../div/span[2]/a").Click();
        }



    }
}
