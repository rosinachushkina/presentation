﻿Feature: CustomerRegistrationsMarkAsPaid
	This test will mark all customer registrations
	as paid in Reports before regression testing begins

@CustomerRegistrationsMarkAsPaid
Scenario Outline: CustomerRegistrationsMarkAsPaid
	Given I open <webSite> and type <phone>
	And I click on SignIn button
	When I type my login and password of a dealer and submit them
	Then I go to the Dashboard
	Then I switch to Reports
	Then I mark all customer registrations as read

	Examples:
	| webSite         | phone      |
	|      bronze	  | 7327572923 | 
	|      silver     |   0        |
	|       gold      |   0	       |