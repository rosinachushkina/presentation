﻿Feature: PublicMessagesCleaning
	This test will erase all public messages from our portals
	so our regression test could go smoothly

@PublicMessagesCleaning
Scenario Outline: PublicMessagesCleaning
	
	Given I open <webSite> and type <phone>
	And I click on SignIn button
	When I type my login and password of a dealer and submit them
	Then I go to the Dashboard
	When I switch to Dashboard > Public Messages
	Then I erase all public messages there (service)

	Examples:
	| webSite         | phone      |
	|      bronze	  | 7327572923 | 
	|      silver     |   0        |
	|       gold      |   0	       |
	