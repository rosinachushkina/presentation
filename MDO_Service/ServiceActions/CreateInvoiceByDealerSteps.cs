﻿using System;
using System.Runtime.Remoting.Proxies;
using System.Threading;
using Common;
using MDOclass;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.Events;
using OpenQA.Selenium.Support.UI;
using TechTalk.SpecFlow;



namespace MDO_Service
{
    [Binding]
    public class CreateInvoiceByDealerSteps : BaseSteps
    {


        private bool ex = false;
        private int totalPages;

        [Then(@"I switch to Invoices")]
        public void ThenISwitchToInvoices()
        {
            WebDriver.GetByPath("//a[@href='/Dashboard/Invoices/List']").Click();
        }


        [Then(@"I click on Mark As Read")]
        public void ThenIClickOnMarkAsRead()
        {
            string totalItemsString = WebDriver.GetByPath("//h4[@id='totalCustomerInvoicesItems']/b").Text;
            int totalItems = Convert.ToInt32(totalItemsString);
            totalPages = (totalItems/15)+1;

            for (int j = 1; j <= totalPages; j++)
            {
                WebDriver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(10));
                WebDriver.GetByPath("//ul[@class='pagination']/li/a[contains(text(), '" + j + "')]").Click();
                Thread.Sleep(1000);
                WebDriver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(0));
                for (int i = 1; i < 16; i++)
                {
                    try
                    {
                        WebDriver.GetByPath("//table/tbody/tr[" + i + "]/td[10]/a[2][contains(text(), 'Mark As Read')]")
                            .Click();
                    }
                    catch (WebDriverException exception)
                    {
                        ex = true;
                        
                        
                    }
                }

                }
            Thread.Sleep(5000);
            WebDriver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(10));
        }
        [Then(@"I clean fucking all")]
        public void ThenICleanFuckingAll()
        {
            WebDriver.GetByPath("//table/tbody/tr/td[10]/a[2][contains(text(), 'Mark As Read')]").Click();
        }

      }

}
