﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using Common;
using Common.Generators;
using Common.Queries;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;

public static class WebDriverExtensions
{
    private static readonly Dictionary<string, Func<string>> macrosFactory = new Dictionary<string, Func<string>>
    {
        {"{{?name}}", NamesGenerator.GetFirstName},
        {"{{?lastname}}", NamesGenerator.GetLastName},
        {"{{?email}}", EmailGenerator.GenerateEmail}
    };

    public static void Go(this IWebDriver webDriver, string url)
    {
        webDriver.Navigate().GoToUrl(url);

    }

    public static IWebElement GetById(this IWebDriver webDriver, string id)
    {
        return webDriver.FindElement(By.Id(id));
    }

    public static IWebElement GetByName(this IWebDriver webDriver, string name)
    {
        return webDriver.FindElement(By.Name(name));
    }

    public static IWebElement GetByPath(this IWebDriver webDriver, string xpath)
    {
        return webDriver.FindElement(By.XPath(xpath));
    }

    public static IWebElement GetByCss(this IWebDriver webDriver, string cssSelector)
    {
        return webDriver.FindElement(By.CssSelector(cssSelector));
    }

    public static ElementsQuery GetBy(this IWebDriver webDriver, string selector)
    {
        return new ElementsQuery(webDriver) {Selector = selector};
    }

    public static string GetCurrentUrl(this IWebDriver webDriver, bool withQueryParams = false)
    {
        var uri = new Uri(webDriver.Url);
        return withQueryParams ? webDriver.Url : uri.GetLeftPart(UriPartial.Path);
    }

    public static void FillField(this IWebDriver webDriver, FormField field)
    {
        if (field == null)
            throw new ArgumentNullException("field");
        IWebElement element = null;
        element = field.Path.Contains("/") ? webDriver.GetByPath(field.Path) : webDriver.GetByName(field.Path);

        if (element == null)
            throw new InvalidOperationException(String.Format("Can't find field by Path '{0}'", field.Path));

        element.SendKeys(macrosFactory.ContainsKey(field.Value) ? macrosFactory[field.Value]() : field.Value);
    }

    public static object ExecuteJavaScript(this IWebDriver webDriver,string javaScript, params object[] args)
    {
        var javaScriptExecutor = (IJavaScriptExecutor) BaseSteps.WebDriver;

        return javaScriptExecutor.ExecuteScript(javaScript, args);
    }

    public static void WaitFullLoad(this IWebDriver webDriver)
    {
        while (!(bool)webDriver.ExecuteJavaScript("return document.readyState == 'complete'") ||
               !(bool)webDriver.ExecuteJavaScript("return (typeof(cash) === 'undefined') ? true : !$.active;"))
        {
            Thread.Sleep(100);
        }

    }

    public static IWebElement waitForFindElement(this IWebDriver driver, By by, int timeoutInSeconds)
    {
        if (timeoutInSeconds > 0)
        {
            var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(timeoutInSeconds));
            return wait.Until(drv => drv.FindElement(by));
        }
        return driver.FindElement(by);
    }

    public static bool IsElementDisplayed(this IWebDriver driver, By element)
    {
        if (driver.FindElements(element).Count > 0)
        {
            if (driver.FindElement(element).Displayed)
                return true;
            else
                return false;
        }
        else
        {
            return false;
        }
    }


    public static bool IsElementEnabled(this IWebDriver driver, By element)
    {
        if (driver.FindElements(element).Count > 0)
        {
            if (driver.FindElement(element).Enabled)
                return true;
            else
                return false;
        }
        else
        {
            return false;
        }
    }
    public static bool Exists(this IWebDriver driver, By selector)
    {
        return driver.FindElements(selector).Any();
    }
}