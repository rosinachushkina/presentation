﻿#region Usings

using System.Collections.Generic;
using System.IO;
using System.Reflection;
using OpenQA.Selenium;
using System.Text;
using System;

#endregion

namespace Common
{  
    public class BaseSteps
    {
        public static IWebDriver WebDriver;
        public static int StandardTimeOut = 10;

    }


    public class FileBasedSteps : BaseSteps
    {

        public static List<string> Images = new List<string>()
        {
            Path.Combine(Path.GetTempPath(),"image.PNG"),
            Path.Combine(Path.GetTempPath(),"thumbnail.PNG")
        };

        public static void InitFiles()
        {
            foreach (var image in Images)
            {
                Assembly a = Assembly.GetCallingAssembly();
                Stream s = a.GetManifestResourceStream("PartsBee.Files."+Path.GetFileName(image));
                BinaryReader sr = new BinaryReader(s);
                var sw = File.Create(image);
                byte[] bytes = sr.ReadBytes((int)s.Length);
                
                sw.Write(bytes,0,bytes.Length);
                sw.Flush();
                sw.Close();
                sr.Close();
            }
        }
        public static void DisposeFiles()
        {
            foreach (var image in Images)
            {
                if(File.Exists(image))
                    File.Delete(image);
            }
        }

    }
}