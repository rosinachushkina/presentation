﻿using System;
using System.Text;

namespace Common.Generators
{
    public static class EmailGenerator
    {
        private const string EMAIL_HOST = "@sharklasers.com";

        public static string GenerateEmail()
        {
            return RandomString(20) + EMAIL_HOST;
        }

        private static string RandomString(int size)
        {
            var rnd = new Random((int) DateTime.Now.Ticks);
            var builder = new StringBuilder();
            char ch;
            for (int i = 0; i < size; i++)
            {
                ch = Convert.ToChar(Convert.ToInt32(Math.Floor(26*rnd.NextDouble() + 65)));
                builder.Append(ch);
            }

            return builder.ToString();
        }
    }
}