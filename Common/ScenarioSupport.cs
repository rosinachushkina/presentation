﻿using System;
using Common;
using NUnit.Framework;
using OpenQA.Selenium.Firefox;
using TechTalk.SpecFlow;

[Binding]
public class ScenarioSupport
{
    //[BeforeTestRun]
    [BeforeScenario]
    [SetUp]
    public static void BeforeTestRun()
    {
        BaseSteps.WebDriver = new FirefoxDriver();
        BaseSteps.WebDriver.Manage().Cookies.DeleteAllCookies();
        BaseSteps.WebDriver.Manage().Window.Maximize();
        BaseSteps.WebDriver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(10));
    }

    //[AfterTestRun]
    [AfterScenario]
    public static void AfterTestRun()
    {
        if (BaseSteps.WebDriver != null)
        {
            BaseSteps.WebDriver.Quit();
            BaseSteps.WebDriver = null;
        }
    }

}