﻿using System;
using System.Collections.Generic;
using System.Linq;
using Common;
using NUnit.Framework;
using TechTalk.SpecFlow;
using TechTalk.SpecFlow.Assist;

namespace MDO.AccountScenarios
{
    [Binding]
    public class RegistrationSteps : BaseSteps
    {
        [Given(@"I am on web site {{(.*)}}")]
        public void GivenIAmOnWebSiteHttpDiamond_Mydealeronline_Com(string website)
        {
            WebDriver.Go(website);
        }

        [When(@"I have clicked on Sign In button {{(.*)}}")]
        public void WhenIHaveClickedOnSignInButtonUcToolbar_HplLogInOut(string elementId)
        {
            WebDriver.GetById(elementId).Click();
            
        }
        
        [Then(@"I'm navigated to login page {{(.*)}}")]
        public void ThenIMNavigatedToHttpDiamond_Mydealeronline_ComDashboardAccountsLogin(string expectedDestination)
        {

            Assert.True(expectedDestination.Equals(WebDriver.GetCurrentUrl(), StringComparison.InvariantCultureIgnoreCase));
        }

        [When(@"I click on register button")]
        public void WhenIClickOnRegisterButton()
        {
            WebDriver.GetByPath("//a[@href='/Registration/default.aspx']").Click();
        }

        [Then(@"I'm navigated to registration page {{(.*)}}")]
        public void ThenIMNavigatedToHttpDiamond_Mydealeronline_ComRegistrationDefault_Aspx(string expectedDestination)
        {
            Assert.True(expectedDestination.Equals(WebDriver.GetCurrentUrl(), StringComparison.InvariantCultureIgnoreCase));
        }

        [When(@"I put all user data")]
        public void WhenIPutAllUserData(Table table)
        {
            var fields = table.CreateSet<FormField>().ToList();
            foreach (var formField in fields)
            {
                WebDriver.FillField(formField);
            }
        }

        [When(@"Click submit button {{(.*)}}")]
        public void WhenClickSubmitButtonCPH_BtnRegister(string buttonId)
        {
            WebDriver.GetById(buttonId).Click();
        }

        [When(@"I put phone number of dealer")]
        public void WhenIPutPhonNumberOfDealer(Table table)
        {
            List<FormField> fields = table.CreateSet<FormField>().ToList();
            foreach (FormField formField in fields)
            {
                WebDriver.FillField(formField);
            }
        }

        [When(@"Click Sing in button {{(.*)}}")]
        public void WhenClickSingInButtonCPH_BtnSignByPhone(string elementId)
        {
            WebDriver.GetById(elementId).Click();
        }

        [Then(@"I'm on home page as registered user")]
        public void ThenIMOnHomePageAsRegisteredUser()
        {
            var element = WebDriver.GetBy("#ddlMessageLink").SetTimer(10000).Find();
            Assert.IsNotNull(element);
            
        }

    }
}