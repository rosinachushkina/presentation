﻿Feature: SendMessageByCustomer
	In order to 
	As a customer
	I want to send a message to dealer

@message_by_customer
Scenario Outline: Message By Customer
	Given I am site <site>, login as a customer with <phone> <email> and <password>
	Then I click on Salvage vehicle {{salvagecars_items}}
	When I press 5 vehicle
	And write {{test message}} to dealer
	#Then I go to the second vehicle
	#Then write second {{test message}} to dealer
	#Then I go to the third vehicle
	#Then write third {{test message}} to dealer
	Then I click Dashboard button {{ucToolbar_hplMyDash}}
	And check messages in my dashboard

	Examples: 
	| site                                      | phone      | email      | password |
	| http://diamondqa.myvirtualinventory.com   | 0          | fara@1.com | 1        |
	| http://platinumqa.myvirtualinventory.com | 0          | fara@1.com | 1        |
	| http://myvirtualinventory.com            | 7327572923 | fara@1.com | 1        |
