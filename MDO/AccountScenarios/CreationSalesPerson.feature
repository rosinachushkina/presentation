﻿@CreationSalesPerson
Feature: CreationSalesPerson
	In order to cheсk site functionality
	As a owner of portal
	I want to create Sales persons for my site 

Background: 
	Given I am on portal webpage {{https://portal.mydealeronline.com}}
	Then I switch site language on English
	Then I click on login button {{ucHeader_lblHdnLogIn}}
	Then I enter user date to login form
		|Path				|Value							|
		|CPH_txtDE			|test1mydealeronline@tsbua.com	|
		|CPH_txtPW			|1								|
	And press Sign in {{CPH_btnSignIn}}
	

@add_sales_person_platinum
Scenario: create Sales person for Platinum account
		Then I click on settings for site {{//*[@id='wrap']/div[4]/div/div[2]/div/div[3]/span[2]/a}}
		When Click on Sales Person Block {{CPH_lblSalesTeamHead}}
		Then I enter data for new sales person
		|Path				|Value							|
		|//*[@id='sales_team_add_new_form']/div[1]/div[1]/input		|FirstName2			|
		|//*[@id='sales_team_add_new_form']/div[1]/div[2]/ input		|LastName2			|
		|//*[@id='sales_team_add_new_form']/div[1]/div[3]/input		|Title				|
		|//*[@id='sales_team_add_new_form']/div[2]/div[1]/input		|PhoneNumber		|
		|//*[@id='sales_team_add_new_form']/div[2]/div[2]/input		|email@email.com	|
		|//*[@id='sales_team_add_new_form']/div[2]/div[3]/input		|Languages			|
		|//*[@id='add_user_name_pass']/div[1]/input					|Username			|
		|//*[@id='SiteUserNamePassword']							|Password			|
		|//*[@id='SiteUserNamePasswordConfirm']						|Password			|
		And check Show this member on site {{//*[@id='sales_team_add_new_form']/div[3]/div[1]/input}}
		And click Add Sales Person button {{CPH_ucSalesTeam_Label1}}
		Then new sales person added {{//*[@id='sales_team_member_grid_area']/tbody/tr[2]/td[2][contains(text(),'FirstName2 LastName2')]}}
		Then I go to the site and check if the sales person is present
		#Then I try to change sale's person info
		Then I delete the created sales person

@add_sales_person_diamond
Scenario: create Sales person for Diamond account
		Then I click on settings for site {{//*[@id='wrap']/div[4]/div/div[3]/div/div[3]/span[2]/a}}
		When Click on Sales Person Block {{CPH_lblSalesTeamHead}}
		Then I enter data for new sales person
		|Path				|Value							|
		|//*[@id='sales_team_add_new_form']/div[1]/div[1]/input		|FirstName2			|
		|//*[@id='sales_team_add_new_form']/div[1]/div[2]/input		|LastName2			|
		|//*[@id='sales_team_add_new_form']/div[1]/div[3]/input		|Title				|
		|//*[@id='sales_team_add_new_form']/div[2]/div[1]/input		|PhoneNumber		|
		|//*[@id='sales_team_add_new_form']/div[2]/div[2]/input		|email@email.com	|
		|//*[@id='sales_team_add_new_form']/div[2]/div[3]/input		|Languages			|
		|//*[@id='add_user_name_pass']/div[1]/input					|Username			|
		|//*[@id='SiteUserNamePassword']							|Password			|
		|//*[@id='SiteUserNamePasswordConfirm']						|Password			|
		And check Show this member on site {{//*[@id='sales_team_add_new_form']/div[3]/div[1]/input}}
		And click Add Sales Person button {{CPH_ucSalesTeam_Label1}}
		Then new sales person added {{//*[@id='sales_team_member_grid_area']/tbody/tr[2]/td[2][contains(text(),'FirstName2 LastName2')]}}
		Then I go to the site and check if the sales person is present
		Then I try to change sale's person info
		And I check if changes are saved
		Then I delete the created sales person



