﻿using Common;
using NUnit.Framework;
using TechTalk.SpecFlow;

[Binding]
public class LoginStepsTest : BaseSteps
{
    [Given(@"I am on {{(.*)}} site")]
    public void GivenNavigate(string site)
    {
        WebDriver.Go(site);
    }

    [When(@"clicked on Sigin/ register button")]
    public void WhenClikedOnSigInRegisterButton()
    {
        WebDriver.GetById("ucToolbar_lblLogInOut").Click();
    }

    [When(@"I have entered login {{(.*)}} and password {{(.*)}}")]
    public void WhenIHaveEnteredLoginFara_ComAndPassword(string login, string password)
    {
        WebDriver.GetByName("username").SendKeys(login);
        WebDriver.GetByName("password").SendKeys(password);
    }

    [When(@"I press Login")]
    public void WhenIPressLogin()
    {
        WebDriver
            .GetByPath(
                "/html/body[@class='login']/div[@class='content']/form[@class='login-form']/div[@class='form-actions']/button[@class='btn green pull-right']")
            .Click();
    }

    [Then(@"I should appear on {{(.*)}}")]
    public void ThenIShouldGoToHomePage(string website)
    {
        Assert.AreEqual(website, WebDriver.Url);
    }
}