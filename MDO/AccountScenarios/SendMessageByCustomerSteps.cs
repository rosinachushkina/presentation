﻿using System;
using System.Collections.ObjectModel;
using Common;
using NUnit.Framework;
using OpenQA.Selenium;
using TechTalk.SpecFlow;

namespace MDO.AccountScenarios
{
    [Binding]
    public class SendMessageByCustomerSteps : BaseSteps
    {
        [Given(@"I am site (.*), login as a customer with (.*) (.*) and (.*)")]
        public void GivenIAmSiteHttpDiamondqa_Myvirtualinventory_ComLoginAsACustomerWithFara_ComAnd(string site, string phone, string email, string password)
        {
            WebDriver.Go(site);
            if (Convert.ToDecimal(phone) != 0)
            {
                WebDriver.GetById("txtPhone").SendKeys(phone);
                WebDriver.GetById("CPH_btnSignByPhone").Click();
            }
            WebDriver.GetById("ucToolbar_lblLogInOut").Click();
            WebDriver.GetByName("username").SendKeys(email);
            WebDriver.GetByName("password").SendKeys(password);
            WebDriver.GetByPath("//div[@class='form-actions']/button").Click();
        }

        [Then(@"I click on Salvage vehicle {{(.*)}}")]
        public void ThenIClickOnSalvageVehicleSalvagecars_Items(string salvageButton)
        {
            WebDriver.GetById(salvageButton).Click();
        }

        [When(@"I press (.*) vehicle")]
        public void WhenIPressVehicle(int vehicleNumber)
        {
            WebDriver.GetByPath("//div[@id='searchResult']/div[" + vehicleNumber + "]//img").Click();
        }

        [Then(@"I go to the second vehicle")]
        public void ThenIGoToTheSecondVehicle()
        {
            WebDriver.GetById("CPH_hplNext").Click();
        }
        [Then(@"I go to the third vehicle")]
        public void ThenIGoToTheThirdVehicle()
        {
            WebDriver.GetById("CPH_hplNext").Click();
        }

        [When(@"write {{(.*)}} to dealer")]
        public void WhenWriteTestMessageToDealer(string message)
        {
            WebDriver.GetBy("//input[@id='txtNewMsg']").SetTimer(2000).Find().SendKeys(message);
            WebDriver.GetBy("//span[@id='CPH_lblSaveMsgCaption']").SetTimer(2000).Find().Click();

            WebDriver.GetBy("//div[@id = 'divAllComments']//li[contains(text(), '" + message + "')]").SetTimer(5000).Find();
        }

        [Then(@"write second {{(.*)}} to dealer")]
        public void ThenWriteTestMessageToDealer(string message)
        {
            WebDriver.GetBy("//input[@id='txtNewMsg']").SetTimer(2000).Find().SendKeys(message);
            WebDriver.GetBy("//span[@id='CPH_lblSaveMsgCaption']").SetTimer(2000).Find().Click();

            WebDriver.GetBy("//div[@id = 'divAllComments']//li[contains(text(), '" + message + "')]").SetTimer(5000).Find();
        }

        [Then(@"write third {{(.*)}} to dealer")]
        public void ThenWriteThirdTestMessageToDealer(string message)
        {
            WebDriver.GetBy("//input[@id='txtNewMsg']").SetTimer(2000).Find().SendKeys(message);
            WebDriver.GetBy("//span[@id='CPH_lblSaveMsgCaption']").SetTimer(2000).Find().Click();

            WebDriver.GetBy("//div[@id = 'divAllComments']//li[contains(text(), '" + message + "')]").SetTimer(5000).Find();
        }


        [Then(@"I click Dashboard button {{(.*)}}")]
        public void ThenIClickDashboardButtonUcToolbar_HplMyDash(string dashBoardButton)
        {
            WebDriver.GetById(dashBoardButton).Click();
        }

        [Then(@"check messages in my dashboard")]
        public void ThenCheckMessagesInMyDashboard()
        {
           ReadOnlyCollection <IWebElement> messageGroup = WebDriver.FindElements(By.XPath("//div[@id='messagesGroupArea']//p/span[3]"));
            
           foreach (IWebElement message in messageGroup)
           {
               //Assert.AreEqual("asdasd", message.Text)
               if (message.Text != "test message")
               {
                   Assert.Fail("Error: Message is not the same");
               }
               else
               {    
                   WebDriver.GetBy("//div[@id='messagesGroupArea']/section").SetTimer(2000).Find().Click();
                   WebDriver.GetBy("//div[@id='messagesGroupArea']/section/div/div/a").SetTimer(2000).Find().Click();
                   var alert = WebDriver.SwitchTo().Alert();
                   alert.Accept();
               }
           }
        }


    }
}
