﻿Feature: Search
	In order to check a search option
	As a customer
	I want to make a search

@search
Scenario Outline: Search
	Given I am logged in <site> as a customer with <phone> <email> and <password>
	Then I click on Search button {{//span[contains(text(),'Search')]}}
	Then I check if images of the vehicle are present


	Examples: 
	| site									    | phone      | email		  |password |
	| http://diamondqa.myvirtualinventory.com   | 0		     |fara@1.com      |1		|
	| http://platinumqa.myvirtualinventory.com  | 0		     |fara@1.com      |1		|
	| http://myvirtualinventory.com				| 7327572923 |fara@1.com      |1	    |
