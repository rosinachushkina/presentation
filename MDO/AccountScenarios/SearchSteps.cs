﻿using System;
using System.Collections.ObjectModel;
using Common;
using NUnit.Framework;
using OpenQA.Selenium;
using TechTalk.SpecFlow;

namespace MDO.AccountScenarios
{
    [Binding]
    public class SearchSteps : BaseSteps

    {

        [Given(@"I am logged in (.*) as a customer with (.*) (.*) and (.*)")]
        public void GivenIAmLoggedInAsACustomerWithAnd(string site, string phone, string email, string password)
        {
            WebDriver.Go(site);
            if (Convert.ToDecimal(phone) != 0)
            {
                WebDriver.GetById("txtPhone").SendKeys(phone);
                WebDriver.GetById("CPH_btnSignByPhone").Click();
            }
            WebDriver.GetById("ucToolbar_lblLogInOut").Click();
            WebDriver.GetByName("username").SendKeys(email);
            WebDriver.GetByName("password").SendKeys(password);
            WebDriver.GetByPath("//div[@class='form-actions']/button").Click();
        }

        [Then(@"I click on Search button {{(.*)}}")]
        public void ThenIClickOnSearchButtonQuickSearchButton(string searchButton)
        {
            WebDriver.GetBy(searchButton).SetTimer(2000).Find().Click();
        }

        [Then(@"I check if images of the vehicle are present")]
        public void ThenICheckIfImagesOfTheVehicleArePresent()
        {

            ReadOnlyCollection<IWebElement> vehicleBlocksList;
            vehicleBlocksList = WebDriver.FindElements(By.XPath("//div[@id='searchResult']/div"));

            ReadOnlyCollection<IWebElement> vehicleImagesList;
            vehicleImagesList =
                WebDriver.FindElements(By.XPath("//div[@id='searchResult']//img[starts-with(@src, 'http://i.mdo360.com/c/pi/')]"));

            Assert.AreEqual(vehicleBlocksList.Count, vehicleImagesList.Count);
        }


    }
}
