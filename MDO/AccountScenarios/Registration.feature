﻿@customer_registration

Feature: MDO customer registration
	I want to ensure 
	that I can register myself on MDO web site as customer

@diamond_customer_registration
Scenario: Register customer Diamond
	Given I am on web site {{http://diamondqa.myvirtualinventory.com/}}
	When I have clicked on Sign In button {{ucToolbar_hplLogInOut2}}
	Then I'm navigated to login page {{http://diamondqa.myvirtualinventory.com/Dashboard/Accounts/Login}}
	When I click on register button
	Then I'm navigated to registration page {{http://diamondqa.myvirtualinventory.com/Registration/default.aspx}}
	When I put all user data 
		 |Path				|Value							|
		 |CPH_txtFN			|{{?name}}							|
		 |CPH_txtLN			|{{?lastname}}							|
		 |CPH_txtEml		|{{?email}}					|
		 |CPH_txtPhone		|097 555 66 99					|
		 |CPH_txtPWD		|0975556699					|
		 |CPH_txtPWDConf	|0975556699					|
	And Click submit button {{CPH_btnRegister}}
	Then I'm on home page as registered user

@platinum_customer_registration
Scenario: Register customer Platinum
	Given I am on web site {{http://platinumqa.myvirtualinventory.com/}}
	When I have clicked on Sign In button {{ucToolbar_hplLogInOut2}}
	Then I'm navigated to login page {{http://platinumqa.myvirtualinventory.com/Dashboard/Accounts/Login}}
	When I click on register button
	Then I'm navigated to registration page {{http://platinumqa.myvirtualinventory.com/Registration/default.aspx}}
	When I put all user data 
		 |Path				|Value							|
		 |CPH_txtFN			|{{?name}}							|
		 |CPH_txtLN			|{{?lastname}}							|
		 |CPH_txtEml		|{{?email}}					|
		 |CPH_txtPhone		|097 555 66 99					|
		 |CPH_txtPWD		|0975556699					|
		 |CPH_txtPWDConf	|0975556699					|
	And Click submit button {{CPH_btnRegister}}
	Then I'm on home page as registered user

@gold_customer_registration
Scenario: Register gold customer
	Given I am on web site {{http://www.myvirtualinventory.com/}}
	When  I put phone number of dealer
			|Path				|Value							|
			|txtPhone			|7327572923						|
	And  Click Sing in button {{CPH_btnSignByPhone}}
	When I have clicked on Sign In button {{ucToolbar_hplLogInOut2}}
	Then I'm navigated to login page {{http://www.myvirtualinventory.com/Dashboard/Accounts/Login}}
	When I click on register button
	Then I'm navigated to registration page {{http://www.myvirtualinventory.com/Registration/default.aspx}}
	When I put all user data 
		 |Path				|Value							|
		 |CPH_txtFN			|{{?name}}							|
		 |CPH_txtLN			|{{?lastname}}							|
		 |CPH_txtEml		|{{?email}}					|
		 |CPH_txtPhone		|097 555 66 99					|
		 |CPH_txtPWD		|0975556699					|
		 |CPH_txtPWDConf	|0975556699					|
	And Click submit button {{CPH_btnRegister}}
	Then I'm on home page as registered user

		 

