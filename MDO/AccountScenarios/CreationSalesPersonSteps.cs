﻿using System.Linq;
using Common;
using NUnit.Framework;
using OpenQA.Selenium;
using TechTalk.SpecFlow;
using TechTalk.SpecFlow.Assist;

namespace MDO.AccountScenarios
{
    [Binding]
    public class CreationSalesPersonSteps : BaseSteps
    {
        [Given(@"I am on portal webpage {{(.*)}}")]
        public void GivenIAmOnPortalWebpageHttpsPortal_Mydealeronline_Com(string website)
        {
            WebDriver.Go(website);
        }

        [Then(@"I switch site language on English")]
        public void ThenISwitchSiteLanguageOnEnglish()
        {
            WebDriver.GetById("ucHeader_ucLanguageSelector_lblHdnLanguage").Click();
            WebDriver.GetByPath(".//*[@id='wrap']/div[1]/div/ul/li[1]/ul/li[1]/a").Click();
        }

        [Then(@"I click on login button {{(.*)}}")]
        public void ThenIClickOnLoginButton(string loginButton)
        {
            WebDriver.GetById(loginButton).Click();
        }

        [Then(@"I enter user date to login form")]
        public void ThenIEnterUserDateToLoginForm(Table table)
        {
            var fields = table.CreateSet<FormField>().ToList();
            foreach (var formField in fields)
            {
                WebDriver.FillField(formField);
            }
        }

        [Then(@"press Sign in {{(.*)}}")]
        public void ThenPressSignIn(string signInButton)
        {
            WebDriver.GetById(signInButton).Click();
        }

        [Then(@"I click on settings for site {{(.*)}}")]
        public void ThenIClicOnSettingsForPlatinumSiteIdDivDivDivDivDivSpan(string accountSettings)
        {
            WebDriver.GetByPath(accountSettings).Click();
        }

        [When(@"Click on Sales Person Block {{(.*)}}")]
        public void ThenClickOnSalesPersonBlockCPH_LblSalesTeamHead(string salesPersonBlock)
        {
            WebDriver.GetById(salesPersonBlock).Click();
        }

        [Then(@"I enter data for new sales person")]
        public void ThenIEnterDataForNewSalesPerson(Table table)
        {
            var fields = table.CreateSet<FormField>().ToList();
            foreach (var formField in fields)
            {
                WebDriver.FillField(formField);
            }
        }

        [Then(@"check Show this member on site {{(.*)}}")]
        public void ThenCheckShowThisMemberOnSiteIdDivDivInput(string showMemberCheckBox)
        {
            WebDriver.GetByPath(showMemberCheckBox).Click();
        }


        [Then(@"click Add Sales Person button {{(.*)}}")]
        public void ThenClickAddSalesPersonButtonCPH_UcSalesTeam_Label(string addSalesPersonButton)
        {
            WebDriver.GetById(addSalesPersonButton).Click();
        }

        [Then(@"new sales person added {{(.*)}}")]
        public void ThenNewSalesPersonAdded(string newSalePerson)
        {
            var element = WebDriver.GetBy(newSalePerson).SetTimer(10000).Find();
            Assert.IsNotNull(element);
        }

        [Then(@"I go to the site and check if the sales person is present")]
        public void ThenIGoToTheSiteAndCheckIfTheSalesPersonIsPresent()
        {
            WebDriver.GetById("CPH_lblPreview").Click();
            WebDriver.SwitchTo().Window(WebDriver.WindowHandles.Last());

            var element = WebDriver.GetBy("//*[@id='salesTeam']/div[2]").SetTimer(10000).Find();
            Assert.IsNotNull(element);

            WebDriver.SwitchTo().Window(WebDriver.WindowHandles.Last()).Close();

            WebDriver.SwitchTo().Window(WebDriver.WindowHandles.First());

        }

        [Then(@"I try to change sale's person info")]
        public void ThenITryToChangeSaleSPersonInfo()
        {
            WebDriver.GetByPath("//*[@id='sales_team_member_grid_area']/tbody/tr[2]/td[6]/a[1]").Click();

            WebDriver.GetBy("//div[@id='sales-team-edit-member-area'][contains(@style, 'block')]").SetTimer(2000).Find();
            WebDriver.FindElement(By.XPath("//div[@id='sales-team-edit-member-area'][contains(@style, 'block')]//*[@name='FirstName']")).Clear();
            WebDriver.FindElement(By.XPath("//div[@id='sales-team-edit-member-area'][contains(@style, 'block')]//*[@name='FirstName']")).SendKeys("Check");
            WebDriver.FindElement(By.XPath("//div[@id='sales-team-edit-member-area'][contains(@style, 'block')]//*[@name='FirstName']")).Clear();
            WebDriver.FindElement(By.XPath("//div[@id='sales-team-edit-member-area'][contains(@style, 'block')]//*[@name='FirstName']")).SendKeys("Check");
            WebDriver.FindElement(By.XPath("//div[@id='sales-team-edit-member-area'][contains(@style, 'block')]//*[@name='LastName']")).Clear();
            WebDriver.FindElement(By.XPath("//div[@id='sales-team-edit-member-area'][contains(@style, 'block')]//*[@name='LastName']")).SendKeys("Check");
            WebDriver.FindElement(By.XPath("//div[@id='sales-team-edit-member-area'][contains(@style, 'block')]//*[@name='LastName']")).Clear();
            WebDriver.FindElement(By.XPath("//div[@id='sales-team-edit-member-area'][contains(@style, 'block')]//*[@name='LastName']")).SendKeys("Check");

            WebDriver.GetBy("//div[@id='sales-team-edit-member-area'][contains(@style, 'block')]").Find().FindElement(By.Id("CPH_ucSalesTeam_lblSaveButton")).Click();

        }

        [Then(@"I check if changes are saved")]
        public void ThenICheckIfChangesAreSaved()
        {
            //WebDriver.GetBy("//*[@id='sales_team_member_grid_area']/tbody/tr[2]/td[2][contains(text(), 'Check Check')]").SetTimer(2000).Find();
            Assert.AreEqual("Check Check", WebDriver.GetBy("//*[@id='sales_team_member_grid_area']/tbody/tr[2]/td[2][contains(text(), 'Check Check')]").SetTimer(2000).Find().Text);
        }

        [Then(@"I delete the created sales person")]
        public void ThenIDeleteTheCreatedSalesPerson()
        {
            WebDriver.GetByPath("//*[@id='sales_team_member_grid_area']/tbody/tr[2]/td[6]/a[2]").Click(); 
            var alert = WebDriver.SwitchTo().Alert();
            alert.Accept();
        }


    }
}
