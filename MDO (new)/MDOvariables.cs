﻿using System;                       //----Здесь лежат все переменные общие для тестов по MDO
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MDO__new_
{
    public class MDOvariables
    {
        //------------------Ссылки на возможные варианты перехода (staging/production)
        public const string ProductionBronze = "http://myvirtualinventory.com/";
        public const string ProductionSilver = "http://diamondqa.myvirtualinventory.com";
        public const string ProductionGold = "http://platinumqa.myvirtualinventory.com";
        public const string StagingBronze = "";
        public const string StagingSilver = "http://diamondqa.st.myvirtualinventory.com";
        public const string StagingGold = "http://platinumqa.st.myvirtualinventory.com";

        public const bool IsProduction = false;
          
    }
}
